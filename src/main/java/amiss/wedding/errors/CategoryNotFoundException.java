package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class CategoryNotFoundException extends ApplicationRuntimeException {

    public CategoryNotFoundException(long id){
        super("The Category with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
