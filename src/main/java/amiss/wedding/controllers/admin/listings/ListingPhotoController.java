package amiss.wedding.controllers.admin.listings;


import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;
import amiss.wedding.errors.ErrorResponse;
import amiss.wedding.services.listings.ListingPhotoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/admin/listing_photos")
public class ListingPhotoController {

    private final ListingPhotoService listingPhotoService;

    @Autowired
    public ListingPhotoController(ListingPhotoService listingPhotoService) {
        this.listingPhotoService = listingPhotoService;
    }
    @GetMapping("")
    public ResponseEntity<List<ListingPhotoDto>> getListingPhotos() {
        return this.listingPhotoService.findAll();
    }

    @GetMapping("/{photo_id}")
    public ResponseEntity<ListingPhotoDto> getListingPhotos(@PathVariable("photo_id") Long photo_id) {
        return this.listingPhotoService.findById(photo_id);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = Listing.class)) }),
            @ApiResponse(responseCode = "204", description = "No Listing"),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Listing not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class)) }) })
    @Operation(summary = "Get all Photos of listing",
            description= "Get all Photos of a listing back")
    @GetMapping(path = "/listing/{listing_id}")
    public ResponseEntity<List<ListingPhotoDto>> findAllByListingId(@PathVariable("listing_id") Long listing_id){
        return this.listingPhotoService.findAllByListingId(listing_id);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = Listing.class)) }),
            @ApiResponse(responseCode = "204", description = "No Listing"),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Listing not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class)) }) })
    @Operation(summary = "Add Cover Image for a listing",
            description= "Add new Cover Image to the Listing, if the listing has a cover Image, will remove the old one")
    @PostMapping("/listing/{listing_id}/add_cover")
    public ResponseEntity<ListingDto> addCoverImg(@PathVariable("listing_id") Long listing_id,
                                                  @RequestBody ListingPhotoDto listingPhotoDto){
        return this.listingPhotoService.addCoverImg(listing_id, listingPhotoDto);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = Listing.class)) }),
            @ApiResponse(responseCode = "204", description = "No Listing"),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Listing not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class)) }) })
    @Operation(summary = "Add Photos to listing",
            description= "Add Photos to the Listing")
    @PostMapping("/listing/{listing_id}/add_listing_photos")
    public ResponseEntity<ListingDto> addListingPhotos(@PathVariable("listing_id") Long listing_id,
                                                  @RequestBody List<ListingPhotoDto> listingPhotoDtolist){
        return this.listingPhotoService.addListingPhotos(listingPhotoDtolist, listing_id);
    }

    @PostMapping(value = "/listing/{listing_id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ListingDto> createListingPhotos(@PathVariable("listing_id") Long listing_id,
                                                          @RequestPart List<MultipartFile> photos){
        return this.listingPhotoService.uploadsPhotos(listing_id, photos);
    }

    @PutMapping(path = "/{photo_id}")
    public ResponseEntity<ListingPhotoDto> updateListingPhoto(@PathVariable("photo_id") Long photo_id,
                                                              @RequestParam(value = "file") MultipartFile file){
        return this.listingPhotoService.update(photo_id, file);
    }

    @DeleteMapping(path = "/{photo_id}")
    public ResponseEntity<String> removeListingPhoto(@PathVariable Long photo_id){
        return this.listingPhotoService.remove(photo_id);
    }
}
