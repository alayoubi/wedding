package amiss.wedding.repositories.articles;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.MiniArticle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MiniArticleRepository extends JpaRepository<MiniArticle, Long> {

    List<MiniArticle> findAllByArticle(Article article);
}
