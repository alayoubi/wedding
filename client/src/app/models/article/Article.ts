import { Listing } from '@core/models/listing/Listing';
export interface Article {
  id: number;
  title: string;
  content: string;
  readTime: number;
  coverImg_id: number;
  coverImg: string;
  articlePhotos_ID: number[];
  articlePhotos: string[];
  category_IDs: number[];
  categories: string[];
  miniArticles_ID: number[];
  miniArticles: MiniArticle[];
  listings_IDs: number[];
  listings: Listing[];
  articleCategories_IDs: number[];
  articleCategories: any;
}

export interface MiniArticle {
  id: number;
  title: string;
  content: string;
  articlePhotos_ID: number[];
  articlePhotos: string[];
  article: number;
  listings_IDs: number[];
  listings: Listing[];
}
