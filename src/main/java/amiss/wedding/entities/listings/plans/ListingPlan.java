package amiss.wedding.entities.listings.plans;


import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.listings.Listing;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "listing_plans")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListingPlan extends BaseEntity {

    @OneToOne()
    @JoinColumn(name="listing_id")
    private Listing listing;

    @OneToOne
    @JoinColumn(name="plan_id")
    private Plan plan;

    @Column(name = "started_at")
    private LocalDateTime startedAt;

    @Column(name = "expires_at")
    private LocalDateTime expiresAt;

    @Column(name = "listing_plan_status", length = 32, columnDefinition = "varchar(255) default 'AVAILABLE'")
    @Enumerated(EnumType.STRING)
    private ListingPlanStatus listingPlanStatus;
}
