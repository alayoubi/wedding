package amiss.wedding.mappers.categories;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.dto.CategoryDto;

public class CategoryMapperImpl implements CategoryMapper{

    @Override
    public Category categoryDtoToCategory(CategoryDto categoryDto) {

        Category category = new Category();
        category.setName(categoryDto.getName());
        category.setDescription(categoryDto.getDescription());
        return category;
    }

    @Override
    public CategoryDto categoryToCategoryDto(Category category) {
        return new CategoryDto(category);
    }

    /**
     * @param category
     * @return
     */
    @Override
    public CategoryDto categoryToBasicCategoryDto(Category category) {
        return new CategoryDto(category.getId(), category.getName(), category.getDescription(), category.isActive(),
                category.getCoverImg() == null ? null : category.getCoverImg().getPath(),
                category.getParentCategory() == null ? null : category.getParentCategory().getId());

    }

}
