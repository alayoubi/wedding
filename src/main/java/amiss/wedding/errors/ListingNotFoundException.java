package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ListingNotFoundException extends ApplicationRuntimeException {

    public ListingNotFoundException(long id) {
        super("The Listing with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
