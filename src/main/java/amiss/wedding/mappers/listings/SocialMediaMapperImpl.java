package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.SocialMedia;
import amiss.wedding.entities.listings.dto.SocialMediaDto;

public class SocialMediaMapperImpl implements SocialMediaMapper{

    public SocialMediaMapperImpl(){

    }
    @Override
    public SocialMedia socialMediaDtoToSocialMedia(SocialMediaDto socialMediaDto) {
        SocialMedia socialMedia = new SocialMedia();
        socialMedia.setSocialMediaTyp(socialMediaDto.getSocialMediaTyp());
        socialMedia.setPath(socialMediaDto.getPath());
        return socialMedia;
    }

    @Override
    public SocialMediaDto socialMediaToSocialMediaDto(SocialMedia socialMedia) {

        SocialMediaDto socialMediaDto = new SocialMediaDto(socialMedia);
        return socialMediaDto;
    }
}
