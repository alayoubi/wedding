package amiss.wedding.services.listings;

import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.AttributeValueDto;
import amiss.wedding.entities.listings.dto.ListingDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;

public interface AttributeValueService {

    ResponseEntity<List<AttributeValueDto>> findAll();
    ResponseEntity<AttributeValueDto> findById(Long id);
    ResponseEntity<List<AttributeValueDto>> findAllByListingId(Long listingId);
    ResponseEntity<AttributeValueDto> create(AttributeValueDto attributeValueDto);

    ResponseEntity<List<AttributeValueDto>> createValuesfurListing(Long listingId, List<AttributeValueDto> attributeValueDtoList);

    ResponseEntity<AttributeValueDto> update(Long id, AttributeValueDto attributeValueDto);
    ResponseEntity<String> remove(Long id);

    Set<AttributeValue> setAndSaveAttributeValue(ListingDto listingDto, Listing createdListing);
}
