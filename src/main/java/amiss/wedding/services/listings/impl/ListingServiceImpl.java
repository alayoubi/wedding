package amiss.wedding.services.listings.impl;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.*;
import amiss.wedding.entities.listings.dto.AttributeValueDto;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.dto.SocialMediaDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanType;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;
import amiss.wedding.entities.listings.plans.ListingPlan;
import amiss.wedding.entities.listings.plans.ListingPlanStatus;
import amiss.wedding.entities.users.client.Client;
import amiss.wedding.errors.*;
import amiss.wedding.mappers.listings.*;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.categories.PlanRepository;
import amiss.wedding.repositories.categories.attribute.AttributeRepository;
import amiss.wedding.repositories.cities.*;
import amiss.wedding.repositories.listings.AttributeValueRepository;
import amiss.wedding.repositories.listings.ListingPhotoRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.repositories.listings.SocialMediaRepository;
import amiss.wedding.repositories.users.ClientRepository;
import amiss.wedding.services.listings.AttributeValueService;
import amiss.wedding.services.listings.ListingPhotoService;
import amiss.wedding.services.listings.ListingService;
import amiss.wedding.services.listings.SocialMediaService;
import amiss.wedding.storage.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ListingServiceImpl implements ListingService {

    private final ListingsRepository listingsRepository;
    private final ClientRepository clientRepository;
    private final CategoryRepository categoryRepository;
    private final RegionRepository regionRepository;
    private final CityRepository cityRepository;
    private final DistrictRepository districtRepository;
    private final PositionRepository positionRepository;
    private final PlanRepository planRepository;
    private final SocialMediaRepository socialMediaRepository;
    private final SocialMediaService socialMediaService;
    private final ListingPhotoRepository listingPhotoRepository;
    private final ListingPhotoService listingPhotoService;
    private final ListingMapper mapper = new ListingMapperImpl();
    private final AttributeRepository attributeRepository;
    private final AttributeValueRepository attributeValueRepository;
    private final AttributeValueService attributeValueService;
    private final AttributeValueMapper attributeValueMapper = new AttributeValueMapperImpl();
    private final SocialMediaMapper socialMediaMapper = new SocialMediaMapperImpl();

    private StorageService storageService;

    public static final String FILE_NAME = "Cover";

    @Override
    public ResponseEntity<List<ListingDto>> findAll() {
        List<Listing> listings = this.listingsRepository.findAll();
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    @Override
    public ResponseEntity<ListingDto> findById(Long id) {
        Listing listing = listingsRepository.findById(id).orElseThrow(() ->
                new ListingNotFoundException(id));
        ListingDto listingDto = mapper.listingToListingDto(listing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ListingDto> findByIdIfActive(Long id) {
        Listing listing = listingsRepository.findById(id).orElseThrow(() ->
                new ListingNotFoundException(id));
        if (listing.getStatus().equals(Status.COMPLETED)) {
            ListingDto listingDto = mapper.listingToListingDto(listing);
            return ResponseEntity.ok(listingDto);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public ResponseEntity<ListingDto> findByClient(Long id) {
        Client client = clientRepository.findById(id).orElseThrow(()->
                new ClientNotFoundException(id));
        Listing listing = listingsRepository.findByClient(client);
        ListingDto listingDto = mapper.listingToListingDto(listing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategory(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() ->
                new CategoryNotFoundException(id));
        List<Listing> listings = listingsRepository.findAllByCategory(category);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    /**
     * @param regionId
     * @return
     */
    @Override
    public ResponseEntity<List<ListingDto>> findAllByRegion(Long regionId) {
        Region region = regionRepository.findById(regionId).orElseThrow(() ->
                new RegionNotFoundException(regionId));
        List<Listing> listings = listingsRepository.findAllByRegion(region);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(listingDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByCity(Long cityId) {
        City city = cityRepository.findById(cityId).orElseThrow(() ->
                new CityNotFoundException(cityId));
        List<Listing> listings = listingsRepository.findAllByCity(city);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByPlan(Long planId) {
        Plan plan = planRepository.findById(planId).orElseThrow(() ->
                new PlanNotFoundException(planId));
        List<Listing> listings = listingsRepository.findAllByPlan(plan);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByStatus(String status) {
        Status currentStatus = Status.findByName(status);
        List<Listing> listings = listingsRepository.findAllByStatus(currentStatus);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategoryAndPlan(Long categoryId, Long planId) {
        Category category = categoryRepository.findById(categoryId).orElseThrow(() ->
                new CategoryNotFoundException(categoryId));

        Plan plan = planRepository.findById(planId).orElseThrow(() ->
                new PlanNotFoundException(planId));
        List<Listing> listings = listingsRepository.findAllByCategoryAndPlan(category, plan);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    /**
     * @param categoryId
     * @param regionId
     * @return
     */
    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategoryAndRegion(Long categoryId, Long regionId) {

        Category category = categoryRepository.findById(categoryId).orElseThrow(() ->
                new CategoryNotFoundException(categoryId));

        Region region = regionRepository.findById(regionId).orElseThrow(() ->
                new RegionNotFoundException(regionId));

        List<Listing> listings = listingsRepository.findAllByCategoryAndRegion(category, region);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategoryAndCity(Long categoryId, Long cityId) {
        Category category = categoryRepository.findById(categoryId).orElseThrow(() ->
                new CategoryNotFoundException(categoryId));

        City city = cityRepository.findById(cityId).orElseThrow(() ->
                new CityNotFoundException(cityId));
        List<Listing> listings = listingsRepository.findAllByCategoryAndCity(category, city);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategoryAndStatus(Long categoryId,
                                                                       String status) {
        Category category = categoryRepository.findById(categoryId).orElseThrow(() ->
                new CategoryNotFoundException(categoryId));

        Status actualStatus = Status.findByName(status);
        List<Listing> listings = listingsRepository.findAllByCategoryAndStatus(category, actualStatus);
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);
    }

    private void setCoverImg(Listing listing, Long listingId, MultipartFile file) {

        if (listing.getCoverImg() != null) {
            ListingPhoto listingPhoto = listingPhotoRepository.findById(listing.getCoverImg().getId()).get();
            listingPhoto.setListing(null);
            listingPhotoRepository.delete(listingPhoto);
            listing.setCoverImg(null);
            listingsRepository.save(listing);
        }
        String key = "listings/" + listingId + "/" + FILE_NAME + "_";

        String link = "https://lilty.sgp1.cdn.digitaloceanspaces.com/";
        link +=  "listings%2F" + listingId + "%2F" + FILE_NAME + "_";
        link = storageService.uploadFile(file, key, link);
        ListingPhoto listingPhoto = new ListingPhoto(key, link, listing, Status.PENDING, file.getSize());
        listingPhotoRepository.save(listingPhoto);
        listing.setCoverImg(listingPhoto);
    }
    @Override
    public ResponseEntity<ListingDto> addCoverImg(Long listingId, ListingPhotoDto listingPhotoDto) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        ListingPhoto coverImg = listingPhotoService.addListingPhoto(listingPhotoDto, listing);
        if (listing.getCoverImg() != null) {
            ListingPhoto listingPhoto = listingPhotoRepository.findById(listing.getCoverImg().getId()).orElseThrow(
                    ()-> new ListingPhotoNotFoundException(listingId));
            listingPhoto.setListing(null);
            listingPhotoRepository.delete(listingPhoto);
        }
        listing.setCoverImg(coverImg);
        final Listing updatedListing = this.listingsRepository.save(listing);
        ListingDto listingDto = mapper.listingToListingDto(updatedListing);
        return ResponseEntity.ok(listingDto);
    }

    @Override
    public ResponseEntity<ListingDto> chooseCoverImg(Long listingId,
                                                     Long photoId) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));

        Optional<ListingPhoto> listingPhoto = this.listingPhotoRepository.findById(photoId);
        if (listingPhoto.isEmpty()) {
            throw new ListingPhotoNotFoundException(photoId);
        }
        ListingPhoto existingListingPhoto = listingPhoto.get();
        listing.setCoverImg(existingListingPhoto);
        final Listing updatedListing = this.listingsRepository.save(listing);
        ListingDto listingDto = mapper.listingToListingDto(updatedListing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ListingDto> create(ListingDto listingDto) {
        Category category = categoryRepository.findById(listingDto.getCategory_id()).orElseThrow(() ->
                new CategoryNotFoundException(listingDto.getCategory_id()));
        final Listing listing = mapper.listingDtoToListing(listingDto);
        listing.setCategory(category);

        City city = cityRepository.findById(listingDto.getCity_id()).orElseThrow(()->
            new CategoryNotFoundException(listingDto.getCity_id()));
        listing.setCity(city);
        listing.setCountry(city.getCountry());
        listing.setRegion(city.getRegion());
        if (listingDto.getDistrict_id() != null) {
            District district = districtRepository.findById(listingDto.getDistrict_id()).orElseThrow(()->
                    new DistrictNotFoundException(listingDto.getDistrict_id()));
            listing.setDistrict(district);
        }

        // Position TODO
        ListingPlan listingPlan = null;
        if (listingDto.getListingPlanDto() != null) {
            listingPlan = new ListingPlan();
            Plan plan = planRepository.findById(listingDto.getListingPlanDto().getPlanDto().getId()).orElseThrow(() ->
                    new PlanNotFoundException(listingDto.getListingPlanDto().getPlanDto().getId()));
            listingPlan.setPlan(plan);
            listingPlan.setStartedAt(listingDto.getListingPlanDto().getStartedAt());
            listingPlan.setExpiresAt(listingPlan.getStartedAt().plusMonths(plan.getDuration()));
            listingPlan.setListingPlanStatus(ListingPlanStatus.AVAILABLE);
        }
        listing.setListingPlan(listingPlan);
        if (listingDto.getClient_id() != null) {
            Client client = clientRepository.findById(listingDto.getClient_id()).orElseThrow(()->
                    new ClientNotFoundException(listingDto.getClient_id()));
            listing.setClient(client);
        }

        setPriority(listing);
        Listing createdListing = this.listingsRepository.save(listing);
        Long id = createdListing.getId();
        listingDto.setId(id);

        Set<AttributeValue> attributeValueSet = attributeValueService.setAndSaveAttributeValue(listingDto,
                createdListing);

        createdListing.setAttributeValues(attributeValueSet);

        Set<SocialMedia> socialMediaSet = socialMediaService.setAndSaveSocialMedias(listingDto, createdListing);
        createdListing.setSocialMedia(socialMediaSet);

        return new ResponseEntity<>(mapper.listingToListingDto(createdListing), HttpStatus.CREATED);
    }


    private void setPriority(Listing listing) {
        if (listing.getPlan().getPlanType().equals(PlanType.FREE)){
            listing.setPriority(0);
        } else if (listing.getPlan().getPlanType().equals(PlanType.SILVER)) {
            listing.setPriority(1);
        } else if (listing.getPlan().getPlanType().equals(PlanType.GOLD)) {
            listing.setPriority(2);
        } else if (listing.getPlan().getPlanType().equals(PlanType.DIAMOND)) {
            listing.setPriority(3);
        }
    }

    @Override
    @Transactional
    public ResponseEntity<ListingDto> update(Long id, ListingDto newListingDto) {
        Optional<Listing> listing = this.listingsRepository.findById(id);
        if (listing.isEmpty()){
            throw new ListingNotFoundException(id);
        }
        Listing existingListing = listing.get();

        existingListing.setTitle(newListingDto.getTitle());
        existingListing.setEmail(newListingDto.getEmail());
        existingListing.setPhone(newListingDto.getPhone());
        existingListing.setAddress(newListingDto.getAddress());
        existingListing.setDescriptions(newListingDto.getDescriptions());
        existingListing.setStatus(newListingDto.getStatus());

        Category category = categoryRepository.findById(newListingDto.getCategory_id()).orElseThrow(() ->
                new CategoryNotFoundException(newListingDto.getCategory_id()));
        existingListing.setCategory(category);

        City city = cityRepository.findById(newListingDto.getCity_id()).orElseThrow(()->
                new CategoryNotFoundException(newListingDto.getCity_id()));
        existingListing.setCity(city);
        existingListing.setCountry(city.getCountry());
        existingListing.setRegion(city.getRegion());
        if (newListingDto.getDistrict_id() != null) {
            District district = districtRepository.findById(newListingDto.getDistrict_id()).orElseThrow(()->
                    new DistrictNotFoundException(newListingDto.getDistrict_id()));
            existingListing.setDistrict(district);
        }

        // Position TODO

        ListingPlan listingPlan = null;
        if (newListingDto.getListingPlanDto() != null) {
            Plan plan = planRepository.findById(newListingDto.getListingPlanDto().getPlanDto().getId()).orElseThrow(() ->
                    new PlanNotFoundException(newListingDto.getListingPlanDto().getPlanDto().getId()));
            listingPlan.setPlan(plan);
            listingPlan.setStartedAt(newListingDto.getListingPlanDto().getStartedAt());
            listingPlan.setExpiresAt(listingPlan.getStartedAt().plusMonths(plan.getDuration()));
            listingPlan.setListingPlanStatus(ListingPlanStatus.AVAILABLE);
        }
        existingListing.setListingPlan(listingPlan);

        if (newListingDto.getClient_id() != null) {
            Client client = clientRepository.findById(newListingDto.getClient_id()).orElseThrow(()->
                    new ClientNotFoundException(newListingDto.getClient_id()));
            existingListing.setClient(client);
        }

// TODO nur die geänderte Value zu Form hinzufügen

//        if (existingListing != null) {
//            // Erstellen oder aktualisieren Sie die geänderten AttributeValue-Objekte
//            AttributeValue updatedAttributeValue = new AttributeValue();
//            updatedAttributeValue.setId(originalAttributeValueId); // Behalten Sie die ID bei
//            updatedAttributeValue.setValue("Neuer Wert");
//
//            // Verknüpfen Sie die geänderten AttributeValue-Objekte mit dem Listing
//            originalListing.getAttributeValues().removeIf(av -> av.getId().equals(originalAttributeValueId));
//            originalListing.getAttributeValues().add(updatedAttributeValue);
//
//            // Speichern Sie das aktualisierte Listing
//            listingRepository.save(originalListing);
//        }


        this.attributeValueRepository.deleteAllByListing(existingListing);
        Set<AttributeValue> attributeValueSet = new HashSet();

        for (AttributeValueDto att : newListingDto.getAttributeValueList()) {
            if (att.getValue() != "" &&  att.getValue() != null) {
                AttributeValue attributeValue = attributeValueMapper.attributeValueDtoToAttributeValue(att);
                attributeValue.setListing(existingListing);
                Attribute attribute = attributeRepository.findById(att.getAttribute_Id()).orElseThrow(() ->
                        new AttributeNotFoundException(att.getAttribute_Id()));
                attributeValue.setAttribute(attribute);
                attributeValue = attributeValueRepository.save(attributeValue);
                attributeValueSet.add(attributeValue);
            }
        }

        this.socialMediaRepository.deleteAllByListing(existingListing);
        Set<SocialMedia> socialMediaSet = new HashSet<>();
        for (SocialMediaDto socialMediaDto : newListingDto.getSocialMediaList()) {
            if (socialMediaDto.getPath() != "" && socialMediaDto.getPath() != null) {
                SocialMedia socialMedia = socialMediaMapper.socialMediaDtoToSocialMedia(socialMediaDto);
                socialMedia.setListing(existingListing);
                socialMedia = socialMediaRepository.save(socialMedia);
                socialMediaSet.add(socialMedia);
            }
        }

        Listing updatedListing = this.listingsRepository.save(existingListing);
        existingListing.getAttributeValues().clear();
        existingListing.getSocialMedia().clear();
        existingListing.setSocialMedia(socialMediaSet);
        existingListing.setAttributeValues(attributeValueSet);

        ListingDto listingDto = mapper.listingToListingDto(updatedListing);

        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @param status
     * @return
     */
    @Override
    public ResponseEntity<ListingDto> updateStatus(Long id, String status) {
        Optional<Listing> listing = this.listingsRepository.findById(id);
        if (listing.isEmpty()){
            throw new ListingNotFoundException(id);
        }
        Listing existingListing = listing.get();
        Status currentStatus = Status.findByName(status);
        existingListing.setStatus(currentStatus);
        Listing updatedListing = this.listingsRepository.save(existingListing);
        ListingDto listingDto = mapper.listingToListingDto(updatedListing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @param planId
     * @return
     */
    @Override
    public ResponseEntity<ListingDto> updatePlan(Long id, Long planId) {
        Optional<Listing> listing = this.listingsRepository.findById(id);
        if (listing.isEmpty()){
            throw new ListingNotFoundException(id);
        }
        Listing existingListing = listing.get();
        Optional<Plan> plan = this.planRepository.findById(planId);
        existingListing.setPlan(plan.get());
        updatePriority(existingListing);
        Listing updatedListing = this.listingsRepository.save(existingListing);
        ListingDto listingDto = mapper.listingToListingDto(updatedListing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @param priority
     * @return
     */
    @Override
    public ResponseEntity<ListingDto> updatePriority(Long id, int priority) {
        Listing existingListing = this.listingsRepository.findById(id).orElseThrow(() ->
                new PlanNotFoundException(id));
        existingListing.setPriority(priority);
        Listing updatedListing = this.listingsRepository.save(existingListing);
        ListingDto listingDto = mapper.listingToListingDto(updatedListing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }

    /**
     * @param listingId
     * @return
     */
    @Override
    public ResponseEntity<String> markAsDeleted(Long listingId) {
        Listing existingListing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new PlanNotFoundException(listingId));
        existingListing.setDeletedAt(Instant.now()); // Setzen Sie den Löschzeitpunkt
        if (existingListing.getAttributeValues() != null || !existingListing.getAttributeValues().isEmpty()) {
            for (AttributeValue attributeValue : existingListing.getAttributeValues() ) {
                attributeValue.setDeletedAt(Instant.now());
                this.attributeValueRepository.save(attributeValue);
            }
        }


        // TODO alle auch delete at für Reviews, Offer, Photos
        socialMediaService.markAsDeleted(existingListing);

        listingsRepository.save(existingListing); // Speichern Sie die Änderung in der Datenbank
        return new ResponseEntity<>("Listing has been removed successfully!", HttpStatus.NO_CONTENT);
    }


    private void updatePriority(Listing existingListing) {
        Plan actualPlan = existingListing.getPlan();
        if (actualPlan.getPlanType().equals(PlanType.FREE)) {
            existingListing.setPriority(0);
        } else if (actualPlan.getPlanType().equals(PlanType.SILVER)) {
            existingListing.setPriority(1);
        }  else if (actualPlan.getPlanType().equals(PlanType.GOLD)) {
            existingListing.setPriority(2);
        }  else if (actualPlan.getPlanType().equals(PlanType.DIAMOND)) {
            existingListing.setPriority(3);
        }
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.categoryRepository.existsById(id);
        if (!exists){
            throw new ListingNotFoundException(id);
        }
        this.listingsRepository.deleteById(id);
        return new ResponseEntity<>("Listing has been removed successfully!", HttpStatus.NO_CONTENT);
    }

    /**
     * @param listing
     * @return
     */
    @Override
    public Listing insert(Listing listing) {
        return this.listingsRepository.save(listing);
    }


    /**
     * @param categoryId
     * @param regionId
     * @param status
     * @return
     */
    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategoryAndRegionAndStatusSortOfPriority(Long categoryId, Long regionId,
                                                                                              String status, int page) {
        Category category = categoryRepository.findById(categoryId).orElseThrow(() ->
                new CategoryNotFoundException(categoryId));

        Region region = regionRepository.findById(regionId).orElseThrow(() ->
                new RegionNotFoundException(regionId));

        Status currentStatus = Status.findByName(status);
        List<Listing> listings = listingsRepository.findAllByCategoryAndRegionAndStatus(category, region, currentStatus,
                Sort.by(Sort.Direction.DESC, "priority"), PageRequest.of(page, 25));
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);    }

    /**
     * @param categoryId
     * @param regionId
     * @param status
     * @param page
     * @return
     */
    @Override
    public ResponseEntity<List<ListingDto>> findAllByCategoryAndRegionAndStatusWithoutPriority(Long categoryId,
                                                                                               Long regionId,
                                                                                               String status,
                                                                                               int page) {
        Category category = categoryRepository.findById(categoryId).orElseThrow(() ->
                new CategoryNotFoundException(categoryId));
        Region region = regionRepository.findById(regionId).orElseThrow(() ->
                new RegionNotFoundException(regionId));
        Status currentStatus = Status.findByName(status);
        List<Listing> listings = listingsRepository.findAllByCategoryAndRegionAndStatusAndPriorityNot(category,
                region, currentStatus,
                0, PageRequest.of(page, 50));
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);    }

    /**
     * @param regionId
     * @param status
     * @return
     */
    @Override
    public ResponseEntity<List<ListingDto>> findAllByRegionAndStatus(Long regionId, String status) {
        Region region = regionRepository.findById(regionId).orElseThrow(() ->
                new RegionNotFoundException(regionId));

        Status currentStatus = Status.findByName(status);
        List<Listing> listings = listingsRepository.findAllByRegionAndStatus(region, currentStatus,
                Sort.by(Sort.Direction.DESC, "priority"));
        List<ListingDto> listingDtoList =
                listings.stream().map(mapper::listingToListingDtoBaseData).collect(Collectors.toList());
        if (listingDtoList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(listingDtoList);    }
}
