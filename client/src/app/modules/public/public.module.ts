import { HeaderComponent } from './../admin/shared/component/header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { LoginComponent } from './components/Login/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FooterComponent } from './shared/component/footer/footer.component';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { MatSelectModule } from '@angular/material/select';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { ListSingleOneComponent } from './components/Listing 1/list-single-one/list-single-one.component';
import { InfoComponent } from './components/Listing 1/info/info.component';
import { HeroSectionComponent } from './components/Listing 1/hero-section/hero-section.component';
import { ContentComponent } from './components/Listing 1/content/content.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { HomeComponent } from './components/home/home.component';
import { FeaturesComponent } from './components/Home Page/features/features.component';
import { FeaturesGalleryComponent } from './components/Home Page/features-gallery/features-gallery.component';
import { FeaturesSectionComponent } from './components/Home Page/features-section/features-section.component';
import { HowWorksComponent } from './components/Home Page/how-works/how-works.component';
import { ReviewsComponent } from './components/Home Page/reviews/reviews.component';
import { SectionComponent } from './components/Home Page/section/section.component';
import { TipsNewsComponent } from './components/Home Page/tips-news/tips-news.component';
import { RealWeddingsComponent } from './components/Home Page/real-weddings/real-weddings.component';
import { RequestQuoteComponent } from './components/Listing 1/request-quote/request-quote.component';
import { AccommodationsAmenitiesIncludedComponent } from './components/Listing 1/accommodations-amenities-included/accommodations-amenities-included.component';
import { ReviewsHeadComponent } from './components/Listing 1/reviews-head/reviews-head.component';
import { ReviewsCommentsComponent } from './components/Listing 1/reviews-comments/reviews-comments.component';
import { RatingFormComponent } from './components/Listing 1/rating-form/rating-form.component';
import { MapComponent } from './components/Listing 1/map/map.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SimilarVendorComponent } from './components/Listing 1/similar-vendor/similar-vendor.component';
import { ListGridViewComponent } from './components/list-grid-view/list-grid-view/list-grid-view.component';
import { LandingComponent } from './components/list-grid-view/landing/landing.component';
import { DropdownMenuComponent } from './components/list-grid-view/dropdown-menu/dropdown-menu.component';
import { WeddingVenueComponent } from './components/list-grid-view/wedding-venue/wedding-venue.component';
import { VendorCategoriesComponent } from './components/vendor-categories/vendor-categories/vendor-categories.component';
import { LandingVendorCategoriesComponent } from './components/vendor-categories/landing-vendor-categories/landing-vendor-categories.component';
import { CategoriesComponent } from './components/vendor-categories/categories/categories.component';
import { VendorByLocationComponent } from './components/vendor-by-location/vendor-by-location/vendor-by-location.component';
import { LandingVendorByLocationComponent } from './components/vendor-by-location/landing-vendor-by-location/landing-vendor-by-location.component';
import { LocationsComponent } from './components/vendor-by-location/locations/locations.component';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { LoginPageComponent } from './components/Login/login-page/login-page.component';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomListingsComponent } from './components/show-custom-listings/custom-listings/custom-listings.component';
import { CustomListingsLandingComponent } from './components/show-custom-listings/custom-listings-landing/custom-listings-landing.component';
import { ListingsComponent } from './components/show-custom-listings/listings/listings.component';
import { SocialMediaComponent } from './components/Listing 1/social-media/social-media.component';
import { ShowCategoryDataComponent } from './components/show-category-data/show-category-data/show-category-data.component';
import { ShowCategoryDataLandingComponent } from './components/show-category-data/show-category-data-landing/show-category-data-landing.component';
import { CategoryDataContentComponent } from './components/show-category-data/category-data-content/category-data-content.component';
import { ShowRegionDataComponent } from './components/show-region-data/show-region-data/show-region-data.component';
import { ShowRegionDataLandingComponent } from './components/show-region-data/show-region-data-landing/show-region-data-landing.component';
import { RegionDataContentComponent } from './components/show-region-data/region-data-content/region-data-content.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { ContactUsComponent } from './components/contact-us/contact-us/contact-us.component';
import { ContactUsLandingComponent } from './components/contact-us/contact-us-landing/contact-us-landing.component';
import { ContactCardsComponent } from './components/contact-us/contact-cards/contact-cards.component';
import { ContactBlockSectionComponent } from './components/contact-us/contact-block-section/contact-block-section.component';
import { CoupleContactComponent } from './components/contact-us-forms/couple-contact/couple-contact.component';
import { CoupleContactLandingComponent } from './components/contact-us-forms/couple-contact-landing/couple-contact-landing.component';
import { CoupleContactFormComponent } from './components/contact-us-forms/couple-contact-form/couple-contact-form.component';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CoupleContactDoneComponent } from './components/contact-us-forms/couple-contact-done/couple-contact-done.component';
import { JoinUsComponent } from './components/contact-us-forms/join-us/join-us.component';
import { JoinUsLandingComponent } from './components/contact-us-forms/join-us-landing/join-us-landing.component';
import { JoinUsFormComponent } from './components/contact-us-forms/join-us-form/join-us-form.component';
import { JoinUsDetailsCardComponent } from './components/contact-us-forms/join-us-details-card/join-us-details-card.component';
import { ImageSliderComponent } from './components/Listing 1/image-slider/image-slider.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoadingInterceptor } from '@core/interceptor/loading/loading.interceptor';
import { DressesComponent } from './components/suppliers/Dresses/dresses/dresses.component';
import { DressesLandingComponent } from './components/suppliers/Dresses/dresses-landing/dresses-landing.component';
import { DressesContentComponent } from './components/suppliers/Dresses/dresses-content/dresses-content.component';
import { CakesComponent } from './components/suppliers/Cakes/cakes/cakes.component';
import { CakesLandingComponent } from './components/suppliers/Cakes/cakes-landing/cakes-landing.component';
import { CakesContentComponent } from './components/suppliers/Cakes/cakes-content/cakes-content.component';
import { FloristComponent } from './components/suppliers/Florist/florist/florist.component';
import { FloristLandingComponent } from './components/suppliers/Florist/florist-landing/florist-landing.component';
import { FloristContentComponent } from './components/suppliers/Florist/florist-content/florist-content.component';
import { PhotographersComponent } from './components/suppliers/Photographers/photographers/photographers.component';
import { PhotographersLandingComponent } from './components/suppliers/Photographers/photographers-landing/photographers-landing.component';
import { PhotographersContentComponent } from './components/suppliers/Photographers/photographers-content/photographers-content.component';
import { VideographersComponent } from './components/suppliers/Videographers/videographers/videographers.component';
import { VideographersLandingComponent } from './components/suppliers/Videographers/videographers-landing/videographers-landing.component';
import { VideographersContentComponent } from './components/suppliers/Videographers/videographers-content/videographers-content.component';
import { ArticlesComponent } from './components/articles/articles/articles.component';
import { ArticlesLandingComponent } from './components/articles/articles-landing/articles-landing.component';
import { ArticlesContentComponent } from './components/articles/articles-content/articles-content.component';
import { ArticleComponent } from './components/article/article/article.component';
import { ArticlePageComponent } from './components/article/article-page/article-page.component';
import { ConfirmPageComponent } from './components/confirm-page/confirm-page.component';

// import { HttpClientModule } from "@angular/common/http";
@NgModule({
  declarations: [
    PublicComponent,
    HomeComponent,
    FeaturesComponent,
    FeaturesGalleryComponent,
    FeaturesSectionComponent,
    HowWorksComponent,
    ReviewsComponent,
    SectionComponent,
    TipsNewsComponent,
    RealWeddingsComponent,
    ContentComponent,
    HeroSectionComponent,
    InfoComponent,
    ListSingleOneComponent,
    RequestQuoteComponent,
    AccommodationsAmenitiesIncludedComponent,
    ReviewsHeadComponent,
    ReviewsCommentsComponent,
    RatingFormComponent,
    MapComponent,
    SimilarVendorComponent,
    ListGridViewComponent,
    LandingComponent,
    DropdownMenuComponent,
    WeddingVenueComponent,
    VendorCategoriesComponent,
    LandingVendorCategoriesComponent,
    CategoriesComponent,
    VendorByLocationComponent,
    LandingVendorByLocationComponent,
    LocationsComponent,
    LoginComponent,
    LoginPageComponent,
    CustomListingsComponent,
    CustomListingsLandingComponent,
    ListingsComponent,
    SocialMediaComponent,
    ShowCategoryDataComponent,
    ShowCategoryDataLandingComponent,
    CategoryDataContentComponent,
    ShowRegionDataComponent,
    ShowRegionDataLandingComponent,
    RegionDataContentComponent,
    ContactUsComponent,
    ContactUsLandingComponent,
    ContactCardsComponent,
    ContactBlockSectionComponent,
    CoupleContactComponent,
    CoupleContactLandingComponent,
    CoupleContactFormComponent,
    CoupleContactDoneComponent,
    JoinUsComponent,
    JoinUsLandingComponent,
    JoinUsFormComponent,
    JoinUsDetailsCardComponent,
    ImageSliderComponent,
    LoaderComponent,
    DressesComponent,
    DressesLandingComponent,
    DressesContentComponent,
    CakesComponent,
    CakesLandingComponent,
    CakesContentComponent,
    FloristComponent,
    FloristLandingComponent,
    FloristContentComponent,
    PhotographersComponent,
    PhotographersLandingComponent,
    PhotographersContentComponent,
    VideographersComponent,
    VideographersLandingComponent,
    VideographersContentComponent,
    ArticlesComponent,
    ArticlesLandingComponent,
    ArticlesContentComponent,
    ArticleComponent,
    ArticlePageComponent,
    ConfirmPageComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    PublicRoutingModule,
    SharedModule,
    MatMenuModule,
    NgxStarRatingModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    CarouselModule,
    HttpClientModule,
    MatCardModule,
    MatTabsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    ReactiveFormsModule,
    NgImageSliderModule,
    MatInputModule,
    MatProgressSpinnerModule,
  ],
  exports: [MatFormFieldModule, MatCardModule,LoaderComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true,
    },
  ],
})
export class PublicModule {}
