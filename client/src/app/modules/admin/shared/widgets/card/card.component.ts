import { Component, OnInit, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exoprting from 'highcharts/modules/exporting';

@Component({
  selector: 'app-widget-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  @Input() label: string | undefined;
  @Input() total: string | undefined;
  @Input() percentage: string | undefined;

  Highcharts = Highcharts;
  chartOptions = {};
  @Input() data = {};
  constructor() {}

  ngOnInit(): void {
    this.chartOptions = {
      chart: {
        type: 'area',
        backgroundColor: null,
        borderWidth: 0,
        margin: [2, 2, 2, 2],
        height: 60,
      },
      title: {
        text: null,
      },
      subtitle: {
        text: null,
      },

      tooltip: {
        shared: true,
        outside: true,
      },
      legend: {
        enabled: false,
      },

      credits: {
        enabled: false,
      },

      exporting: {
        enabled: false,
      },
      xAxis: {
        labels: { enabled: false },
        title: { text: null },
        startOnTick: false,
        endOnTick: false,
        tickOptions: [],
      },
      YAxis: {
        labels: { enabled: false },
        title: { text: null },
        startOnTick: false,
        endOnTick: false,
        tickOptions: [],
      },

      series: [
        {
          data: this.data,
        },
      ],
    };
    HC_exoprting(Highcharts);

    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 300);
  }
}
