package amiss.wedding.mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.plans.Feature;
import amiss.wedding.entities.categories.plans.FeatureDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanDto;
import amiss.wedding.entities.categories.plans.PlanType;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Position;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.client.Client;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.LinkedHashSet;

import amiss.wedding.mappers.categories.PlanMapperImpl;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class PlanMapperImplTest {
    /**
     * Method under test: {@link PlanMapperImpl#planDtoToPlan(PlanDto)}
     */
    @Test
    void testPlanDtoToPlan() {
        PlanMapperImpl planMapperImpl = new PlanMapperImpl();
        Plan actualPlanDtoToPlanResult = planMapperImpl.planDtoToPlan(new PlanDto());
        assertFalse(actualPlanDtoToPlanResult.isVideo());
        assertNull(actualPlanDtoToPlanResult.getPlanType());
        assertNull(actualPlanDtoToPlanResult.getDuration());
        assertNull(actualPlanDtoToPlanResult.getCountOfPhotos());
        assertNull(actualPlanDtoToPlanResult.getCost());
    }

    /**
     * Method under test: {@link PlanMapperImpl#planDtoToPlan(PlanDto)}
     */
    @Test
    void testPlanDtoToPlan2() {
        PlanMapperImpl planMapperImpl = new PlanMapperImpl();
        PlanDto.PlanDtoBuilder category_idResult = PlanDto.builder().category_id(1L);
        BigDecimal cost = BigDecimal.valueOf(1L);
        PlanDto.PlanDtoBuilder durationResult = category_idResult.cost(cost).countOfPhotos(3).duration(1);
        PlanDto planDto = durationResult.features(new HashSet<>()).id(1L).planType(PlanType.FREE).video(true).build();
        Plan actualPlanDtoToPlanResult = planMapperImpl.planDtoToPlan(planDto);
        assertTrue(actualPlanDtoToPlanResult.isVideo());
        assertEquals(PlanType.FREE, actualPlanDtoToPlanResult.getPlanType());
        assertEquals(1, actualPlanDtoToPlanResult.getDuration().intValue());
        assertNull(actualPlanDtoToPlanResult.getCountOfPhotos());
        BigDecimal expectedCost = cost.ONE;
        BigDecimal cost2 = actualPlanDtoToPlanResult.getCost();
        assertSame(expectedCost, cost2);
        assertEquals("1", cost2.toString());
    }

    /**
     * Method under test: {@link PlanMapperImpl#planDtoToPlan(PlanDto)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testPlanDtoToPlan3() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.plans.PlanDto.getDuration()" because "planDto" is null
        //       at amiss.wedding.mappers.categories.PlanMapperImpl.planDtoToPlan(PlanMapperImpl.java:13)
        //   See https://diff.blue/R013 to resolve this issue.

        (new PlanMapperImpl()).planDtoToPlan(null);
    }

    /**
     * Method under test: {@link PlanMapperImpl#planToPlanDto(Plan)}
     */
    @Test
    void testPlanToPlanDto() {
        PlanMapperImpl planMapperImpl = new PlanMapperImpl();

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(new Listing());
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(new ListingPhoto());
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(new Category());
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(coverImg3);
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(parentCategory3);
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category2);
        BigDecimal cost = BigDecimal.valueOf(1L);
        plan.setCost(cost);
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Feature feature = new Feature();
        feature.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setId(1L);
        feature.setPlan(plan);
        feature.setTitle("Dr");
        feature.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setVersion(1);

        LinkedHashSet<Feature> features = new LinkedHashSet<>();
        features.add(feature);

        Plan plan2 = new Plan();
        plan2.setCategory(category);
        plan2.setCost(BigDecimal.valueOf(1L));
        plan2.setCountOfPhotos(3);
        plan2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDuration(1);
        plan2.setId(1L);
        plan2.setListings(new HashSet<>());
        plan2.setPlanType(PlanType.FREE);
        plan2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setVersion(1);
        plan2.setVideo(true);
        plan2.setFeatures(features);
        PlanDto actualPlanToPlanDtoResult = planMapperImpl.planToPlanDto(plan2);
        assertEquals(1L, actualPlanToPlanDtoResult.getCategory_id().longValue());
        assertTrue(actualPlanToPlanDtoResult.isVideo());
        assertEquals(PlanType.FREE, actualPlanToPlanDtoResult.getPlanType());
        assertEquals(1L, actualPlanToPlanDtoResult.getId().longValue());
        assertEquals(1, actualPlanToPlanDtoResult.getFeatures().size());
        assertEquals(1, actualPlanToPlanDtoResult.getDuration().intValue());
        assertEquals(3, actualPlanToPlanDtoResult.getCountOfPhotos().intValue());
        BigDecimal expectedCost = cost.ONE;
        BigDecimal cost2 = actualPlanToPlanDtoResult.getCost();
        assertSame(expectedCost, cost2);
        assertEquals("1", cost2.toString());
    }

    /**
     * Method under test: {@link PlanMapperImpl#featureToFeatureDto(Feature)}
     */
    @Test
    void testFeatureToFeatureDto() {
        PlanMapperImpl planMapperImpl = new PlanMapperImpl();

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category);
        plan.setCost(BigDecimal.valueOf(1L));
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Feature feature = new Feature();
        feature.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setId(1L);
        feature.setPlan(plan);
        feature.setTitle("Dr");
        feature.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setVersion(1);
        FeatureDto actualFeatureToFeatureDtoResult = planMapperImpl.featureToFeatureDto(feature);
        assertEquals(1L, actualFeatureToFeatureDtoResult.getId().longValue());
        assertEquals("Dr", actualFeatureToFeatureDtoResult.getTitle());
        assertEquals(1L, actualFeatureToFeatureDtoResult.getPlan_id().longValue());
    }

    /**
     * Method under test: {@link PlanMapperImpl#featureToFeatureDto(Feature)}
     */
    @Test
    void testFeatureToFeatureDto2() {
        PlanMapperImpl planMapperImpl = new PlanMapperImpl();

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(new ListingPhoto());
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(new Category());
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        City city = new City();
        city.setCountry(new Country());
        city.setDistricts(new HashSet<>());
        city.setId(1L);
        city.setListing(new HashSet<>());
        city.setName("Name");
        city.setName_en("Name en");
        city.setRegion(new Region());

        Client client = new Client();
        client.setAddress("42 Main St");
        client.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setId(1L);
        client.setListing(new HashSet<>());
        client.setMobile("Mobile");
        client.setName("Name");
        client.setPhoto("alice.liddell@example.org");
        client.setRole("Role");
        client.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setUser(new AppUser());
        client.setVersion(1);

        Country country = new Country();
        country.setCapital_city(new City());
        country.setCode("Code");
        country.setId(1L);
        country.setName("Name");
        country.setName_en("Name en");
        country.setRegions(new HashSet<>());

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        District district = new District();
        district.setCity(new City());
        district.setId(1L);
        district.setListing(new HashSet<>());
        district.setName("Name");
        district.setName_en("Name en");
        district.setRegion(new Region());

        Plan plan = new Plan();
        plan.setCategory(new Category());
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Position position = new Position();
        position.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setId(1L);
        position.setLatitude(1L);
        position.setListing(new Listing());
        position.setLongitude(1L);
        position.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setVersion(1);

        Region region = new Region();
        region.setActive(true);
        region.setCapital_city(new City());
        region.setCities(new HashSet<>());
        region.setCode("Code");
        region.setCountry(new Country());
        region.setId(1L);
        region.setListing(new HashSet<>());
        region.setName("Name");
        region.setName_en("Name en");

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(category);
        listing.setCity(city);
        listing.setClient(client);
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(country);
        listing.setCoverImg(coverImg);
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(district);
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(plan);
        listing.setPosition(position);
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(region);
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(listing);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing2);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg4);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(coverImg3);
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(parentCategory2);
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(coverImg2);
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(parentCategory3);
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Plan plan2 = new Plan();
        plan2.setCategory(category2);
        plan2.setCost(BigDecimal.valueOf(1L));
        plan2.setCountOfPhotos(3);
        plan2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDuration(1);
        plan2.setFeatures(new HashSet<>());
        plan2.setId(1L);
        plan2.setListings(new HashSet<>());
        plan2.setPlanType(PlanType.FREE);
        plan2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setVersion(1);
        plan2.setVideo(true);
        Feature feature = mock(Feature.class);
        when(feature.getPlan()).thenReturn(plan2);
        when(feature.getId()).thenReturn(1L);
        when(feature.getTitle()).thenReturn("Dr");
        FeatureDto actualFeatureToFeatureDtoResult = planMapperImpl.featureToFeatureDto(feature);
        assertEquals(1L, actualFeatureToFeatureDtoResult.getId().longValue());
        assertEquals("Dr", actualFeatureToFeatureDtoResult.getTitle());
        assertEquals(1L, actualFeatureToFeatureDtoResult.getPlan_id().longValue());
        verify(feature).getPlan();
        verify(feature).getId();
        verify(feature).getTitle();
    }

    /**
     * Method under test: {@link PlanMapperImpl#FeatureDtoToFeature(FeatureDto)}
     */
    @Test
    void testFeatureDtoToFeature() {
        PlanMapperImpl planMapperImpl = new PlanMapperImpl();
        assertNull(planMapperImpl.FeatureDtoToFeature(new FeatureDto()).getTitle());
    }

    /**
     * Method under test: {@link PlanMapperImpl#FeatureDtoToFeature(FeatureDto)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFeatureDtoToFeature2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.plans.FeatureDto.getTitle()" because "featureDto" is null
        //       at amiss.wedding.mappers.categories.PlanMapperImpl.FeatureDtoToFeature(PlanMapperImpl.java:43)
        //   See https://diff.blue/R013 to resolve this issue.

        (new PlanMapperImpl()).FeatureDtoToFeature(null);
    }
}

