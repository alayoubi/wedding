import { ArticlePageComponent } from './components/article/article-page/article-page.component';
import { ArticleComponent } from './components/article/article/article.component';

import { ArticlesComponent } from './../public/components/articles/articles/articles.component';
import { VideographersComponent } from './components/suppliers/Videographers/videographers/videographers.component';
import { PhotographersComponent } from './components/suppliers/Photographers/photographers/photographers.component';
import { FloristComponent } from './components/suppliers/Florist/florist/florist.component';
import { CakesComponent } from './components/suppliers/Cakes/cakes/cakes.component';
import { DressesComponent } from './components/suppliers/Dresses/dresses/dresses.component';
import { ImageSliderComponent } from './components/Listing 1/image-slider/image-slider.component';
import { JoinUsComponent } from './components/contact-us-forms/join-us/join-us.component';
import { CoupleContactComponent } from './components/contact-us-forms/couple-contact/couple-contact.component';
import { ContactUsComponent } from './components/contact-us/contact-us/contact-us.component';
import { ShowRegionDataComponent } from './components/show-region-data/show-region-data/show-region-data.component';
import { ShowCategoryDataComponent } from './components/show-category-data/show-category-data/show-category-data.component';
import { CustomListingsComponent } from './components/show-custom-listings/custom-listings/custom-listings.component';
import { NotFoundComponent } from './shared/component/not-found/not-found.component';
import { LoginComponent } from './components/Login/login/login.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PublicComponent } from './public.component';
import { HomeComponent } from './components/home/home.component';
import { ListSingleOneComponent } from './components/Listing 1/list-single-one/list-single-one.component';
import { ListGridViewComponent } from './components/list-grid-view/list-grid-view/list-grid-view.component';
import { VendorCategoriesComponent } from './components/vendor-categories/vendor-categories/vendor-categories.component';
import { VendorByLocationComponent } from './components/vendor-by-location/vendor-by-location/vendor-by-location.component';
import { LoginPageComponent } from './components/Login/login-page/login-page.component';
import { HomePageComponent } from './components/Home Page/home-page/home-page.component';
import { ConfirmPageComponent } from './components/confirm-page/confirm-page.component';
const routes: Routes = [
  { path: '', component: PublicComponent },
  { path: 'home', component: HomeComponent },
  {
    path: 'home/list_single/:listingId',
    component: ListSingleOneComponent,
  },
  {
    path: 'vendors/listings/:category/:catID/:region/:regID',
    component: ListGridViewComponent,
  },
  {
    path: 'vendors/vendors-by-category',
    component: VendorCategoriesComponent,
  },
  {
    path: 'vendors/vendors-by-location',
    component: VendorByLocationComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'confirm',
    component : ConfirmPageComponent
  },
  {
    path: 'notFound',
    component: NotFoundComponent,
  },
  {
    path: 'custom-listings',
    component: CustomListingsComponent,
  },
  {
    path: 'listings/category/:category/:category_id',
    component: ShowCategoryDataComponent,
  },
  {
    path: 'listings/region/:region/:region_id',
    component: ShowRegionDataComponent,
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
  },
  {
    path: 'contact/couple-contact',
    component: CoupleContactComponent,
  },
  {
    path: 'contact/join-us',
    component: JoinUsComponent,
  },
  {
    path: 'image-slider/:id',
    component: ImageSliderComponent,
  },
  {
    path: 'suppliers/dresses',
    component: DressesComponent,
  },
  {
    path: 'suppliers/cakes',
    component: CakesComponent,
  },
  {
    path: 'suppliers/florists',
    component: FloristComponent,
  },
  {
    path: 'suppliers/photo-graphers',
    component: PhotographersComponent,
  },
  {
    path: 'suppliers/video-graphers',
    component: VideographersComponent,
  },
  {
    path: 'blog/articles',
    component: ArticlesComponent,
  },
  {
    path: 'blog/article/:article-id',
    component: ArticlePageComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
