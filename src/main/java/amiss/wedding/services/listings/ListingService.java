package amiss.wedding.services.listings;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ListingService {

    ResponseEntity<List<ListingDto>> findAll();
    ResponseEntity<List<ListingDto>> findAllByStatus(String status);
    ResponseEntity<ListingDto> findById(Long id);
    ResponseEntity<ListingDto> findByIdIfActive(Long id);
    ResponseEntity<ListingDto> findByClient(Long id);
    ResponseEntity<List<ListingDto>> findAllByCategory(Long id);
    ResponseEntity<List<ListingDto>> findAllByRegion(Long regionId);
    ResponseEntity<List<ListingDto>> findAllByCity(Long cityId);
    ResponseEntity<List<ListingDto>> findAllByPlan(Long planId);
    ResponseEntity<List<ListingDto>> findAllByCategoryAndPlan(Long categoryId, Long planId);
    ResponseEntity<List<ListingDto>> findAllByCategoryAndRegion(Long categoryId, Long regionId);
    ResponseEntity<List<ListingDto>> findAllByCategoryAndCity(Long categoryId, Long cityId);
    ResponseEntity<List<ListingDto>> findAllByCategoryAndStatus(Long categoryId, String status);
    ResponseEntity<List<ListingDto>> findAllByCategoryAndRegionAndStatusSortOfPriority(Long categoryId, Long regionId, String status,
                                                                                       int page);
    ResponseEntity<List<ListingDto>> findAllByCategoryAndRegionAndStatusWithoutPriority(Long categoryId, Long regionId, String status,
                                                                                        int page);
    ResponseEntity<List<ListingDto>> findAllByRegionAndStatus(Long regionId, String status);
    ResponseEntity<ListingDto> addCoverImg(Long listingId, ListingPhotoDto listingPhotoDto);
    ResponseEntity<ListingDto> chooseCoverImg(Long listingId, Long photoId);
    ResponseEntity<ListingDto> create(ListingDto listingDto);
    ResponseEntity<ListingDto> update(Long id, ListingDto listingDto);
    ResponseEntity<ListingDto> updateStatus(Long id, String status);
    ResponseEntity<ListingDto> updatePlan(Long id, Long planId);
    ResponseEntity<ListingDto> updatePriority(Long id, int priority);
    ResponseEntity<String> markAsDeleted(Long listingId);
    ResponseEntity<String> remove(Long id);
    Listing insert(Listing listing);
}
