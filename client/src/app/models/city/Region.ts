export interface Region {
code: any;
  id: number;
  name: string;
  name_en: string;
  capital_city: string;
  active: boolean;
  country: string;
}
