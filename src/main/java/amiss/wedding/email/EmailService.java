package amiss.wedding.email;

import amiss.wedding.config.ReloadableMailSender;
import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.VerificationToken;
import amiss.wedding.errors.UserNotFoundException;
import amiss.wedding.repositories.users.UserRepository;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;


import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

@Component
public class EmailService {

    private SettingsProvider settingsProvider;
    private SpringTemplateEngine thymeleafTemplateEngine;
    private ReloadableMailSender reloadableMailSender;

    private UserRepository userRepository;



    public EmailService(
                        SpringTemplateEngine thymeleafTemplateEngine,
                        UserRepository userRepository) {
        this.settingsProvider = settingsProvider;
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
        this.userRepository = userRepository;
    }

    @Async
    public void sendVerification(String emailTo, VerificationToken verifyToken) throws Exception {
        Map<String, Object> variables = new HashMap<>();

        @NotBlank String baseUrl = settingsProvider.getAllSettings().getBasicSettings().getBaseUrl();
        variables.put("url", baseUrl + "/portal/register/validate/" + verifyToken.getToken());

        AppUser user = userRepository.findUserByEmail(emailTo).orElseThrow(() -> new UserNotFoundException(-1));

        sendMessage(emailTo, "Email Verifikation", variables, Template.VERIFICATION);
    }


    @Async
    public void sendMessage(String to, String subject, Map<String, Object> variables, Template template) throws Exception {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(variables);
        String htmlBody;

        htmlBody = thymeleafTemplateEngine.process(String.valueOf(template), thymeleafContext);

        sendHtmlMessage(to, subject, htmlBody);

    }

    public void sendHtmlMessage(String to, String subject, String htmlBody) throws Exception {
        JavaMailSender emailSender = getMailSender();

        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        emailSender.send(message);

    }

    private JavaMailSender getMailSender() {
        return this.reloadableMailSender.getMailSender();
    }


}
