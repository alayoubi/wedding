package amiss.wedding.registration;


import amiss.wedding.security.auth.AuthenticationResponse;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/registration")
@AllArgsConstructor
public class RegistrationController {

    private final RegistrationService registrationService;

    @PostMapping
    @ApiOperation(value = "Register a new User", notes = "Provide a Registration Object, registers user-account")
    public ResponseEntity<AuthenticationResponse> register(
            @RequestBody RegistrationRequest request) {
        return registrationService.registerNewUserAccount(request);
    }

    @GetMapping(path = "/confirm/{token}")
    public ResponseEntity confirm(@PathVariable("token") String token) {
        return registrationService.confirmToken(token);
    }
}
