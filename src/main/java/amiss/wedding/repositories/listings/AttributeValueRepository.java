package amiss.wedding.repositories.listings;

import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.Listing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AttributeValueRepository extends JpaRepository<AttributeValue, Long> {

    List<AttributeValue> findByListing(Listing listing);

    void deleteAllByListing(Listing listing);
}
