package amiss.wedding.services.categories;

import amiss.wedding.entities.categories.dto.CategoryDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CategoryService {

    ResponseEntity<List<CategoryDto>> findAll();
    ResponseEntity<CategoryDto> findById(Long id);

    ResponseEntity<CategoryDto> findByName(String name);

    ResponseEntity<CategoryDto> create(CategoryDto categoryDto);
    ResponseEntity<CategoryDto> update(Long id, CategoryDto categoryDto);
    ResponseEntity<String> remove(Long id);
}
