import { Listing } from './../../../../../models/listing/Listing';
import { Component, Input, OnInit } from '@angular/core';
import { SectionComponent } from '../../Home Page/section/section.component';
import * as AS from '../../Home Page/section/section.component';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ListingService } from '@core/services/listing/listing.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-wedding-venue',
  templateUrl: './wedding-venue.component.html',
  styleUrls: ['./wedding-venue.component.css'],
})
export class WeddingVenueComponent implements OnInit {
  public listings: Array<any>;
  public listingsByCatAndReg: any;
  public listingAttributes: any;
  public listingAttributesMain: Array<any> = [];
  public title: any;

  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.getListingsByCatAndReg();
  }

  public getListingsByCatAndReg(): void {
    this.route.params.subscribe((params) => {
      this.listingService
        .getListingByCatAndReg(params['catID'], params['regID'])
        .subscribe((listingService) => {
          this.listingsByCatAndReg = listingService;
          this.listingAttributes =
            this.listingsByCatAndReg[0].attributeValueList;
          console.log(this.listingAttributes);
          // this.title =" this.listing.title;"
          // this.titleService.setTitle(`${this.title}`);
          console.log(this.listingsByCatAndReg);
        });
    });
  }
}
