package amiss.wedding.entities.listings;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.users.AppUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "reviews")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Review extends BaseEntity {

    @ManyToOne
    @JoinColumn(name="listing_id", nullable=false)
    private Listing listing;

    @NotNull
    @NotBlank(message = "Title may not be blank")
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @Column(name = "contents", nullable=false)
    private String content;

    @Column(name = "datePosts", nullable=false)
    private Date datePost;

    @Column(name = "ratings", nullable=false)
    private float rating;

    @ManyToOne()
    @JoinColumn(name="user_id")
    private AppUser user;
}
