import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWeddingHallComponent } from './add-wedding-hall.component';

describe('AddWeddingHallComponent', () => {
  let component: AddWeddingHallComponent;
  let fixture: ComponentFixture<AddWeddingHallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddWeddingHallComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddWeddingHallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
