import { ListingPhotoService } from '@core/services/listingPhoto/listing-photo.service';
import { Component, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-upload-photo-overlay',
  templateUrl: './upload-photo-overlay.component.html',
  styleUrls: ['./upload-photo-overlay.component.css'],
})
export class UploadPhotoOverlayComponent {
  constructor(
    public dialog: MatDialogRef<UploadPhotoOverlayComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private listingPhotoService: ListingPhotoService
  ) {}

  public id: number =  this.data?.id;
  public photos: File[] = [];
  urls: any [] = [];

  ngOnInit(): void {
    console.log(this.data, 'data');
  }

  onListingPhotosChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        this.photos.push(fileList[i]);
        var reader = new FileReader();
        reader.readAsDataURL(fileList[i]);
        reader.onload = (events:any)=>{
          this.urls.push(events.target.result)
        }
      }
    }

  }

  onSubmit() {
    const formData = new FormData();

    console.log(this.id)
    for (let i = 0; i < this.photos?.length; i++) {
      formData.append('photos', this.photos[i]);
    }


    this.listingPhotoService.uploadsListingPhoto(this.id, formData).subscribe(
      (response) => {
        // File upload success

        console.log("photo uploded");

        // alert for 2 secound

        this.close();
      },
      (error) => {
        // File upload error
      }
    );

    console.log(formData);
  }

  close() {
    this.dialog.close();
  }
}
