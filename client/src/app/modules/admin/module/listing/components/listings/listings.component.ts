import { FormControl, FormGroup } from '@angular/forms';
import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';
import { ListingService } from '@core/services/listing/listing.service';
import { UpdateListingComponent } from '../update-listing/update-listing.component';
import { CategoryService } from '@core/services/category/category.service';
import { AnyAaaaRecord } from 'dns';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Listing } from '@core/models/listing/Listing';

export interface product {
  id: Number;
  title: string;
  category: string;
  description: string;
  image: string;
  rating: any;
  price: number;
  status:string
}

@Component({
  selector: 'app-listings',
  templateUrl: './listings.component.html',
  styleUrls: ['./listings.component.css'],
})
export class ListingsComponent implements OnInit {

  updateStatusForm = new FormGroup({
    status:new FormControl()
  })

  public listings: any[] = [];

  public listing: Listing;


  // public categories: Category[];

  public nameP: any;

  public r;

  categories: any;

  indexes=1

  @Input() list: any = {};

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private categoryService: CategoryService,
    private listingService: ListingService,
    private title: Title,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    title.setTitle('الإعلانات');
  }

  status = ['COMPLETED', 'COMPLETED', 'COMPLETED', 'COMPLETED'];

  public selectedStatus: any

  displayedColumns: string[] = [
    'id',
    'image',
    'title',
    'category',
    'region',
    'status',
    'action',
  ];
  dataSource!: MatTableDataSource<product>;

  ngAfterViewInit() {}

  ngOnInit(): void {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    this.getAllCategories();
    this.getListings();
  }

  getListings() {
    this.listingService.getListings().subscribe((res) => {
      this.listings = res;
      this.dataSource = new MatTableDataSource(this.listings);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      // this.listings =res.map((item, index) => {
      //   return {
      //     ...item,
      //     rowNumber: index + 1,
      //   };
      // });
    });
  }

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
    });
  }

  applyFilter(event: Event) {
    const filterVal = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterVal.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async openDialog(row: any) {
    await this.getListingById(row.id);
    let dialogConf = new MatDialogConfig();
    dialogConf.width = '100%';
    dialogConf.disableClose = true;
    dialogConf.autoFocus = false;
    dialogConf.height = '80%';
    dialogConf.enterAnimationDuration = '700ms';
    dialogConf.data = this.listing;
    this.dialog.open(UpdateListingComponent, dialogConf);
  }

  getListingById(id: number) {
    return new Promise<void>((resolve, reject) => {
      this.listingService
        .getListing(id)
        .subscribe(
          (listingService) => {
            this.listing = listingService;
            console.log("test: " + this.listing);
            resolve(); // Resolve the promise when the operation is complete
          },
          (error) => {
            reject(error); // Reject the promise if there's an error
          }
        );
    });
  }


  select_status(ev: any) {
    this.selectedStatus = ev;
    this.updateStatusForm.get('status')?.setValue(this.selectedStatus)
    console.log(this.selectedStatus);
    console.log(this.updateStatusForm.value,'update status')
  }

  delete(listingId: any) {
    // this.listingService.deleteListing(listingId).subscribe((res) => {
    //   console.log(res, 'deleted Successfully');
    //   this.snackBar.open('✔ تم حذف الإعلان بنجاح', 'تم', {
    //     duration: 2000,
    //     panelClass: ['green-snackbar', 'login-snackbar', 'my-custom-snackbar'],
    //   });
    //   this.getListings()
    // });
  }

  addListing() {
    this.router.navigate(['/admin/listings/add']);
  }
}
