package amiss.wedding.entities.articles.dto;


import amiss.wedding.entities.articles.ArticlePhoto;
import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ArticlePhotoDto {
    private Long id;
    private String fileName;

    private String title;
    private String path;
    private Long article_Id;
    private Long miniArticle_Id;
    private double size;

    public ArticlePhotoDto(ArticlePhoto articlePhoto){
        this.id = articlePhoto.getId();
        this.fileName = articlePhoto.getFileName();
        this.path = articlePhoto.getPath();
        this.title = (articlePhoto.getTitle() != null) ? articlePhoto.getTitle() : articlePhoto.getFileName();
        this.article_Id = (articlePhoto.getArticle() != null) ? articlePhoto.getArticle().getId() : null;
        this.miniArticle_Id = (articlePhoto.getMiniArticle() != null) ? articlePhoto.getMiniArticle().getId() : null;
        this.size = articlePhoto.getSize();
    }
}
