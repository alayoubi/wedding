package amiss.wedding.entities.articles;

public enum ArticleType {

    INFORMATION,
    GALLERY
}
