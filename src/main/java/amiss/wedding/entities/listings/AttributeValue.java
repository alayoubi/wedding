package amiss.wedding.entities.listings;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.categories.attributes.Attribute;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "attribute_values")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AttributeValue extends BaseEntity {

    @Lob
    @Column(name = "value")
    private String value;

    @ManyToOne
    @JoinColumn(name="attribute_id", nullable=false)
    private Attribute attribute;

    @ManyToOne
    @JoinColumn(name="listing_id", nullable=false)
    private Listing listing;

    @Column(name = "active", columnDefinition = "boolean  default false")
    private boolean active;
}
