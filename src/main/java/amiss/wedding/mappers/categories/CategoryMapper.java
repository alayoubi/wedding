package amiss.wedding.mappers.categories;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.dto.CategoryDto;

public interface CategoryMapper {

    Category categoryDtoToCategory(CategoryDto categoryDto);

    CategoryDto categoryToCategoryDto(Category category);

    CategoryDto categoryToBasicCategoryDto(Category category);
}
