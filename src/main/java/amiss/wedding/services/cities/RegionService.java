package amiss.wedding.services.cities;

import amiss.wedding.entities.cities.dto.RegionDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface RegionService {

    ResponseEntity<List<RegionDto>> findAll();
    ResponseEntity<List<RegionDto>> findAllActive();

    ResponseEntity<List<RegionDto>> findAllByCountry(Long country_id);
    ResponseEntity<List<RegionDto>> findAllByCountryAndActive(Long country_id);

    ResponseEntity<RegionDto> findById(Long id);
    ResponseEntity<RegionDto> create(RegionDto regionDto);
    ResponseEntity<RegionDto> update(Long id, RegionDto regionDto);
    ResponseEntity<String> remove(Long id);
}
