package amiss.wedding.controllers.admin.listings;


import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.errors.ErrorResponse;
import amiss.wedding.services.listings.ListingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/admin/listings")
@AllArgsConstructor
public class AdminListingController {

    private final ListingService listingService;

    @GetMapping("")
    @Operation(summary = "Get All Listing",
            description= "Get All Listing")
    public ResponseEntity<List<ListingDto>> getListings(){
        return this.listingService.findAll();
    }


    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ok", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = Listing.class)) }),
            @ApiResponse(responseCode = "204", description = "No Listing"),
            @ApiResponse(responseCode = "400", description = "Invalid ID supplied"),
            @ApiResponse(responseCode = "404", description = "Listing not found"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content =
                    { @Content(mediaType = "application/json", schema =
                    @Schema(implementation = ErrorResponse.class)) }) })
    @Operation(summary = "Get All Listing by Status",
            description= "current status are pending, processing, completed, decline, archived")
    @GetMapping(path = "/status/{status}")
    public ResponseEntity<List<ListingDto>> getListingByStatus(@PathVariable("status") String status){
        return this.listingService.findAllByStatus(status);
    }

    @GetMapping(path = "/category/{category_id}/region/{region_id}")
    public ResponseEntity<List<ListingDto>> getListingByCategoryAndRegion(@PathVariable("category_id") Long category_id
            , @PathVariable("region_id") Long region_id){
        return this.listingService.findAllByCategoryAndRegion(category_id, region_id);
    }

    @GetMapping(path = "/{listing_id}")
    public ResponseEntity<ListingDto> getListing(@PathVariable("listing_id") Long listing_id){
        return this.listingService.findById(listing_id);
    }

    @GetMapping(path = "/client/{client_id}")
    public ResponseEntity<ListingDto> getListingByClient(@PathVariable("client_id") Long client_id){
        return this.listingService.findByClient(client_id);
    }

    @GetMapping(path = "/category/{category_id}")
    public ResponseEntity<List<ListingDto>> getListingByCategory(@PathVariable("category_id") Long category_id){
        return this.listingService.findAllByCategory(category_id);
    }

    @GetMapping(path = "/category/{category_id}/plan/{plan_id}")
    public ResponseEntity<List<ListingDto>> getListingByCategoryAndPlan(@PathVariable("category_id") Long category_id
            , @PathVariable("plan_id") Long plan_id){
        return this.listingService.findAllByCategoryAndPlan(category_id, plan_id);
    }

    @GetMapping(path = "/region/{region_id}")
    public ResponseEntity<List<ListingDto>> getListingByRegion(@PathVariable("region_id") Long region_id){
        return this.listingService.findAllByRegion(region_id);
    }

    @GetMapping(path = "/region/{region_id}/status/{status}")
    public ResponseEntity<List<ListingDto>> getListingByRegionAndStatus(
            @PathVariable("region_id") Long region_id,
            @PathVariable("status") String status){
        return this.listingService.findAllByRegionAndStatus(region_id, status);
    }

    @GetMapping(path = "/category/{category_id}/city/{city_id}")
    public ResponseEntity<List<ListingDto>> getListingByCategoryAndCity(@PathVariable("category_id") Long category_id
            , @PathVariable("city_id") Long city_id){
        return this.listingService.findAllByCategoryAndCity(category_id, city_id);
    }

    @GetMapping(path = "/city/{city_id}")
    public ResponseEntity<List<ListingDto>> getListingByCity(@PathVariable("city_id") Long city_id){
        return this.listingService.findAllByCity(city_id);
    }

    @GetMapping(path = "/plan/{plan_id}")
    public ResponseEntity<List<ListingDto>> getListingByPlan(@PathVariable("plan_id") Long plan_id){
        return this.listingService.findAllByPlan(plan_id);
    }

    @GetMapping(path = "/category/{category_id}/status/{status}")
    public ResponseEntity<List<ListingDto>> getListingByCategoryAndStatus(@PathVariable("category_id") Long category_id
            , @PathVariable("status") String status){
        return this.listingService.findAllByCategoryAndStatus(category_id, status);
    }

    @PostMapping()
    public ResponseEntity<ListingDto> createListing(@RequestBody ListingDto listingDto){
        return this.listingService.create(listingDto);
    }

    @PutMapping("/{listingId}/cover/{photo_id}")
    public ResponseEntity<ListingDto> chooseCoverImg(@PathVariable("listingId") Long listingId,
                                                     @PathVariable("photo_id") Long photo_id) {
        return this.listingService.chooseCoverImg(listingId, photo_id);
    }
    @PutMapping(path = "/{listingId}")
    public ResponseEntity<ListingDto> updateListing(@PathVariable("listingId") Long listingId,
                                                    @RequestBody ListingDto listingDto){
        return this.listingService.update(listingId, listingDto);
    }

    @PutMapping(path = "/{listingId}/status/{status}")
    public ResponseEntity<ListingDto> updateListing(@PathVariable("listingId") Long listingId,
                                                    @PathVariable("status") String status){
        return this.listingService.updateStatus(listingId, status);
    }

    @PutMapping(path = "/{listingId}/plan/{planId}")
    public ResponseEntity<ListingDto> updateListing(@PathVariable("listingId") Long listingId,
                                                    @PathVariable("planId") Long planId){
        return this.listingService.updatePlan(listingId, planId);
    }

    @PutMapping(path = "/{listingId}/priority/{priority}")
    public ResponseEntity<ListingDto> updatePriority(@PathVariable("listingId") Long listingId,
                                                     @PathVariable("priority") int priority){
        return this.listingService.updatePriority(listingId, priority);
    }

    @DeleteMapping(path = "/{listingId}")
    public ResponseEntity<String> removeListing(@PathVariable Long listingId){
        return this.listingService.remove(listingId);
    }
}
