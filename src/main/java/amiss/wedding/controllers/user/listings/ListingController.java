package amiss.wedding.controllers.user.listings;

import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.services.listings.ListingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/listings")
@AllArgsConstructor
public class ListingController {

    private final ListingService listingService;

    @GetMapping(path = "/{listing_id}")
    public ResponseEntity<ListingDto> getListing(@PathVariable("listing_id") Long listing_id){
        return this.listingService.findByIdIfActive(listing_id);
    }

    @GetMapping(path = "/category/{category_id}/region/{region_id}/{page}")
    public ResponseEntity<List<ListingDto>> getActiveListingsByCategoryAndRegion(
            @PathVariable("category_id") Long category_id,
            @PathVariable("region_id") Long region_id,
            @PathVariable("page") int page){
        return this.listingService.findAllByCategoryAndRegionAndStatusSortOfPriority(category_id, region_id,
                "completed", page);
    }

    @GetMapping(path = "/category/{category_id}/region/{region_id}/no_plan/{page}")
    public ResponseEntity<List<ListingDto>> getActiveListingsByCategoryAndRegionWithoutPlan(
            @PathVariable("category_id") Long category_id,
            @PathVariable("region_id") Long region_id,
            @PathVariable("page") int page){
        return this.listingService.findAllByCategoryAndRegionAndStatusWithoutPriority(
                category_id, region_id, "completed", page);
    }

}
