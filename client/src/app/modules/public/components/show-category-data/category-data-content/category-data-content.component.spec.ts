import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryDataContentComponent } from './category-data-content.component';

describe('CategoryDataContentComponent', () => {
  let component: CategoryDataContentComponent;
  let fixture: ComponentFixture<CategoryDataContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryDataContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoryDataContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
