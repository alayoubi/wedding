package amiss.wedding.mappers.categories;

import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;

public interface AttributeMapper {

    Attribute attributeDtoToAttribute(AttributeDto attributeDto);

    AttributeDto attributeToAttributeDto(Attribute attribute);
}
