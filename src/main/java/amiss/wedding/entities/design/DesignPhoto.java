package amiss.wedding.entities.design;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "design_photos")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DesignPhoto extends BaseEntity {

    @NotNull
    @NotBlank(message = "Path may not be blank")
    @Column(name = "path")
    private String path;

    @Column(name = "status", length = 32, columnDefinition = "varchar(32) default 'pending'")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "size")
    private double size;
}
