package amiss.wedding.registration;

import amiss.wedding.repositories.users.UserRepository;
import amiss.wedding.security.auth.AuthenticationResponse;
import amiss.wedding.security.auth.AuthenticationService;
import amiss.wedding.services.users.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService{

    private final UserRepository userRepository;
    private final UserServiceImpl userService;
    private final AuthenticationService authenticationService;

    /**
     * @param request
     * @return
     */
    @Override
    public ResponseEntity<AuthenticationResponse> registerNewUserAccount(
            RegistrationRequest request) {
        boolean userExists = userRepository
                .findUserByEmail(request.email())
                .isPresent();

        if (userExists) {
            // TODO check of attributes are the same and
            // TODO if email not confirmed send confirmation email.
            throw new IllegalStateException("The Email: " + request.email() + " already taken");
        }
        var savedUser = userService.registerNewUserAccount(request);
        AuthenticationResponse  authenticationResponse = authenticationService.creatTokens(savedUser);

        return new ResponseEntity<>(authenticationResponse, HttpStatus.CREATED);
    }

    /**
     * @param token
     * @return
     */
    @Override
    public ResponseEntity confirmToken(String token) {
        return userService.activateUserByToken(token);
    }
}
