import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinUsDetailsCardComponent } from './join-us-details-card.component';

describe('JoinUsDetailsCardComponent', () => {
  let component: JoinUsDetailsCardComponent;
  let fixture: ComponentFixture<JoinUsDetailsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinUsDetailsCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JoinUsDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
