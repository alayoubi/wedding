package amiss.wedding.controllers.client.listings;


import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.services.listings.ListingService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/client/listings")
@AllArgsConstructor
public class ClientListingController {

    private final ListingService listingService;

    @GetMapping("")
    public ResponseEntity<List<ListingDto>> getListings(){
        return this.listingService.findAll();
    }

}
