package amiss.wedding.services.listings.impl;

import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.SocialMedia;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.dto.SocialMediaDto;
import amiss.wedding.errors.ListingNotFoundException;
import amiss.wedding.errors.SocialMediaNotFoundException;
import amiss.wedding.mappers.listings.SocialMediaMapper;
import amiss.wedding.mappers.listings.SocialMediaMapperImpl;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.repositories.listings.SocialMediaRepository;
import amiss.wedding.services.listings.SocialMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SocialMediaServiceImpl implements SocialMediaService {

    private final SocialMediaRepository socialMediaRepository;
    private final ListingsRepository listingsRepository;
    private final SocialMediaMapper mapper = new SocialMediaMapperImpl();

    @Autowired
    public SocialMediaServiceImpl(ListingsRepository listingsRepository,
                                  SocialMediaRepository socialMediaRepository) {
        this.socialMediaRepository = socialMediaRepository;
        this.listingsRepository = listingsRepository;
    }

    @Override
    public ResponseEntity<List<SocialMediaDto>> findAll() {
        List<SocialMedia> socialMedias = this.socialMediaRepository.findAll();
        List<SocialMediaDto> socialMediaDtoList =
                socialMedias.stream().map(mapper::socialMediaToSocialMediaDto).collect(Collectors.toList());
        return new ResponseEntity<>(socialMediaDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<SocialMediaDto> findById(Long id) {
        Optional<SocialMedia> socialMedia = this.socialMediaRepository.findById(id);
        if (socialMedia.isEmpty()) {
            throw new SocialMediaNotFoundException(id);
        }
        SocialMediaDto socialMediaDto = mapper.socialMediaToSocialMediaDto(socialMedia.get());
        return new ResponseEntity<>(socialMediaDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<SocialMediaDto>> findAllByListingId(Long listingId) {
        Listing listing = listingsRepository.findById(listingId).orElseThrow(()->
                new ListingNotFoundException(listingId));

        List<SocialMedia> socialMedias = this.socialMediaRepository.findAllByListing(listing);
        List<SocialMediaDto> socialMediaDtoList =
                socialMedias.stream().map(mapper::socialMediaToSocialMediaDto).collect(Collectors.toList());
        return new ResponseEntity<>(socialMediaDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<SocialMediaDto> create(SocialMediaDto socialMediaDto, Long listingId) {
        Listing listing = listingsRepository.findById(socialMediaDto.getListingId()).orElseThrow(()->
                new ListingNotFoundException(socialMediaDto.getListingId()));
        final SocialMedia socialMedia = mapper.socialMediaDtoToSocialMedia(socialMediaDto);
        socialMedia.setListing(listing);
        final SocialMedia createdSocialMedia = this.socialMediaRepository.save(socialMedia);
        SocialMediaDto createdSocialMediaDto = mapper.socialMediaToSocialMediaDto(createdSocialMedia);
        return new ResponseEntity<>(createdSocialMediaDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<SocialMediaDto> update(Long id, SocialMediaDto newSocialMediaDto) {
        Optional<SocialMedia> socialMedia = this.socialMediaRepository.findById(id);
        if (socialMedia.isEmpty()){
            throw new SocialMediaNotFoundException(id);
        }
        SocialMedia existingSocialMedia = socialMedia.get();
        existingSocialMedia.setPath(newSocialMediaDto.getPath());
        existingSocialMedia.setSocialMediaTyp(newSocialMediaDto.getSocialMediaTyp());
        SocialMedia updatedSocialMedia = this.socialMediaRepository.save(existingSocialMedia);
        SocialMediaDto socialMediaDto = mapper.socialMediaToSocialMediaDto(updatedSocialMedia);
        return new ResponseEntity<>(socialMediaDto, HttpStatus.OK);
    }

    /**
     * @param listingDto
     * @param createdListing
     */
    @Override
    public Set<SocialMedia> setAndSaveSocialMedias(ListingDto listingDto, Listing createdListing) {

        Set<SocialMedia> socialMediaSet = new HashSet<>();
        if (listingDto.getSocialMediaList() != null || !listingDto.getSocialMediaList().isEmpty()) {
            for (SocialMediaDto socialMediaDto: listingDto.getSocialMediaList()) {
                if (!(socialMediaDto.getPath() == "" || socialMediaDto.getPath() == null)) {
                    SocialMedia socialMedia = mapper.socialMediaDtoToSocialMedia(socialMediaDto);
                    socialMedia.setListing(createdListing);
                    socialMedia = socialMediaRepository.save(socialMedia);
                    socialMediaSet.add(socialMedia);
                }
            }
        }
        return socialMediaSet;
    }


    @Override
    public void markAsDeleted(Listing listing) {
        if (listing.getSocialMedia() != null || !listing.getSocialMedia().isEmpty()) {
            for (SocialMedia socialMedia : listing.getSocialMedia() ) {
                markAsDeleted(socialMedia);
            }
        }
    }


    @Override
    public void markAsDeleted(SocialMedia socialMedia) {
        socialMedia.setDeletedAt(Instant.now());
        this.socialMediaRepository.save(socialMedia);
    }
    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.socialMediaRepository.existsById(id);
        if (!exists) {
            throw new SocialMediaNotFoundException(id);
        }
        this.socialMediaRepository.deleteById(id);
        return new ResponseEntity<>("Social media has been removed successfuly!", HttpStatus.NO_CONTENT);
    }
}
