import { API } from './../../config/api.config';
import { Observable, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from '@core/models/city/City';

@Injectable({
  providedIn: 'root',
})
export class CityService {
  private cities: City[];

  constructor(private http: HttpClient) {}

  getCities(): Observable<City[]> {
    if (this.cities) {
      // Return cached categories if they exist
      return of(this.cities);
    } else {
      // Load categories from API and cache the result
      return this.http
        .get<City[]>(API.CITIES.BASE)
        .pipe(tap((categories) => (this.cities = this.cities)));
    }
  }

 

  getCitiy(cityId: number): Observable<City> {
    return this.http.get<City>(API.CITIES.ID(cityId));
  }

  getCitiesByRegion(regionId: number): Observable<City[]> {
    return this.http.get<City[]>(API.CITIES.REGIONID(regionId));
  }
}
