export interface Country{
  id: number;
  name: string;
  name_en: string;
  code: string;
  capital_city: number;
  regions: string[];
}