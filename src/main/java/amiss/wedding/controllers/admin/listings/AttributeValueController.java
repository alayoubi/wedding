package amiss.wedding.controllers.admin.listings;

import amiss.wedding.entities.listings.dto.AttributeValueDto;
import amiss.wedding.services.listings.AttributeValueService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/attributeValue")
@AllArgsConstructor
public class AttributeValueController {

    private final AttributeValueService attributeValueService;

    @GetMapping("")
    public ResponseEntity<List<AttributeValueDto>> getAttributeValues(){
        return this.attributeValueService.findAll();
    }

    @GetMapping(path = "/{attributeValue_id}")
    public ResponseEntity<AttributeValueDto> getAttributeValue(@PathVariable("attributeValue_id") Long attributeValue_id){
        return this.attributeValueService.findById(attributeValue_id);
    }

    @GetMapping(path = "/listing/{listing_id}")
    public ResponseEntity<List<AttributeValueDto>> getAttributeValueByListing(@PathVariable("listing_id") Long listing_id){
        return this.attributeValueService.findAllByListingId(listing_id);
    }

    @PostMapping()
    public ResponseEntity<AttributeValueDto> createAttributeValue(@RequestBody AttributeValueDto attributeValueDto){
        return this.attributeValueService.create(attributeValueDto);
    }

    @PostMapping(path = "/listing/{listing_id}/values")
    public ResponseEntity<List<AttributeValueDto>> createAttributeValue(@PathVariable("listing_id") Long listing_id,
                                                                        @RequestBody List<AttributeValueDto> attributeValueDtoList ){
        return this.attributeValueService.createValuesfurListing(listing_id, attributeValueDtoList);
    }

    @PutMapping(path = "/{attributeValue_id}")
    public ResponseEntity<AttributeValueDto> updateAttributeValue(
            @PathVariable("attributeValue_id") Long attributeValue_id,
            @RequestBody AttributeValueDto attributeValueDto){
        return this.attributeValueService.update(attributeValue_id, attributeValueDto);
    }

    @DeleteMapping(path = "/{attributeValue_id}")
    public ResponseEntity<String> removeAttributeValue(@PathVariable Long attributeValue_id){
        return this.attributeValueService.remove(attributeValue_id);
    }
}
