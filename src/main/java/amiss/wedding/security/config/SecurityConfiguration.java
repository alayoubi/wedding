package amiss.wedding.security.config;


import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static amiss.wedding.entities.users.Permission.*;
import static amiss.wedding.entities.users.Role.ADMIN;
import static amiss.wedding.entities.users.Role.MANAGER;
import static com.google.api.client.http.HttpMethods.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration {

    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;

    String [] ALLOWED_METHODS = {"GET", "POST", "PUT", "DELETE", "OPTIONS"};

    String [] ALLOWED_ORIGINS = {"https://lilty-net.firebaseapp.com", "lilty-net.firebaseapp.com",
            "http://localhost:8080", "http://localhost:4200", "http://localhost:4201", "http://localhost:4202",
            "http://143.244.132.99:8080", "http://64.227.160.199:8080",
            "https://lilty-net.web.app", "lilty-net.web.app"};
    public final String[] PUBLIC_ENDPOINTS = {
        "/api/v1/categories/**",
            "/api/v1/users/**",
            "/api/v1/countries/**",
            "/api/v1/regions/**",
            "/api/v1/cities/**",
            "/api/v1/districts/**",
            "/api/v1/roles/**",
            "/api/v1/attributes/**",
            "/api/v1/plans/**",
            "/api/v1/clients/**",
            "/api/v1/listings/**",
            "/api/v1/attributeValue/**",
            "/api/v1/articles/**",
            "api/v1/registration/**",
            "/api/v1/admin/**"
    };

    private static final String[] AUTH_WHITELIST = {
            // for Swagger UI v2
            "/v2/api-docs",
            "/swagger-ui.html",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/webjars/**",

            // for Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**"
    };

    String [] PUBLIC_END_POINTS = {"/api/v1/auth/authenticate", "/api/v1/auth/refresh-token", "/api/v1/auth/logout"};
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .exceptionHandling()
                .and().httpBasic()
//                .authenticationEntryPoint(jwtUnAuthResponse)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers("/api/v1/auth/**").permitAll()
                .antMatchers("/swagger-ui.html",
                        "/webjars/springfox-swagger-ui/**",
                        "/swagger-resources/**",
                        "/swagger-ui.html/**").permitAll()
                .antMatchers("/index.html", "/", "/*.css", "/*.js", "/assets/**").permitAll()
                .antMatchers("/public/**", "/admin/**", "/client/**", "/user/**").permitAll()
                .antMatchers(HttpMethod.GET, PUBLIC_ENDPOINTS).permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/listings/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/listings/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/v1/listings/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/admin/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/admin/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/v1/admin/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/listingPhotos/**").permitAll()
                .antMatchers(HttpMethod.PUT, "/api/v1/listingPhotos/**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/api/v1/listingPhotos/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/registration/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/registration/**").permitAll()

                .antMatchers("/api/v1/management/**").hasAnyRole(ADMIN.name(), MANAGER.name())
                .antMatchers(GET, "/api/v1/management/**").hasAnyAuthority(ADMIN_READ.name(), MANAGER_READ.name())
                .antMatchers(POST, "/api/v1/management/**").hasAnyAuthority(ADMIN_CREATE.name(), MANAGER_CREATE.name())
                .antMatchers(PUT, "/api/v1/management/**").hasAnyAuthority(ADMIN_UPDATE.name(), MANAGER_UPDATE.name())
                .antMatchers(DELETE, "/api/v1/management/**").hasAnyAuthority(ADMIN_DELETE.name(), MANAGER_DELETE.name())
                .anyRequest().authenticated()
//                .sessionManagement()
//                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .logout()
                .logoutUrl("/api/v1/auth/logout")
                .addLogoutHandler(logoutHandler)
                .logoutSuccessHandler((request, response, authentication) -> SecurityContextHolder.clearContext());

        return http.build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods(ALLOWED_METHODS)
                        .allowedOrigins(ALLOWED_ORIGINS);
            }
        };
    }
}
