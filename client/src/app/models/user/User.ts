export interface User {
  id: number;
  role: string;
  active: boolean;
  email: string;
}
