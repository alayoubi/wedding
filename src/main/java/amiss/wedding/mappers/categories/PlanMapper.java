package amiss.wedding.mappers.categories;

import amiss.wedding.entities.categories.plans.Feature;
import amiss.wedding.entities.categories.plans.FeatureDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanDto;

public interface PlanMapper {

    Plan planDtoToPlan(PlanDto planDto);

    PlanDto planToPlanDto(Plan plan);

    FeatureDto featureToFeatureDto(Feature feature);

    Feature FeatureDtoToFeature(FeatureDto featureDto);
}
