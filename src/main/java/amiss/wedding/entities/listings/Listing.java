package amiss.wedding.entities.listings;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.cities.*;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.photos.ListingVideo;
import amiss.wedding.entities.listings.plans.ListingPlan;
import amiss.wedding.entities.users.client.Client;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "listings")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "deleted_at IS NULL")
public class Listing extends BaseEntity {


    @NotNull
    @NotBlank(message = "Title may not be blank")
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @Column(name = "email", length = 100)
    @Size(max = 100, message = "Email may not be more than 100 characters")
    private String email;

    @Column(name = "phone", length = 32)
    @Size(max = 32, message = "Phone may not be more than 100 characters")
    private String phone;

    @Column(name = "address")
    private String address;

    @Lob
    @Column(name = "descriptions")
    private String descriptions;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cover_id", referencedColumnName = "id")
    private ListingPhoto coverImg;

    @Column(name = "status", length = 32, columnDefinition = "varchar(32) default 'PENDING'")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne
    @JoinColumn(name="country_id", nullable=false)
    private Country country;

    @ManyToOne
    @JoinColumn(name="region_id", nullable=false)
    private Region region;

    @ManyToOne
    @JoinColumn(name="sub_region_id")
    private SubRegion subRegion;

    @ManyToOne
    @JoinColumn(name="city_id", nullable=false)
    private City city;


    @ManyToOne
    @JoinColumn(name="district_id")
    private District district;

    @OneToOne
    @JoinColumn(name="position_id")
    private Position position;

    @ManyToOne
    @JoinColumn(name="client_id")
    private Client client;

    @ManyToOne
    @JoinColumn(name="plan_id", nullable = false)
    private Plan plan;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "listing_plans_id", referencedColumnName = "id")
    private ListingPlan listingPlan;

    @ManyToOne
    @JoinColumn(name="category_id", nullable = false)
    private Category category;

    @OneToMany(mappedBy="listing", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Review> reviews;

    @OneToMany(mappedBy="listing")
    private Set<ListingPhoto> listingPhotos = new HashSet();;

    @OneToMany(mappedBy="listing")
    private Set<ListingVideo> listingVideos = new HashSet();

    @OneToMany(mappedBy="listing", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AttributeValue> attributeValues = new HashSet();

    @OneToMany(mappedBy="listing", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SocialMedia> socialMedia = new HashSet();

    @OneToMany(mappedBy="listing", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Offer> offers;

    @Column(name = "priority", columnDefinition = "integer default 0")
    private int priority;

    @Column(name = "capacity")
    private String capacity;

    @Column(name = "price")
    private String price;

    @Column(name = "cont_of_reviews", columnDefinition = "integer default 0")
    private int contOfReviews;

    @Column(name = "cont_of_photos", columnDefinition = "integer default 0")
    private int contOfPhotos;

    @Column(name = "cont_of_videos", columnDefinition = "integer default 0")
    private int contOfVideo;

    @ManyToMany
    @JoinTable(name = "listing_article",
            joinColumns = @JoinColumn(name = "listing_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "article_id", referencedColumnName = "id"))
    private Set<Article> articles = new HashSet();

    @ManyToMany
    @JoinTable(name = "listing_miniArticle",
            joinColumns = @JoinColumn(name = "listing_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "miniArticle_id", referencedColumnName = "id"))
    private Set<MiniArticle> miniArticles = new HashSet();

    public void setAttributeValues(Set<AttributeValue> attributeValues) {
        if (this.attributeValues != null) {
            this.attributeValues.clear();
        }
        if (attributeValues != null) {
            this.attributeValues.addAll(attributeValues);
        }
    }

    public void setSocialMedia(Set<SocialMedia> socialMedia) {
        if (this.socialMedia != null) {
            this.socialMedia.clear();
        }
        if (socialMedia != null) {
            this.socialMedia.addAll(socialMedia);
        }
    }

    public void setListingPhotos(Set<ListingPhoto> listingPhotos) {
        if (this.listingPhotos != null) {
            this.listingPhotos.clear();
        }
        if (listingPhotos != null) {
            this.listingPhotos.addAll(listingPhotos);
        }
    }
}
