import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoupleContactComponent } from './couple-contact.component';

describe('CoupleContactComponent', () => {
  let component: CoupleContactComponent;
  let fixture: ComponentFixture<CoupleContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoupleContactComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoupleContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
