package amiss.wedding.services.users;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.dto.UserDto;
import amiss.wedding.registration.RegistrationRequest;
import org.springframework.http.ResponseEntity;

import java.security.Principal;
import java.util.List;

public interface UserService {

    public AppUser getUser(Principal principal);
    ResponseEntity<List<UserDto>> findAll();
    ResponseEntity<UserDto> findById(Long id);
    ResponseEntity<UserDto> create(UserDto userDto);
    ResponseEntity<UserDto> update(Long id, UserDto userDto);
    ResponseEntity<String> remove(Long id);
    ResponseEntity activateUserByToken(String token);

    AppUser registerNewUserAccount(RegistrationRequest request);

}
