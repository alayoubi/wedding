package amiss.wedding.email;

import javax.validation.constraints.NotBlank;

public class BasicSettings {

    @NotBlank
    private String baseUrl;

    public String getBaseUrl() {
        return baseUrl.endsWith("/") ? baseUrl.substring(0, baseUrl.length()-1) : baseUrl;
    }

}
