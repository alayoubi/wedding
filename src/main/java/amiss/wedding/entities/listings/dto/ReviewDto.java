package amiss.wedding.entities.listings.dto;

import amiss.wedding.entities.listings.Review;
import lombok.*;

import java.util.Date;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReviewDto {

    private Long id;
    private Long listingId;
    private String title;
    private String content;
    private Date datePost;
    private float rating;
    private Long userId;

    public ReviewDto(Review review) {
        this.id = review.getId();
        this.listingId = review.getListing().getId();
        this.title = review.getTitle();
        this.content = review.getContent();
        this.datePost = review.getDatePost();
        this.rating = review.getRating();
        this.userId = review.getUser().getId();
    }
}
