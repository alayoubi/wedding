import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloristContentComponent } from './florist-content.component';

describe('FloristContentComponent', () => {
  let component: FloristContentComponent;
  let fixture: ComponentFixture<FloristContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloristContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FloristContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
