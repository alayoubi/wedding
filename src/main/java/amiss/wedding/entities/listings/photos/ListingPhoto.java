package amiss.wedding.entities.listings.photos;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.Status;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "listing_photos")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ListingPhoto extends BaseEntity {

    @Column(name = "fileName")
    private String fileName;

    @NotNull
    @NotBlank(message = "Path may not be blank")
    @Column(name = "path")
    private String path;

    @ManyToOne
    @JoinColumn(name="listing_id")
    private Listing listing;

    @Column(name = "status", length = 32, columnDefinition = "varchar(32) default 'pending'")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "size")
    private double size;
}
