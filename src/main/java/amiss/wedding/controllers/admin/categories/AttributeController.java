package amiss.wedding.controllers.admin.categories;


import amiss.wedding.entities.categories.attributes.AttributeType;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;
import amiss.wedding.services.categories.AttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/attributes")
public class AttributeController {

    private final AttributeService attributeService;

    @Autowired
    public AttributeController(AttributeService attributeService) {
        this.attributeService = attributeService;
    }

    @GetMapping("")
    public ResponseEntity<List<AttributeDto>> getAttributes(){
        return this.attributeService.findAll();
    }

    @GetMapping(path = "/{attributeId}")
    public ResponseEntity<AttributeDto> getAttribute(@PathVariable("attributeId") Long attributeId){
        return this.attributeService.findById(attributeId);
    }

    @GetMapping(path = "/category/{category_id}")
    public ResponseEntity<List<AttributeDto>> getAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategory(category_id);
    }

    @GetMapping(path = "/category/{category_id}/main")
    public ResponseEntity<List<AttributeDto>> getMainAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategoryAndAttributeTyp(category_id, AttributeType.MAIN);
    }

    @GetMapping(path = "/category/{category_id}/secondary")
    public ResponseEntity<List<AttributeDto>> getSecondaryAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategoryAndAttributeTyp(category_id, AttributeType.SECONDARY);
    }

    @GetMapping(path = "/category/{category_id}/general")
    public ResponseEntity<List<AttributeDto>> getGeneralAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategoryAndAttributeTyp(category_id, AttributeType.GENERAL);
    }

    @GetMapping(path = "/category/{category_id}/questions")
    public ResponseEntity<List<AttributeDto>> getQuestionsAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategoryAndAttributeTyp(category_id, AttributeType.QUESTIONS);
    }

    @GetMapping(path = "/category/{category_id}/covid")
    public ResponseEntity<List<AttributeDto>> getCovidAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategoryAndAttributeTyp(category_id, AttributeType.COVID);
    }

    @GetMapping(path = "/category/{category_id}/other")
    public ResponseEntity<List<AttributeDto>> getOtherAttributesByCategoryId(@PathVariable("category_id") Long category_id){
        return this.attributeService.findByCategoryAndAttributeTyp(category_id, AttributeType.OTHER);
    }

    @PostMapping("/category/{category_id}")
    public ResponseEntity<AttributeDto> createAttribute(@RequestBody AttributeDto attributeDto,
                                                        @PathVariable("category_id") Long category_id){
        return this.attributeService.create(attributeDto, category_id);
    }

    @PutMapping(path = "/{attributeId}")
    public ResponseEntity<AttributeDto> updateAttribute(@PathVariable Long attributeId,
                                                        @RequestBody AttributeDto attributeDto){
        return this.attributeService.update(attributeId, attributeDto);
    }

    @DeleteMapping(path = "/{attributeId}")
    public ResponseEntity<String> removeAttribute(@PathVariable Long attributeId){
        return this.attributeService.remove(attributeId);
    }

}
