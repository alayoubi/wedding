import { CountryService } from './../../../../../services/city/country.service';
import { ListingService } from './../../../../../services/listing/listing.service';
import { Listing } from './../../../../../models/listing/Listing';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-hero-section',
  templateUrl: './hero-section.component.html',
  styleUrls: ['./hero-section.component.css'],
})
export class HeroSectionComponent implements OnInit {
  @Input() listing:Listing

  constructor(
    private listingService: ListingService,
    private countryService: CountryService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
  }



  public getCountryById(): void {
    this.countryService.getCountry(this.listing.country_id);
  }
}
