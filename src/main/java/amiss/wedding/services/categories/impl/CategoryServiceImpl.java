package amiss.wedding.services.categories.impl;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.dto.CategoryDto;
import amiss.wedding.mappers.categories.CategoryMapper;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.mappers.categories.CategoryMapperImpl;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.services.categories.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    private static final CategoryMapper mapper = new CategoryMapperImpl();

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public ResponseEntity<List<CategoryDto>> findAll() {
        List<Category> categories = this.categoryRepository.findAll();
        List<CategoryDto> categoryDtoList =
                categories.stream().map(mapper::categoryToBasicCategoryDto).collect(Collectors.toList());
        return new ResponseEntity<>(categoryDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CategoryDto> findById(Long id) {
        Optional<Category> category = this.categoryRepository.findById(id);
        if (category.isEmpty()){
            throw new CategoryNotFoundException( id );
        }
        CategoryDto categoryDto = mapper.categoryToCategoryDto(category.get());
        return new ResponseEntity<>(categoryDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CategoryDto> findByName(String name) {
            Category category = categoryRepository.findByName(name);
        CategoryDto categoryDto = mapper.categoryToCategoryDto(category);
        return new ResponseEntity<>(categoryDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CategoryDto> create(CategoryDto categoryDto) {
        final Category category = mapper.categoryDtoToCategory(categoryDto);
        final Category createdCategory = this.categoryRepository.save(category);
        return new ResponseEntity<>(categoryDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<CategoryDto> update(Long id, CategoryDto newCategory) {
        Optional<Category> category = this.categoryRepository.findById(id);
        if (category.isEmpty()){
            throw new CategoryNotFoundException( id );
        }
        Category existingCategory = category.get();
        existingCategory.setName(newCategory.getName());
        existingCategory.setDescription(newCategory.getDescription());
        Category updatedCategory = this.categoryRepository.save(existingCategory);
        CategoryDto categoryDto = mapper.categoryToCategoryDto(updatedCategory);
        return  new ResponseEntity<>(categoryDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.categoryRepository.existsById(id);
        if (!exists){
            throw new CategoryNotFoundException(id);
        }
        this.categoryRepository.deleteById(id);
        return new ResponseEntity<>("Category has been removed successfully!", HttpStatus.NO_CONTENT);
    }
}
