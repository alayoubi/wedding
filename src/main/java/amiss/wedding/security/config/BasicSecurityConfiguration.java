//package amiss.wedding.security.config;
//
//import amiss.wedding.repositories.users.UserRepository;
//import amiss.wedding.security.RestAuthenticationEntryPoint;
//import amiss.wedding.security.repositories.JwtUnAuthResponse;
//import amiss.wedding.security.services.UserDetailsServiceImpl;
//import amiss.wedding.security.utils.AuthFilter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class BasicSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    private final UserRepository userRepository;
//
//    @Autowired
//    private JwtUnAuthResponse jwtUnAuthResponse;
//
//    private final RestAuthenticationEntryPoint authenticationEntryPoint;
//
//    @Value("${jwt.secret}")
//    private String jwtSecret;
//    @Value("${jwt.issuer}")
//    private String jwtIssuer;
//    @Value("${jwt.type}")
//    private String jwtType;
//    @Value("${jwt.audience}")
//    private String jwtAudience;
//    public BasicSecurityConfiguration(UserRepository userRepository,
//                                      RestAuthenticationEntryPoint authenticationEntryPoint) {
//        this.userRepository = userRepository;
//        this.authenticationEntryPoint = authenticationEntryPoint;
//    }
//    @Bean
//    public UserDetailsService userDetailsService() {
//        return new UserDetailsServiceImpl(userRepository);
//    }
//
//    @Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    public final String[] PUBLIC_ENDPOINTS = {
//        "/api/v1/categories/**",
//            "/api/v1/users/**",
//            "/api/v1/countries/**",
//            "/api/v1/regions/**",
//            "/api/v1/cities/**",
//            "/api/v1/districts/**",
//            "/api/v1/roles/**",
//            "/api/v1/attributes/**",
//            "/api/v1/plans/**",
//            "/api/v1/clients/**",
//            "/api/v1/listings/**",
//            "/api/v1/attributeValue/**",
//            "/api/v1/articles/**",
//    };
//
//    String [] PUBLIC_END_POINTS = {"/api/v1/auth/login", "/api/v1/auth/refresh-token", "/api/v1/auth/logout"};
//
//
//
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.cors().and().csrf().disable()
//                .exceptionHandling()
//                .and().httpBasic()
//                .authenticationEntryPoint(jwtUnAuthResponse)
//                .and()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .authorizeRequests()
//                .antMatchers(PUBLIC_END_POINTS).permitAll()
//                .antMatchers("/swagger-ui.html",
//                        "/webjars/springfox-swagger-ui/**",
//                        "/swagger-resources/**",
//                        "/swagger-ui.html/**").permitAll()
//                .antMatchers("/index.html", "/", "/*.css", "/*.js", "/assets/**").permitAll()
//                .antMatchers("/public/**", "/admin/**", "/client/**", "/user/**").permitAll()
//                .antMatchers(HttpMethod.GET, PUBLIC_ENDPOINTS).permitAll()
//                .antMatchers(HttpMethod.POST, "/api/v1/listings/**").permitAll()
//                .antMatchers(HttpMethod.PUT, "/api/v1/listings/**").permitAll()
//                .antMatchers(HttpMethod.DELETE, "/api/v1/listings/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/api/v1/listingPhotos/**").permitAll()
//                .antMatchers(HttpMethod.PUT, "/api/v1/listingPhotos/**").permitAll()
//                .antMatchers(HttpMethod.DELETE, "/api/v1/listingPhotos/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/api/vacancies/{vacancyId}/rating/structure")
//                .hasAnyRole("ADMINISTRATOR", "EMPLOYEE")
//                .antMatchers(HttpMethod.POST, "/api/users/{id}/documents/bundle.tar/token").hasAnyRole("ADMINSTATOR", "EMPLOYEE")
//                .antMatchers(HttpMethod.POST, "/api/documents/{id}/download/token").hasAnyRole("ADMINSTATOR", "EMPLOYEE")
//                .anyRequest().authenticated()
//                .and()
//                .addFilterBefore(authFilter(), UsernamePasswordAuthenticationFilter.class);
//    }
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/v2/api-docs/**");
//        web.ignoring().antMatchers("/swagger.json");
//        web.ignoring().antMatchers("/swagger-ui.html");
//        web.ignoring().antMatchers("/swagger-resources/**");
//        web.ignoring().antMatchers("/webjars/**");
//    }
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
//    }
//
//    @Override
//    @Bean
//    protected AuthenticationManager authenticationManager() throws Exception {
//        // TODO Auto-generated method stub
//        return super.authenticationManager();
//    }
//
//    @Bean
//    public AuthFilter authFilter() {
//        return new AuthFilter();
//    }
//
//}
