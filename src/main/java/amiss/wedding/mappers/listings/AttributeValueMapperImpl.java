package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.dto.AttributeValueDto;

public class AttributeValueMapperImpl implements AttributeValueMapper{


    public AttributeValueMapperImpl() {
    }

    @Override
    public AttributeValue attributeValueDtoToAttributeValue(AttributeValueDto attributeValueDto) {
        AttributeValue attributeValue = new AttributeValue();
        attributeValue.setValue(attributeValueDto.getValue());
        attributeValue.setActive(attributeValueDto.isActive());
        return  attributeValue;
    }

    @Override
    public AttributeValueDto attributeValueToAttributeValueDto(AttributeValue attributeValue) {
        AttributeValueDto attributeDto = new AttributeValueDto(attributeValue);
        return attributeDto;
    }
}
