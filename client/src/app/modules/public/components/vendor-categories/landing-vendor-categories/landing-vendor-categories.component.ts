import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-vendor-categories',
  templateUrl: './landing-vendor-categories.component.html',
  styleUrls: ['./landing-vendor-categories.component.css']
})
export class LandingVendorCategoriesComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('البائعين حسب التصنيف')
   }

  ngOnInit(): void {
  }

}
