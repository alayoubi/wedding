package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class SocialMediaNotFoundException extends ApplicationRuntimeException {

    public SocialMediaNotFoundException(long id){
        super("The social media with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
