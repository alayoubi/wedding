// Bitte die richtige Server Config eintragen;

// const SERVER_BASE = 'https://issamalayoubi.com/api/v1';
 // const SERVER_BASE = 'http://localhost:8080/api/v1';
 const SERVER_BASE = 'http://212.87.214.159:8080/api/v1';

const API_ROUTES = {
  LOGIN: {
    ADMIN: `${SERVER_BASE}/login`,
  },
  REGISTRATION: {
    Base: `${SERVER_BASE}/registration`,
  },
  LOGOUT: {
    BASE: `${SERVER_BASE}/auth/logout`,
  },
  CATEGORIES: {
    BASE: `${SERVER_BASE}/categories`,
    ID: (categoryId: any) => `${SERVER_BASE}/categories/${categoryId}`,
  },
  COUNTRIES: {
    BASE: `${SERVER_BASE}/countries`,
    ID: (countryId: any) => `${SERVER_BASE}/countries/${countryId}`,
  },
  REGIONS: {
    BASE: `${SERVER_BASE}/regions`,
    COUNTRY: {
      ID: (regionId: any) => `${SERVER_BASE}/regions/${regionId}`,
      AKTIVE: (regionId: any) => `${SERVER_BASE}/regions/active/${regionId}`,
    },
  },
  CITIES: {
    BASE: `${SERVER_BASE}/cities`,
    ID: (cityId: any) => `${SERVER_BASE}/cities/${cityId}`,
    REGIONID: (regionId: any) => `${SERVER_BASE}/cities/region/${regionId}`,
  },
  DISTRICTS: {
    BASE: `${SERVER_BASE}/districts`,
    ID: (districtId: any) => `${SERVER_BASE}/districts/${districtId}`,
    REGIONID: (regionId: any) => `${SERVER_BASE}/districts/region/${regionId}`,
    CITYID: (cityId: any) => `${SERVER_BASE}/districts/city/${cityId}`,
  },
  ATTRIBUTES: {
    BASE: `${SERVER_BASE}/attributes`,
    ID: (attributeId: any) => `${SERVER_BASE}/attributes/${attributeId}`,
    POST: (categoryId: any) =>
      `${SERVER_BASE}/attributes/category/${categoryId}`,
    CATEGORY: {
      ID: (countryId: any) => `${SERVER_BASE}/attributes/category/${countryId}`,
      MAIN: (countryId: any) =>
        `${SERVER_BASE}/attributes/category/${countryId}/main`,
      SECONDARY: (countryId: any) =>
        `${SERVER_BASE}/attributes/category/${countryId}/secondary`,
      GENERAL: (countryId: any) =>
        `${SERVER_BASE}/attributes/category/${countryId}/general`,
      QUESTIONS: (countryId: any) =>
        `${SERVER_BASE}/attributes/category/${countryId}/questions`,
      COVID: (countryId: any) =>
        `${SERVER_BASE}/attributes/category/${countryId}/covid`,
      OTHER: (countryId: any) =>
        `${SERVER_BASE}/attributes/category/${countryId}/other`,
    },
  },
  LISTINGS: {
    BASE: `${SERVER_BASE}/listings`,
    ID: (listingId: any) => `${SERVER_BASE}/listings/${listingId}`,
    CATIDANDREGID: (catID: any, regID: any) =>
      `${SERVER_BASE}/listings/category/${catID}/region/${regID}`,
    DATACATID: (categoryId: any) =>
      `${SERVER_BASE}/listings/category/${categoryId}`,
    DATAREGID: (regionId: any) => `${SERVER_BASE}/listings/region/${regionId}`,
    UPDATELISTING: (listingId: any) => `${SERVER_BASE}/listings/${listingId}`,
    DELETELISTING: (listingId: any) => `${SERVER_BASE}/listings/${listingId}`,
  },
  ARTICLES: {
    BASE: `${SERVER_BASE}/articles`,
    ID: (articleId: any) => `${SERVER_BASE}/articles/${articleId}`,
    DELETEAARTICLE: (articleId: any) => `${SERVER_BASE}/articles/${articleId}`,
    ADDMINIARTICLE: `${SERVER_BASE}/admin/mini_articles`,
  },
  LISTINGSPHOTO: {
    BASE: `${SERVER_BASE}/listingPhotos`,
    LISTING: {
      ID: (listingId: any) =>
        `${SERVER_BASE}/listingPhotos/listing/${listingId}`,
    },
    DELETELISTING: (listingPhotoId: any) =>
      `${SERVER_BASE}/listingPhotos/listing/${listingPhotoId}`,
  },
};

export const API = API_ROUTES;
