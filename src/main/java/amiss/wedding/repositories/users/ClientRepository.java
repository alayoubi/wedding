package amiss.wedding.repositories.users;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.client.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findByUser(AppUser user);
}
