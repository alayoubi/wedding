package amiss.wedding.services.users.impl;


import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.client.Client;
import amiss.wedding.entities.users.client.dto.ClientDto;
import amiss.wedding.errors.ClientNotFoundException;
import amiss.wedding.errors.UserNotFoundException;
import amiss.wedding.mappers.users.ClientMapper;
import amiss.wedding.mappers.users.ClientMapperImpl;
import amiss.wedding.repositories.users.ClientRepository;
import amiss.wedding.repositories.users.UserRepository;
import amiss.wedding.services.users.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    private final UserRepository userRepository;
    private static final ClientMapper mapper = new ClientMapperImpl();

    public ClientServiceImpl(ClientRepository clientRepository, UserRepository userRepository) {
        this.clientRepository = clientRepository;
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<List<ClientDto>> findAll() {
        List<Client> clients = this.clientRepository.findAll();
        List<ClientDto> clientDtoList =
                clients.stream().map(mapper::clientToClientDto).collect(Collectors.toList());
        return new ResponseEntity<>(clientDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ClientDto> findById(Long id) {
        Optional<Client> client = this.clientRepository.findById(id);
        if (client.isEmpty()){
            throw new ClientNotFoundException(id);
        }
        ClientDto clientDto = mapper.clientToClientDto(client.get());
        return new ResponseEntity<>(clientDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ClientDto> findByUser(Long id) {
        Optional<AppUser> user = this.userRepository.findById(id);
        if (user.isEmpty()){
            throw new UserNotFoundException(id);
        }
        AppUser existingUser = user.get();

        Client client = this.clientRepository.findByUser(existingUser);
        ClientDto clientDto = mapper.clientToClientDto(client);
        return new ResponseEntity<>(clientDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ClientDto> create(ClientDto clientDto) {
        Optional<AppUser> user = this.userRepository.findById(clientDto.getUserId());
        if (user.isEmpty()){
            throw new UserNotFoundException(clientDto.getUserId());
        }
        final AppUser existingUser = user.get();
        Client client = mapper.clientDtoToClient(clientDto);
        client.setUser(existingUser);
        final Client createdClient = this.clientRepository.save(client);
        return new ResponseEntity<>(clientDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<ClientDto> update(Long id, ClientDto newClientDto) {
        Optional<Client> client = this.clientRepository.findById(id);
        if (client.isEmpty()){
            throw new ClientNotFoundException(id);
        }
        Client existingClient = client.get();
        AppUser user = userRepository.findById(newClientDto.getUserId()).orElseThrow(() ->
                new UserNotFoundException(newClientDto.getUserId()));
        existingClient.setUser(user);
        existingClient.setName(newClientDto.getName());
        existingClient.setAddress(newClientDto.getAddress());
        existingClient.setMobile(newClientDto.getMobile());
        existingClient.setPhoto(newClientDto.getPhoto());
        Client updatedClient = this.clientRepository.save(existingClient);
        ClientDto clientDto = mapper.clientToClientDto(updatedClient);
        return new ResponseEntity<>(clientDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.clientRepository.existsById(id);
        if (!exists){
            throw new ClientNotFoundException(id);
        }
        this.clientRepository.deleteById(id);
        return new ResponseEntity<>("Client has ben removed successfully!", HttpStatus.NO_CONTENT);
    }
}
