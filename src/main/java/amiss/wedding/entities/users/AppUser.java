package amiss.wedding.entities.users;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Review;
import amiss.wedding.entities.users.client.Client;
import amiss.wedding.security.token.Token;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@Builder
@AllArgsConstructor
public class AppUser extends BaseEntity implements UserDetails {

    private String firstname;

    private String lastname;

    @JsonIgnore
    @Column(nullable = false)
    @ApiModelProperty(notes = "The person's encrypted password")
    public String password;

    @Column(name = "email", length = 100, nullable = false, unique = true)
    @Size(max = 100, message = "Email may not be more than 100 characters")
    @ApiModelProperty(notes = "The person's email")
    private String email;

    @Column(name = "active", columnDefinition = "boolean  default false")
    private boolean active;

    @Column(name = "enabled", columnDefinition = "boolean  default false")
    private boolean locked;


    @Column(name = "email_verified_at")
    private Instant verified_at;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @OneToMany(mappedBy="user")
    private Set<Review> reviews;

    @ApiModelProperty(notes = "The person's roles")
    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "user")
    private List<Token> tokens;

    public AppUser(){ }

    public AppUser(String email) {
        this.email = email;
        this.active = false;
    }

    public AppUser(String password, String email, Role role) {
        this.password = password;
        this.email = email;
        this.role = role;
        this.active = false;
    }

    public AppUser(String firstname, String lastname, String password, String email, Role role) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.email = email;
        this.role = role;
        this.active = false;
    }

    public void addRole(Role role){

        this.role = role;
    }


    /**
     * @return
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return role.getAuthorities();
    }

    /**
     * @return
     */
    @Override
    public String getUsername() {
        return getEmail();
    }

    /**
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * @return
     */
    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    /**
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * @return
     */
    @Override
    public boolean isEnabled() {
        return isActive();
    }
}
