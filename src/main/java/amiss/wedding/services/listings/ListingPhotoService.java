package amiss.wedding.services.listings;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface ListingPhotoService {

    ResponseEntity<List<ListingPhotoDto>> findAll();
    ResponseEntity<ListingPhotoDto> findById(Long id);
    ResponseEntity<List<ListingPhotoDto>> findAllByListingId(Long listingId);
    ResponseEntity<ListingPhotoDto> create(MultipartFile multipartFile, Long listingId);
    ResponseEntity<ListingDto> uploadsPhotos(Long listingId, List<MultipartFile> photos);
    ResponseEntity<ListingPhotoDto> update(Long id, MultipartFile multipartFile);
    ResponseEntity<String> remove(Long id);
    ListingPhoto addListingPhoto(ListingPhotoDto listingPhotoDto, Listing listing);
    Set<ListingPhoto> addListOfListingPhoto(Set<ListingPhotoDto> listingPhotoDtoSet, Listing listing);
    ResponseEntity<ListingDto> addCoverImg(Long listingId, ListingPhotoDto listingPhotoDto);

    ResponseEntity<ListingDto> addListingPhotos(List<ListingPhotoDto> listingPhotoDtolist, Long listingId);

}
