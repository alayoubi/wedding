package amiss.wedding.storage;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StorageFileDescription {

    private String fileName;
    private String storageRelativePath;
    private long size;

}
