package amiss.wedding.services.categories.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.attributes.AttributeType;
import amiss.wedding.entities.categories.attributes.ValueType;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanType;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Position;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.client.Client;
import amiss.wedding.errors.AttributeNotFoundException;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.categories.attribute.AttributeRepository;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {AttributeServiceImpl.class})
@ExtendWith(SpringExtension.class)
class AttributeServiceImplTest {
    @MockBean
    private AttributeRepository attributeRepository;

    @Autowired
    private AttributeServiceImpl attributeServiceImpl;

    @MockBean
    private CategoryRepository categoryRepository;

    /**
     * Method under test: {@link AttributeServiceImpl#findAll()}
     */
    @Test
    void testFindAll() {
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findAll()).thenReturn(attributeList);
        ResponseEntity<List<AttributeDto>> actualFindAllResult = attributeServiceImpl.findAll();
        assertEquals(attributeList, actualFindAllResult.getBody());
        assertEquals(HttpStatus.OK, actualFindAllResult.getStatusCode());
        assertTrue(actualFindAllResult.getHeaders().isEmpty());
        verify(attributeRepository).findAll();
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findAll()}
     */
    @Test
    void testFindAll2() {
        when(attributeRepository.findAll()).thenThrow(new AttributeNotFoundException(1L));
        assertThrows(AttributeNotFoundException.class, () -> attributeServiceImpl.findAll());
        verify(attributeRepository).findAll();
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findById(Long)}
     */
    @Test
    void testFindById() {
        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Attribute attribute = new Attribute();
        attribute.setActive(true);
        attribute.setAttributeType(AttributeType.MAIN);
        attribute.setAttributeValues(new HashSet<>());
        attribute.setCategory(category);
        attribute.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setId(1L);
        attribute.setName("Name");
        attribute.setUnit("Unit");
        attribute.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setValueType(ValueType.TEXT);
        attribute.setVersion(1);
        Optional<Attribute> ofResult = Optional.of(attribute);
        when(attributeRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<AttributeDto> actualFindByIdResult = attributeServiceImpl.findById(1L);
        assertTrue(actualFindByIdResult.hasBody());
        assertTrue(actualFindByIdResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualFindByIdResult.getStatusCode());
        AttributeDto body = actualFindByIdResult.getBody();
        assertEquals("Name", body.getName());
        assertEquals(1L, body.getId().longValue());
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals(AttributeType.MAIN, body.getAttributeType());
        assertTrue(body.isActive());
        assertEquals("Unit", body.getUnit());
        assertEquals(ValueType.TEXT, body.getValueType());
        verify(attributeRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#create(AttributeDto, Long)}
     */
    @Test
    void testCreate() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.attributes.Attribute.getId()" because "attribute" is null
        //       at amiss.wedding.entities.categories.attributes.dto.AttributeDto.<init>(AttributeDto.java:27)
        //       at amiss.wedding.mappers.categories.AttributeMapperImpl.attributeToAttributeDto(AttributeMapperImpl.java:26)
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.create(AttributeServiceImpl.java:65)
        //   See https://diff.blue/R013 to resolve this issue.

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(new ListingPhoto());
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(new Category());
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        City city = new City();
        city.setCountry(new Country());
        city.setDistricts(new HashSet<>());
        city.setId(1L);
        city.setListing(new HashSet<>());
        city.setName("Name");
        city.setName_en("Name en");
        city.setRegion(new Region());

        Client client = new Client();
        client.setAddress("42 Main St");
        client.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setId(1L);
        client.setListing(new HashSet<>());
        client.setMobile("Mobile");
        client.setName("Name");
        client.setPhoto("alice.liddell@example.org");
        client.setRole("Role");
        client.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setUser(new AppUser());
        client.setVersion(1);

        Country country = new Country();
        country.setCapital_city(new City());
        country.setCode("Code");
        country.setId(1L);
        country.setName("Name");
        country.setName_en("Name en");
        country.setRegions(new HashSet<>());

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        District district = new District();
        district.setCity(new City());
        district.setId(1L);
        district.setListing(new HashSet<>());
        district.setName("Name");
        district.setName_en("Name en");
        district.setRegion(new Region());

        Plan plan = new Plan();
        plan.setCategory(new Category());
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Position position = new Position();
        position.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setId(1L);
        position.setLatitude(1L);
        position.setListing(new Listing());
        position.setLongitude(1L);
        position.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setVersion(1);

        Region region = new Region();
        region.setActive(true);
        region.setCapital_city(new City());
        region.setCities(new HashSet<>());
        region.setCode("Code");
        region.setCountry(new Country());
        region.setId(1L);
        region.setListing(new HashSet<>());
        region.setName("Name");
        region.setName_en("Name en");

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(category);
        listing.setCity(city);
        listing.setClient(client);
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(country);
        listing.setCoverImg(coverImg);
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(district);
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(plan);
        listing.setPosition(position);
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(region);
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(listing);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing2);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg4);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(coverImg3);
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(parentCategory2);
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(coverImg2);
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(parentCategory3);
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Attribute attribute = new Attribute();
        attribute.setActive(true);
        attribute.setAttributeType(AttributeType.MAIN);
        attribute.setAttributeValues(new HashSet<>());
        attribute.setCategory(category2);
        attribute.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setId(1L);
        attribute.setName("Name");
        attribute.setUnit("Unit");
        attribute.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setValueType(ValueType.TEXT);
        attribute.setVersion(1);
        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        when(attributeRepository.save(Mockito.<Attribute>any())).thenReturn(attribute);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        AttributeServiceImpl attributeServiceImpl = new AttributeServiceImpl(attributeRepository, categoryRepository);
        ResponseEntity<AttributeDto> actualCreateResult = attributeServiceImpl.create(new AttributeDto(), 1L);
        assertTrue(actualCreateResult.hasBody());
        assertTrue(actualCreateResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.CREATED, actualCreateResult.getStatusCode());
        AttributeDto body = actualCreateResult.getBody();
        assertEquals("Name", body.getName());
        assertEquals(1L, body.getId().longValue());
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals(AttributeType.MAIN, body.getAttributeType());
        assertTrue(body.isActive());
        assertEquals("Unit", body.getUnit());
        assertEquals(ValueType.TEXT, body.getValueType());
        verify(attributeRepository).save(Mockito.<Attribute>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#create(AttributeDto, Long)}
     */
    @Test
    void testCreate2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.attributes.Attribute.getId()" because "attribute" is null
        //       at amiss.wedding.entities.categories.attributes.dto.AttributeDto.<init>(AttributeDto.java:27)
        //       at amiss.wedding.mappers.categories.AttributeMapperImpl.attributeToAttributeDto(AttributeMapperImpl.java:26)
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.create(AttributeServiceImpl.java:65)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        when(attributeRepository.save(Mockito.<Attribute>any())).thenThrow(new AttributeNotFoundException(1L));
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        AttributeServiceImpl attributeServiceImpl = new AttributeServiceImpl(attributeRepository, categoryRepository);
        assertThrows(AttributeNotFoundException.class, () -> attributeServiceImpl.create(new AttributeDto(), 1L));
        verify(attributeRepository).save(Mockito.<Attribute>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#update(Long, AttributeDto)}
     */
    @Test
    void testUpdate() {
        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Attribute attribute = new Attribute();
        attribute.setActive(true);
        attribute.setAttributeType(AttributeType.MAIN);
        attribute.setAttributeValues(new HashSet<>());
        attribute.setCategory(category);
        attribute.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setId(1L);
        attribute.setName("Name");
        attribute.setUnit("Unit");
        attribute.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        attribute.setValueType(ValueType.TEXT);
        attribute.setVersion(1);
        Optional<Attribute> ofResult = Optional.of(attribute);
        when(attributeRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        when(categoryRepository.findById(Mockito.<Long>any())).thenThrow(new AttributeNotFoundException(1L));
        assertThrows(AttributeNotFoundException.class, () -> attributeServiceImpl.update(1L, new AttributeDto()));
        verify(attributeRepository).findById(Mockito.<Long>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#remove(Long)}
     */
    @Test
    void testRemove() {
        doNothing().when(attributeRepository).deleteById(Mockito.<Long>any());
        when(attributeRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        ResponseEntity<String> actualRemoveResult = attributeServiceImpl.remove(1L);
        assertEquals("Attribute has been removed successfully!", actualRemoveResult.getBody());
        assertEquals(HttpStatus.NO_CONTENT, actualRemoveResult.getStatusCode());
        assertTrue(actualRemoveResult.getHeaders().isEmpty());
        verify(attributeRepository).existsById(Mockito.<Long>any());
        verify(attributeRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#remove(Long)}
     */
    @Test
    void testRemove2() {
        doThrow(new AttributeNotFoundException(1L)).when(attributeRepository).deleteById(Mockito.<Long>any());
        when(attributeRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        assertThrows(AttributeNotFoundException.class, () -> attributeServiceImpl.remove(1L));
        verify(attributeRepository).existsById(Mockito.<Long>any());
        verify(attributeRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#remove(Long)}
     */
    @Test
    void testRemove3() {
        when(attributeRepository.existsById(Mockito.<Long>any())).thenReturn(false);
        assertThrows(AttributeNotFoundException.class, () -> attributeServiceImpl.remove(1L));
        verify(attributeRepository).existsById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategory(Long)}
     */
    @Test
    void testFindByCategory() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategory(AttributeServiceImpl.java:104)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategory(Mockito.<Category>any())).thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryResult = (new AttributeServiceImpl(attributeRepository,
                categoryRepository)).findByCategory(1L);
        assertEquals(attributeList, actualFindByCategoryResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryResult.getStatusCode());
        assertTrue(actualFindByCategoryResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategory(Mockito.<Category>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategory(Long)}
     */
    @Test
    void testFindByCategory2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategory(AttributeServiceImpl.java:104)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        when(attributeRepository.findByCategory(Mockito.<Category>any())).thenThrow(new AttributeNotFoundException(1L));
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        assertThrows(AttributeNotFoundException.class,
                () -> (new AttributeServiceImpl(attributeRepository, categoryRepository)).findByCategory(1L));
        verify(attributeRepository).findByCategory(Mockito.<Category>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategory(Long)}
     */
    @Test
    void testFindByCategory3() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategory(AttributeServiceImpl.java:104)
        //   See https://diff.blue/R013 to resolve this issue.

        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> emptyResult = Optional.empty();
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);
        assertThrows(CategoryNotFoundException.class,
                () -> (new AttributeServiceImpl(mock(AttributeRepository.class), categoryRepository)).findByCategory(1L));
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryAndAttributeTypResult = (new AttributeServiceImpl(
                attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L, AttributeType.MAIN);
        assertEquals(attributeList, actualFindByCategoryAndAttributeTypResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryAndAttributeTypResult.getStatusCode());
        assertTrue(actualFindByCategoryAndAttributeTypResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenThrow(new AttributeNotFoundException(1L));
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        assertThrows(AttributeNotFoundException.class,
                () -> (new AttributeServiceImpl(attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L,
                        AttributeType.MAIN));
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp3() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> emptyResult = Optional.empty();
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);
        assertThrows(CategoryNotFoundException.class,
                () -> (new AttributeServiceImpl(mock(AttributeRepository.class), categoryRepository))
                        .findByCategoryAndAttributeTyp(1L, AttributeType.MAIN));
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp4() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryAndAttributeTypResult = (new AttributeServiceImpl(
                attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L, AttributeType.SECONDARY);
        assertEquals(attributeList, actualFindByCategoryAndAttributeTypResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryAndAttributeTypResult.getStatusCode());
        assertTrue(actualFindByCategoryAndAttributeTypResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp5() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryAndAttributeTypResult = (new AttributeServiceImpl(
                attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L, AttributeType.GENERAL);
        assertEquals(attributeList, actualFindByCategoryAndAttributeTypResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryAndAttributeTypResult.getStatusCode());
        assertTrue(actualFindByCategoryAndAttributeTypResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp6() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryAndAttributeTypResult = (new AttributeServiceImpl(
                attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L, AttributeType.QUESTIONS);
        assertEquals(attributeList, actualFindByCategoryAndAttributeTypResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryAndAttributeTypResult.getStatusCode());
        assertTrue(actualFindByCategoryAndAttributeTypResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp7() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryAndAttributeTypResult = (new AttributeServiceImpl(
                attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L, AttributeType.COVID);
        assertEquals(attributeList, actualFindByCategoryAndAttributeTypResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryAndAttributeTypResult.getStatusCode());
        assertTrue(actualFindByCategoryAndAttributeTypResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link AttributeServiceImpl#findByCategoryAndAttributeTyp(Long, AttributeType)}
     */
    @Test
    void testFindByCategoryAndAttributeTyp8() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.AttributeServiceImpl.findByCategoryAndAttributeTyp(AttributeServiceImpl.java:126)
        //   See https://diff.blue/R013 to resolve this issue.

        AttributeRepository attributeRepository = mock(AttributeRepository.class);
        ArrayList<Attribute> attributeList = new ArrayList<>();
        when(attributeRepository.findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any()))
                .thenReturn(attributeList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<AttributeDto>> actualFindByCategoryAndAttributeTypResult = (new AttributeServiceImpl(
                attributeRepository, categoryRepository)).findByCategoryAndAttributeTyp(1L, AttributeType.OTHER);
        assertEquals(attributeList, actualFindByCategoryAndAttributeTypResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryAndAttributeTypResult.getStatusCode());
        assertTrue(actualFindByCategoryAndAttributeTypResult.getHeaders().isEmpty());
        verify(attributeRepository).findByCategoryAndAttributeType(Mockito.<Category>any(), Mockito.<AttributeType>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }
}

