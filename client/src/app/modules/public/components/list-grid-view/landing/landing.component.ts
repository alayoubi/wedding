import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ListingService } from '@core/services/listing/listing.service';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements OnInit {
  public titleStr :any
  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router,
    private title: Title
  ) {
    this.route.params.subscribe((params) => {
      this.title.setTitle(`${params['category']} في  ${params['region']}`)
      this.titleStr = title
      console.log(this.titleStr)
    });
  }

  ngOnInit(): void {

  }


  }


