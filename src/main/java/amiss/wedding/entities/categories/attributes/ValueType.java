package amiss.wedding.entities.categories.attributes;

public enum ValueType {
    TEXT, INTEGER, DOUBLE, BOOLEAN
}
