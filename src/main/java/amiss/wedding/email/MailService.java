package amiss.wedding.email;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.VerificationToken;
import amiss.wedding.errors.UserNotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class MailService {

    private final static Logger LOGGER = LoggerFactory
            .getLogger(EmailService.class);
    private final JavaMailSender mailSender;

    private final SpringTemplateEngine thymeleafTemplateEngine;

    @Async
    public void sendVerification(String emailTo, VerificationToken verifyToken) {
        Map<String, Object> variables = new HashMap<>();
        String content = "<!DOCTYPE html>\n" +
                "<html xmlns:th=\"http://www.thymeleaf.org\">\n" +
                "<head>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "</head>\n" +
                "<body>\n" +
                "<p>يرجى التحقق من هذا البريد الإلكتروني باستخدام الرابط التالي:</p>\n" +
                "<h3><a href=\"[[URL]]\" target=\"_self\">تفعيل</a></h3>\n" +
                "</body>\n" +
                "</html>";

        @NotBlank final String baseUrl =
                ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();

        variables.put("url", baseUrl + "/api/v1/registration/confirm/" + verifyToken.getToken());

        String url = baseUrl + "/api/v1/registration/confirm/" + verifyToken.getToken();
        content = content.replace("[[URL]]", url);;
        sendHtmlMessage(emailTo, "Email Verification", content);
//        sendMessage(emailTo, "Email Verification", variables, Template.VERIFICATION);
    }


    @Async
    public void sendMessage(String to,
                            String subject,
                            Map<String, Object> variables,
                            Template template) {
        Context thymeleafContext = new Context();
        thymeleafContext.setVariables(variables);
        String htmlBody;
        htmlBody = thymeleafTemplateEngine.process(String.valueOf(template), thymeleafContext);
        sendHtmlMessage(to, subject, htmlBody);
    }

    public void sendHtmlMessage(String to, String subject, String htmlBody) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper =
                    new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(htmlBody, true);
            helper.setFrom("wedding.lilty@gmail.com");
            mailSender.send(message);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }

    @Async
    public void send(String to, String email) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper =
                    new MimeMessageHelper(mimeMessage, "utf-8");
            helper.setText(email, true);
            helper.setTo(to);
            helper.setSubject("Confirm your email");
            helper.setFrom("wedding.lilty@gmail.com");
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            LOGGER.error("failed to send email", e);
            throw new IllegalStateException("failed to send email");
        }
    }
}
