package amiss.wedding.entities.cities;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "countries")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the contact")
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", length = 100)
    @Size(max = 100, message = "Name may not be more than 100 characters")
    private String name;
    @NotNull
    @Column(name = "name_en", length = 64)
    private String name_en;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "capital_city", referencedColumnName = "id")
    private City capital_city;
    @NotNull
    @Column(name = "code", length = 5)
    private String code;

    @OneToMany(mappedBy="country")
    private Set<Region> regions;

    public Country (String name, String name_en, String code) {
        this.name = name;
        this.name_en = name_en;
        this.code = code;
    }
}
