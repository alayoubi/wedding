package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.plans.ListingPlan;
import amiss.wedding.entities.listings.plans.ListingPlanDto;

public interface ListingPlanMapper {

    ListingPlan listingPlanDtoToListingPlan(ListingPlanDto listingPlanDto);
    ListingPlanDto listingPlanToListingPlanDto(ListingPlan listingPlan);
}
