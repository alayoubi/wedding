package amiss.wedding.entities.design;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.users.AppUser;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "experiences")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Experience extends BaseEntity {

    @NotNull
    @NotBlank(message = "Title may not be blank")
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @Column(name = "contents", nullable=false)
    private String content;

    @Column(name = "datePosts", nullable=false)
    private Date datePost;

    @ManyToOne
    @JoinColumn(name="city_id", nullable=false)
    private City city;

    @Column(name = "ratings", nullable=false)
    private float rating;

    @OneToMany()
    private Set<DesignPhoto> designPhotos;

    @ManyToOne()
    @JoinColumn(name="user_id")
    private AppUser user;
}
