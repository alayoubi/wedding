package amiss.wedding.controllers.admin.listings;


import amiss.wedding.entities.listings.dto.SocialMediaDto;
import amiss.wedding.services.listings.SocialMediaService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/admin/listings/social media/")
@AllArgsConstructor
public class AdminSocialMediaController {

    private final SocialMediaService socialMediaService;

    @GetMapping("")
    public ResponseEntity<List<SocialMediaDto>> findAll() {
        return this.socialMediaService.findAll();
    }

    @GetMapping(path = "{social_media_id}")
    public ResponseEntity<SocialMediaDto> findById(@PathVariable("social_media_id") Long social_media_id) {
        return this.socialMediaService.findById(social_media_id);
    }

    @GetMapping("listings/{listing_id}")
    public ResponseEntity<List<SocialMediaDto>> findAllByListingId(@PathVariable("listing_id") Long listing_id) {
        return this.socialMediaService.findAllByListingId(listing_id);
    }

    @PostMapping("{listing_id}")
    public  ResponseEntity<SocialMediaDto> create(@RequestBody SocialMediaDto socialMediaDto,
                                                             @PathVariable("listing_id") Long listing_id) {
        return this.socialMediaService.create(socialMediaDto, listing_id);
    }

    @PutMapping(path = "{social_media_id}")
    public ResponseEntity<SocialMediaDto> updateSocialMedia(@PathVariable("social_media_id") Long socialMedia_id,
                                                            @RequestBody SocialMediaDto socialMediaDto){
        return this.socialMediaService.update(socialMedia_id, socialMediaDto);
    }

    @DeleteMapping(path = "social media/{social_media_id}")
    public ResponseEntity<String> removeSocialMedia(@PathVariable("social_media_id") Long socialMedia_id) {
        return this.socialMediaService.remove(socialMedia_id);
    }
}
