package amiss.wedding.controllers.admin.articles;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.services.articles.ArticleService;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashSet;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {AdminArticleController.class})
@ExtendWith(SpringExtension.class)
class AdminArticleControllerTest {
    @Autowired
    private AdminArticleController adminArticleController;

    @MockBean
    private ArticleService articleService;

    /**
     * Method under test: {@link AdminArticleController#createArticle(ArticleDto)}
     */
    @Test
    void testCreateArticle() throws Exception {
        when(articleService.findAll()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));

        ArticleDto articleDto = new ArticleDto();
        articleDto.setArticleCategories(new HashSet<>());
        articleDto.setArticleCategories_IDs(new HashSet<>());
        articleDto.setArticlePhotos(new HashSet<>());
        articleDto.setArticlePhotos_ID(new HashSet<>());
        articleDto.setCategories(new HashSet<>());
        articleDto.setCategory_IDs(new HashSet<>());
        articleDto.setContent("Not all who wander are lost");
        articleDto.setCoverImg("Cover Img");
        articleDto.setCreat_at(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        articleDto.setId(1L);
        articleDto.setListings(new HashSet<>());
        articleDto.setListings_IDs(new HashSet<>());
        articleDto.setMiniArticles(new HashSet<>());
        articleDto.setMiniArticles_ID(new HashSet<>());
        articleDto.setReadTime(1);
        articleDto.setTitle("Dr");
        String content = (new ObjectMapper()).writeValueAsString(articleDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/admin/articles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link AdminArticleController#getArticles()}
     */
    @Test
    void testGetArticles() throws Exception {
        when(articleService.findAll()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/admin/articles");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link AdminArticleController#getArticlesByCategory(Long)}
     */
    @Test
    void testGetArticlesByCategory() throws Exception {
        when(articleService.findAllByCategory(Mockito.<Long>any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .get("/api/v1/admin/articles/category/{category_id}", 1L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link AdminArticleController#getListing(Long)}
     */
    @Test
    void testGetListing() throws Exception {
        when(articleService.findById(anyLong())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/admin/articles/{article_id}",
                1L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link AdminArticleController#getListing(Long)}
     */
    @Test
    void testGetListing2() throws Exception {
        when(articleService.findAll()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        when(articleService.findById(anyLong())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/admin/articles/{article_id}",
                "", "Uri Variables");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link AdminArticleController#removeArticle(Long)}
     */
    @Test
    void testRemoveArticle() throws Exception {
        when(articleService.remove(Mockito.<Long>any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .delete("/api/v1/admin/articles/{article_id}", 1L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link AdminArticleController#updateArticle(Long, ArticleDto)}
     */
    @Test
    void testUpdateArticle() throws Exception {
        when(articleService.update(Mockito.<Long>any(), Mockito.<ArticleDto>any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));

        ArticleDto articleDto = new ArticleDto();
        articleDto.setArticleCategories(new HashSet<>());
        articleDto.setArticleCategories_IDs(new HashSet<>());
        articleDto.setArticlePhotos(new HashSet<>());
        articleDto.setArticlePhotos_ID(new HashSet<>());
        articleDto.setCategories(new HashSet<>());
        articleDto.setCategory_IDs(new HashSet<>());
        articleDto.setContent("Not all who wander are lost");
        articleDto.setCoverImg("Cover Img");
        articleDto.setCreat_at(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()));
        articleDto.setId(1L);
        articleDto.setListings(new HashSet<>());
        articleDto.setListings_IDs(new HashSet<>());
        articleDto.setMiniArticles(new HashSet<>());
        articleDto.setMiniArticles_ID(new HashSet<>());
        articleDto.setReadTime(1);
        articleDto.setTitle("Dr");
        String content = (new ObjectMapper()).writeValueAsString(articleDto);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .put("/api/v1/admin/articles/{article_id}", 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(adminArticleController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }
}

