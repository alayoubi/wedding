package amiss.wedding.entities.categories;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.categories.plans.Plan;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Category extends BaseEntity {

    @NotNull
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", length = 100)
    @Size(max = 100, message = "Name may not be more than 100 characters")
    private String name;

    @NotNull
    @NotBlank(message = "Description may not be blank")
    @Column(name = "description")
    private String description;

    @Column(name = "active", columnDefinition = "boolean  default false")
    private boolean active;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cover_id", referencedColumnName = "id")
    private ListingPhoto coverImg;

    @OneToMany(mappedBy="category")
    private Set<Plan> plan = new HashSet();

    @OneToMany(mappedBy="category")
    private Set<Attribute> attributes = new HashSet();

    @OneToMany(mappedBy = "category")
    private Set<Listing> listings = new HashSet();

    @ManyToMany(mappedBy = "categories")
    @JsonIgnore
    private Set<Article> articles = new HashSet();

    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Category parentCategory;

    public Category(String name, String description, Category parentCategory){
        this.name = name;
        this.description = description;
        this.parentCategory = parentCategory;
    }
}
