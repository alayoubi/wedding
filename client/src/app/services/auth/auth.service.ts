import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '@core/config/api.config';
import { Register } from '@core/models/auth/auth';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  register(register: any): Observable<Register> {
    return this.http.post<Register>(API.REGISTRATION.Base,register)
  }
  logout() {
    return this.http.post(API.LOGOUT.BASE,{});
  }
}
