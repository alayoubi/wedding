import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographersContentComponent } from './photographers-content.component';

describe('PhotographersContentComponent', () => {
  let component: PhotographersContentComponent;
  let fixture: ComponentFixture<PhotographersContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotographersContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PhotographersContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
