package amiss.wedding.services.articles.impl;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.MiniArticleDto;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.errors.ArticleNotFoundException;
import amiss.wedding.errors.ListingNotFoundException;
import amiss.wedding.errors.MiniArticleNotFoundException;
import amiss.wedding.mappers.articles.ArticleMapper;
import amiss.wedding.mappers.articles.impl.ArticleMapperImpl;
import amiss.wedding.repositories.articles.ArticleRepository;
import amiss.wedding.repositories.articles.MiniArticleRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.articles.ArticlePhotoService;
import amiss.wedding.services.articles.MiniArticleService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MiniArticleServiceImpl implements MiniArticleService {

    private final ListingsRepository listingsRepository;
    private final ArticleRepository articleRepository;
    private final MiniArticleRepository miniArticleRepository;
    private final ArticleMapper mapper = new ArticleMapperImpl();
    private final ArticlePhotoService articlePhotoService;

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<MiniArticleDto>> findAll() {
        List<MiniArticle> miniArticleDtos = this.miniArticleRepository.findAll();
        List<MiniArticleDto> miniArticleDtoList =
                miniArticleDtos.stream().map(mapper::miniArticleToMiniArticleDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(miniArticleDtoList, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<MiniArticleDto> findById(long id) {
        Optional<MiniArticle> miniArticle = this.miniArticleRepository.findById(id);
        if (miniArticle.isEmpty()) {
            throw new ArticleNotFoundException(id);
        }
        MiniArticleDto miniArticleDto = mapper.miniArticleToMiniArticleDto(miniArticle.get());
        return new ResponseEntity<>(miniArticleDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<List<MiniArticleDto>> findAllByArticle(Long id) {
        Article article = this.articleRepository.findById(id).orElseThrow(() ->
                new MiniArticleNotFoundException(id));

        List<MiniArticle> miniArticleDtos = this.miniArticleRepository.findAllByArticle(article);

        List<MiniArticleDto> miniArticleDtoList =
                miniArticleDtos.stream().map(mapper::miniArticleToMiniArticleDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(miniArticleDtoList, HttpStatus.OK);
    }

    /**
     * @param articleDto
     * @return
     */
    @Override
    public ResponseEntity<MiniArticleDto> createStartMiniArticle(MiniArticleDto articleDto) {
        MiniArticle miniArticle = mapper.miniArticleDtoToMiniArticle(articleDto);
        MiniArticle savedMiniArticle =  miniArticleRepository.save(miniArticle);
        return new ResponseEntity<>(mapper.miniArticleToMiniArticleDto(savedMiniArticle),
                HttpStatus.OK);
    }

    /**
     * @param articleDto
     * @return
     */
    @Override
    public ResponseEntity<MiniArticleDto> create(MiniArticleDto articleDto) {
        Set<Listing> listings = new HashSet<>();
        for (Long id: articleDto.getListings_ID()) {
            Optional<Listing> listing = this.listingsRepository.findById(id);
            if (listing.isEmpty()){
                throw new ListingNotFoundException(id);
            }
            listings.add(listing.get());
        }
        final MiniArticle miniArticle = mapper.miniArticleDtoToMiniArticle(articleDto);
        miniArticle.setListings(listings);
        MiniArticle savedMiniArticle =  miniArticleRepository.save(miniArticle);

        return new ResponseEntity<>(mapper.miniArticleToMiniArticleDto(savedMiniArticle),
                HttpStatus.OK);
    }

    /**
     * @param miniArticleDtoSet
     * @return
     */
    @Override
    public Set<MiniArticle> createFullMiniArticle(Set<MiniArticleDto> miniArticleDtoSet, Long article_Id) {
        Set<MiniArticle> miniArticleSet = new HashSet<>();
        for (MiniArticleDto miniArticleDto: miniArticleDtoSet) {
            MiniArticle miniArticle = mapper.miniArticleDtoToMiniArticle(miniArticleDto);

            // Add ArticlePhotos
            Set<ArticlePhoto> articlePhoto = new HashSet<>();
            if (!miniArticleDto.getArticlePhotoDto().isEmpty()) {
                articlePhoto = articlePhotoService.creatListOfArticlePhoto(miniArticleDto.getArticlePhotoDto());
            }
            miniArticle.setMiniArticlePhotos(articlePhoto);
            miniArticleRepository.save(miniArticle);
            miniArticleSet.add(miniArticle);
        }
        return miniArticleSet;
    }


    /**
     * @param id
     * @param miniArticleDto
     * @return
     */
    @Override
    public ResponseEntity<MiniArticleDto> update(Long id, MiniArticleDto miniArticleDto) {
        MiniArticle miniArticle = this.miniArticleRepository.findById(id).orElseThrow(() ->
                new MiniArticleNotFoundException(id));
        Set<Listing> listings = new HashSet<>();
        for (Long listing_id: miniArticleDto.getListings_ID()) {
            Optional<Listing> listing = this.listingsRepository.findById(listing_id);
            if (listing.isEmpty()){
                throw new ListingNotFoundException(listing_id);
            }
            listings.add(listing.get());
        }
        miniArticle.setTitle(miniArticleDto.getTitle());
        miniArticle.setContent(miniArticleDto.getContent());
        miniArticle.setListings(listings);
        MiniArticle savedMiniArticle =  miniArticleRepository.save(miniArticle);
        return new ResponseEntity<>(mapper.miniArticleToMiniArticleDto(savedMiniArticle),
                HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.miniArticleRepository.existsById(id);
        if (!exists) {
            throw new MiniArticleNotFoundException(id);
        }
        this.miniArticleRepository.deleteById(id);
        return new ResponseEntity<>("Mini article has been removed successfully!", HttpStatus.NO_CONTENT);
    }
}
