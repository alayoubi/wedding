import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimilarVendorComponent } from './similar-vendor.component';

describe('SimilarVendorComponent', () => {
  let component: SimilarVendorComponent;
  let fixture: ComponentFixture<SimilarVendorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimilarVendorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SimilarVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
