package amiss.wedding.errors;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ListingPlanNotFoundException extends ApplicationRuntimeException {

    public ListingPlanNotFoundException(long id) {
        super("The listing Plan with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

    public ListingPlanNotFoundException(Listing listing) {
        super("The listing Plan for this Listing : " + listing.getTitle() + " was not found", HttpStatus.NOT_FOUND);

    }
}
