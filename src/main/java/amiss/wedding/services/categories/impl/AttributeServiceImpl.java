package amiss.wedding.services.categories.impl;

import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.attributes.AttributeType;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.errors.AttributeNotFoundException;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.mappers.categories.AttributeMapper;
import amiss.wedding.mappers.categories.AttributeMapperImpl;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.categories.attribute.AttributeRepository;
import amiss.wedding.services.categories.AttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AttributeServiceImpl implements AttributeService {

    private final AttributeRepository attributeRepository;

    private final CategoryRepository categoryRepository;

    private final AttributeMapper mapper = new AttributeMapperImpl();

    @Autowired
    public AttributeServiceImpl(AttributeRepository attributeRepository, CategoryRepository categoryRepository) {
        this.attributeRepository = attributeRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public ResponseEntity<List<AttributeDto>> findAll() {
        List<Attribute> attributes = this.attributeRepository.findAll();
        List<AttributeDto> attributeDtoList =
                attributes.stream().map(mapper::attributeToAttributeDto).collect(Collectors.toList());

        return new ResponseEntity<>(attributeDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AttributeDto> findById(Long id) {
        Optional<Attribute> attribute = this.attributeRepository.findById(id);
        if (attribute.isEmpty()){
            throw new AttributeNotFoundException(id);
        }
        AttributeDto attributeDto = mapper.attributeToAttributeDto(attribute.get());
        return new ResponseEntity<>(attributeDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AttributeDto> create(AttributeDto attributeDto, Long category_id) {
        Category category = categoryRepository.findById(category_id).orElseThrow(() ->
                new CategoryNotFoundException(category_id));
        final Attribute attribute = mapper.attributeDtoToAttribute(attributeDto);
        attribute.setCategory(category);
        final Attribute createdAttribute = this.attributeRepository.save(attribute);
        return new ResponseEntity<>(mapper.attributeToAttributeDto(createdAttribute), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<AttributeDto> update(Long id, AttributeDto newAttribute) {
        Optional<Attribute> attribute = this.attributeRepository.findById(id);
        if (attribute.isEmpty()){
            throw new AttributeNotFoundException(id);
        }
        Attribute existingAttribute = attribute.get();

        Category category = categoryRepository.findById(existingAttribute.getCategory().getId()).orElseThrow(() ->
                new CategoryNotFoundException(existingAttribute.getCategory().getId()));

        existingAttribute.setName(newAttribute.getName());
        existingAttribute.setValueType(newAttribute.getValueType());
        existingAttribute.setAttributeType(newAttribute.getAttributeType());
        existingAttribute.setActive(newAttribute.isActive());
        existingAttribute.setCategory(category);
        Attribute updatedAttribute = this.attributeRepository.save(existingAttribute);
        AttributeDto attributeDto = mapper.attributeToAttributeDto(updatedAttribute);
        return new ResponseEntity<>(attributeDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.attributeRepository.existsById(id);
        if (!exists){
            throw new AttributeNotFoundException(id);
        }
        this.attributeRepository.deleteById(id);
        return new ResponseEntity<>("Attribute has been removed successfully!", HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<AttributeDto>> findByCategory(Long category_id) {
        Optional<Category> category = categoryRepository.findById(category_id);

        if (category.isEmpty()){
            throw new CategoryNotFoundException(category_id);
        }
        Category existingCategory = category.get();

        Stream<Attribute> attributes = attributeRepository.findByCategory(existingCategory).stream();


        List<AttributeDto> attributeDtoList =
                attributes.map(mapper::attributeToAttributeDto).collect(Collectors.toList());

        return new ResponseEntity<>(attributeDtoList, HttpStatus.OK);
    }

    /**
     * @param category_id
     * @param attributeType
     * @return
     */
    @Override
    public ResponseEntity<List<AttributeDto>> findByCategoryAndAttributeTyp(Long category_id, AttributeType attributeType) {
        Optional<Category> category = categoryRepository.findById(category_id);
        if (category.isEmpty()){
            throw new CategoryNotFoundException(category_id);
        }
        Category existingCategory = category.get();

        Stream<Attribute> attributes = attributeRepository.findByCategoryAndAttributeType(existingCategory, attributeType).stream();
        List<AttributeDto> attributeDtoList =
                attributes.map(mapper::attributeToAttributeDto).collect(Collectors.toList());

        return new ResponseEntity<>(attributeDtoList, HttpStatus.OK);
    }
}
