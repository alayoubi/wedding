import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cakes',
  templateUrl: './cakes.component.html',
  styleUrls: ['./cakes.component.css']
})
export class CakesComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('قوالب الحلوى و الضيافة')
   }

  ngOnInit(): void {
  }

}
