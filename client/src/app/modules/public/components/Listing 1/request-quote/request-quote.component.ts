import { Component, NgModule, OnInit } from '@angular/core';

import {
  FormControl,
  Validators,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
@Component({
  selector: 'app-request-quote',
  templateUrl: './request-quote.component.html',
  styleUrls: ['./request-quote.component.css'],
})
export class RequestQuoteComponent implements OnInit {
  public onFocus :boolean = false
  constructor() {}

  @NgModule({
    imports: [FormControl, Validators, ReactiveFormsModule, FormGroup],
  })
  bookForm = new FormGroup({
    user: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required]),
    date: new FormControl(''),
    place: new FormControl('', [Validators.required]),
  });

  get userInp() {
    return this.bookForm.get('user');
  }
  get emailInp() {
    return this.bookForm.get('email');
  }
  get phoneInp() {
    return this.bookForm.get('phone');
  }
  get dateInp() {
    return this.bookForm.get('date');
  }
  get placeInp() {
    return this.bookForm.get('place');
  }

  submit() {
    console.log(this.bookForm.valid);
    console.log(this.bookForm.value);
    this.bookForm.reset();
    // this.isValid = this.registerForm.valid;
  }

  ngOnInit(): void {}
}
