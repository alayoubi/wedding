package amiss.wedding.mappers.articles;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.articles.dto.MiniArticleDto;

public interface ArticleMapper {
    Article articleDtoToArticle(ArticleDto articleDto);
    ArticleDto articleToArticleDto(Article article);
    ArticleDto articleToArticleDtoBaseData(Article article);
    MiniArticle miniArticleDtoToMiniArticle(MiniArticleDto miniArticleDto);
    MiniArticleDto miniArticleToMiniArticleDto(MiniArticle miniArticle);
    MiniArticleDto miniArticleToMiniArticleDtoBaseData(MiniArticle miniArticle);
}
