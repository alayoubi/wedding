package amiss.wedding.repositories.articles;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.MiniArticle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticlePhotoRepository extends JpaRepository<ArticlePhoto, Long> {

    List<ArticlePhoto> findAllByArticle(Article article);

    List<ArticlePhoto> findAllByMiniArticle(MiniArticle miniArticle);
}
