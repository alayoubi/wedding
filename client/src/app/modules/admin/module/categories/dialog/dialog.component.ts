import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryService } from '@core/services/category/category.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent {
  categoriesForm!: FormGroup;
  plansForm!: FormGroup;
  actionBtn: string = 'Save';

  constructor(
    private formBuilder: FormBuilder,
    private api: CategoryService,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private dialogRef: MatDialogRef<DialogComponent>,
  ) {}


  ngOnInit(): void {
    this.categoriesForm = this.formBuilder.group({
      name: ['', Validators.required],
      active: ['', Validators.required],
      // attributes_ID: ['', Validators.required],
      // coverImg_id: ['', Validators.required],
      // cover_img: ['', Validators.required],
      description: ['', Validators.required],
      // id: ['', Validators.required],

      // category_id:[Number,Validators.required],
      // valueType:['',Validators.required],
      // attributeType:['',Validators.required],
      unit: ['', Validators.required],
      // attributes_ID: [[],Validators.required]
    });
    this.plansForm = this.formBuilder.group({
      // category_id: ['', Validators.required],
      //   cost: ['', Validators.required],
      duration: ['', Validators.required],
      //   id: ['', Validators.required],
      //   planType:['', Validators.required],
    });

    if (this.editData) {
      this.categoriesForm.controls['name'].setValue(this.editData.name);
      this.categoriesForm.controls['active'].setValue(this.editData.active);
      // this.categoriesForm.controls['attributes_ID'].setValue(this.editData.attributes_ID);
      // this.categoriesForm.controls['coverImg_id'].setValue(this.editData.coverImg_id);
      // this.categoriesForm.controls['id'].setValue(this.editData.id);

      this.categoriesForm.controls['description'].setValue(
        this.editData.description
      );
      this.plansForm.controls['duration'].setValue(this.editData.duration);
      // this.categoriesForm.controls['valueType'].setValue(this.editData.plans);
      // this.categoriesForm.controls['attributeType'].setValue(this.editData.attributes);
      this.categoriesForm.controls['unit'].setValue(this.editData.unit);
      // this.categoriesForm.controls['attributes_ID'].setValue(this.editData.description);
      this.actionBtn = 'Update';
    }
  }

  addCategory() {
    if (!this.editData) {
      if (this.categoriesForm.valid) {
        this.api.postCategory(this.categoriesForm.value).subscribe({
          next: (res) => {
            alert('تم اضافة التصنيف بنجاح');
            this.categoriesForm.reset();
            this.dialogRef.close('save');
          },
          error: () => {
            alert('خطأ اثناء إضلفة التصنيف ');
          },
        });
      }
      console.log(this.categoriesForm.value);
    } else {
      this.updateCategory();
    }
  }

  updateCategory() {
    this.api
      .putCategory(this.categoriesForm.value, this.editData.id) /**/
      .subscribe({
        next: () => {
          alert('تم تحديث التصنيف بنجاح');
          this.categoriesForm.reset();
          this.dialogRef.close('update');
        },
        error: () => {
          alert('خطأ أثناء تحديث السجل');
        },
      });
    console.log(this.categoriesForm.value);
  }


}
