package amiss.wedding.services.categories.impl;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.plans.Feature;
import amiss.wedding.entities.categories.plans.FeatureDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanDto;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.errors.PlanNotFoundException;
import amiss.wedding.mappers.categories.PlanMapper;
import amiss.wedding.mappers.categories.PlanMapperImpl;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.categories.FeatureRepository;
import amiss.wedding.repositories.categories.PlanRepository;
import amiss.wedding.services.categories.PlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class PlanServiceImpl implements PlanService {

    private final PlanRepository planRepository;
    private final CategoryRepository categoryRepository;
    private final FeatureRepository featureRepository;

    private final PlanMapper mapper = new PlanMapperImpl();

    @Override
    public ResponseEntity<List<PlanDto>> findAll() {
        List<Plan> plans = this.planRepository.findAll();
        List<PlanDto> planDtoList =
                plans.stream().map(mapper::planToPlanDto).collect(Collectors.toList());
        return new ResponseEntity<>(planDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PlanDto> findById(Long id) {
        Optional<Plan> plan = this.planRepository.findById(id);
        if (plan.isEmpty()){
            throw new PlanNotFoundException(id);
        }
        PlanDto planDto = mapper.planToPlanDto(plan.get());
        return new ResponseEntity<>(planDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PlanDto> create(PlanDto planDto) {
        Category category = categoryRepository.findById(planDto.getCategory_id()).orElseThrow(() ->
                new CategoryNotFoundException(planDto.getId()));
        final Plan plan = mapper.planDtoToPlan(planDto);
        plan.setCategory(category);
        Plan createdPlan = planRepository.save(plan);
        Set<Feature> features = new HashSet();;

        if (!planDto.getFeatures().isEmpty()) {
            for (String title: planDto.getFeatures()) {
                Feature feature = new Feature(title, createdPlan);
                feature = featureRepository.save(feature);
                features.add(feature);
            }
        }
        createdPlan.setFeatures(features);
        return new ResponseEntity<>(mapper.planToPlanDto(createdPlan), HttpStatus.CREATED);
    }

    /**
     * @param plan_id
     * @param featureDto
     * @return
     */
    @Override
    public ResponseEntity<PlanDto> createFeature(Long plan_id, FeatureDto featureDto) {
        Plan plan = planRepository.findById(plan_id).orElseThrow(() ->
                new PlanNotFoundException(plan_id));
        Feature feature = mapper.FeatureDtoToFeature(featureDto);
        feature.setPlan(plan);
        featureRepository.save(feature);
        return new ResponseEntity<>(mapper.planToPlanDto(plan), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<PlanDto> update(Long id, PlanDto planDto) {
        Plan existingPlan = this.planRepository.findById(id).orElseThrow(() ->
                new PlanNotFoundException(id));
        Category category = categoryRepository.findById(planDto.getCategory_id()).orElseThrow(() ->
                new CategoryNotFoundException(planDto.getId()));

        existingPlan.setDuration(planDto.getDuration());
        existingPlan.setCost(planDto.getCost());
        existingPlan.setPlanType(planDto.getPlanType());
        existingPlan.setCategory(category);

        if (existingPlan.getFeatures() != null || !existingPlan.getFeatures().isEmpty()) {
            for (Feature existedFeature : existingPlan.getFeatures()) {
                this.featureRepository.deleteById(existedFeature.getId());
            }
        }
        if (!planDto.getFeatures().isEmpty()) { // Todo delete the old features
            for (String title: planDto.getFeatures()) {
                Feature feature = new Feature(title, existingPlan);
                featureRepository.save(feature);
            }
        }
        Plan updatedPlan = this.planRepository.save(existingPlan);
        return new ResponseEntity<>(mapper.planToPlanDto(updatedPlan), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exist = this.planRepository.existsById(id);
        if (!exist){
            throw new PlanNotFoundException(id);
        }
        this.planRepository.deleteById(id);
        return new ResponseEntity<>("Plan has been removed successfully!", HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<PlanDto>> findByCategory(Long category_id) {
        Optional<Category> category = categoryRepository.findById(category_id);
        if (category.isEmpty()){
            throw new CategoryNotFoundException(category_id);
        }
        Stream<Plan> plans = this.planRepository.findByCategory(category.get()).stream();
        List<PlanDto> planDtoList =
                plans.map(mapper::planToPlanDto).collect(Collectors.toList());
        return new ResponseEntity<>(planDtoList, HttpStatus.OK);
    }
}
