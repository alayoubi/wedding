package amiss.wedding.controllers.admin.cities;

import amiss.wedding.entities.cities.dto.CountryDto;
import amiss.wedding.services.cities.CountryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/countries")
public class CountryController {


    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }


    @GetMapping("")
    @ApiOperation(value = "List<CountryDto>", notes = "Send all Countries")
    public ResponseEntity<List<CountryDto>> getCountries(){
        return this.countryService.findAll();
    }

    @GetMapping(path = "/{countryId}")
    @ApiOperation(value = "CountryDto", notes = "ارسال بلد من ال ID")
    public ResponseEntity<CountryDto> getCountry(@PathVariable Long countryId){
        return this.countryService.findById(countryId);
    }

//    @PostMapping()
//    public ResponseEntity<CityDto> createCity(@RequestBody CityDto cityDto){
//        return this.cityService.create(cityDto);
//    }
//
//    @PutMapping(path = "/{cityId}")
//    public ResponseEntity<CityDto> updateCity(@PathVariable Long cityId, @RequestBody CityDto cityDto){
//        return this.cityService.update(cityId, cityDto);
//    }
//
//    @DeleteMapping(path = "/{countryId}")
//    public ResponseEntity<String> removeCountry(@PathVariable Long countryId){
//        return this.cityService.remove(cityId);
//    }
}
