package amiss.wedding.services.users;

import amiss.wedding.entities.users.Role;

import java.util.List;

public interface RoleService {

    void setRole(long id, Role role);
}
