package amiss.wedding.storage;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
@Slf4j
public class StorageService implements IStorage{

    @Value(value = "${spring.profiles.active}")
    private String version;

    @Value("${application.bucket.name}")
    private String bucketName;

    @Autowired
    private AmazonS3 amazonS3;

    @Override
    public String uploadFile(MultipartFile file, String key, String path) {
        String time = System.currentTimeMillis() + "";
        /* String originalFilename = file.getOriginalFilename();
        String subName;
        if (originalFilename.length() == 3) {
            subName = originalFilename + "Lilty";
        } else if (originalFilename.length() > 3) {
            subName = originalFilename.substring(originalFilename.length() - 3);
        } else {
            // whatever is appropriate in this case
            throw new IllegalArgumentException("word has fewer than 3 characters!");
        }*/
        String fileName = key + time + "_lilty";
        path +=  time + "_lilty";
        try {
            File fileObj = convertMultiPartFileToFile(file);
            amazonS3.putObject(new PutObjectRequest(bucketName, fileName, fileObj)
                            .withCannedAcl(CannedAccessControlList.PublicRead));
            fileObj.delete();
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " + "means your request made it "
                    + "to Amazon S3, but was rejected with an error response" + " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

        }  catch (AmazonClientException ace) {
        System.out.println("Caught an AmazonClientException, which " + "means the client encountered " + "an internal error while trying to "
                + "communicate with S3, " + "such as not being able to access the network.");
        System.out.println("Error Message: " + ace.getMessage());
        }
        return path;
    }

    @Override
    public byte[] downloadFile(String fileName) {
        S3Object s3Object = amazonS3.getObject(bucketName, fileName);
        S3ObjectInputStream inputStream = s3Object.getObjectContent();
        try {
            byte[] content = IOUtils.toByteArray(inputStream);
            return content;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String deleteFile(String fileName) {
        amazonS3.deleteObject(bucketName, fileName);
        return fileName + " removed ...";
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(file.getOriginalFilename());
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (IOException e) {
            log.error("Error converting multipartFile to file", e);
        }
        return convertedFile;
    }

    @Override
    public String store(InputStream data, String key) {
        try {
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentEncoding("UTF-8");
            amazonS3.putObject(new PutObjectRequest(bucketName, key, data, objectMetadata));
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " + "means your request made it "
                    + "to Amazon S3, but was rejected with an error response" + " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());

        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " + "means the client encountered " + "an internal error while trying to "
                    + "communicate with S3, " + "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());

        }

        return key;

    }

    @Override
    public InputStream read(String key) {
        return amazonS3.getObject(bucketName, key).getObjectContent();
    }

    @Override
    public void writeAsJson(String key, Object data) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(data);
            write(key, json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(String key, String content) {
        amazonS3.putObject(bucketName, key, content);
    }

    @Override
    public boolean exists(String key) {
        return amazonS3.doesObjectExist(bucketName, key);
    }

    public void storeString(String key, String content) {
        amazonS3.putObject(bucketName,
                key,
                content);
    }

    public void storeResource(String key) {
        if (!amazonS3.doesObjectExist(bucketName, key)) {
            System.out.println("Copy: " + key);
            ClassPathResource resource = new ClassPathResource(key);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentEncoding("UTF-8");
            try (InputStream data = resource.getInputStream()) {
                amazonS3.putObject(new PutObjectRequest(bucketName, key, data, objectMetadata));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
