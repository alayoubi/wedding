package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.ListingDto;

public interface ListingMapper {
    Listing listingDtoToListing(ListingDto listingDto);
    ListingDto listingToListingDto(Listing listing);
    ListingDto listingToListingDtoBaseData(Listing listing);

}
