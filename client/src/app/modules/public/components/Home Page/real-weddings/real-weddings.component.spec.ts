import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RealWeddingsComponent } from './real-weddings.component';

describe('RealWeddingsComponent', () => {
  let component: RealWeddingsComponent;
  let fixture: ComponentFixture<RealWeddingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RealWeddingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RealWeddingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
