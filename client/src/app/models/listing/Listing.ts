export class Listing {
  id: number;
  title: string;
  email: string;
  phone: string;
  address: string;
  descriptions: string;
  coverImg: string;
  status: string;
  country_id: number;
  country: string;
  region_id: number;
  region: string;
  city_id: number;
  city: string;
  district_id: number;
  district: string;
  position_id: number;
  client_id: number;
  plan_id: number;
  categorie_id: number;
  categorie: string;
  reviewsList: Review[];
  listingsPhotos: any[];
  socialMediaList: SocialMedia[];
  attributeValueList: AttributeValue[];
  offersList: Offer[];
}


export class SocialMedia{
  id: number;
  path: string;
  listing_Id: number;
  socialMediaTyp: string;
}


export class AttributeValue {
  id: number;
  value: string;
  listing_Id: number;
  attribute_Id: number;
  attribute: string;
  typ: string;
  active: boolean;
}

export class Review{

  id: number;
  listing_Id: number;
  title: string;
  content: string;
  rating: number;
  datePost: Date;

}

export class Offer{
  id: number;
  title: string;
  listingId: number;
  content: string;
  expiryDate: Date;
}
