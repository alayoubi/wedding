import { HttpErrorResponse } from '@angular/common/http';
import { ListingService } from '@core/services/listing/listing.service';
import { Attribute } from '@core/models/category/Attribute';
import { AttributeService } from '@core/services/category/attribute.service';
// import { Attribute } from '@core/models/category/Attribute';
import { DistrictService } from '@core/services/city/district.service';
import { District } from '@core/models/city/District';
import { City } from '@core/models/city/City';
import { Category } from 'src/app/models/category/Category';
import { CategoryService } from 'src/app/services/category/category.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Region } from '@core/models/city/Region';
import { RegionService } from '@core/services/city/region.service';
import { CityService } from '@core/services/city/city.service';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateAttributeComponent } from '../create-attribute/create-attribute.component';
import { Observable, map, startWith } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatSnackBar } from '@angular/material/snack-bar';


import { Listing } from '@core/models/listing/Listing';
import { UploadPhotoOverlayComponent } from '../upload-photo-overlay/upload-photo-overlay.component';
@Component({
  selector: 'app-add.listing.componts',
  templateUrl: './add.listing.componts.component.html',
  styleUrls: ['./add.listing.componts.component.css'],
})
export class AddListingCompontsComponent implements OnInit {
  addListingForm = new FormGroup({
    title: new FormControl<string | null>(null, Validators.required),
    email: new FormControl<string | null>(null, Validators.required),
    phone: new FormControl<string | null>(null, Validators.required),
    address: new FormControl<string | null>(null, Validators.required),
    descriptions: new FormControl<string | null>(null, Validators.required),
    capacity: new FormControl<string | null>(null, Validators.required),
    price: new FormControl<string | null>(null, Validators.required),
    status: new FormControl<string | null>(null, Validators.required),
    country_id: new FormControl<number | null>(null, Validators.required),
    region_id: new FormControl<number | null>(null, Validators.required),
    city_id: new FormControl<number | null>(null, Validators.required),
    district_id: new FormControl<number | null>(null, Validators.required),
    client_id: new FormControl<number | null>(null, Validators.required),
    plan_id: new FormControl<number | null>(1, Validators.required),
    category_id: new FormControl<number | null>(null, Validators.required),
    attributeValueList: new FormArray<
      FormGroup<{
        attribute_Id: FormControl<number | null>;
        value: FormControl<string | null>;
      }>
    >([]),
    mainAttributeValue: new FormArray<
      FormGroup<{
        id: FormControl<number | null>;
        value: FormControl<string | null>;
        name: FormControl<string | null>;
      }>
    >([]),
    secondaryAttributeValue: new FormArray<
      FormGroup<{
        id: FormControl<number | null>;
        value: FormControl<string | null>;
        name: FormControl<string | null>;
      }>
    >([]),
    generaleAttributeValue: new FormArray<
      FormGroup<{
        id: FormControl<number | null>;
        value: FormControl<boolean | null>;
        name: FormControl<string | null>;
      }>
    >([]),
    questionAttributeValue: new FormArray<
      FormGroup<{
        id: FormControl<number | null>;
        value: FormControl<string | null>;
        name: FormControl<string | null>;
      }>
    >([]),
    covidAttributeValue: new FormArray<
      FormGroup<{
        id: FormControl<number | null>;
        value: FormControl<boolean | null>;
        name: FormControl<string | null>;
      }>
    >([]),
    otherAttributeValue: new FormArray<
      FormGroup<{
        id: FormControl<number | null>;
        value: FormControl<string | null>;
        name: FormControl<string | null>;
      }>
    >([]),
    socialMediaList : new FormArray([
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("PHONE", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("EMAIL", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("WHATSAPP", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("FACEBOOK", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("INSTAGRAM", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("TWITTER", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("TIKTOK", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("SNAPCHAT", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("PRIVATE", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("YOUTUBE", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("PINTEREST", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("TELEGRAM", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("LINKEDIN", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("OTHER", null),
      }),
    ]),
    employees: new FormArray([
      new FormGroup({
        name: new FormControl<string | null>(''),
        job: new FormControl<string | null>(''),
        phone_emp: new FormControl<string | null>(''),
      }),
    ]),
  });

  loading = false;

  public statuses: String[] = ["PENDING",
  "PROCESSING",
  "COMPLETED",
  "DECLINE" ]

  public categories: Category[];
  public regions: Region[];
  public cities: City[];
  public districts: District[];
  public mainAttributes: Attribute[] = [];
  public secondaryAttributes: Attribute[] = [];
  public generaleAttributes: Attribute[] = [];
  public questionAttributes: Attribute[] = [];
  public covidAttributes: Attribute[] = [];
  public otherAttributes: Attribute[] = [];

  public photos: File[] = [];

  public selectedCategory: number;
  public selectedRegion: number;
  public selectedCity: number;
  public selectedDistrict: number;

  public labelPrice: String;
  public labelCapacity: String;
  public r;

  public parentMessage = 'Hello World!';

  receiveMessage($event) {
//    console.log($event);
  }

  constructor(
    private snackBar: MatSnackBar,
    private listingsService: ListingService,
    private categoryService: CategoryService,
    private regionService: RegionService,
    private cityservice: CityService,
    private districtService: DistrictService,
    private attributeService: AttributeService,
    public dialog: MatDialog
  ) {
    this.categories = [];
    this.regions = [];
    this.cities = [];
    this.districts = [];
  }
  // auto complete for categories
  myControlCat = new FormControl<string | Category>('');
  filteredOptionsCat: Observable<Category[]>;

  displayFnCat(category: Category): string {
    return category && category.name ? category.name : '';
  }

  private _filterCat(name: string): Category[] {
    const filterValue = name.toLowerCase();

    return this.categories.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }

  // auto complete for Regions
  myControlReg = new FormControl<string | Region>('');
  filteredOptionsReg: Observable<Region[]>;

  displayFnReg(region: Region): string {
    return region && region.name ? region.name : '';
  }

  private _filterReg(name: string): Region[] {
    const filterValue = name.toLowerCase();

    return this.regions.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }

  // auto complete for Cities
  myControlCit = new FormControl<string | City>('');
  filteredOptionsCit: Observable<City[]>;

  displayFnCit(city: City): string {
    return city && city.name ? city.name : '';
  }

  private _filterCit(name: string): City[] {
    const filterValue = name.toLowerCase();

    return this.cities.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }

  // auto complete for Districts
  myControlDis = new FormControl<string | District>('');
  filteredOptionsDis: Observable<District[]>;

  displayFnDis(district: District): string {
    return district && district.name ? district.name : '';
  }

  private _filterDis(name: string): District[] {
    const filterValue = name.toLowerCase();

    return this.districts.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }

  //get Selected Category
  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    this.selectedCategory = event.option.value.id;
    this.getLabel();
    this.addListingForm.get('category_id')?.setValue(this.selectedCategory);
    this.getMainAttributesByCategory(this.selectedCategory);
    this.getSecondaryAttributesByCategory(this.selectedCategory);
    this.getGeneralAttributesByCategory(this.selectedCategory);
    this.getQuestionsAttributesByCategory(this.selectedCategory);
    this.getCovidAttributesByCategory(this.selectedCategory);
    this.getOtherAttributesByCategory(this.selectedCategory);
  }
  //
  ngOnInit(): void {
    this.getAllCategories();
    this.getAllActiveRegion();
    this.onFocus();
  }

  getLabel(){
    if (this.selectedCategory == 1) {
      this.labelCapacity = "السعة";
      this.labelPrice = "سعر القسمين يبدأ من";
    } else {
      this.labelCapacity =  "السعة";
      this.labelPrice =  "السعر للشخص الواحد";
    }
  }
  openDialog(row?: any) {
    this.r = row;
    let dialogConf = new MatDialogConfig();
    dialogConf.width = '100%';
    dialogConf.disableClose = true;
    dialogConf.autoFocus = false;
    dialogConf.height = '80%';
    dialogConf.enterAnimationDuration = '700ms';
    dialogConf.data = row;
    this.dialog.open(CreateAttributeComponent, dialogConf);
  }

  openDialogUploadPhoto(row?: any) {
    this.r = row;
    let dialogConf = new MatDialogConfig();
    dialogConf.width = '100%';
    dialogConf.disableClose = true;
    dialogConf.autoFocus = false;
    dialogConf.height = '80%';
    dialogConf.enterAnimationDuration = '700ms';
    dialogConf.data = row;
    this.dialog.open(UploadPhotoOverlayComponent, dialogConf);
  }



  public onAddListing(): void {
    this.snackBar.open(' تم إضافة الإعلان بنجاح ✔', 'تم', {
      duration: 2000,

      panelClass: ['green-snackbar', 'login-snackbar', 'my-custom-snackbar'],
    });

    this.loading = true;

    this.addListingForm.controls.mainAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValueDto = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValueDto
          );
        }
      }
    );

    this.addListingForm.controls.secondaryAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValueDto = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValueDto
          );
        }
      }
    );

    this.addListingForm.controls.generaleAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value == true) {
          const attributeValueDto = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl("true"),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValueDto
          );
        }
      }
    );

    this.addListingForm.controls.questionAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValueDto = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValueDto
          );
        }
      }
    );

    this.addListingForm.controls.covidAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value == true) {
          const attributeValueDto = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl("true"),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValueDto
          );
        }
      }
    );

    this.addListingForm.controls.otherAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValueDto = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValueDto
          );
        }
      }
    );

    this.listingsService.addListing(this.addListingForm.value).subscribe(
      (response: Listing) => {
        // console.log(response);
        this.loading = false;

        this.openDialogUploadPhoto(response);
      },
      (error: HttpErrorResponse) => {

        this.loading = false;

      alert(error.message);

      },
      () =>{
          console.log("done");
      }

    );

  }


  //Deal with employees
  createEmployee() {
    const employee = <FormArray>this.addListingForm.controls['employees'];
    employee.push(
      new FormGroup({
        name: new FormControl('', Validators.required),
        job: new FormControl(''),
        phone_emp: new FormControl(''),
      })
    );
  }

  removeEmployee(index) {
    const employee = <FormArray>this.addListingForm.controls.employees;
    employee.removeAt(index);
  }

  //

  public reset(addForm: FormGroup): void {
    addForm.reset();
  }

  onCategoryChange(): void {}

  onFocus(ev?: any, id?: any) {}

  autoFill() {
    this.addListingForm.get("title")?.setValue("صالة الرياض التجريبية");
    this.addListingForm.get("email")?.setValue("test.test@gmail.oom");
    this.addListingForm.get("phone")?.setValue("01574892100");
    this.addListingForm.get("address")?.setValue("الرياض شارع علي بابا");
    this.addListingForm.get("descriptions")?.setValue("الوصف المناسب للصالة الجديدة");
    this.addListingForm.get("status")?.setValue("COMPLETED");
    this.addListingForm.get("country_id")?.setValue(1);
    this.addListingForm.get("region_id")?.setValue(1);
    this.addListingForm.get("city_id")?.setValue(1);
    this.addListingForm.get("district_id")?.setValue(1);
    this.addListingForm.get("category_id")?.setValue(1);

  }
  onRegionChange(event: MatAutocompleteSelectedEvent): void {
    this.selectedRegion = event.option.value.id;
    this.addListingForm.get('region_id')?.setValue(this.selectedRegion);
    this.getAllCitiesByRegion(this.selectedRegion);
  }

  onCityChange(event: MatAutocompleteSelectedEvent): void {
    this.selectedCity = event.option.value.id;
    this.addListingForm.get('city_id')?.setValue(this.selectedCity);
    this.getAllDistrictsByCity(this.selectedCity);
  }

  onDistrictChange(event: MatAutocompleteSelectedEvent): void {
    this.selectedDistrict = event.option.value.id;
    this.addListingForm.get('district_id')?.setValue(this.selectedDistrict);
  }
  // file js
  selectedFile: any = null;

  onListingPhotosChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        this.photos.push(fileList[i]);
      }
    }
  }
  // end file

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categories) => {
      this.categories = categories;
      this.filteredOptionsCat = this.myControlCat.valueChanges.pipe(
        startWith(''),
        map((value) => {
          const name = typeof value === 'string' ? value : value?.name;
          return name
            ? this._filterCat(name as string)
            : this.categories.slice();
        })
      );
    });
  }

  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
      this.filteredOptionsReg = this.myControlReg.valueChanges.pipe(
        startWith(''),
        map((value) => {
          const name = typeof value === 'string' ? value : value?.name;
          return name ? this._filterReg(name as string) : this.regions.slice();
        })
      );
    });
  }

  public getAllCitiesByRegion(regionId: number) {
    this.cityservice.getCitiesByRegion(regionId).subscribe((cityservice) => {
      this.cities = cityservice;
      this.filteredOptionsCit = this.myControlCit.valueChanges.pipe(
        startWith(''),
        map((value) => {
          const name = typeof value === 'string' ? value : value?.name;
          return name ? this._filterCit(name as string) : this.cities.slice();
        })
      );
    });
  }

  public getAllDistrictsByCity(cityId: number) {
    this.districtService
      .getDistrictsByCity(cityId)
      .subscribe((districtService) => {
        this.districts = districtService;
        this.filteredOptionsDis = this.myControlDis.valueChanges.pipe(
          startWith(''),
          map((value) => {
            const name = typeof value === 'string' ? value : value?.name;
            return name
              ? this._filterDis(name as string)
              : this.districts.slice();
          })
        );
      });
  }

  public getMainAttributesByCategory(categoryId: number) {
    this.attributeService
      .getMainAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        this.mainAttributes = attributes;
        attributes.forEach((attribute) => {
          const mainAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(''),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.mainAttributeValue.push(
            mainAttributeFromGroup
          );
        });
      });
  }

  public get mainAttributeValue() {
    return this.addListingForm.controls.mainAttributeValue as FormArray;
  }
  public getSecondaryAttributesByCategory(categoryId: number) {
    this.attributeService
      .getSecondaryAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        this.secondaryAttributes = attributes;
        attributes.forEach((attribute) => {
          const secondaryAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(''),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.secondaryAttributeValue.push(
            secondaryAttributeFromGroup
          );
        });
      });
  }

  public getGeneralAttributesByCategory(categoryId: number) {
    this.attributeService
      .getGeneralAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        this.generaleAttributes = attributes;
        attributes.forEach((attribute) => {
          const generaleAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(false),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.generaleAttributeValue.push(
            generaleAttributeFromGroup
          );
        });
      });
  }

  public getQuestionsAttributesByCategory(categoryId: number) {
    this.attributeService
      .getQuestionsAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        this.questionAttributes = attributes;
        attributes.forEach((attribute) => {
          const questionAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(''),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.questionAttributeValue.push(
            questionAttributeFromGroup
          );
        });
      });
  }

  public getCovidAttributesByCategory(categoryId: number) {
    this.attributeService
      .getCovidAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        this.covidAttributes = attributes;
        attributes.forEach((attribute) => {
          const covidAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(false),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.covidAttributeValue.push(
            covidAttributeFromGroup
          );
        });
      });
  }

  public getOtherAttributesByCategory(categoryId: number) {
    this.attributeService
      .getOtherAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        this.otherAttributes = attributes;
        attributes.forEach((attribute) => {
          const otherAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(''),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.otherAttributeValue.push(
            otherAttributeFromGroup
          );
        });
      });
  }
}
