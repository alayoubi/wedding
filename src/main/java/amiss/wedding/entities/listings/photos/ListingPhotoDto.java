package amiss.wedding.entities.listings.photos;


import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Status;
import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListingPhotoDto {

    private Long id;
    private String fileName;
    private String path;
    private Status status;
    private Long listing_Id;
    private double size;
    public ListingPhotoDto(ListingPhoto listingPhoto){
        this.id = listingPhoto.getId();
        this.fileName = listingPhoto.getFileName();
        this.path = listingPhoto.getPath();
        this.status = listingPhoto.getStatus();
        this.listing_Id = listingPhoto.getListing().getId();
        this.size = listingPhoto.getSize();
    }
}
