import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DressesContentComponent } from './dresses-content.component';

describe('DressesContentComponent', () => {
  let component: DressesContentComponent;
  let fixture: ComponentFixture<DressesContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DressesContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DressesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
