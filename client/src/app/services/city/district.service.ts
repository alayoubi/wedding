import { District } from './../../models/city/District';
import { API } from './../../config/api.config';
import { Observable, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DistrictService {
  private districs: District[];
  constructor(private http: HttpClient) {}

  getDistrictsByCity(cityId: number): Observable<District[]> {
    if (this.districs) {
      // Return cached categories if they exist
      return of(this.districs);
    } else {
      // Load categories from API and cache the result
      return this.http
        .get<District[]>(API.DISTRICTS.CITYID(cityId))
        .pipe(tap((categories) => (this.districs = this.districs)));
    }

  }

 
}
