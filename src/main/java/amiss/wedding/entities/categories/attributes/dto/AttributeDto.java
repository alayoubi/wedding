package amiss.wedding.entities.categories.attributes.dto;


import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.attributes.AttributeType;
import amiss.wedding.entities.categories.attributes.ValueType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttributeDto {

    private Long id;
    private String name;
    private Long category_id;
    private ValueType valueType;
    private AttributeType attributeType;
    private String unit;
    private boolean active;

    public AttributeDto(Attribute attribute) {
        this.id = attribute.getId();
        this.name = attribute.getName();
        this.category_id = attribute.getCategory().getId();
        this.valueType = attribute.getValueType();
        this.attributeType = attribute.getAttributeType();
        this.unit = attribute.getUnit();
        this.active = attribute.isActive();
    }
}
