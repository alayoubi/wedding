package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class CityNotFoundException extends ApplicationRuntimeException {

    public CityNotFoundException(Long id){
        super("The City with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
