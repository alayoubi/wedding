package amiss.wedding.services.listings;

import amiss.wedding.entities.listings.plans.ListingPlanDto;
import org.springframework.http.ResponseEntity;

public interface ListingPlanService {

    ResponseEntity<ListingPlanDto> findById(Long listingPlanId);

    ResponseEntity<ListingPlanDto> findListingPlanForListingById(Long listingId);

    ResponseEntity<ListingPlanDto> create(Long planId, Long listingId);

    ResponseEntity<ListingPlanDto> update(ListingPlanDto listingPlanDto);

    ListingPlanDto expired(Long listingPlanDto);

}
