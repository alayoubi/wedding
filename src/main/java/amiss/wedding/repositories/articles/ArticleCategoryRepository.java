package amiss.wedding.repositories.articles;

import amiss.wedding.entities.articles.ArticleCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleCategoryRepository extends JpaRepository<ArticleCategory, Long> {

}
