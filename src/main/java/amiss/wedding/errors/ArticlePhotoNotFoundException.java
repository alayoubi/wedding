package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ArticlePhotoNotFoundException extends ApplicationRuntimeException {

    public ArticlePhotoNotFoundException(long id) {
        super("The Photo with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
