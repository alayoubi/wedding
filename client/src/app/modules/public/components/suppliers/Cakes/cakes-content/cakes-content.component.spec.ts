import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CakesContentComponent } from './cakes-content.component';

describe('CakesContentComponent', () => {
  let component: CakesContentComponent;
  let fixture: ComponentFixture<CakesContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CakesContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CakesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
