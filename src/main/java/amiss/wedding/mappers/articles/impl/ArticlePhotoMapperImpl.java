package amiss.wedding.mappers.articles.impl;

import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.dto.ArticlePhotoDto;
import amiss.wedding.mappers.articles.ArticlePhotoMapper;

public class ArticlePhotoMapperImpl implements ArticlePhotoMapper {

    /**
     * @param articlePhotoDto
     * @return
     */
    @Override
    public ArticlePhoto articlePhotoDtoToArticlePhoto(ArticlePhotoDto articlePhotoDto) {
        return new ArticlePhoto(articlePhotoDto.getFileName(),
                articlePhotoDto.getPath(),
                articlePhotoDto.getTitle(),
                null,
                null,
                articlePhotoDto.getSize());
    }

    /**
     * @param articlePhoto
     * @return
     */
    @Override
    public ArticlePhotoDto ArticlePhotoToArticlePhotoDto(ArticlePhoto articlePhoto) {
        return new ArticlePhotoDto(articlePhoto);
    }
}
