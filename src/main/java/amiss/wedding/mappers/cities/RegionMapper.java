package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.cities.dto.RegionDto;

public interface RegionMapper {
    Region regionDtoToRegion(RegionDto regionDto);
    RegionDto regionToRegionDto(Region region);
}
