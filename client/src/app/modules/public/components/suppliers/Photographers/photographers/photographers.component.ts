import { Title } from '@angular/platform-browser';
import { title } from 'process';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photographers',
  templateUrl: './photographers.component.html',
  styleUrls: ['./photographers.component.css']
})
export class PhotographersComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('المصوين')
   }

  ngOnInit(): void {
  }

}
