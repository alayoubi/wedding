import { API } from '@core/config/api.config';
import { Observable } from 'rxjs';
import { Article } from './../../models/article/Article';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {}

  /**
   *
   * @returns
   */
  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(API.ARTICLES.BASE, {
      responseType: 'json',
    });
  }

  getArticleById(articleId: any): Observable<Article[]> {
    return this.http.get<Article[]>(API.ARTICLES.ID(articleId));
  }

  deleteArticle(articleId:any) {
    return this.http.delete(API.ARTICLES.DELETEAARTICLE(articleId))
  }

  addMiniArticle(miniArticle:any) {
    return this.http.post(API.ARTICLES.ADDMINIARTICLE,miniArticle)
  }
}
