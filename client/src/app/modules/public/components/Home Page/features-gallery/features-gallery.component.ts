import { Component, OnInit } from '@angular/core';
import { Category } from '@core/models/category/Category';
import { CategoryService } from '@core/services/category/category.service';

@Component({
  selector: 'app-features-gallery',
  templateUrl: './features-gallery.component.html',
  styleUrls: ['./features-gallery.component.css'],
})
export class FeaturesGalleryComponent implements OnInit {
  public categories: Category[];


  constructor(private categoryService: CategoryService) {}

  ngOnInit(): void {
    this.getAllCategories()
  }

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
      console.log(this.categories)
    });
  }
}
