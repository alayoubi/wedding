package amiss.wedding.mappers.categories;

import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;

public class AttributeMapperImpl implements AttributeMapper{

    public AttributeMapperImpl() {
    }

    @Override
    public Attribute attributeDtoToAttribute(AttributeDto attributeDto) {

        Attribute attribute = new Attribute();
        attribute.setName(attributeDto.getName());
        attribute.setAttributeType(attributeDto.getAttributeType());
        attribute.setValueType(attributeDto.getValueType());
        attribute.setUnit(attributeDto.getUnit());
        attribute.setActive(attributeDto.isActive());
        return attribute;
    }

    @Override
    public AttributeDto attributeToAttributeDto(Attribute attribute) {

        AttributeDto attributeDto = new AttributeDto(attribute);
        return attributeDto;
    }
}
