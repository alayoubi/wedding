import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingVendorByLocationComponent } from './landing-vendor-by-location.component';

describe('LandingVendorByLocationComponent', () => {
  let component: LandingVendorByLocationComponent;
  let fixture: ComponentFixture<LandingVendorByLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingVendorByLocationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingVendorByLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
