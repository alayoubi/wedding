package amiss.wedding.controllers.admin.articles;


import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.services.articles.ArticlePhotoService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/admin/article photos")
@AllArgsConstructor
public class AdminArticlePhotoController {

    private final ArticlePhotoService articlePhotoService;


    @PostMapping(value = "/article/{article_id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ArticleDto> createArticlePhoto(@PathVariable("article_id") Long article_id,
                                                         @RequestPart List<MultipartFile> photos){
        return this.articlePhotoService.uploadArticlePhotos(article_id, photos);
    }
}
