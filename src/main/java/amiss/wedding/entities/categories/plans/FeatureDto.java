package amiss.wedding.entities.categories.plans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeatureDto {

    private Long id;
    private String title;
    private Long plan_id;

    public FeatureDto(Feature feature) {
        this.id = feature.getId();
        this.title = feature.getTitle();
        this.plan_id = feature.getPlan().getId();
    }
}
