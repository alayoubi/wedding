import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DressesLandingComponent } from './dresses-landing.component';

describe('DressesLandingComponent', () => {
  let component: DressesLandingComponent;
  let fixture: ComponentFixture<DressesLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DressesLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DressesLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
