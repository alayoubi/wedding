//package amiss.wedding.config;
//
//import amiss.wedding.entities.articles.Article;
//import amiss.wedding.entities.articles.ArticlePhoto;
//import amiss.wedding.entities.articles.MiniArticle;
//import amiss.wedding.entities.categories.Category;
//import amiss.wedding.entities.categories.attributes.Attribute;
//import amiss.wedding.entities.categories.attributes.AttributeType;
//import amiss.wedding.entities.categories.attributes.ValueType;
//import amiss.wedding.entities.categories.dto.CategoryDto;
//import amiss.wedding.entities.cities.*;
//import amiss.wedding.entities.listings.*;
//import amiss.wedding.entities.listings.plans.Plan;
//import amiss.wedding.entities.listings.plans.PlanType;
//import amiss.wedding.entities.users.AppUser;
//import amiss.wedding.entities.users.Role;
//import amiss.wedding.entities.users.client.Client;
//import amiss.wedding.mappers.categories.CategoryMapper;
//import amiss.wedding.mappers.categories.CategoryMapperImpl;
//import amiss.wedding.mappers.RoleMapper;
//import amiss.wedding.mappers.RoleMapperImpl;
//import amiss.wedding.repositories.articles.ArticlePhotoRepository;
//import amiss.wedding.repositories.articles.ArticleRepository;
//import amiss.wedding.repositories.articles.MiniArticleRepository;
//import amiss.wedding.repositories.categories.CategoryRepository;
//import amiss.wedding.repositories.categories.PlanRepository;
//import amiss.wedding.repositories.categories.attribute.AttributeRepository;
//import amiss.wedding.repositories.cities.CityRepository;
//import amiss.wedding.repositories.cities.CountryRepository;
//import amiss.wedding.repositories.cities.DistrictRepository;
//import amiss.wedding.repositories.cities.RegionRepository;
//import amiss.wedding.repositories.listings.AttributeValueRepository;
//import amiss.wedding.repositories.listings.ListingsRepository;
//import amiss.wedding.repositories.listings.SocialMediaRepository;
//import amiss.wedding.repositories.users.ClientRepository;
//import amiss.wedding.repositories.users.RoleRepository;
//import amiss.wedding.repositories.users.UserRepository;
//import amiss.wedding.storage.StorageService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//import java.io.File;
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Optional;
//import java.util.Set;
//
//@Configuration
//public class CategoryCommandLineRunner {
//
//    @Bean
//    CommandLineRunner initDatabase(PasswordEncoder passwordEncoder,
//                                   PlanRepository planRepository,
//                                   UserRepository userRepository,
//                                   CategoryRepository categoryRepository,
//                                   CountryRepository countryRepository,
//                                   RegionRepository regionRepository,
//                                   CityRepository cityRepository,
//                                   DistrictRepository districtRepository,
//                                   RoleRepository roleRepository,
//                                   AttributeRepository attributeRepository,
//                                   ClientRepository clientRepository,
//                                   ListingsRepository listingsRepository,
//                                   SocialMediaRepository socialMediaRepository,
//                                   AttributeValueRepository attributeValueRepository,
//                                   ArticleRepository articleRepository,
//                                   ArticlePhotoRepository articlePhotoRepository,
//                                   MiniArticleRepository miniArticleRepository){
//
//        HashMap<String, Role> rolesHashMap = new HashMap<String, Role>();
//
//        if (countryRepository.count() == 0) {
//            initCitiesFromjson(countryRepository,
//                    regionRepository,
//                    cityRepository,
//                    districtRepository);
//        }
//        if (roleRepository.count() == 0) {
//                rolesHashMap = initRole(roleRepository);
//                initRoleUndUser(rolesHashMap, userRepository, passwordEncoder, clientRepository);
//
//                initCategory(categoryRepository, attributeRepository, planRepository);
//
//            initListing(countryRepository,
//                    categoryRepository,
//                    attributeRepository,
//                    planRepository,
//                    regionRepository,
//                    cityRepository,
//                    districtRepository,
//                    userRepository,
//                    passwordEncoder,
//                    clientRepository,
//                    listingsRepository,
//                    attributeValueRepository,
//                    socialMediaRepository,
//                    rolesHashMap);
//        }
//
//        if (articleRepository.count() < 1) {
//            initArticle(articleRepository, articlePhotoRepository);
//        }
//        if (miniArticleRepository.count() < 1) {
//            initArticleandMiniArticle(miniArticleRepository, articleRepository, articlePhotoRepository);
//        }
//        if (listingsRepository.count() < 15) {
//            initListing_forTest(countryRepository,
//                    categoryRepository,
//                    attributeRepository,
//                    planRepository,
//                    regionRepository,
//                    cityRepository,
//                    districtRepository,
//                    userRepository,
//                    passwordEncoder,
//                    clientRepository,
//                    listingsRepository,
//                    attributeValueRepository,
//                    socialMediaRepository,
//                    rolesHashMap);
//        }
//        return args -> {
//
//        };
//    }
//
//    private void initArticle(ArticleRepository articleRepository, ArticlePhotoRepository articlePhotoRepository) {
//
//        Article article = new Article();
//        article.setReadTime(10);
//        article.setTitle("تجهيزات العروس");
//        String content = "<p>لكل فتاة قائمة تقوم بتجهيزها في فترة خطوبتها لتحديد الأشياء المهمة و الغير مهمة بالنسبة لها في حياتها الزوجية، التي تكون مبنية على رأيها و رأي زوجها المستقبلي، و تكون متناسبة مع جسدها و ذوقها و شخصيتها، فبعض الفتيات تتسائل كثيرا عن الأشياء التي يجب اقتنائها لحياة مختلفة.</p>\n" +
//                "<p>يقدم لك موقع ليلتي العديد من النصائح لتختاري ما يناسبك دون الوقوع في الحيرة و التعب الذهني و التردد، لكي لا تنسي أصغرها.</p>\n" +
//                "<h3>التحضيرات النفسية و المعنوية :</h3>\n" +
//                "<p>تعد جزء مهم قبل البدء بحياة جديدة مشتركة التي تترتب على الزوجين قبل البدء بخطوة مصيرية تتطلب النضج الكافي و القدرة على خلق لغة تواصل مميزة بين الزوجين للوصول إلى الإستقرار في العلاقة و الحب و الإهتمام والتفاهم المتبادل بين الطرفين.</p>\n" +
//                "<h3>التحضيرات الجسدية :</h3>\n" +
//                "<p>و تشمل العديد من الخطوات التي يجدر على العروس تطبيقا تبعا لوقتها و اهتماماتها</p>\n" +
//                "<ul style=\"list-style-type: circle;\">\n" +
//                "<li>إتباع ريجيم صحي لتتزودي بالعناصر الغذائية الهامة كالمعادن و البروتينات لعدم ظهور النقص على بشرتك و تكون نقية و بقمة رونقها و جاذبيتها و صحة جسدك أيضا.</li>\n" +
//                "<li>النوم بشكل كافي لكمال إطلالتك و لكي لا تتأثر بشرتك و صحتك النفسية و الجسدية في وسط كل تلك الأعمال الملزمة بإنجازها.</li>\n" +
//                "<li>\n" +
//                "<p>الاعتناء بالبشرة و الشعر و البحث عن وصفات مفيدة، فمعظم الفتيات تعاني من تقصيف الشعر و القشرة فاستخدمي الزيوت و الكريمات و البلسم لتقويته، اهتمي أيضا بوضع واقي شمسي على بشرتك قبل التعرض للشمس بنصف ساعة و كرري هذه العملية كل ساعتين.</p>\n" +
//                "</li>\n" +
//                "<li>شرب الماء بكمية كبيرة للإستفادة منها على بشرتك و نشاطك و لياقتك.</li>\n" +
//                "</ul>\n" +
//                "<p>ابدأي بكل هذا قبل بشهر عزيزتي لا تقلقي ستكون إطلالتك باهرة و مثالية.</p>\n" +
//                "<h3>التحضيرات الشخصية :</h3>\n" +
//                "<p>&nbsp;من أهم الأشياء التي تهتم بها العروس، عادة ما تبدأ العروس بشراء الملابس بعد الخطوبة فورا لكن يقدم لك موقع ليلتي نصيحة مهمة و هي عدم الإسراع بهذه الخطوة بسبب تغير وزنك و بالأخص بعد الزواج بالزيادة و النقصان و لمواكبة صيحات الموضة اول بأول، يمكنك البدء قبل الزفاف بفترة معقولة بحيث تكون بعيدة عن ايام اقتراب اليوم الموعود لتجنب التوتر و الضغط الزائد.</p>\n" +
//                "<p><strong>الملابس الداخلية و ملابس النوم</strong> : يجب أن تكون متنوعة الموديلات و الألوان و تتضمن فستان أبيض للنوم من الحرير أو الساتان الذي يعتبر من القطع الرئيسية في تجهيزاتك، الذي تختارينه بالدرجة و الموديل تبعا لذوقك و ذوق زوجك المستقبلي.</p>\n" +
//                "<p><strong>البيجامات</strong> : يجب أن تتنوع خاماتها بين الحرير و القطن فاختاري الملابس المنزلية الأنيقة الفضفاضة و التي يمكن ارتدائها بجميع تغيرات جسدك و وزنك، التي تعطي إطلالات ظريفة يومية</p>\n" +
//                "<p><strong>الملابس النهارية</strong> :كملابس العمل و التسوق و الخروج مع الأصدقاء و تعتمد على اسلوب ملابسك القديم، احرصي على التنوع في خاماتها و ألوانها، و تشتمل الأحذية و الحقائب المريحة و الكاجوال</p>\n" +
//                "<p><strong>الملابس الرسمية:</strong> إذا كنتي سيدة أعمال تحتاجين لها بكثرة و بشكل يومي، كالجاكيت الرسمي و البنطال القماشي، يندرج تحت مسمى الملابس الرسمية أيضا فساتين الساتان أو الكروشيه، و طبعا مصحوبة بكعب عالي و رفيع و حقيبة صغيرة لتعطي إطلالة مليئة برزانة لم تعهد قبلا.</p>\n" +
//                "<p><strong>فساتين السهرة:</strong> يكفي شراء فستانين أو أكثر للمناسبات العائلية و القريبة حسب ميزانيتك و للمناسبات الخاصة بينك و بين زوجك كعيد زواجكما أو أعياد الميلاد و غيرها.</p>\n" +
//                "<p><strong>الملابس الرياضية:</strong> إذا كنت من محبي الرياضة و النوادي و أيضا مهتمة لرشاقتك و المحافظة عليها فيجب اقتناء زي أو اثنين،تندرج معها ملابس السباحة التي تحتاجينها في فترة شهر العسل.</p>\n" +
//                "<p>أهم الأشياء التي تقتنيها العروس بحب هي الإكسسوارات تعد من أهم الأشياء لك كزوجة، و التي تختلف عن إكسسوارات عرسك الباهظة و الكبيرة، بينما الإكسسوارات اليومية احرصي على اختيار أشياء ناعمة ذات ألوان محايدة تعطي رونقا رقيقا لك و إطلالة من الطراز الرفيع، تشتمل الخاتم و العقد و الأساور و الحلق و الساعة و النظارات الشمسية، اجعليها كمرحلة أخيرة لتطبيق ألوانها مع ألوان ملابسك.</p>\n" +
//                "<p><strong>العبايات:</strong> من أهم الأشياء التي تسارع الفتاة السعودية حصرا على اقتنائها، فهي كغيرها من الثياب تعد مهمة جدا لإستقبال الضيوف، لها العديد من الموديلات و الألوان الملفتة و الجذابة.</p>\n" +
//                "<p>فموقع ليلتي يساعدك كثيرا في إختيار الأنسب لك دائما.</p>\n" +
//                "<p><strong>العطور:</strong> لها اهتمامات بالغة من قبل الجميع، فالروائح لها دورا كبيرا بتشكيل الذكريات الجميلة، كالعطور العربية التي تحافظ على رائحة تدوم طويلا كالعنبر و المسك، و العطور الأجنبية التي تختارينها تبعا لذوقك و شخصيتك، احرصي على اختيار روائح مميزة و فريدة لتجعليهم يتسائلون عن صاحبة هذه الرائحة الساحرة.</p>\n" +
//                "<p><strong>الحقائب السفرية:</strong> يجب إختيار أحجام مختلفة لاستعمالها في نقل ملابسك و أغراضك لمنزلك الجديد، و أيضا استخدامها في أي سفرة مع زوجك، فاحرصي على إختيار ذات النوع الجيد و المميز.</p>\n" +
//                "<p><strong>مستلزمات شخصية:</strong> للحفاظ و العناية اليومية لجسمك كالبراون و مستحضرات الاستحمام و فرشاة الأسنان و مطهر الفم و شباشب و المناشف لليدين و الوجه و الجسم و البرانص، كل تلك الأشياء تعد مهمة جدا لابد من أخذها بعين الإعتبار.</p>\n" +
//                "<p><strong>مستلزمات أخرى:</strong> تختلف تبعا للعادات و التقاليد و التي يجب عدم إهمالها، كالمفرش السادة و المطرز الذي يتناسب مع غرفة النوم، كالشموع و البخور و المعطرات و الصابون و معطرات الثياب.</p>\n" +
//                "<p><strong>مستحضرات التجميل و الكريمات:</strong> لكل فتاة قائمة لا محدودة من المكياجات و مستحضرات التجميل و تختلف من فتاة لأخرى و تفضيلاتها و ذوقها و تعد من الأشياء التي تجذب جميع العروسات لإكمال احتياجاتها منها، كأحمر الشفاه و البلشر و الكونتور و التنت و غيرها من الأشياء المهمة .</p>\n" +
//                "<p>يحرص موقع ليلتي على أخبارك بأهم المعلومات التي تساعدك في حياتك الزوجية و ما قبل الزوجية، لكي لا تشغلي عقلك و تتوترين و إكتمال كل خطوات حياتك على أكمل وجه.</p>";
//
//        article.setContent(content);
//        articleRepository.save(article);
//
//        ArticlePhoto articleCover = new ArticlePhoto();
//        articleCover.setFileName("WhatsApp Image 2023-03-03 at 19.20.04.jpeg");
//        articleCover.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F1%2FWhatsApp%20Image%202023-03-03%20at%2019.20.04.jpeg");
//        articleCover.setSize(31);
//        articleCover.setArticle(article);
//        articlePhotoRepository.save(articleCover);
//        article.setCoverImg(articleCover);
//        articleRepository.save(article);
//
//
//        ArticlePhoto articlePhoto = new ArticlePhoto();
//        articlePhoto.setFileName("WhatsApp Image 2023-03-03 at 19.20.01.jpeg");
//        articlePhoto.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F1%2FWhatsApp%20Image%202023-03-03%20at%2019.20.01.jpeg");
//        articlePhoto.setSize(24);
//        articlePhoto.setArticle(article);
//        articlePhotoRepository.save(articlePhoto);
//
//        ArticlePhoto articlePhoto2 = new ArticlePhoto();
//        articlePhoto2.setFileName("WhatsApp Image 2023-03-03 at 19.20.01 (1).jpeg");
//        articlePhoto2.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F1%2FWhatsApp%20Image%202023-03-03%20at%2019.20.01%20(1).jpeg");
//        articlePhoto2.setSize(22);
//        articlePhoto2.setArticle(article);
//        articlePhotoRepository.save(articlePhoto2);
//
//        ArticlePhoto articlePhoto3 = new ArticlePhoto();
//        articlePhoto3.setFileName("WhatsApp Image 2023-03-03 at 19.20.02.jpeg");
//        articlePhoto3.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F1%2FWhatsApp%20Image%202023-03-03%20at%2019.20.02.jpeg");
//        articlePhoto3.setSize(40);
//        articlePhoto3.setArticle(article);
//        articlePhotoRepository.save(articlePhoto3);
//
//        ArticlePhoto articlePhoto4 = new ArticlePhoto();
//        articlePhoto4.setFileName("WhatsApp Image 2023-03-03 at 19.20.03.jpeg");
//        articlePhoto4.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F1%2FWhatsApp%20Image%202023-03-03%20at%2019.20.03.jpeg");
//        articlePhoto4.setSize(27);
//        articlePhoto4.setArticle(article);
//        articlePhotoRepository.save(articlePhoto4);
//
//    }
//
//    private void initArticleandMiniArticle(MiniArticleRepository miniArticleRepository,
//                                           ArticleRepository articleRepository, ArticlePhotoRepository articlePhotoRepository) {
//
//        Article article = new Article();
//        article.setReadTime(10);
//        article.setTitle("أنواع طرحات العروس");
//        String content = "<p>في أجمل ليالي العمر يجب أن تتألق العروس بفستان و طرحة تكون أنيقة بشكل لافت و متناسق مع شكل الفستان لتكمل إطلالة فخمة و مبهرة ، و لا شكك أيضا أن كل فتاة تحلم بإطلالة تبرز اناقتها و جمالها فإن الطرحة تعد من الإكسسوارات الأنثوية المهمة في ليلة الزفاف ، وتعد مندمجة مع الثقافات التي تناقلتها الأجيال عبر الزمن منذ القدم فلها قيمة عاطفية و معنوية للعروس .</p>\n" +
//                "<p>إن موقع ليلتي يساعدك كثيرا بأختيار ما يناسب شخصيتك و ذوقك و يعرفك على كل ما يخطر ببالك من أشياء و مستلزمات عرسك لتكون ليلتك ليلة من ليالي أحلامك و ذكرى جميلة طوال عمرك لإختيار الأفضل و كما يقال المعرفة هي القدرة على كل شيء منها القدرة على الإختيار بشكل صحيح .</p>\n" +
//                "<p>فالطرحات لها أشكال و أنواع مختلفة تتميز كل منها عن الأخرى بأشياء كثيرة :</p>";
//        article.setContent(content);
//        articleRepository.save(article);
//
//        ArticlePhoto articleCover = new ArticlePhoto();
//        articleCover.setFileName("WhatsApp Image 2023-02-17 at 10.34.24.jpeg");
//        articleCover.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F2%2FWhatsApp%20Image%202023-02-17%20at%2010.34.24.jpeg");
//        articleCover.setSize(20);
//        articleCover.setArticle(article);
//        articlePhotoRepository.save(articleCover);
//        article.setCoverImg(articleCover);
//        articleRepository.save(article);
//
//        MiniArticle miniArticle = new MiniArticle();
//        miniArticle.setArticle(article);
//        miniArticle.setTitle("الطرحة القصيرة");
//        content = "<p>النوع المفضل لدى الكثير ، تغطي الوجه حتى آخر الذقن ، تحتوي على طبقة واحدة من الشيفون ، تتميز أيضا بأنها تتناسب مع جميع موديلات الفساتين إن كان ضيقا أو&nbsp; منفوشا ، تعطي مظهرا كلاسيكيا بعيدا عن الصخب و تعد أكثر الطرحات راحة للعروس .</p>";
//        miniArticle.setContent(content);
//        miniArticleRepository.save(miniArticle);
//
//        ArticlePhoto articlePhoto = new ArticlePhoto();
//        articlePhoto.setFileName("WhatsApp Image 2023-02-17 at 10.34.23.jpeg");
//        articlePhoto.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F2%2FWhatsApp%20Image%202023-02-17%20at%2010.34.23.jpeg");
//        articlePhoto.setSize(15);
//        articlePhoto.setMiniArticle(miniArticle);
//        articlePhotoRepository.save(articlePhoto);
//
//        MiniArticle miniArticle_2 = new MiniArticle();
//        miniArticle_2.setArticle(article);
//        miniArticle_2.setTitle("الطرحة الشريطية");
//        content = "<p>&nbsp;هي شريطا من الزخارف و التصاميم الإبداعية من الدانتيل الموضوع على الشيفون، فهي تتناسب مع الفتاة التي تملك ملامح ناعمة لأنها تعطي مظهرا جذابا و الراحة و الخفة، تغني عن إكسسوارات الشعر كالتاج .</p>";
//        miniArticle_2.setContent(content);
//        miniArticleRepository.save(miniArticle_2);
//
//        ArticlePhoto articlePhoto_2 = new ArticlePhoto();
//        articlePhoto_2.setFileName("WhatsApp Image 2023-02-17 at 10.37.50.jpeg");
//        articlePhoto_2.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F2%2FWhatsApp%20Image%202023-02-17%20at%2010.37.50.jpeg");
//        articlePhoto_2.setSize(12);
//        articlePhoto_2.setMiniArticle(miniArticle_2);
//        articlePhotoRepository.save(articlePhoto_2);
//
//        MiniArticle miniArticle_3 = new MiniArticle();
//        miniArticle_3.setArticle(article);
//        miniArticle_3.setTitle("الطرحة التي تصل إلى الكتفين أو الطرحة الطائرة");
//        content = "<p>من الأنواع التي تعد أقل رسمية تصل إلى خط الكتف تحتوي على عدة طبقات من التول و أحيانا تحتوي خطا من الساتان على الحواف حسب تفضيلاتك و ذوقك ، تعطي مظهرا لا مثيل له ، تتناسب مع الفساتين الناعمة ذو الأكتاف و الصدر المكشوف.</p>";
//        miniArticle_3.setContent(content);
//        miniArticleRepository.save(miniArticle_3);
//
//        ArticlePhoto articlePhoto_3 = new ArticlePhoto();
//        articlePhoto_3.setFileName("WhatsApp Image 2023-02-17 at 10.34.20.jpeg");
//        articlePhoto_3.setPath("https://lilty.sgp1.cdn.digitaloceanspaces.com/articles%2F2%2FWhatsApp%20Image%202023-02-17%20at%2010.34.20.jpeg");
//        articlePhoto_3.setSize(24);
//        articlePhoto_3.setMiniArticle(miniArticle_3);
//        articlePhotoRepository.save(articlePhoto_3);
//
//    }
//
//    private void initCitiesFromjson(CountryRepository countryRepository,
//                                   RegionRepository regionRepository,
//                                   CityRepository cityRepository,
//                                   DistrictRepository districtRepository) {
//        if (countryRepository.count() == 0) {
//            Country country = new Country("المملكة العربية السعودية", "The Kingdom of Saudi Arabia", "KSA");
//            countryRepository.save(country);
//
//            // create Object Mapper
//            ObjectMapper mapper = new ObjectMapper();
//
//            // read JSON file and map/convert to java POJO
//            try {
//                Region[] region = mapper.readValue(new File("src/main/resources/json/regions.json"), Region[].class);
//                for (Region wert:region) {
//                    regionRepository.save(wert);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            try {
//                CityTest[] cityTests = mapper.readValue(new File("src/main/resources/json/cities.json"), CityTest[].class);
//                for (CityTest wert:cityTests) {
//                    Region region = regionRepository.findById(wert.getRegion_id()).get();
//                    City city = new City(wert.getCity_id(), wert.getName_ar(), wert.getName_en(), country, region, null, null);
//                    cityRepository.save(city);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            try {
//                DistrictTest[] districtTests = mapper.readValue(new File("src/main/resources/json/districts.json"), DistrictTest[].class);
//                for (DistrictTest wert:districtTests) {
//                    City city = cityRepository.findById(wert.getCity_id()).get();
//                    Region region = regionRepository.findById(wert.getRegion_id()).get();
//                    District district = new District(null, wert.getName_ar(), wert.getName_en(), region, city, null);
//                    districtRepository.save(district);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            City riead = cityRepository.findById(Long.valueOf(3)).get();
//            country.setCapital_city(riead);
//            countryRepository.save(country);
//
//            insertCapitalForRegion(3, 1, cityRepository, regionRepository);
//            insertCapitalForRegion(6, 2, cityRepository, regionRepository);
//            insertCapitalForRegion(14, 3, cityRepository, regionRepository);
//            insertCapitalForRegion(11, 4, cityRepository, regionRepository);
//            insertCapitalForRegion(13, 5, cityRepository, regionRepository);
//            insertCapitalForRegion(15, 6, cityRepository, regionRepository);
//            insertCapitalForRegion(1, 7, cityRepository, regionRepository);
//            insertCapitalForRegion(10, 8, cityRepository, regionRepository);
//            insertCapitalForRegion(2213, 9, cityRepository, regionRepository);
//            insertCapitalForRegion(17, 10, cityRepository, regionRepository);
//            insertCapitalForRegion(3417, 11, cityRepository, regionRepository);
//            insertCapitalForRegion(1542, 12, cityRepository, regionRepository);
//            insertCapitalForRegion(2237, 13, cityRepository, regionRepository);
//
//        }
//    }
//
//    private void insertCapitalForRegion(int city_id, int region_id,
//                                        CityRepository cityRepository, RegionRepository regionRepository) {
//        City capital = cityRepository.findById(Long.valueOf(city_id)).get();
//        Region region = regionRepository.findById(Long.valueOf(region_id)).get();
//        region.setCapital_city(capital);
//        regionRepository.save(region);
//    }
//
//    private HashMap<String, Role> initRole(RoleRepository roleRepository){
//
//        HashMap<String, Role> rolesHashMap = new HashMap<String, Role>();
//
//        RoleMapper roleMapper = new RoleMapperImpl();
//        Role role = new Role("ADMIN",null);
//        roleRepository.save(role);
//        rolesHashMap.put("ADMIN", role);
//
//        role = new Role("Employee",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Employee", role);
//
//        role = new Role("Client",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Client", role);
//
//        role = new Role("User",null);
//        roleRepository.save(role);
//        rolesHashMap.put("User", role);
//
//        role = new Role("Admin Wedding Halls",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Admin Wedding Halls", role);
//
//        role = new Role("Admin Hotels",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Admin Hotels", role);
//
//        role = new Role("Admin Hair and makeup",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Admin Hair and makeup", role);
//
//        role = new Role("Admin Photographer",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Admin Photographer", role);
//
//        role = new Role("Admin Cars",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Admin Cars", role);
//
//        role = new Role("Customer",null);
//        roleRepository.save(role);
//        rolesHashMap.put("Customer", role);
//
//        return rolesHashMap;
//
//    }
//    private void initRoleUndUser(HashMap<String, Role> rolesHashMap, UserRepository userRepository,
//                                 PasswordEncoder passwordEncoder, ClientRepository clientRepository) {
//
//        if (userRepository.findUserByEmail("admin@gmail.com").isEmpty()){
//            AppUser admin = new AppUser();
//            admin.setPassword(passwordEncoder.encode("admin!"));
//            admin.setEmail("admin@gmail.com");
//            admin.setActive(true);
//            admin.addRole(rolesHashMap.get("ADMIN"));
//            userRepository.saveAndFlush(admin);
//        }
//
//        if (userRepository.findUserByEmail("issam.ias.ias@gmail.com").isEmpty()){
//            AppUser admin = new AppUser();
//            admin.setPassword(passwordEncoder.encode("issam!"));
//            admin.setEmail("issam.ias.ias@gmail.com");
//            admin.setActive(true);
//            admin.addRole(rolesHashMap.get("ADMIN"));
//            userRepository.saveAndFlush(admin);
//        }
//
//        if (userRepository.findUserByEmail("alayoubi.issam@gmail.com").isEmpty()){
//            AppUser userClient = new AppUser();
//            userClient.setPassword(passwordEncoder.encode("issam!"));
//            userClient.setEmail("alayoubi.issam@gmail.com");
//            userClient.setActive(true);
//            userClient.addRole(rolesHashMap.get("ADMIN"));
//            userRepository.saveAndFlush(userClient);
//        }
//
//        if (userRepository.findUserByEmail("client.issam@gmail.com").isEmpty()){
//            AppUser userClient = new AppUser();
//            userClient.setPassword(passwordEncoder.encode("issam!"));
//            userClient.setEmail("client.issam@gmail.com");
//            userClient.setActive(true);
//            userClient.addRole(rolesHashMap.get("ADMIN"));
//            userRepository.saveAndFlush(userClient);
//            Client client = new Client("عصام الأيوبي", "010101010101", "الرياض", null, "المدير", userClient, null);
//            clientRepository.save(client);
//
//        }
//
//        if (userRepository.findUserByEmail("amjad@suhportal.local").isEmpty()){
//            AppUser userClient = new AppUser();
//            userClient.setPassword(passwordEncoder.encode("amjad!"));
//            userClient.setEmail("amjad@suhportal.local");
//            userClient.setActive(true);
//            userClient.addRole(rolesHashMap.get("Client"));
//            userRepository.saveAndFlush(userClient);
//            Client client = new Client("amjad", "010101010102", "جدة", null, "المدير", userClient, null);
//            clientRepository.save(client);
//        }
//
//        if (userRepository.findUserByEmail("karam@suhportal.local").isEmpty()){
//            AppUser userClient = new AppUser();
//            userClient.setPassword(passwordEncoder.encode("karam!"));
//            userClient.setEmail("karam@suhportal.local");
//            userClient.setActive(true);
//            userClient.addRole(rolesHashMap.get("Client"));
//            userRepository.saveAndFlush(userClient);
//            Client client = new Client("Karam", "010101010103", "مكة", null, "المدير", userClient, null);
//            clientRepository.save(client);
//        }
//
//        if (userRepository.findUserByEmail("orwa@suhportal.local").isEmpty()){
//            AppUser userClient = new AppUser();
//            userClient.setPassword(passwordEncoder.encode("orwa!"));
//            userClient.setEmail("orwa@suhportal.local");
//            userClient.setActive(true);
//            userClient.addRole(rolesHashMap.get("Client"));
//            userRepository.saveAndFlush(userClient);
//            Client client = new Client("Orwa", "010101010104", "جدة", null, "المدير", userClient, null);
//            clientRepository.save(client);
//        }
//
//        if (userRepository.findUserByEmail("obada@suhportal.local").isEmpty()){
//            AppUser userClient = new AppUser();
//            userClient.setPassword(passwordEncoder.encode("obada!"));
//            userClient.setEmail("obada@suhportal.local");
//            userClient.setActive(true);
//            userClient.addRole(rolesHashMap.get("Client"));
//            userRepository.saveAndFlush(userClient);
//            Client client = new Client("Obada", "010101010105", "الرياض", null, "المدير", userClient, null);
//            clientRepository.save(client);
//        }
//    }
//    private void initListing(CountryRepository countryRepository,
//                             CategoryRepository categoryRepository,
//                             AttributeRepository attributeRepository,
//                             PlanRepository planRepository,
//                             RegionRepository regionRepository,
//                             CityRepository cityRepository,
//                             DistrictRepository districtRepository,
//                             UserRepository userRepository,
//                             PasswordEncoder passwordEncoder,
//                             ClientRepository clientRepository,
//                             ListingsRepository listingsRepository,
//                             AttributeValueRepository attributeValueRepository,
//                             SocialMediaRepository socialMediaRepository,
//                             HashMap<String, Role> rolesHashMap){
//
//        // Create Client
//        AppUser userAhmad = new AppUser();
//        userAhmad.setPassword(passwordEncoder.encode("ahmad!"));
//        userAhmad.setEmail("ahmad@suhportal.local");
//        userAhmad.setActive(true);
//        userAhmad.addRole(rolesHashMap.get("Client"));
//        userRepository.saveAndFlush(userAhmad);
//        Client clientAhmad = new Client("السيد أحمد اللحام", "010101010102", "الرياض", null, "المدير", userAhmad, null);
//        clientRepository.save(clientAhmad);
//        // Creat Listing قاعات زفاف في الرياض
//        Listing listing_1 = new Listing();
//        Optional<Category> c1 = categoryRepository.findById(Long.valueOf(1));
//        listing_1.setCategory(c1.get());
//        City city_Afef = cityRepository.findById(Long.valueOf(418)).get();
//        listing_1.setCountry(countryRepository.findById(Long.valueOf(1)).get());
//        listing_1.setRegion(regionRepository.findById(Long.valueOf(1)).get());
//        listing_1.setCity(city_Afef);
//        listing_1.setDistrict(districtRepository.findByNameAndCity("حي الديرة", city_Afef));
//        listing_1.setPosition(null);
//        listing_1.setClient(clientAhmad);
//        listing_1.setPlan(planRepository.findByCategoryAndAndPlanType(c1.get(), PlanType.FREE));
//        listing_1.setTitle("قاعة حروف ليلتي");
//        listing_1.setEmail(userAhmad.getEmail());
//        listing_1.setPhone(clientAhmad.getMobile());
//        listing_1.setAddress(clientAhmad.getAddress());
//        listing_1.setDescriptions("قاعة حروف ليلتي ملخص عن الخدمة\n" +
//                "إن كنت تبحث عن الفخامة والرقي، اختر قاعة حروف ليلتي فهي واحدة من أفضل القاعات وأكثرها تميزاً من حيث المساحات الرحبة أو الخدمات المتكاملة أو الديكورات الفخمة والتي تجعل من حفلة مناسباتك تحفة فنية وذكرى خالدة، وتقع القاعة في وسط مدينة الرياض في حي القادسية وبالتالي في مكان مثالي وسهل الوصول إليه من كل المدعوين من كل مناطق الرياض، وأياً كان نوع الاحتفال الذي تريد إقامته سواءً حفل زفاف أو ملكه أو عيد ميلاد أو حفل تخرج، ومهما كان أعداد المدعوين لديك وكان احتفالك كبيراً أو صغيراً فإن حفلتك ستكون حدث مثالي ومنظم على أكمل وجه من ناحية الخدمات والتنسيق.\n" +
//                "\n" +
//                "القاعات والاتساع\n" +
//                "أكثر ما يميز القاعة هو التوزيع المثالي للمساحات بشكل يتناسب مع نوع الحفل وعدد الضيوف، وتحتوي القاعة على عدة صالات يتم تنظيمها بإشراف فريق متخصص بالحفلات ومنها:\n" +
//                "● قاعة الرجال وهي تتميز بمساحات ممتازة يمكن أن تتسع حوالي 450 ضيف من الأحبة والأصدقاء.\n" +
//                "● قاعة السيدات تتسع حوالي 500 سيدة وتحتوي على درج للزفة وممر خاص للعروس ومنصة مرتفعة.\n" +
//                "● قاعة منفصلة للطعام تتسع حوالي 550 ضيف ومنظمة بالطاولات الدائرية والكراسي المريحة.\n" +
//                "\n" +
//                "الديكورات\n" +
//                "تتسم قاعة حروف ليلتي بديكورات بقمة الفخامة والرقي بما فيها من مزيج من التصاميم العصرية والكلاسيكية مما يضفي على حفلة زفافك أجواء رومانسية مميزة، وتحتوي القاعة على أسقف مرتفعة موزع فيها الإضاءات الساحرة والثريات الكريستالية، وأما الجدران فهي مزخرفة بأنماط تفيض بالأصالة والجمال ومحاطة بالستائر المنسدلة لإعطاء الخصوصية، وتحتوي القاعة على درج للزفة مزين بأجمل باقات الورود الطبييعية ويتصل بممر رخامي براق وينتهي بمنصة مرتفعة يتوضع عليها الكوشة المصممة بطريق مذهلة والمسلط عليها إضاءة الكشافات لجعل العروس محط الأنظار، كما يوجد كنب ملكية على طرفي ممر العروس ومن خلفها موزعة الطاولات والكراسي الفخمة والتي تم اختيارها بعناية لتناسب جمالية القاعة.\n" +
//                "\n" +
//                "الخدمات\n" +
//                "توفر القاعة خدمات متكاملة ومتنوعة تلبي كافة احتياجاتك وتشمل كافة النواحي من حيث التجهيزات وتقديم الطعام والخدمة، ومنها:\n" +
//                "◾ فريق عمل متكامل تم اختياره من أصحاب الخبرات والكفاءات العالية في تنظيم الحفلات وتقديم الخدمات حيث يشمل فريق العمل مشرفين قاعات وقهوجية صبابين وصبابات ومباشرات وعمال نظافة.\n" +
//                "◾ تجهيزات كاملة داخل القاعة من حيث الإضاءة والصوت ومعدات الدي جي وليزر وبخار وكشاف عروس.\n" +
//                "◾ تحوي قاعة السيدات على مدخل مستقل للعروس وغرفة للعروس ودرج للزفة، كما يتم تنسق الكوشة بأجمل التصاميم.\n" +
//                "◾ مواقف مخصصة للسيارات وحراسة أمنية للحفل.");
//        listing_1.setStatus(Status.COMPLETED);
//        listingsRepository.save(listing_1);
//        attributeValueRepository.save(new AttributeValue("500", attributeRepository.findById(Long.valueOf(1)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("500", attributeRepository.findById(Long.valueOf(2)).get(),  listing_1, true));
//        attributeValueRepository.save(new AttributeValue("1200", attributeRepository.findById(Long.valueOf(3)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("3", attributeRepository.findById(Long.valueOf(4)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("350", attributeRepository.findById(Long.valueOf(5)).get(), listing_1,true));
//
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(6)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(7)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(8)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(9)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(10)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(11)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(12)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(13)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(14)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(15)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(16)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(17)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(18)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(19)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(20)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(21)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(22)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(23)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(24)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(25)).get(), listing_1, true));
//
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(35)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(36)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(37)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(38)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(39)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(40)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(41)).get(), listing_1, true));
//        attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(42)).get(), listing_1, true));
//
//        attributeValueRepository.save(new
//                AttributeValue("تبدأ الأسعار في قاعة التخصصي للاحتفالات و المؤتمرات للقسمين من 40.000 ريال واكثر وتختلف الاسعار بحسب الخدمات وعد المدعوين وتواريخ الحجز.",
//                attributeRepository.findById(Long.valueOf(45)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("تتراوح سعة قسم الرجال في قاعة التخصصي للاحتفالات و المؤتمرات بين 400-250 شخص.تتراوح سعة قسم السيدات في قاعة التخصصي للاحتفالات و المؤتمرات بين 400-250 شخص.تتراوح سعة صالة الطعام في قاعة التخصصي للاحتفالات و المؤتمرات بين 300-500.",
//                attributeRepository.findById(Long.valueOf(46)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("لا",
//                attributeRepository.findById(Long.valueOf(47)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("نعم",
//                attributeRepository.findById(Long.valueOf(48)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("نعم",
//                attributeRepository.findById(Long.valueOf(49)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("لا",
//                attributeRepository.findById(Long.valueOf(50)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("100",
//                attributeRepository.findById(Long.valueOf(51)).get(), listing_1, true));
//        attributeValueRepository.save(new
//                AttributeValue("حفل خطوبة, حفل استقبال مولود جديد, حفل تخرج, حفل عيد ميلاد, افتتاحيات/ اجتماعات و مؤتمرات, ليلة الغمرة",
//                attributeRepository.findById(Long.valueOf(52)).get(), listing_1, true));
//    }
//
//    private void initListing_forTest(CountryRepository countryRepository,
//                             CategoryRepository categoryRepository,
//                             AttributeRepository attributeRepository,
//                             PlanRepository planRepository,
//                             RegionRepository regionRepository,
//                             CityRepository cityRepository,
//                             DistrictRepository districtRepository,
//                             UserRepository userRepository,
//                             PasswordEncoder passwordEncoder,
//                             ClientRepository clientRepository,
//                             ListingsRepository listingsRepository,
//                             AttributeValueRepository attributeValueRepository,
//                             SocialMediaRepository socialMediaRepository,
//                             HashMap<String, Role> rolesHashMap){
//
//        for (int i = 0; i < 25; i++) {
//
//            AppUser user = new AppUser();
//            user.setPassword(passwordEncoder.encode("ahmad" + i + "!"));
//            user.setEmail(i + "ahmad" + i + "@suhportal" + i + ".local");
//            user.setActive(true);
//            user.addRole(rolesHashMap.get("Client"));
//            userRepository.saveAndFlush(user);
//            Client client = new Client("السيد " + i + " اللحام", "010101010102", "الرياض", null, "المدير", user, null);
//            clientRepository.save(client);
//            // Creat Listing قاعات زفاف في الرياض
//            Listing listing = new Listing();
//            Optional<Category> c1 = categoryRepository.findById(Long.valueOf(1));
//            listing.setCategory(c1.get());
//            City city_Afef = cityRepository.findById(Long.valueOf(418)).get();
//            listing.setCountry(countryRepository.findById(Long.valueOf(1)).get());
//            listing.setRegion(regionRepository.findById(Long.valueOf(1)).get());
//            listing.setCity(city_Afef);
//            listing.setDistrict(districtRepository.findByNameAndCity("حي الديرة", city_Afef));
//            listing.setPosition(null);
//            listing.setClient(client);
//            listing.setPlan(planRepository.findByCategoryAndAndPlanType(c1.get(), PlanType.FREE));
//            String title = "قاعة الرياض" + i;
//            listing.setTitle(title);
//            listing.setEmail(user.getEmail());
//            listing.setPhone(client.getMobile());
//            listing.setAddress(client.getAddress());
//            listing.setDescriptions(title + " ملخص عن الخدمة\n" +
//                    "إن كنت تبحث عن الفخامة والرقي، اختر قاعة حروف ليلتي فهي واحدة من أفضل القاعات وأكثرها تميزاً من حيث المساحات الرحبة أو الخدمات المتكاملة أو الديكورات الفخمة والتي تجعل من حفلة مناسباتك تحفة فنية وذكرى خالدة، وتقع القاعة في وسط مدينة الرياض في حي القادسية وبالتالي في مكان مثالي وسهل الوصول إليه من كل المدعوين من كل مناطق الرياض، وأياً كان نوع الاحتفال الذي تريد إقامته سواءً حفل زفاف أو ملكه أو عيد ميلاد أو حفل تخرج، ومهما كان أعداد المدعوين لديك وكان احتفالك كبيراً أو صغيراً فإن حفلتك ستكون حدث مثالي ومنظم على أكمل وجه من ناحية الخدمات والتنسيق.\n" +
//                    "\n" +
//                    "القاعات والاتساع\n" +
//                    "أكثر ما يميز القاعة هو التوزيع المثالي للمساحات بشكل يتناسب مع نوع الحفل وعدد الضيوف، وتحتوي القاعة على عدة صالات يتم تنظيمها بإشراف فريق متخصص بالحفلات ومنها:\n" +
//                    "● قاعة الرجال وهي تتميز بمساحات ممتازة يمكن أن تتسع حوالي 450 ضيف من الأحبة والأصدقاء.\n" +
//                    "● قاعة السيدات تتسع حوالي 500 سيدة وتحتوي على درج للزفة وممر خاص للعروس ومنصة مرتفعة.\n" +
//                    "● قاعة منفصلة للطعام تتسع حوالي 550 ضيف ومنظمة بالطاولات الدائرية والكراسي المريحة.\n" +
//                    "\n" +
//                    "الديكورات\n" +
//                    "تتسم قاعة حروف ليلتي بديكورات بقمة الفخامة والرقي بما فيها من مزيج من التصاميم العصرية والكلاسيكية مما يضفي على حفلة زفافك أجواء رومانسية مميزة، وتحتوي القاعة على أسقف مرتفعة موزع فيها الإضاءات الساحرة والثريات الكريستالية، وأما الجدران فهي مزخرفة بأنماط تفيض بالأصالة والجمال ومحاطة بالستائر المنسدلة لإعطاء الخصوصية، وتحتوي القاعة على درج للزفة مزين بأجمل باقات الورود الطبييعية ويتصل بممر رخامي براق وينتهي بمنصة مرتفعة يتوضع عليها الكوشة المصممة بطريق مذهلة والمسلط عليها إضاءة الكشافات لجعل العروس محط الأنظار، كما يوجد كنب ملكية على طرفي ممر العروس ومن خلفها موزعة الطاولات والكراسي الفخمة والتي تم اختيارها بعناية لتناسب جمالية القاعة.\n" +
//                    "\n" +
//                    "الخدمات\n" +
//                    "توفر القاعة خدمات متكاملة ومتنوعة تلبي كافة احتياجاتك وتشمل كافة النواحي من حيث التجهيزات وتقديم الطعام والخدمة، ومنها:\n" +
//                    "◾ فريق عمل متكامل تم اختياره من أصحاب الخبرات والكفاءات العالية في تنظيم الحفلات وتقديم الخدمات حيث يشمل فريق العمل مشرفين قاعات وقهوجية صبابين وصبابات ومباشرات وعمال نظافة.\n" +
//                    "◾ تجهيزات كاملة داخل القاعة من حيث الإضاءة والصوت ومعدات الدي جي وليزر وبخار وكشاف عروس.\n" +
//                    "◾ تحوي قاعة السيدات على مدخل مستقل للعروس وغرفة للعروس ودرج للزفة، كما يتم تنسق الكوشة بأجمل التصاميم.\n" +
//                    "◾ مواقف مخصصة للسيارات وحراسة أمنية للحفل.");
//            listing.setStatus(Status.COMPLETED);
//            listingsRepository.save(listing);
//            attributeValueRepository.save(new AttributeValue("500", attributeRepository.findById(Long.valueOf(1)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("500", attributeRepository.findById(Long.valueOf(2)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("1200", attributeRepository.findById(Long.valueOf(3)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("3", attributeRepository.findById(Long.valueOf(4)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("350", attributeRepository.findById(Long.valueOf(5)).get(), listing, true));
//
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(6)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(7)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(8)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(9)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(10)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(11)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(12)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(13)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(14)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(15)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(16)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(17)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(18)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(19)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(20)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(21)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(22)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(23)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(24)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(25)).get(), listing, true));
//
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(35)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(36)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(37)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(38)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(39)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(40)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(41)).get(), listing, true));
//            attributeValueRepository.save(new AttributeValue("true", attributeRepository.findById(Long.valueOf(42)).get(), listing, true));
//
//            attributeValueRepository.save(new
//                    AttributeValue("تبدأ الأسعار في قاعة التخصصي للاحتفالات و المؤتمرات للقسمين من 40.000 ريال واكثر وتختلف الاسعار بحسب الخدمات وعد المدعوين وتواريخ الحجز.",
//                    attributeRepository.findById(Long.valueOf(45)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("تتراوح سعة قسم الرجال في قاعة التخصصي للاحتفالات و المؤتمرات بين 400-250 شخص.تتراوح سعة قسم السيدات في قاعة التخصصي للاحتفالات و المؤتمرات بين 400-250 شخص.تتراوح سعة صالة الطعام في قاعة التخصصي للاحتفالات و المؤتمرات بين 300-500.",
//                    attributeRepository.findById(Long.valueOf(46)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("لا",
//                    attributeRepository.findById(Long.valueOf(47)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("نعم",
//                    attributeRepository.findById(Long.valueOf(48)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("نعم",
//                    attributeRepository.findById(Long.valueOf(49)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("لا",
//                    attributeRepository.findById(Long.valueOf(50)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("100",
//                    attributeRepository.findById(Long.valueOf(51)).get(), listing, true));
//            attributeValueRepository.save(new
//                    AttributeValue("حفل خطوبة, حفل استقبال مولود جديد, حفل تخرج, حفل عيد ميلاد, افتتاحيات/ اجتماعات و مؤتمرات, ليلة الغمرة",
//                    attributeRepository.findById(Long.valueOf(52)).get(), listing, true));
//
//
//            SocialMedia instagram = new SocialMedia(i + "INSTAGRAM.lilte", listing, SocialMediaTyp.INSTAGRAM);
//            socialMediaRepository.save(instagram);
//            SocialMedia facebok = new SocialMedia(i + "FACEBOOK.lilte.com", listing, SocialMediaTyp.FACEBOOK);
//            socialMediaRepository.save(facebok);
//            SocialMedia whatsapp = new SocialMedia(i + "WHATSAPP.lilte.com", listing, SocialMediaTyp.WHATSAPP);
//            socialMediaRepository.save(whatsapp);
//            SocialMedia linkedin = new SocialMedia(i + "LINKEDIN.lilte.com", listing, SocialMediaTyp.LINKEDIN);
//            socialMediaRepository.save(linkedin);
//            SocialMedia telegram = new SocialMedia(i + "TELEGRAM.lilte.com", listing, SocialMediaTyp.TELEGRAM);
//            socialMediaRepository.save(telegram);
//            SocialMedia twitter = new SocialMedia(i + "TWITTER.lilte.com", listing, SocialMediaTyp.TWITTER);
//            socialMediaRepository.save(twitter);
//            SocialMedia pinterst = new SocialMedia(i + "PINTEREST.lilte.com", listing, SocialMediaTyp.PINTEREST);
//            socialMediaRepository.save(pinterst);
//            SocialMedia snap = new SocialMedia(i + "SNAPCHAT.lilte.com", listing, SocialMediaTyp.SNAPCHAT);
//            socialMediaRepository.save(snap);
//            SocialMedia tiktok = new SocialMedia(i + "TIKTOK.lilte.com", listing, SocialMediaTyp.TIKTOK);
//            socialMediaRepository.save(tiktok);
//            SocialMedia privat = new SocialMedia(i + "PRIVATE.lilte.com", listing, SocialMediaTyp.PRIVATE);
//            socialMediaRepository.save(privat);
//            SocialMedia other = new SocialMedia(i + "OTHER.lilte.com", listing, SocialMediaTyp.OTHER);
//            socialMediaRepository.save(other);
//            SocialMedia youtube = new SocialMedia(i + "YOUTUBE.lilte.com", listing, SocialMediaTyp.YOUTUBE);
//            socialMediaRepository.save(youtube);
//        }
//    }
//
//    private void initCategory(CategoryRepository categoryRepository, AttributeRepository attributeRepository, PlanRepository planRepository) {
//
//        CategoryMapper categoryMapper = new CategoryMapperImpl();
//
//        Category c1 = categoryMapper.categoryDtoToCategory(new CategoryDto("قاعات الزفاف",
//                "افضل قاعات في المملكة لحفل زفافك أو" +
//                        " خطوبتك ستجدها بين يديك في موقعنا، اختر قاعة الزفاف التي تناسب أحلامك سواء من قاعات الفنادق " +
//                        "أو قصور افراح المملكة أو حتى الاستراحات والمطاعم وتواصل مع القائمين عليها مباشرة وانت في منزلك!",
//                true, null));
//        categoryRepository.save(c1);
//        attributeRepository.save(new Attribute("سعة قسم الرجال", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c1));
//        attributeRepository.save(new Attribute( "سعة قسم النساء", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c1));
//        attributeRepository.save(new Attribute( "السعر للشخص الواحد", ValueType.DOUBLE, AttributeType.MAIN,"ريال",true, c1));
//        attributeRepository.save(new Attribute( "عدد القاعات", ValueType.INTEGER, AttributeType.MAIN,"قاعة",true, c1));
//        attributeRepository.save(new Attribute("سعة قاعة المطعم", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c1));
//
//        attributeRepository.save(new Attribute("مشرفة قاعة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مسؤولة عبايات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مفتشة جوالات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute(" صبابات ومباشرات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("درج للزفة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("حراسة امنية", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("غرفة لكبار الزوار VIP", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("ليزر", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("بخار", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("الكوشة وتنسيق القاعة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مشروبات ساخنة وباردة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("طهي ذبائح", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مواقف سيارات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("التصوير الفوتوغرافي والفيديو", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("عاملات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("قهوجية للرجال", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مدخل مستقل للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("غرفة تجهيز للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute(" إمكانية اقامة عدة حفلات في نفس الوقت", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("إضاءة للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("دي جي - اجهزة صوتية", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute(" بوفيه الطعام", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("كيك الزفاف", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مساحة خارجية للمناسبات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("السماح بدخول الأطفال", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مساحة خارجية للمناسبات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("قاعة طعام منفصلة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("إضاءة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c1));
//
//        attributeRepository.save(new Attribute("مسافة واسعة بين الطاولات", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("معقم اليدين في اماكن محددة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("تزويد طاقم الخدمة بالقفازات والأقنعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("تطهير كامل قبل كل مناسبة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("تهوية منتظمة للقاعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مراعاة تدابير التباعد الاجتماعي", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute(" تطهير كامل قبل كل مناسبة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("ادوات طعام للاستعمال مرة واحدة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("قياس الحرارة عند المدخل", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("مناديل معقمة لكل طاولة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//        attributeRepository.save(new Attribute("تزويد طاقم الخدمة بالقفازات والأقنعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c1));
//
//        attributeRepository.save(new Attribute("كم تبلغ تكلفة حفلات الزفاف؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("كم تبلغ سعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("السعر أقل عند حجز قاعة النساء فقط؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("السعر يتضمن العشاء؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("يختلف السعر عند الحجز بأيام العطل و نهاية الاسبوع؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("يمكن الحجز من دون عشاء؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("ما هو أقل عدد لحجز القاعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//        attributeRepository.save(new Attribute("ما هي المناسبات التي يمكن استضافتها؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c1));
//
//        planRepository.save(new Plan(12, new BigDecimal("0.00"), c1, PlanType.FREE, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("500"), c1, PlanType.SILVER, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("1500"), c1, PlanType.GOLD, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("2500"), c1, PlanType.DIAMOND, null, null));
//
//
//
//        // الفنادق
//        Category c2 = categoryMapper.categoryDtoToCategory(new CategoryDto("الفنادق"
//                ,"إذا كنت ترغب باقامة حفل زفافك في احدى قاعات " +
//                "الفنادق في المملكة فإنك في المكان المناسب حيث جمعنا لك في هذه القائمة مجموعة كبيرة من قاعات" +
//                " فنادق الرياض مع صورها وتفاصيها واسعارها، كما يمكنك التواصل مع قاعة الفندق بشكل مباشر موقعنا.",
//                true, null));
//        categoryRepository.save(c2);
//        attributeRepository.save(new Attribute("عدد نجوم الفندق", ValueType.INTEGER, AttributeType.MAIN,"نجوم",true, c2));
//
//        attributeRepository.save(new Attribute("سعة قسم الرجال", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c2));
//        attributeRepository.save(new Attribute("سعة قسم النساء", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c2));
//        attributeRepository.save(new Attribute("السعر للشخص الواحد", ValueType.DOUBLE, AttributeType.MAIN,"ريال",true, c2));
//        attributeRepository.save(new Attribute("عدد القاعات", ValueType.INTEGER, AttributeType.MAIN,"قاعة",true, c2));
//        attributeRepository.save(new Attribute("سعة قاعة المطعم", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c2));
//        attributeRepository.save(new Attribute("عدد الغرف في الفندق", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c2));
//
//        attributeRepository.save(new Attribute("مشرفة قاعة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مسؤولة عبايات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مفتشة جوالات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute(" صبابات ومباشرات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("درج للزفة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("حراسة امنية", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("غرفة لكبار الزوار VIP", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("ليزر", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("بخار", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("الكوشة وتنسيق القاعة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("قاعة طعام منفصلة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("إضاءة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مشروبات ساخنة وباردة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("طهي ذبائح", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مواقف سيارات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("التصوير الفوتوغرافي والفيديو", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("عاملات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("قهوجية للرجال", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مدخل مستقل للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("غرفة تجهيز للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute(" إمكانية اقامة عدة حفلات في نفس الوقت", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("إضاءة للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("دي جي - اجهزة صوتية", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute(" بوفيه الطعام", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("كيك الزفاف", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مساحة خارجية للمناسبات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("السماح بدخول الأطفال", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مساحة خارجية للمناسبات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("جناح مجاني للعروسين ليلة الزفاف", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c2));
//
//
//        attributeRepository.save(new Attribute("مسافة واسعة بين الطاولات", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("معقم اليدين في اماكن محددة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("تزويد طاقم الخدمة بالقفازات والأقنعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("تطهير كامل قبل كل مناسبة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("تهوية منتظمة للقاعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مراعاة تدابير التباعد الاجتماعي", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute(" تطهير كامل قبل كل مناسبة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("ادوات طعام للاستعمال مرة واحدة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("قياس الحرارة عند المدخل", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("مناديل معقمة لكل طاولة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//        attributeRepository.save(new Attribute("تزويد طاقم الخدمة بالقفازات والأقنعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c2));
//
//        attributeRepository.save(new Attribute("كم تبلغ تكلفة حفلات الزفاف؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("كم تبلغ سعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("السعر أقل عند حجز قاعة النساء فقط؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("ما هي المناسبات التي يستقبلها؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("ما هي الخدمات والمرافق؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("في أي وقت تغلق القاعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("كيفية الوصول الى الفندق؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("السعر يتضمن العشاء؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("يختلف السعر عند الحجز بأيام العطل و نهاية الاسبوع؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("يمكن الحجز من دون عشاء؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//        attributeRepository.save(new Attribute("ما هو أقل عدد لحجز القاعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c2));
//
//
//        planRepository.save(new Plan(12, new BigDecimal("0.00"), c2, PlanType.FREE, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("500"), c2, PlanType.SILVER, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("1500"), c2, PlanType.GOLD, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("2500"), c2, PlanType.DIAMOND, null, null));
//
//
//
//        Category c3 = categoryMapper.categoryDtoToCategory(new CategoryDto("قصور الافراح"
//                , "وصلت إلى المكان المثالي للبحث عن استراحات للمناسبات بالرياض حيث توجد كل الخيارات من استراحات " +
//                "افراح بالرياض رخيصه إلى استراحات فخمه للمناسبات بالرياض وماعليك سوى البحث والاختيار !",
//                true, null));
//        categoryRepository.save(c3);
//
//        attributeRepository.save(new Attribute("سعة قسم الرجال", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c3));
//        attributeRepository.save(new Attribute( "سعة قسم النساء", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c3));
//        attributeRepository.save(new Attribute( "السعر للشخص الواحد", ValueType.DOUBLE, AttributeType.MAIN,"ريال",true, c3));
//        attributeRepository.save(new Attribute( "عدد القاعات", ValueType.INTEGER, AttributeType.MAIN,"قاعة",true, c3));
//        attributeRepository.save(new Attribute("سعة قاعة المطعم", ValueType.INTEGER, AttributeType.MAIN,"شخص",true, c3));
//
//        attributeRepository.save(new Attribute("مشرفة قاعة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مسؤولة عبايات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مفتشة جوالات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute(" صبابات ومباشرات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("درج للزفة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("حراسة امنية", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("غرفة لكبار الزوار VIP", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("ليزر", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("بخار", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("الكوشة وتنسيق القاعة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مشروبات ساخنة وباردة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("طهي ذبائح", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مواقف سيارات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("التصوير الفوتوغرافي والفيديو", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("عاملات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("قهوجية للرجال", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مدخل مستقل للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("غرفة تجهيز للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute(" إمكانية اقامة عدة حفلات في نفس الوقت", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("إضاءة للعروس", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("دي جي - اجهزة صوتية", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute(" بوفيه الطعام", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("كيك الزفاف", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مساحة خارجية للمناسبات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("السماح بدخول الأطفال", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مساحة خارجية للمناسبات", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("قاعة طعام منفصلة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("إضاءة", ValueType.BOOLEAN, AttributeType.GENERAL, "يوجد",true, c3));
//
//        attributeRepository.save(new Attribute("مسافة واسعة بين الطاولات", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("معقم اليدين في اماكن محددة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("تزويد طاقم الخدمة بالقفازات والأقنعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("تطهير كامل قبل كل مناسبة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("تهوية منتظمة للقاعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مراعاة تدابير التباعد الاجتماعي", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute(" تطهير كامل قبل كل مناسبة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("ادوات طعام للاستعمال مرة واحدة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("قياس الحرارة عند المدخل", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("مناديل معقمة لكل طاولة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//        attributeRepository.save(new Attribute("تزويد طاقم الخدمة بالقفازات والأقنعة", ValueType.BOOLEAN, AttributeType.COVID, "يوجد",true, c3));
//
//        attributeRepository.save(new Attribute("كم تبلغ تكلفة حفلات الزفاف؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("كم تبلغ سعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("السعر أقل عند حجز قاعة النساء فقط؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("السعر يتضمن العشاء؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("يختلف السعر عند الحجز بأيام العطل و نهاية الاسبوع؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("يمكن الحجز من دون عشاء؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("ما هو أقل عدد لحجز القاعة؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//        attributeRepository.save(new Attribute("ما هي المناسبات التي يمكن استضافتها؟", ValueType.TEXT, AttributeType.QUESTIONS, "جواب",true, c3));
//
//        planRepository.save(new Plan(12, new BigDecimal("0.00"), c3, PlanType.FREE, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("500"), c3, PlanType.SILVER, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("1500"), c3, PlanType.GOLD, null, null));
//        planRepository.save(new Plan(12, new BigDecimal("2500"), c3, PlanType.DIAMOND, null, null));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto("الاستراحات"
//                , "وصلت إلى المكان المثالي للبحث عن استراحات للمناسبات في المملكة حيث توجد كل الخيارات من استراحات" +
//                " افراح المملكة رخيصه إلى استراحات فخمه للمناسبات بالرياض وماعليك سوى البحث والاختيار !",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto("المطاعم",
//                 "أفخم قاعات مطاعم المملكة بين يديك الأن،" +
//                         " وكل ماعليك هو اختيار القاعة التي تناسبك والضغط على زر التواصل لتحصل على كل التفاصيل والأسعار!",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "التصوير الفوتوغرافي والفيديو",
//                "اختر عبر زفاف.نت افضل استديو تصوير زواجات في المملكة وتعرف من خلاله على أرقى مصورين الزواجات " +
//                        "وأكثرهم خبرة في هذا المجال، تواصل معهم عبر الأرقام التي وضعناها لك واستعلم عن كل التفاصيل.",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "دعوة زواج",
//                "أبهر المدعوين لحفل زفافك بأفخم بطاقات دعوة زواج تتميز بها مطابع المملكة" +
//                        " تواصل معهم استفسر عن الأسعار واحجز تصميمك الخاص سريعاً من أهم محلات كروت زواج بالمملكة.",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "كوش وتنسيق حفلات",
//                "لم يعد اختيار شركة تنسيق حفلات الزفاف في المملكة بالأمر الصعب، لأنه بإمكانك الان تصفح افضل الخيارات من شركات تنظيم الاعراس وشركات كوش افراح في مدين" +
//                        "ة المملكة، بمجرد دخولك الى موقعنا لاختيار أفضل شركات تنسيق حفلات زواج في المملكة.",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "فستان زفاف",
//                "هنا ستجدي كافة محلات فساتين زفاف في المملكة، حيث نقدم لك مجموعة كبيرة من فساتين عرايس المملكة والمتاجر "
//                        + "المتخصصة في بيع وتأجير فساتين الزفاف وحتى دور الأزياء المتخصصة بتفصيل فساتين افراح.",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "الشعر والمكياج",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "كيك الزفاف",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "بوفيه مفتوح وضيافة",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "زفات ودي جي",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "خواتم ومجوهرات الزفاف",
//                "يجب المعالجة",
//                true, null)));
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "مراكز تجميل و عناية بالبشرة",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "توزيعات الافراح والمناسبات",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "فساتين سهرة وخطوبة",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "زهور زفاف",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "سيارات زفة",
//                "يجب المعالجة",
//                true, null)));
//
//        categoryRepository.save(categoryMapper.categoryDtoToCategory(new CategoryDto(
//                "عبايات",
//                "يجب المعالجة",
//                true, null)));
//    }
//}
