package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class RegionNotFoundException extends ApplicationRuntimeException {

    public RegionNotFoundException(Long id){
        super("The Region with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
