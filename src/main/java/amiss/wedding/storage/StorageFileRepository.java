package amiss.wedding.storage;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StorageFileRepository extends JpaRepository<StorageFile,Long> {
    StorageFile getByRelativePath(String relativePath);
}
