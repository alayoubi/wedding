import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '@core/config/api.config';

@Injectable({
  providedIn: 'root'
})
export class ListingPhotoService {

  constructor(private http: HttpClient) {}

  uploadsListingPhoto(listingId: number, formData: FormData): Observable<any> {
    return this.http.post<any>(API.LISTINGSPHOTO.LISTING.ID(listingId), formData);
  }
}
