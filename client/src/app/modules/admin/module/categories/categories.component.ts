import { Category } from 'src/app/models/category/Category';
import { CategoryService } from 'src/app/services/category/category.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
})

export class CategoriesComponent implements OnInit {
  listCategory!: Category[];
  displayedColumns: string[] = [
    'id',
    'name',
    'active',
    'action',

];
  dataSource: any;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private CategoryService: CategoryService,
    private dialog: MatDialog
  ) {}

  ngAfterViewInit() {}

  ngOnInit(): void {
    this.getCategory();
  }

  getCategory() {
    this.CategoryService.getCategories().subscribe({
      next: (res) => {
        this.listCategory = res;
        this.dataSource = new MatTableDataSource(this.listCategory);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
    });





  }


  editCategory(row: any) {
    this.dialog
      .open(DialogComponent, {
        width: '70%',
        data: row,
      })
      .afterClosed()
      .subscribe((val) => {
        if (val === 'update') {
          this.getCategory();
        }
      });
  }

  deleteCategory(id: number) {
    this.CategoryService.deleteCategory(id).subscribe({
      next: (res) => {
        alert('نم حذف التصنيف بنجاح');
      },
      error: () => {
        alert('حدث خطأ اثناء حذف التصنيف');
      },
    });
  }

  openDialog() {
    this.dialog
      .open(DialogComponent, {
        width: '70%',
      })
      .afterClosed()
      .subscribe((val) => {
        if (val === 'save') {
        }
      });

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
