import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '@core/services/auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private r: Router,
    private authService: AuthService,
    private snackBar: MatSnackBar
  ) {}
  isRegistered: boolean = false;
  menu_icon_variable: boolean = false;
  menuVariable: boolean = false;

  isMenuOpen: boolean = false;
  ngOnInit(): void {
    if (localStorage.getItem('access_token')) {
      this.isRegistered = true;
    }
  }
  openMenu() {
    this.isMenuOpen = !this.isMenuOpen;
    this.menu_icon_variable = !this.menu_icon_variable;
  }
  display: any;
  isBlock: boolean = false;
  className: string = 'fa fa-bars';
  onClick() {
    if (!this.isBlock) {
      this.display = 'block';
      this.isBlock = true;
      this.className = 'fa fa-times';
    } else {
      this.display = 'none';
      this.isBlock = false;
      this.className = 'fa fa-bars';
    }
  }

  logout() {
    this.authService.logout().subscribe(
      (res: any) => {
        // this.registerForm.reset();
      this.isRegistered = false;

        this.snackBar.open('✔ تم تسجيل الخروج بنجاح', 'تم', {
          duration: 2000,
          panelClass: [
            'green-snackbar',
            'login-snackbar',
            'my-custom-snackbar',
          ],
        });
        localStorage.removeItem('access_token')
        localStorage.removeItem('refresh_token');
        setTimeout(() => {
          this.r.navigate(['/']);
        }, 3000);
      },
      (error) => {
        this.snackBar.open(`${error.error.message}`, 'حاول مجدددا!', {
          duration: 3000,
          panelClass: ['red-snackbar', 'login-snackbar', 'my-custom-snackbar'],
        });
      }
    );
  }
}
