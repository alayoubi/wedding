package amiss.wedding.entities.articles.dto;


import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticleCategory;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.listings.Listing;
import lombok.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ArticleDto {

    private Long id;
    private String title;
    private String content;
    private int readTime;
    private String coverImg;
    private ArticlePhotoDto coverImgDto;
    private Date creat_at;
    private Set<Long> articlePhotos_ID = new HashSet();
    private Set<String> articlePhotos = new HashSet();
    private Set<ArticlePhotoDto> articlePhotoDto = new HashSet();
    private Set<Long> category_IDs = new HashSet();
    private Set<String> categories = new HashSet();
    private Set<Long> miniArticles_ID = new HashSet();
    private Set<MiniArticleDto> miniArticles = new HashSet();
    private Set<Long> listings_IDs = new HashSet();
    private Set<String> listings = new HashSet();
    private Set<Long> articleCategories_IDs = new HashSet();
    private Set<String> articleCategories = new HashSet();

    public ArticleDto(Article article){
        this.id = article.getId();
        this.title = article.getTitle();
        this.content = (article.getContent() != null) ? article.getContent() : null;
        this.readTime = article.getReadTime();
        if (article.getCoverImg() != null) {
            this.coverImg = article.getCoverImg().getPath();
            this.coverImgDto = new ArticlePhotoDto(
                    article.getCoverImg().getId(),
                    article.getCoverImg().getFileName(),
                    article.getCoverImg().getTitle(),
                    article.getCoverImg().getPath(),
                    article.getId(),
                    null,
                    article.getCoverImg().getSize()
            );
        }
        this.creat_at = Date.from(article.getUpdatedAt());
        if (article.getArticlePhotos() != null && !article.getArticlePhotos().isEmpty()) {
            for (ArticlePhoto articlePhoto: article.getArticlePhotos()) {
                this.articlePhotos.add(articlePhoto.getPath());
                this.articlePhotos_ID.add(articlePhoto.getId());
                articlePhotoDto.add(new ArticlePhotoDto(
                        articlePhoto.getId(),
                        articlePhoto.getFileName(),
                        articlePhoto.getTitle(),
                        articlePhoto.getPath(),
                        article.getId(),
                        null,
                        articlePhoto.getSize()
                ));
            }
        }
        if (article.getCategories()!= null && !article.getCategories().isEmpty()) {
            for (Category category: article.getCategories()) {
                this.category_IDs.add(category.getId());
                this.categories.add(category.getName());
            }
        }
        if (article.getMiniArticles()!= null && !article.getMiniArticles().isEmpty()) {
            for (MiniArticle miniArticle : article.getMiniArticles()) {
                this.miniArticles.add(new MiniArticleDto(miniArticle));
                this.miniArticles_ID.add(miniArticle.getId());
            }
        }
        if (article.getListings()!= null && !article.getListings().isEmpty()) {
            for (Listing listing: article.getListings()) {
                this.listings.add(listing.getTitle());
                this.listings_IDs.add(listing.getId());
            }
        }

        if (article.getArticleCategories() != null && !article.getArticleCategories().isEmpty()) {
            for (ArticleCategory articleCategory : article.getArticleCategories()) {
                this.articleCategories.add(articleCategory.getName());
                this.articleCategories_IDs.add(articleCategory.getId());
            }
        }
    }
}
