package amiss.wedding.entities.cities;


import amiss.wedding.entities.listings.Listing;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "districts")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class District{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the contact")
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", length = 100)
    @Size(max = 100, message = "Name may not be more than 100 characters")
    private String name;

    @NotNull
    @Column(name = "name_en", length = 64)
    private String name_en;

    @ManyToOne
    @JoinColumn(name="region_id", nullable=false)
    private Region region;

    @ManyToOne
    @JoinColumn(name="sub_region_id")
    private SubRegion subRegion;

    @ManyToOne
    @JoinColumn(name="city_id", nullable=false)
    private City city;

    @OneToMany(mappedBy="district")
    private Set<Listing> listing;

}
