import { API } from './../../config/api.config';
import { Observable, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Region } from '@core/models/city/Region';
@Injectable({
  providedIn: 'root',
})
export class RegionService {
  private regions: Region[];

  constructor(private http: HttpClient) {}

  getRegions(): Observable<Region[]> {
    if (this.regions) {
      // Return cached categories if they exist
      return of(this.regions);
    } else {
      // Load categories from API and cache the result
      return this.http
        .get<Region[]>(API.REGIONS.BASE)
        .pipe(tap((categories) => (this.regions = this.regions)));
    }
  }


  getRegionsByCountry(countryId: number): Observable<Region[]> {
    return this.http.get<Region[]>(API.REGIONS.COUNTRY.ID(countryId));
  }

  getActiveRegionsByCountry(countryId: number): Observable<Region[]> {
    return this.http.get<Region[]>(API.REGIONS.COUNTRY.AKTIVE(countryId));
  }
}
