package amiss.wedding.entities.listings.plans;

public enum ListingPlanStatus {

    AVAILABLE,
    NOT_AVAILABLE,
    IN_PROGRESS,
    BOOKED_OUT,
    UPCOMING,
    EXPIRED
}
