package amiss.wedding.config;

import amiss.wedding.email.EmailSettings;
import amiss.wedding.email.SettingsProvider;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

public class ReloadableMailSender {

    private JavaMailSender mailSender;
    private SettingsProvider settingsProvider;


    public ReloadableMailSender(SettingsProvider settingsProvider) {
        this.settingsProvider = settingsProvider;
        reload();
    }

    public JavaMailSender getMailSender() {
        return this.mailSender;
    }

    public void reload() {
        EmailSettings emailSettings = settingsProvider.getAllSettings().getEmailSettings();

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailSettings.getHost());
        mailSender.setPort(emailSettings.getPort());

        mailSender.setUsername(emailSettings.getUsername());
        mailSender.setPassword(emailSettings.getPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", emailSettings.getSmtp().getAuth());
        props.put("mail.smtp.starttls.enable", emailSettings.getSmtp().getStarttls());
        props.put("mail.debug", String.valueOf(emailSettings.getDebug() != null ? emailSettings.getDebug() : false));

        this.mailSender = mailSender;
    }

}
