import { Component, OnInit } from '@angular/core';
import { DATA } from '../../../../../config/data.config';

@Component({
  selector: 'app-join-us-details-card',
  templateUrl: './join-us-details-card.component.html',
  styleUrls: ['./join-us-details-card.component.css']
})
export class JoinUsDetailsCardComponent implements OnInit {

  public data = DATA

  constructor() { }

  ngOnInit(): void {
    console.log(this.data)
  }

}
