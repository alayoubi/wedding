package amiss.wedding.mappers.users;

import amiss.wedding.entities.users.AppUser;

import amiss.wedding.entities.users.dto.UserDto;


public class UserMapperImpl implements UserMapper{


    @Override
    public AppUser userDtoToUser(UserDto userDto) {

        AppUser user = new AppUser(userDto.getEmail());
        return user;
    }

    @Override
    public UserDto userToUserDto(AppUser user) {
        UserDto userDto = new UserDto(user);
        return userDto;
    }

}
