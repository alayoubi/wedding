import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMiniArticleComponent } from './add-mini-article.component';

describe('AddMiniArticleComponent', () => {
  let component: AddMiniArticleComponent;
  let fixture: ComponentFixture<AddMiniArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMiniArticleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddMiniArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
