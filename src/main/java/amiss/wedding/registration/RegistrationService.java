package amiss.wedding.registration;

import amiss.wedding.security.auth.AuthenticationResponse;
import org.springframework.http.ResponseEntity;

public interface RegistrationService {

    public ResponseEntity<AuthenticationResponse> registerNewUserAccount(RegistrationRequest request);

    public ResponseEntity confirmToken(String token);

}
