import { AddWeddingHallComponent } from './module/listing/add-wedding-hall/add-wedding-hall.component';
import { PostsComponent } from './module/posts/posts.component';
import { DashboardComponent } from './module/dashboard/dashboard.component';
import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './default/default.component';
import { AdminComponent } from './admin.component';
import { CustomersComponent } from './module/customers/customers.component';
import { CategoriesComponent } from './module/categories/categories.component';
import { AddListingCompontsComponent } from './module/listing/components/add.listing.componts/add.listing.componts.component';
import { ArticlesComponent } from './module/articles/articles/articles.component';
import { ListingsComponent } from './module/listing/components/listings/listings.component';
import { CreateAttributeComponent } from './module/listing/components/create-attribute/create-attribute.component';
import { UpdateComponent } from './module/categories/update/update.component';
import { AddArticleComponent } from './module/articles/add-article/add-article.component';

const routes: Routes = [

  {
    path: '',
    component: DefaultComponent,
    children: [
      { path: '', component: DashboardComponent },
      {path:'att',component:CreateAttributeComponent},
      { path: 'posts', component: PostsComponent },
      { path: 'customers', component: CustomersComponent },
      { path: 'categories', component: CategoriesComponent },
      { path: 'update/:id', component: UpdateComponent},

      { path: 'listings',
        children: [
      {path:'',component:ListingsComponent},
        { path: 'add', component: AddListingCompontsComponent },
        { path: 'addhall', component: AddWeddingHallComponent },
      ]
    },
      { path: 'articles', component: ArticlesComponent },
      {path: 'articles/add-article',component:AddArticleComponent}
    ],
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
