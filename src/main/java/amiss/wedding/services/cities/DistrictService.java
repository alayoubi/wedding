package amiss.wedding.services.cities;

import amiss.wedding.entities.cities.dto.CountryDto;
import amiss.wedding.entities.cities.dto.DistrictDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DistrictService {

    ResponseEntity<List<DistrictDto>> findAll();
    ResponseEntity<List<DistrictDto>> findAllByRegion(Long region_id);
    ResponseEntity<List<DistrictDto>> findAllByCity(Long city_id);
    ResponseEntity<DistrictDto> findById(Long id);
    ResponseEntity<DistrictDto> create(DistrictDto districtDto);

    ResponseEntity<DistrictDto> update(Long id, DistrictDto districtDto);
    ResponseEntity<String> remove(Long id);

}
