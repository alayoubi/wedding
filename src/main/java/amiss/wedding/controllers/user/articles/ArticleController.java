package amiss.wedding.controllers.user.articles;


import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.services.articles.ArticleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/articles")
@AllArgsConstructor
public class ArticleController {

    private final ArticleService articleService;


    @GetMapping("")
    public ResponseEntity<List<ArticleDto>> getArticles() {
        return this.articleService.findAll();
    }


    @GetMapping(path = "/{article_id}")
    public ResponseEntity<ArticleDto> getListing(@PathVariable("article_id") Long article_id){
        return this.articleService.findById(article_id);
    }
}
