import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideographersComponent } from './videographers.component';

describe('VideographersComponent', () => {
  let component: VideographersComponent;
  let fixture: ComponentFixture<VideographersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideographersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VideographersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
