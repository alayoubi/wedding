import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoupleContactLandingComponent } from './couple-contact-landing.component';

describe('CoupleContactLandingComponent', () => {
  let component: CoupleContactLandingComponent;
  let fixture: ComponentFixture<CoupleContactLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoupleContactLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoupleContactLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
