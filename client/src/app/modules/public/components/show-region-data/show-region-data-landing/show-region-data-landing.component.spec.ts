import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowRegionDataLandingComponent } from './show-region-data-landing.component';

describe('ShowRegionDataLandingComponent', () => {
  let component: ShowRegionDataLandingComponent;
  let fixture: ComponentFixture<ShowRegionDataLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowRegionDataLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowRegionDataLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
