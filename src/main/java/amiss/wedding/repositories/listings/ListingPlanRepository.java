package amiss.wedding.repositories.listings;


import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.plans.ListingPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingPlanRepository extends JpaRepository<ListingPlan, Long> {

    ListingPlan findByListing(Listing listing);
}
