import { Title } from '@angular/platform-browser';
import { title } from 'process';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-videographers',
  templateUrl: './videographers.component.html',
  styleUrls: ['./videographers.component.css']
})
export class VideographersComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('مصورين الفيديو')
   }

  ngOnInit(): void {
  }

}
