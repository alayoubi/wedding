package amiss.wedding.mappers.articles;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.articles.dto.ArticlePhotoDto;
import amiss.wedding.entities.articles.dto.MiniArticleDto;

public interface ArticlePhotoMapper {

    ArticlePhoto articlePhotoDtoToArticlePhoto(ArticlePhotoDto articlePhotoDto);
    ArticlePhotoDto ArticlePhotoToArticlePhotoDto(ArticlePhoto articlePhoto);


}
