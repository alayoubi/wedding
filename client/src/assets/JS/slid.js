const productContainers = [...document.querySelectorAll(".product")];
const nxtBtn = [...document.querySelectorAll("#right")];
const preBtn = [...document.querySelectorAll("#left")];

productContainers.forEach((item, i) => {
  let containerDimensions = item.getBoundingClientRect();
  let containerWidth = containerDimensions.width;

  nxtBtn[i].addEventListener("click", () => {
    item.scrollLeft += containerWidth;
  });

  preBtn[i].addEventListener("click", () => {
    item.scrollLeft -= containerWidth;
  });
});

alert
// (';;;;;;;')
