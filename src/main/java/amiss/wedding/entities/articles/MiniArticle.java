package amiss.wedding.entities.articles;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Listing;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "mini_articles")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MiniArticle extends BaseEntity {

    @NotNull
    @NotBlank(message = "Title may not be blank")
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @Column(name = "contents", columnDefinition="MEDIUMTEXT")
    private String content;

    @OneToMany(mappedBy="miniArticle")
    private Set<ArticlePhoto> miniArticlePhotos;
    
    @ManyToOne
    @JoinColumn(name="article_id")
    private Article article;

    @ManyToMany(mappedBy = "miniArticles")
    @JsonIgnore
    private Set<Listing> listings;
}
