package amiss.wedding.entities.cities;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Listing;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;


@Entity
@Table(name = "regions")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the contact")
    @EqualsAndHashCode.Include
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "capital_city", referencedColumnName = "id")
    private City capital_city;

    @NotNull
    @Column(name = "code", length = 2)
    private String code;

    @NotNull
    @Column(name = "name", length = 64)
    private String name;

    @NotNull
    @Column(name = "name_en", length = 64)
    private String name_en;

    @Column(name = "active", columnDefinition = "boolean  default false")
    private boolean active;
    @ManyToOne
    @JoinColumn(name="country_id")
    private Country country;

    @OneToMany(mappedBy="region")
    private Set<City> cities;

    @OneToMany(mappedBy="region")
    private Set<Listing> listing;

}
