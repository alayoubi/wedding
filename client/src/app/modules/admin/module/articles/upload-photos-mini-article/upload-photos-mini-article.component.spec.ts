import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPhotosMiniArticleComponent } from './upload-photos-mini-article.component';

describe('UploadPhotosMiniArticleComponent', () => {
  let component: UploadPhotosMiniArticleComponent;
  let fixture: ComponentFixture<UploadPhotosMiniArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadPhotosMiniArticleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadPhotosMiniArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
