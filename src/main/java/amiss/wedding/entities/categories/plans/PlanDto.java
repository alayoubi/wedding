package amiss.wedding.entities.categories.plans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlanDto {

    private Long id;
    private Integer duration;
    private BigDecimal cost;
    private Long category_id;
    private PlanType planType;
    private Set<String> features = new HashSet();
    private Integer countOfPhotos;
    private boolean video;

    public PlanDto(Plan plan){
        this.id = plan.getId();
        this.duration = plan.getDuration();
        this.cost = plan.getCost();
        this.category_id = plan.getCategory().getId();
        this.planType = plan.getPlanType();
        this.countOfPhotos = plan.getCountOfPhotos();
        this.video = plan.isVideo();
        if (!plan.getFeatures().isEmpty()) {
            for (Feature features: plan.getFeatures()) {
                this.features.add(features.getTitle());
            }
        }
    }
}
