import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Client } from '@core/models/client/Client';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  getClients(): Observable<Client[]> {
    return this.http.get<Client[]>(`http://localhost:8080/api/v1/clients`);
  }

  getClient(userId: number): Observable<Client> {
    return this.http.get<Client>(`http://localhost:8080/api/v1/clients/${userId}`);
  }
}
