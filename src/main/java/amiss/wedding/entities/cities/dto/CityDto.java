package amiss.wedding.entities.cities.dto;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import lombok.*;

import java.util.Set;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CityDto {

    private Long id;
    private String name;
    private String name_en;
    private String country;
    private String region;

    public CityDto(String name){
        this.name = name;
    }

    public CityDto(City city){
        this.id = city.getId();
        this.name = city.getName();
        this.name_en = city.getName_en();
        this.country = city.getCountry().getName();
        this.region = city.getRegion().getName();
    }
}
