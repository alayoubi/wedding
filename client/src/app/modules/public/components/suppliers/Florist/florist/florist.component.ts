import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-florist',
  templateUrl: './florist.component.html',
  styleUrls: ['./florist.component.css']
})
export class FloristComponent implements OnInit {

  constructor(title: Title) {
    title.setTitle('منسقين الأزهار')
   }

  ngOnInit(): void {
  }

}
