package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.dto.DistrictDto;

public interface DistrictMapper {
    District districtDtoToDistrict(DistrictDto districtDto);
    DistrictDto districtToDistrictDto(District district);

    DistrictDto districtToDistrictDtoBaseData(District district);

}
