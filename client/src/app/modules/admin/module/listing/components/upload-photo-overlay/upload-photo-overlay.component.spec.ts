import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPhotoOverlayComponent } from './upload-photo-overlay.component';

describe('UploadPhotoOverlayComponent', () => {
  let component: UploadPhotoOverlayComponent;
  let fixture: ComponentFixture<UploadPhotoOverlayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadPhotoOverlayComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UploadPhotoOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
