//package amiss.wedding.security.controllers;
//
//
//import amiss.wedding.security.utils.AccessTokenDto;
//import amiss.wedding.security.services.AuthService;
//import amiss.wedding.security.utils.JWTResponseDto;
//import amiss.wedding.security.utils.LoginDto;
//import lombok.RequiredArgsConstructor;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//@RequiredArgsConstructor
//@RestController
//@RequestMapping("/api/v1/auth")
//public class AuthController {
//    private final AuthService authService;
//
//
//    @PostMapping("/login")
//    public ResponseEntity<JWTResponseDto> login (@RequestBody LoginDto loginRequest){
//
//        JWTResponseDto jwtResponseDto = authService.login(loginRequest.getUsername(), loginRequest.getPassword());
//
//        return ResponseEntity.ok(jwtResponseDto);
//    }
//
//
//    @PostMapping("/refresh-token")
//    public ResponseEntity<AccessTokenDto> refreshAccessToken(@RequestParam String refreshToken) {
//
//        AccessTokenDto dto = authService.refreshAccessToken(refreshToken);
//
//        return ResponseEntity.ok(dto);
//    }
//
//
//    @PostMapping("/logout")
//    public ResponseEntity<?> logout(@RequestParam String refreshToken) {
//
//        authService.logoutUser(refreshToken);
//
//        return ResponseEntity.ok(null);
//    }
//
//}