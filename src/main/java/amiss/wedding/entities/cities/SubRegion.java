package amiss.wedding.entities.cities;

import amiss.wedding.entities.listings.Listing;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "sub_regions")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SubRegion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the contact")
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @Column(name = "name", length = 64)
    private String name;

    @NotNull
    @Column(name = "name_en", length = 64)
    private String name_en;

    @ManyToOne
    @JoinColumn(name="country_id", nullable=false)
    private Country country;

    @ManyToOne
    @JoinColumn(name="region_id", nullable=false)
    private Region region;

    @OneToMany(mappedBy="subRegion")
    private Set<Listing> listing;

    @OneToMany(mappedBy="subRegion")
    private Set<City> cities;

    @OneToMany(mappedBy="subRegion")
    private Set<District> districts;
}
