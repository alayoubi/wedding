package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.plans.ListingPlan;
import amiss.wedding.entities.listings.plans.ListingPlanDto;

public class ListingPlanMapperImpl implements ListingPlanMapper{
    /**
     * @param listingPlanDto
     * @return
     */
    @Override
    public ListingPlan listingPlanDtoToListingPlan(ListingPlanDto listingPlanDto) {
        return null;
    }

    /**
     * @param listingPlan
     * @return
     */
    @Override
    public ListingPlanDto listingPlanToListingPlanDto(ListingPlan listingPlan) {
        return new ListingPlanDto(listingPlan);
    }
}
