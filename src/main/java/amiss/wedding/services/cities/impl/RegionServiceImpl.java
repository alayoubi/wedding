package amiss.wedding.services.cities.impl;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.cities.dto.RegionDto;
import amiss.wedding.errors.CountryNotFoundException;
import amiss.wedding.errors.RegionNotFoundException;
import amiss.wedding.mappers.cities.RegionMapper;
import amiss.wedding.mappers.cities.RegionMapperImpl;
import amiss.wedding.repositories.cities.CityRepository;
import amiss.wedding.repositories.cities.CountryRepository;
import amiss.wedding.repositories.cities.RegionRepository;
import amiss.wedding.services.cities.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class RegionServiceImpl implements RegionService {

    private final CountryRepository countryRepository;
    private final RegionRepository regionRepository;
    private final CityRepository cityRepository;

    private static final RegionMapper mapper = new RegionMapperImpl();

    @Autowired
    public RegionServiceImpl(CountryRepository countryRepository,
                             RegionRepository regionRepository,
                             CityRepository cityRepository) {
        this.countryRepository = countryRepository;
        this.regionRepository = regionRepository;
        this.cityRepository = cityRepository;
    }

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<RegionDto>> findAll() {
        List<Region> regions = this.regionRepository.findAll();
        List<RegionDto> regionDtoList =
                regions.stream().map(mapper::regionToRegionDto).collect(Collectors.toList());
        return new ResponseEntity<>(regionDtoList, HttpStatus.OK);
    }

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<RegionDto>> findAllActive() {
        List<Region> regions = this.regionRepository.findAllByActive(true);
        List<RegionDto> regionDtoList =
                regions.stream().map(mapper::regionToRegionDto).collect(Collectors.toList());
        return new ResponseEntity<>(regionDtoList, HttpStatus.OK);
    }

    /**
     * @param country_id
     * @return
     */
    @Override
    public ResponseEntity<List<RegionDto>> findAllByCountry(Long country_id) {
        Optional<Country> country = this.countryRepository.findById(country_id);
        if (country.isEmpty()) {
            throw new CountryNotFoundException(country_id);
        }
        List<Region> regions = this.regionRepository.findAllByCountry(country.get());
        List<RegionDto> regionDtoList =
                regions.stream().map(mapper::regionToRegionDto).collect(Collectors.toList());
        return new ResponseEntity<>(regionDtoList, HttpStatus.OK);
    }

    /**
     * @param country_id
     * @return
     */
    @Override
    public ResponseEntity<List<RegionDto>> findAllByCountryAndActive(Long country_id) {
        Optional<Country> country = this.countryRepository.findById(country_id);
        if (country.isEmpty()) {
            throw new CountryNotFoundException(country_id);
        }
        List<Region> regions = this.regionRepository.findAllByCountryAndActive(country.get(), true);
        List<RegionDto> regionDtoList =
                regions.stream().map(mapper::regionToRegionDto).collect(Collectors.toList());
        return new ResponseEntity<>(regionDtoList, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<RegionDto> findById(Long id) {
        Optional<Region> region = this.regionRepository.findById(id);
        if (region.isEmpty()) {
            throw new RegionNotFoundException(id);
        }
        RegionDto regionDto = mapper.regionToRegionDto(region.get());
        return new ResponseEntity<>(regionDto, HttpStatus.OK);
    }

    /**
     * @param regionDto
     * @return
     */
    @Override
    public ResponseEntity<RegionDto> create(RegionDto regionDto) {
        final Region region = mapper.regionDtoToRegion(regionDto);
        final Region createdRegion = this.regionRepository.save(region);
        return new ResponseEntity<>(regionDto, HttpStatus.CREATED);
    }

    /**
     * @param id
     * @param regionDto
     * @return
     */
    @Override
    public ResponseEntity<RegionDto> update(Long id, RegionDto regionDto) {
        Optional<Region> region = this.regionRepository.findById(id);
        if (region.isEmpty()) {
            throw new RegionNotFoundException(id);
        }
        Region existingRegion = region.get();
        existingRegion.setName(regionDto.getName());
        existingRegion.setName_en(regionDto.getName_en());
        existingRegion.setCode(regionDto.getCode());
        existingRegion.setActive(regionDto.isActive());
        City city = cityRepository.findByNameIs(regionDto.getCapital_city());
        existingRegion.setCapital_city(city);
        Country country = countryRepository.findByName(regionDto.getCountry());
        existingRegion.setCountry(country);
        regionRepository.save(existingRegion);
        return new ResponseEntity<>(regionDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.regionRepository.existsById(id);
        if (!exists) {
            throw new RegionNotFoundException(id);
        }
        this.regionRepository.deleteById(id);
        return new ResponseEntity<>("Region has been removed successfully!", HttpStatus.NO_CONTENT);
    }
}
