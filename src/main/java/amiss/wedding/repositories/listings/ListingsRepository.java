package amiss.wedding.repositories.listings;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.users.client.Client;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingsRepository extends JpaRepository<Listing, Long> {

    Listing findByClient(Client client);
    List<Listing> findAllByCategory(Category category);
    List<Listing> findAllByPlan(Plan plan);
    List<Listing> findAllByRegion(Region region);
    List<Listing> findAllByRegionAndStatus(Region region, Status status, Sort sort);
    List<Listing> findAllByCity(City city);
    List<Listing> findAllByStatus(Status status);
    List<Listing> findAllByCategoryAndPlan(Category category, Plan plan);
    List<Listing> findAllByCategoryAndRegion(Category category, Region region);
    List<Listing> findAllByCategoryAndRegionAndStatus(Category category, Region region, Status status, Sort sort,
                                                      PageRequest pageRequest);
    List<Listing> findAllByCategoryAndRegionAndStatusAndPriorityNot(Category category, Region region, Status status,
                                                                    int priority,
                                                                    PageRequest ageRequest);
    List<Listing> findAllByCategoryAndCity(Category category, City city);
    List<Listing> findAllByCategoryAndStatus(Category category, Status status);
}
