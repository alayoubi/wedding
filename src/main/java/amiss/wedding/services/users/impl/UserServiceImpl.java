package amiss.wedding.services.users.impl;

import amiss.wedding.email.MailService;
import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.Role;
import amiss.wedding.entities.users.VerificationToken;
import amiss.wedding.errors.TokenNotValidException;
import amiss.wedding.mappers.users.UserMapper;
import amiss.wedding.mappers.users.UserMapperImpl;
import amiss.wedding.entities.users.dto.UserDto;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.repositories.users.UserRepository;
import amiss.wedding.registration.RegistrationRequest;
import amiss.wedding.services.users.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final UserVerificationServiceImpl userVerificationService;

    private  static final UserMapper mapper = new UserMapperImpl();
    private final MailService mailService;
    private final PasswordEncoder passwordEncoder;


    @Override
    public AppUser getUser(Principal principal) {
        return repository.findUserByEmail(principal.getName())
                .orElseThrow(() -> new UsernameNotFoundException(principal.getName()));
    }


    @Override
    public ResponseEntity<List<UserDto>> findAll() {
        List<AppUser> users = this.repository.findAll();
        List<UserDto> userDtoList =
                users.stream().map(mapper::userToUserDto).collect(Collectors.toList());
        return new ResponseEntity<>(userDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDto> findById(Long id) {
        Optional<AppUser> user = this.repository.findById(id);
        if (user.isEmpty()){
            throw  new UsernameNotFoundException("The User wih id: " + id + " was not found!");
        }
        UserDto userDto = mapper.userToUserDto(user.get());
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDto> create(UserDto userDto) {
        final AppUser user = mapper.userDtoToUser(userDto);
        user.setPassword(userDto.password);
        final AppUser createdUser = this.repository.save(user);
        return new ResponseEntity<>(userDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<UserDto> update(Long id, UserDto newUser) {
        Optional<AppUser> user = this.repository.findById(id);
        if (user.isEmpty()){
            throw  new UsernameNotFoundException("The User wih id: " + id + " was not found!");
        }
        AppUser existingUser = user.get();
        existingUser.setEmail(newUser.getEmail());
        existingUser.setRole(Role.valueOf(newUser.getRole()));

        existingUser.setActive(newUser.isActive());
        AppUser updatedUser = this.repository.save(existingUser);
        UserDto userDto = mapper.userToUserDto(updatedUser);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {

        boolean exists = this.repository.existsById(id);
        if (!exists){
            throw new CategoryNotFoundException(id);
        }
        this.repository.deleteById(id);
        return new ResponseEntity<>("User has been removed successfully", HttpStatus.NO_CONTENT);
    }
    public ResponseEntity activateUserByToken(String token) {
        AppUser user = this.userVerificationService.getUser(token);
        if (user == null) {
            throw new TokenNotValidException(token);
        }
        this.userVerificationService.invalidateToken(token);
        user.setActive(true);

        user.setVerified_at(Instant.now());
        this.repository.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * @param request RegistrationRequest has {firstName, lastName, email, password, role}
     * @return
     */
    @Override
    public AppUser registerNewUserAccount(RegistrationRequest request) {
        var user = AppUser.builder()
                .firstname(request.firstName())
                .lastname(request.lastName())
                .email(request.email())
                .password(passwordEncoder.encode(request.password()))
                .role(request.role() == null ? Role.USER : request.role())
                .build();
        var savedUser = repository.save(user);
        VerificationToken verificationToken = userVerificationService.createAndSaveToken(user);
        mailService.sendVerification(savedUser.getEmail(), verificationToken);
        return savedUser;
    }
}
