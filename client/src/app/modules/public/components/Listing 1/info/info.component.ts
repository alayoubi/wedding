import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Listing } from '@core/models/listing/Listing';
import { ListingService } from '@core/services/listing/listing.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent implements OnInit {
  @Input() listing:any
  listingAttributes: any;

  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit(): void {
  }

}
