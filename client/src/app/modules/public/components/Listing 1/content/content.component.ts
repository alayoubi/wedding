import { ListingService } from './../../../../../services/listing/listing.service';
import { Listing } from './../../../../../models/listing/Listing';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css'],
})
export class ContentComponent implements OnInit {

  @Input() listing:any
  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router,
    private titleService: Title
  ) {}

  ngOnInit(): void {

  }


}
//  this.router.navigate(['/notFound']);
