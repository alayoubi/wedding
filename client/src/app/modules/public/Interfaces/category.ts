export interface categoryInterFace {
  id: number;
  name: string;
  description: string;
  active :boolean
}
