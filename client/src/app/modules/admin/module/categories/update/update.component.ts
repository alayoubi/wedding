import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Attribute } from '@core/models/category/Attribute';
import { Category } from '@core/models/category/Category';
import { AttributeService } from '@core/services/category/attribute.service';
import { CategoryService } from '@core/services/category/category.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateComponent {
  // displayedColumns: string[] = ['id', 'name', 'description', ];
  id: any;
  categoriesForm = new FormGroup({
    description: new FormControl(''),
    name: new FormControl(''),
    active: new FormControl(''),
    cover_img: new FormControl(''),
    // plans: new FormArray([
    //     new FormGroup({
    //       planType:new FormControl('')

    //   })
    // ])
  });
  attrForm = new FormGroup({
    name: new FormControl(''),
    valueType: new FormControl(''),
    attributeType: new FormControl(''),
    unit: new FormControl(''),
    active: new FormControl(''),
  });

  constructor(
    private route: ActivatedRoute,
    private api: CategoryService,

    // private categoryService: CategoryService,
    private attributeService: AttributeService // @Inject(MAT_DIALOG_DATA) public data: any, // private snackBar: MatSnackBar,
  ) // public dialog: MatDialogRef<UpdateComponent>
  {}

  public categories: Category[];
  public mainAttributes: Attribute[];
  // public selectedCategory = this.addCategoryForm.controls.category_id.value;

  ngOnInit() {
    // this.getAllCategories();
    // this.getMainAttributesByCategory(this.selectedCategory);

    this.id = this.route.snapshot.params['id'];
    this.api
      .grtCategory(this.route.snapshot.params['id'])
      .subscribe((result) => {
        this.categoriesForm = new FormGroup({
          description: new FormControl(result['description']),
          name: new FormControl(result['name']),
          active: new FormControl(result['active']),
          cover_img: new FormControl(result['cover_img']),

        });
      });
    this.attributeService
      .getAttribute(this.route.snapshot.params['id'])
      .subscribe((result) => {
        this.attrForm = new FormGroup({
          name: new FormControl(result['name']),
          valueType: new FormControl(result['valueType']),
          attributeType: new FormControl(result['attributeType']),
          unit: new FormControl(result['unit']),
          active: new FormControl(result['']),
        });
        console.log(result);
      });
  }

  // updateCategory() {
  // this.api
  //   .putCategory(this.categoriesForm.value, this.id) /**/
  //   .subscribe({
  //     next: () => {
  //       alert('تم تحديث التصنيف بنجاح');
  //       this.categoriesForm.reset();
  //     },
  //     error: () => {
  //       alert('خطأ أثناء تحديث السجل');
  //     },
  //   });
  // console.log(this.categoriesForm.value);
  // }
}
// addCategoryForm = new FormGroup({
//   name: new FormControl<string | null>(
//     this.data?.name || '',
//     Validators.required
//   ),
//   description: new FormControl<string | null>(
//     this.data?.description || '',
//     Validators.required
//   ),
//   active: new FormControl<string | null>(
//     this.data?.active || '',
//     Validators.required
//   ),
//   cover_img: new FormControl<string | null>(
//     this.data?.cover_img || '',
//     Validators.required
//   ),

//   plan_id: new FormControl<number | null>(
//     this.data?.plan_id || '',
//     Validators.required
//   ),
//   category_id: new FormControl<number>(
//     this.data?.category_id,
//     Validators.required
//   ),
//   attributes: new FormArray<
//     FormGroup<{
//       id:FormControl<number|null>
//       unit:FormControl<string|null>
//       attribute_Id: FormControl<number | null>;
//       value: FormControl<any | null>;
//     }>
//   >([]),
// plans: new FormArray<FormGroup<{
//   id: FormControl<number | null>,
//   value: FormControl<string | null>,
//   name: FormControl<string | null>
// }>>([]),
// });
// update() {
//   this.snackBar.open(' تم تعديل التصنيف بنجاح ✔', 'تم', {
//     duration: 2000,

//     panelClass: ['green-snackbar', 'login-snackbar', 'my-custom-snackbar'],
//   });

//   this.addCategoryForm.controls.attributes.controls.forEach(
//     (attribute) => {
//       if (attribute.controls.value.value != '') {
//         const attributes = new FormGroup({
//           attribute_Id: new FormControl(attribute.controls.attribute_Id.value),
//           value: new FormControl(attribute.controls.value.value),
//         });
//         this.addCategoryForm.controls.attributes.push(
//           attributes
//         );
//       }
//     }
//   );
//   console.log(this.addCategoryForm.value);
//   this.categoryService.updateCategory(this.data?.id, this.addCategoryForm.value).subscribe(
//     (response: Category) => {
//       console.log(response);

//     },
//     (error: HttpErrorResponse) => {
//     alert(error.message);
//    },
//    () =>{
//         console.log("done");
//    }

//   );
// }

// get attributes(): FormArray {
//   return this.addCategoryForm.get('attributes') as FormArray;

// }
// public getAllCategories(): void {
//   this.categoryService.getCategories().subscribe((categoryService) => {
//     this.categories = categoryService;
//   });
// }

// public getMainAttributesByCategory(categoryId: any) {
//   this.attributeService
//     .getMainAttributesByCategory(categoryId)
//     .subscribe((attributes) => {
//       attributes.forEach((attribute) => {
//         var value = '';
//         this.data?.attributes.forEach((element) => {
//           if (element.attribute_Id == attribute.id) {
//             value = element.value;
//           }
//         });
//         const attributesFromGroup = new FormGroup({
//           id: new FormControl(attribute.id),
//           value: new FormControl(value),
//           name: new FormControl(attribute.name),
//         });
//         this.addCategoryForm.controls.attributes.push(attributesFromGroup);
//       });
//     });
// }
