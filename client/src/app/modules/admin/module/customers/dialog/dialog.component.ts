import { Client } from '@core/models/client/Client';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from '../services/api.service';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  customersForm!: FormGroup;
  actionBtn: string = 'Save';


  constructor(private formBuilder: FormBuilder,
    private api:ApiService,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private dialogRef: MatDialogRef<DialogComponent>) { }



  ngOnInit(): void {
    this.customersForm = this.formBuilder.group({
      customersName: ['', Validators.required],
      email: ['', Validators.required],
      telephone: ['', Validators.required],
      userName: ['', Validators.required],
      category: ['', Validators.required],
      city: ['', Validators.required],
      advertising: ['', Validators.required],
    });

    if (this.editData) {
      this.customersForm.controls['customersName'].setValue(
        this.editData.customersName
      );
      this.customersForm.controls['email'].setValue(this.editData.email);
      this.customersForm.controls['telephone'].setValue(
        this.editData.telephone
      );
      this.customersForm.controls['userName'].setValue(this.editData.userName);
      this.customersForm.controls['category'].setValue(this.editData.category);
      this.customersForm.controls['city'].setValue(this.editData.city);
      this.customersForm.controls['advertising'].setValue(
        this.editData.advertising
      );
      this.actionBtn = 'Update';
    }
  }

  addCustomers() {
  if (!this.editData) {
    if (this.customersForm.valid) {
      this.api.postcustomers(this.customersForm.value).subscribe({

        next: (res) => {
          alert('customer added successfully');
          this.customersForm.reset();
          this.dialogRef.close('save');
        },
        error: () => {
          alert('error while added the customer');
        },
      });
    }
    } else {
      this.updateCustomers();
    }
  }

  updateCustomers() {
    this.api
      .putCustomers(this.customersForm.value, this.editData.id)
      .subscribe({
        next: () => {
          alert('customer updated Successfully');
          this.customersForm.reset();
          this.dialogRef.close('update');
        },
        error: () => {
          alert('error while update the record!!');
        },
      });

  }
}
