package amiss.wedding.controllers.admin.articles;

import amiss.wedding.entities.articles.dto.MiniArticleDto;
import amiss.wedding.services.articles.MiniArticleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/admin/mini_articles")
@AllArgsConstructor
public class AdminMiniArticleController {

    private final MiniArticleService miniArticleService;

    @GetMapping("")
    public ResponseEntity<List<MiniArticleDto>> getMiniArticles() {
        return this.miniArticleService.findAll();
    }

    @GetMapping(path = "/{article_id}")
    public ResponseEntity<MiniArticleDto> getMiniArticle(@PathVariable("mini_article_id") Long mini_article_id){
        return this.miniArticleService.findById(mini_article_id);
    }

    @PostMapping()
    public ResponseEntity<MiniArticleDto> createMiniArticle(@RequestBody MiniArticleDto miniArticleDto){
        return this.miniArticleService.createStartMiniArticle(miniArticleDto);
    }

    @PutMapping(path = "/{mini_article_id}")
    public ResponseEntity<MiniArticleDto> updateMiniArticle(@PathVariable("mini_article_id") Long mini_article_id,
                                                            @RequestBody MiniArticleDto miniArticleDto){
        return this.miniArticleService.update(mini_article_id, miniArticleDto);
    }

    @DeleteMapping(path = "/{mini_article_id}")
    public ResponseEntity<String> removeMiniArticle(@PathVariable("mini_article_id") Long mini_article_id){
        return this.miniArticleService.remove(mini_article_id);
    }
}
