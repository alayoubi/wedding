package amiss.wedding.entities.listings.dto;

import amiss.wedding.entities.categories.attributes.AttributeType;
import amiss.wedding.entities.listings.AttributeValue;
import lombok.*;


@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AttributeValueDto {

    private Long id;

    private String value;

    private Long listing_Id;

    private Long attribute_Id;
    private String attribute;

    private AttributeType typ;
    private boolean active;

    public AttributeValueDto(AttributeValue attributeValue){
        this.id = attributeValue.getId();
        this.value = attributeValue.getValue();
        this.listing_Id = attributeValue.getListing().getId();
        this.attribute_Id = attributeValue.getAttribute().getId();
        this.attribute = attributeValue.getAttribute().getName();
        this.typ = attributeValue.getAttribute().getAttributeType();
        this.active = attributeValue.isActive();
    }
}
