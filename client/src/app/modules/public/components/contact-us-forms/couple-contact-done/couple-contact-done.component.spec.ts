import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoupleContactDoneComponent } from './couple-contact-done.component';

describe('CoupleContactDoneComponent', () => {
  let component: CoupleContactDoneComponent;
  let fixture: ComponentFixture<CoupleContactDoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoupleContactDoneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoupleContactDoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
