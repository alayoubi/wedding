import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomListingsLandingComponent } from './custom-listings-landing.component';

describe('CustomListingsLandingComponent', () => {
  let component: CustomListingsLandingComponent;
  let fixture: ComponentFixture<CustomListingsLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomListingsLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomListingsLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
