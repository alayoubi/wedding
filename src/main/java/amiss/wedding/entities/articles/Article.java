package amiss.wedding.entities.articles;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.listings.Listing;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;


@Entity
@Table(name = "articles")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Article extends BaseEntity {

    @NotNull
    @NotBlank(message = "Title may not be blank")
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @ManyToMany
    @JoinTable(name = "article_categorie",
            joinColumns = @JoinColumn(name = "article_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "categorie_id", referencedColumnName = "id"))
    @ApiModelProperty(notes = "The Category")
    private Set<Category> categories;

    @Column(name = "contents", columnDefinition="MEDIUMTEXT")
    private String content;

    @Column(name = "read_time")
    private int readTime;

    @ApiModelProperty(notes = "The type of the article")
    @Enumerated(EnumType.STRING)
    private ArticleType articleType;


    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "cover_id", referencedColumnName = "id")
    private ArticlePhoto coverImg;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy="article")
    private Set<ArticlePhoto> articlePhotos;

    @OneToMany(mappedBy="article")
    private Set<MiniArticle> miniArticles;

    @ManyToMany(mappedBy = "articles")
    @JsonIgnore
    private Set<Listing> listings;

    @ManyToMany
    @JoinTable(name = "article_article_categorie",
            joinColumns = @JoinColumn(name = "article_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "article_categorie_id", referencedColumnName = "id"))
    @ApiModelProperty(notes = "The person's roles")
    private Set<ArticleCategory> articleCategories;


}
