import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomListingsComponent } from './custom-listings.component';

describe('CustomListingsComponent', () => {
  let component: CustomListingsComponent;
  let fixture: ComponentFixture<CustomListingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomListingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomListingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
