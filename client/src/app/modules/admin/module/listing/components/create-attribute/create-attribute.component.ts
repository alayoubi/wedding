import { Attribute } from '@core/models/category/Attribute';
import {
  Component,
  Inject,
  Input,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CategoryService } from '@core/services/category/category.service';
import { AttributeService } from '@core/services/category/attribute.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { Category } from '@core/models/category/Category';
import { Observable, map, startWith } from 'rxjs';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-attribute',
  templateUrl: './create-attribute.component.html',
  styleUrls: ['./create-attribute.component.css'],
})
export class CreateAttributeComponent implements OnInit {
  toggleValue = false;

  createAtt = new FormGroup({
    name: new FormControl<string | null>('', Validators.required),
    category_id: new FormControl<number | null>(null, Validators.required),
    valueType: new FormControl<string | null>('', Validators.required),
    attributeType: new FormControl<string | null>('', Validators.required),
    unit: new FormControl<string | null>('', Validators.required),
    active: new FormControl<boolean | null>(false, Validators.required),
  });

  public valueType: Array<string> = [
    'BOOLEAN',
    'DOUBLE',
    'INTEGER',
    'TEXT',
  ];

  public categories: any;

  public selectedCategory: any;

  public idOptionReg: any;
  public engNameCat: any;
  public engNameReg: any;

  constructor(
    private snackBar: MatSnackBar,
    public dialog: MatDialogRef<CreateAttributeComponent>,
    private categoryService: CategoryService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private attService: AttributeService
  ) {
    this.categories = [];
  }
  @Input() list: any = {};

  // auto complete for categories
  myControl = new FormControl<string | Category>('');
  filteredOptions: Observable<Category[]>;

  displayFn(category: Category): string {
    return category && category.name ? category.name : '';
  }

  private _filter(name: string): Category[] {
    const filterValue = name.toLowerCase();

    return this.categories.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }
  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    this.selectedCategory = event.option.value.id;
    console.log('Selected option:', this.selectedCategory);
  }
  //get Selected Category

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value) => {
        const name = typeof value === 'string' ? value : value?.name;
        return name ? this._filter(name as string) : this.categories.slice();
      })
    );
    this.getAllCategories();
    console.log(this.data, 'qqqqqqqqqqqq');
    console.log(this.toggleValue, 'toggle');
  }

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
    });
  }

  onToggleChange(event: MatSlideToggleChange) {
    this.toggleValue = event.checked;
    this.createAtt.get('active')?.setValue(this.toggleValue);

    console.log('Toggle value:', this.toggleValue);
  }

  reset(form?: any) {
    form.reset();
  }
  select_attribute_value_type(ev: any) {
    console.log(ev);
    this.createAtt.get('valueType')?.setValue(ev);
  }
  select_attribute_type(ev: any) {
    console.log(ev);
    this.createAtt.get('attributeType')?.setValue(ev);
  }
  select_unit(ev: any) {
    console.log(ev);
    this.createAtt.get('unit')?.setValue(ev);
  }

  close() {
    this.dialog.close();
    // this.title.setTitle('Books');
  }
  add() {
    console.log(this.createAtt.value);
    this.attService
      .createAtt(this.selectedCategory, this.createAtt.value)
      .subscribe((res) => {
        console.log(res);
      });
    this.snackBar.open(' تم إضافة الصفة بنجاح ✔', 'تم', {
      duration: 2000,

      panelClass: ['green-snackbar', 'login-snackbar', 'my-custom-snackbar'],
    });
    console.log(this.createAtt.value);
    this.close();
  }
}
