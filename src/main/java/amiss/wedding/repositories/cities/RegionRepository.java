package amiss.wedding.repositories.cities;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

    List<Region> findAllByActive(boolean active);

    Region findByName(String name);
    List<Region> findAllByCountry(Country country);
    List<Region> findAllByCountryAndActive(Country country, boolean active);

}
