import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionDataContentComponent } from './region-data-content.component';

describe('RegionDataContentComponent', () => {
  let component: RegionDataContentComponent;
  let fixture: ComponentFixture<RegionDataContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegionDataContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegionDataContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
