package amiss.wedding.entities.articles;

import amiss.wedding.entities.BaseEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "article_photos")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ArticlePhoto extends BaseEntity {


    @Column(name = "fileName")
    private String fileName;

    @NotNull
    @NotBlank(message = "Path may not be blank")
    @Column(name = "path")
    private String path;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name="miniArticle_id")
    private MiniArticle miniArticle;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="article_id")
    private Article article;

    @Column(name = "size")
    private double size;
}
