package amiss.wedding.services.cities.impl;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.dto.CountryDto;
import amiss.wedding.errors.CountryNotFoundException;
import amiss.wedding.mappers.cities.CountryMapper;
import amiss.wedding.mappers.cities.CountryMapperImpl;
import amiss.wedding.repositories.cities.CityRepository;
import amiss.wedding.repositories.cities.CountryRepository;
import amiss.wedding.services.cities.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;
    private static final CountryMapper mapper = new CountryMapperImpl();

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository, CityRepository cityRepository) {
        this.countryRepository = countryRepository;
        this.cityRepository = cityRepository;
    }

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<CountryDto>> findAll() {
        List<Country> countires = this.countryRepository.findAll();
        List<CountryDto> countryDtoList =
                countires.stream().map(mapper::countryToCountryDto).collect(Collectors.toList());
        return new ResponseEntity<>(countryDtoList, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<CountryDto> findById(Long id) {
        Optional<Country> country = this.countryRepository.findById(id);
        if (country.isEmpty()) {
            throw new CountryNotFoundException(id);
        }
        CountryDto countryDto = mapper.countryToCountryDto(country.get());
        return new ResponseEntity<>(countryDto, HttpStatus.OK);
    }

    /**
     * @param countryDto
     * @return
     */
    @Override
    public ResponseEntity<CountryDto> create(CountryDto countryDto) {
        final Country country = mapper.countryDtoToCountry(countryDto);
        final Country createdCountry = this.countryRepository.save(country);
        return new ResponseEntity<>(countryDto, HttpStatus.CREATED);
    }

    /**
     * @param id
     * @param countryDto
     * @return
     */
    @Override
    public ResponseEntity<CountryDto> update(Long id, CountryDto countryDto) {
        Optional<Country> country = this.countryRepository.findById(id);
        if (country.isEmpty()) {
            throw new CountryNotFoundException(id);
        }
        Country existingCountry = country.get();
        existingCountry.setName(countryDto.getName());
        existingCountry.setName_en(countryDto.getName_en());
        existingCountry.setCode(countryDto.getCode());
        City city = cityRepository.findByNameIs(countryDto.getCapitalCity());
        existingCountry.setCapital_city(city);
        countryRepository.save(existingCountry);
        return new ResponseEntity<>(countryDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.countryRepository.existsById(id);
        if (!exists) {
            throw new CountryNotFoundException(id);
        }
        this.countryRepository.deleteById(id);
        return new ResponseEntity<>("Country has been removed successfully", HttpStatus.NO_CONTENT);
    }
}
