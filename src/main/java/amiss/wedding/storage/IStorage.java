package amiss.wedding.storage;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface IStorage {


    String uploadFile(MultipartFile data, String key, String link);

    byte[] downloadFile(String key);

    String deleteFile(String fileName);

    String store(InputStream data, String key);

    InputStream  read(String key);

    void writeAsJson(String key, Object data);

    void write(String key, String content);

    boolean exists(String key);
}
