package amiss.wedding.services.listings.impl;

import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.AttributeValueDto;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.errors.AttributeNotFoundException;
import amiss.wedding.errors.AttributeValueNotFoundException;
import amiss.wedding.errors.ListingNotFoundException;
import amiss.wedding.mappers.listings.AttributeValueMapper;
import amiss.wedding.mappers.listings.AttributeValueMapperImpl;
import amiss.wedding.repositories.categories.attribute.AttributeRepository;
import amiss.wedding.repositories.listings.AttributeValueRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.listings.AttributeValueService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AttributeValueServiceImpl implements AttributeValueService {

    private final AttributeValueRepository attributeValueRepository;
    private final ListingsRepository listingsRepository;

    private final AttributeRepository attributeRepository;
    private final AttributeValueMapper mapper = new AttributeValueMapperImpl();

    @Override
    public ResponseEntity<List<AttributeValueDto>> findAll() {

        List<AttributeValue> attributeValues = this.attributeValueRepository.findAll();
        List<AttributeValueDto> attributeValueDtoList =
                attributeValues.stream().map(mapper::attributeValueToAttributeValueDto).collect(Collectors.toList());

        return new ResponseEntity<>(attributeValueDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AttributeValueDto> findById(Long id) {
        Optional<AttributeValue> attributeValue = this.attributeValueRepository.findById(id);
        if (attributeValue.isEmpty()){
            throw new AttributeValueNotFoundException(id);
        }
        AttributeValueDto attributeValueDto = mapper.attributeValueToAttributeValueDto(attributeValue.get());
        return new ResponseEntity<>(attributeValueDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<AttributeValueDto>> findAllByListingId(Long listingId) {
        Listing listing = listingsRepository.findById(listingId).orElseThrow(()->
                new ListingNotFoundException(listingId));
        List<AttributeValue> attributeValues = this.attributeValueRepository.findByListing(listing);
        List<AttributeValueDto> attributeValueDtoList =
                attributeValues.stream().map(mapper::attributeValueToAttributeValueDto).collect(Collectors.toList());
        return new ResponseEntity<>(attributeValueDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<AttributeValueDto> create(AttributeValueDto attributeValueDto) {
        Listing listing = listingsRepository.findById(attributeValueDto.getListing_Id()).orElseThrow(()->
                new ListingNotFoundException(attributeValueDto.getListing_Id()));
        final AttributeValue attributeValue = mapper.attributeValueDtoToAttributeValue(attributeValueDto);
        attributeValue.setListing(listing);

        Attribute attribute = attributeRepository.findById(attributeValueDto.getAttribute_Id()).orElseThrow(()->
                new AttributeNotFoundException(attributeValueDto.getAttribute_Id()));
        attributeValue.setAttribute(attribute);
        final AttributeValue createdAttributeValue = this.attributeValueRepository.save(attributeValue);
        AttributeValueDto createdAttributeValueDto = mapper.attributeValueToAttributeValueDto(createdAttributeValue);
        return new ResponseEntity<>(createdAttributeValueDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<AttributeValueDto>> createValuesfurListing(
            Long listingId, List<AttributeValueDto> attributeValueDtoList) {
        Listing listing = listingsRepository.findById(listingId).orElseThrow(()->
                new ListingNotFoundException(listingId));
        List<AttributeValueDto> list = null;
        for (AttributeValueDto value: attributeValueDtoList) {
            AttributeValue attributeValue = mapper.attributeValueDtoToAttributeValue(value);
            attributeValue.setListing(listing);

            Attribute attribute = attributeRepository.findById(value.getAttribute_Id()).orElseThrow(()->
                    new AttributeNotFoundException(value.getAttribute_Id()));
            attributeValue.setAttribute(attribute);

            AttributeValue createdAttributeValue = this.attributeValueRepository.save(attributeValue);
            AttributeValueDto createdAttributeValueDto =
                    mapper.attributeValueToAttributeValueDto(createdAttributeValue);

//            list.add(createdAttributeValueDto); // TODO
        }

        return new ResponseEntity<>(list, HttpStatus.CREATED);
    }


    @Override
    public ResponseEntity<AttributeValueDto> update(Long id, AttributeValueDto newAttributeValueDto) {
        Optional<AttributeValue> attributeValue = this.attributeValueRepository.findById(id);
        if (attributeValue.isEmpty()){
            throw new AttributeValueNotFoundException(id);
        }
        AttributeValue existingAttributeValue = attributeValue.get();
        existingAttributeValue.setValue(newAttributeValueDto.getValue());
        existingAttributeValue.setActive(newAttributeValueDto.isActive());
        AttributeValue updatedAttributeValue = this.attributeValueRepository.save(existingAttributeValue);
        AttributeValueDto attributeValueDto = mapper.attributeValueToAttributeValueDto(updatedAttributeValue);
        return new ResponseEntity<>(attributeValueDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.attributeValueRepository.existsById(id);
        if (!exists){
            throw new AttributeValueNotFoundException(id);
        }
        this.attributeValueRepository.deleteById(id);
        return new ResponseEntity<>("AttributeValue has been removed successfully!", HttpStatus.NO_CONTENT);
    }

    /**
     * @param listingDto
     * @param createdListing
     * @return
     */
    @Override
    public Set<AttributeValue> setAndSaveAttributeValue(ListingDto listingDto, Listing createdListing) {
        Set<AttributeValue> attributeValueSet = new HashSet();
        if (!listingDto.getAttributeValueList().isEmpty()) {
            for (AttributeValueDto att:listingDto.getAttributeValueList()) {
                if (!(att.getValue() == "" ||  att.getValue() == null)) {
                    AttributeValue attributeValue = mapper.attributeValueDtoToAttributeValue(att);
                    attributeValue.setListing(createdListing);
                    Attribute attribute = attributeRepository.findById(att.getAttribute_Id()).orElseThrow(() ->
                            new AttributeNotFoundException(att.getAttribute_Id()));
                    attributeValue.setAttribute(attribute);
                    attributeValue = attributeValueRepository.save(attributeValue);
                    attributeValueSet.add(attributeValue);
                }
            }
        }
        return attributeValueSet;
    }
}
