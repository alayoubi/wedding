package amiss.wedding.services.listings;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.SocialMedia;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.dto.SocialMediaDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;

public interface SocialMediaService {

    ResponseEntity<List<SocialMediaDto>> findAll();
    ResponseEntity<SocialMediaDto> findById(Long id);
    ResponseEntity<List<SocialMediaDto>> findAllByListingId(Long listingId);
    ResponseEntity<SocialMediaDto> create(SocialMediaDto socialMediaDto, Long listingId);
    ResponseEntity<SocialMediaDto> update(Long id, SocialMediaDto socialMediaDto);
    Set<SocialMedia> setAndSaveSocialMedias(ListingDto listingDto, Listing createdListing);
    void markAsDeleted(Listing listing);
    void markAsDeleted(SocialMedia socialMedia);
    ResponseEntity<String> remove(Long id);
}
