package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.dto.AttributeValueDto;

public interface AttributeValueMapper {

    AttributeValue attributeValueDtoToAttributeValue(AttributeValueDto attributeValueDto);

    AttributeValueDto attributeValueToAttributeValueDto(AttributeValue attributeValue);
}
