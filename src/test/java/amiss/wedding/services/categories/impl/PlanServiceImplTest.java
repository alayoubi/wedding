package amiss.wedding.services.categories.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.plans.Feature;
import amiss.wedding.entities.categories.plans.FeatureDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanDto;
import amiss.wedding.entities.categories.plans.PlanType;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Position;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.client.Client;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.errors.PlanNotFoundException;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.categories.FeatureRepository;
import amiss.wedding.repositories.categories.PlanRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {PlanServiceImpl.class})
@ExtendWith(SpringExtension.class)
class PlanServiceImplTest {
    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private FeatureRepository featureRepository;

    @MockBean
    private PlanRepository planRepository;

    @Autowired
    private PlanServiceImpl planServiceImpl;

    /**
     * Method under test: {@link PlanServiceImpl#findAll()}
     */
    @Test
    void testFindAll() {
        ArrayList<Plan> planList = new ArrayList<>();
        when(planRepository.findAll()).thenReturn(planList);
        ResponseEntity<List<PlanDto>> actualFindAllResult = planServiceImpl.findAll();
        assertEquals(planList, actualFindAllResult.getBody());
        assertEquals(HttpStatus.OK, actualFindAllResult.getStatusCode());
        assertTrue(actualFindAllResult.getHeaders().isEmpty());
        verify(planRepository).findAll();
    }

    /**
     * Method under test: {@link PlanServiceImpl#findAll()}
     */
    @Test
    void testFindAll2() {
        when(planRepository.findAll()).thenThrow(new PlanNotFoundException(1L));
        assertThrows(PlanNotFoundException.class, () -> planServiceImpl.findAll());
        verify(planRepository).findAll();
    }

    /**
     * Method under test: {@link PlanServiceImpl#findById(Long)}
     */
    @Test
    void testFindById() {
        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category);
        BigDecimal cost = BigDecimal.valueOf(1L);
        plan.setCost(cost);
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);
        Optional<Plan> ofResult = Optional.of(plan);
        when(planRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<PlanDto> actualFindByIdResult = planServiceImpl.findById(1L);
        assertTrue(actualFindByIdResult.hasBody());
        assertTrue(actualFindByIdResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualFindByIdResult.getStatusCode());
        PlanDto body = actualFindByIdResult.getBody();
        assertTrue(body.getFeatures().isEmpty());
        assertEquals(1, body.getDuration().intValue());
        assertEquals(3, body.getCountOfPhotos().intValue());
        BigDecimal expectedCost = cost.ONE;
        BigDecimal cost2 = body.getCost();
        assertSame(expectedCost, cost2);
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals(PlanType.FREE, body.getPlanType());
        assertTrue(body.isVideo());
        assertEquals(1L, body.getId().longValue());
        assertEquals("1", cost2.toString());
        verify(planRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#create(PlanDto)}
     */
    @Test
    void testCreate() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.plans.Plan.getId()" because "plan" is null
        //       at amiss.wedding.entities.categories.plans.PlanDto.<init>(PlanDto.java:28)
        //       at amiss.wedding.mappers.categories.PlanMapperImpl.planToPlanDto(PlanMapperImpl.java:24)
        //       at amiss.wedding.services.categories.impl.PlanServiceImpl.create(PlanServiceImpl.java:66)
        //   See https://diff.blue/R013 to resolve this issue.

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(new ListingPhoto());
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(new Category());
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        City city = new City();
        city.setCountry(new Country());
        city.setDistricts(new HashSet<>());
        city.setId(1L);
        city.setListing(new HashSet<>());
        city.setName("Name");
        city.setName_en("Name en");
        city.setRegion(new Region());

        Client client = new Client();
        client.setAddress("42 Main St");
        client.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setId(1L);
        client.setListing(new HashSet<>());
        client.setMobile("Mobile");
        client.setName("Name");
        client.setPhoto("alice.liddell@example.org");
        client.setRole("Role");
        client.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setUser(new AppUser());
        client.setVersion(1);

        Country country = new Country();
        country.setCapital_city(new City());
        country.setCode("Code");
        country.setId(1L);
        country.setName("Name");
        country.setName_en("Name en");
        country.setRegions(new HashSet<>());

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        District district = new District();
        district.setCity(new City());
        district.setId(1L);
        district.setListing(new HashSet<>());
        district.setName("Name");
        district.setName_en("Name en");
        district.setRegion(new Region());

        Plan plan = new Plan();
        plan.setCategory(new Category());
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Position position = new Position();
        position.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setId(1L);
        position.setLatitude(1L);
        position.setListing(new Listing());
        position.setLongitude(1L);
        position.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setVersion(1);

        Region region = new Region();
        region.setActive(true);
        region.setCapital_city(new City());
        region.setCities(new HashSet<>());
        region.setCode("Code");
        region.setCountry(new Country());
        region.setId(1L);
        region.setListing(new HashSet<>());
        region.setName("Name");
        region.setName_en("Name en");

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(category);
        listing.setCity(city);
        listing.setClient(client);
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(country);
        listing.setCoverImg(coverImg);
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(district);
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(plan);
        listing.setPosition(position);
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(region);
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(listing);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing2);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg4);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(coverImg3);
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(parentCategory2);
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(coverImg2);
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(parentCategory3);
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Plan plan2 = new Plan();
        plan2.setCategory(category2);
        BigDecimal cost = BigDecimal.valueOf(1L);
        plan2.setCost(cost);
        plan2.setCountOfPhotos(3);
        plan2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDuration(1);
        plan2.setFeatures(new HashSet<>());
        plan2.setId(1L);
        plan2.setListings(new HashSet<>());
        plan2.setPlanType(PlanType.FREE);
        plan2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setVersion(1);
        plan2.setVideo(true);
        PlanRepository planRepository = mock(PlanRepository.class);
        when(planRepository.save(Mockito.<Plan>any())).thenReturn(plan2);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        PlanServiceImpl planServiceImpl = new PlanServiceImpl(planRepository, categoryRepository,
                mock(FeatureRepository.class));
        ResponseEntity<PlanDto> actualCreateResult = planServiceImpl.create(new PlanDto());
        assertTrue(actualCreateResult.hasBody());
        assertTrue(actualCreateResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.CREATED, actualCreateResult.getStatusCode());
        PlanDto body = actualCreateResult.getBody();
        assertTrue(body.getFeatures().isEmpty());
        assertEquals(1, body.getDuration().intValue());
        assertEquals(3, body.getCountOfPhotos().intValue());
        BigDecimal expectedCost = cost.ONE;
        BigDecimal cost2 = body.getCost();
        assertSame(expectedCost, cost2);
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals(PlanType.FREE, body.getPlanType());
        assertTrue(body.isVideo());
        assertEquals(1L, body.getId().longValue());
        assertEquals("1", cost2.toString());
        verify(planRepository).save(Mockito.<Plan>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#create(PlanDto)}
     */
    @Test
    void testCreate2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.plans.Plan.getId()" because "plan" is null
        //       at amiss.wedding.entities.categories.plans.PlanDto.<init>(PlanDto.java:28)
        //       at amiss.wedding.mappers.categories.PlanMapperImpl.planToPlanDto(PlanMapperImpl.java:24)
        //       at amiss.wedding.services.categories.impl.PlanServiceImpl.create(PlanServiceImpl.java:66)
        //   See https://diff.blue/R013 to resolve this issue.

        PlanRepository planRepository = mock(PlanRepository.class);
        when(planRepository.save(Mockito.<Plan>any())).thenThrow(new PlanNotFoundException(1L));
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        PlanServiceImpl planServiceImpl = new PlanServiceImpl(planRepository, categoryRepository,
                mock(FeatureRepository.class));
        assertThrows(PlanNotFoundException.class, () -> planServiceImpl.create(new PlanDto()));
        verify(planRepository).save(Mockito.<Plan>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#createFeature(Long, FeatureDto)}
     */
    @Test
    void testCreateFeature() {
        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category);
        plan.setCost(BigDecimal.valueOf(1L));
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);
        Optional<Plan> ofResult = Optional.of(plan);
        when(planRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing2);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(new ListingPhoto());
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(new Category());
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category parentCategory4 = new Category();
        parentCategory4.setActive(true);
        parentCategory4.setArticles(new HashSet<>());
        parentCategory4.setAttributes(new HashSet<>());
        parentCategory4.setCoverImg(coverImg4);
        parentCategory4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setDescription("The characteristics of someone or something");
        parentCategory4.setId(1L);
        parentCategory4.setListings(new HashSet<>());
        parentCategory4.setName("Name");
        parentCategory4.setParentCategory(parentCategory3);
        parentCategory4.setPlan(new HashSet<>());
        parentCategory4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setVersion(1);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(coverImg3);
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(parentCategory4);
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Plan plan2 = new Plan();
        plan2.setCategory(category2);
        BigDecimal cost = BigDecimal.valueOf(1L);
        plan2.setCost(cost);
        plan2.setCountOfPhotos(3);
        plan2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDuration(1);
        plan2.setFeatures(new HashSet<>());
        plan2.setId(1L);
        plan2.setListings(new HashSet<>());
        plan2.setPlanType(PlanType.FREE);
        plan2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setVersion(1);
        plan2.setVideo(true);

        Feature feature = new Feature();
        feature.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setId(1L);
        feature.setPlan(plan2);
        feature.setTitle("Dr");
        feature.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        feature.setVersion(1);
        when(featureRepository.save(Mockito.<Feature>any())).thenReturn(feature);
        ResponseEntity<PlanDto> actualCreateFeatureResult = planServiceImpl.createFeature(1L, new FeatureDto());
        assertTrue(actualCreateFeatureResult.hasBody());
        assertTrue(actualCreateFeatureResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.CREATED, actualCreateFeatureResult.getStatusCode());
        PlanDto body = actualCreateFeatureResult.getBody();
        assertTrue(body.getFeatures().isEmpty());
        assertEquals(1, body.getDuration().intValue());
        assertEquals(3, body.getCountOfPhotos().intValue());
        BigDecimal expectedCost = cost.ONE;
        BigDecimal cost2 = body.getCost();
        assertSame(expectedCost, cost2);
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals(PlanType.FREE, body.getPlanType());
        assertTrue(body.isVideo());
        assertEquals(1L, body.getId().longValue());
        assertEquals("1", cost2.toString());
        verify(planRepository).findById(Mockito.<Long>any());
        verify(featureRepository).save(Mockito.<Feature>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#createFeature(Long, FeatureDto)}
     */
    @Test
    void testCreateFeature2() {
        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category);
        plan.setCost(BigDecimal.valueOf(1L));
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);
        Optional<Plan> ofResult = Optional.of(plan);
        when(planRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        when(featureRepository.save(Mockito.<Feature>any())).thenThrow(new PlanNotFoundException(1L));
        assertThrows(PlanNotFoundException.class, () -> planServiceImpl.createFeature(1L, new FeatureDto()));
        verify(planRepository).findById(Mockito.<Long>any());
        verify(featureRepository).save(Mockito.<Feature>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#update(Long, PlanDto)}
     */
    @Test
    void testUpdate() {
        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(listing);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(new Listing());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg2);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory2);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category);
        plan.setCost(BigDecimal.valueOf(1L));
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);
        Optional<Plan> ofResult = Optional.of(plan);
        when(planRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        when(categoryRepository.findById(Mockito.<Long>any())).thenThrow(new PlanNotFoundException(1L));
        assertThrows(PlanNotFoundException.class, () -> planServiceImpl.update(1L, new PlanDto()));
        verify(planRepository).findById(Mockito.<Long>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }


    /**
     * Method under test: {@link PlanServiceImpl#remove(Long)}
     */
    @Test
    void testRemove() {
        doNothing().when(planRepository).deleteById(Mockito.<Long>any());
        when(planRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        ResponseEntity<String> actualRemoveResult = planServiceImpl.remove(1L);
        assertEquals("Plan has been removed successfully!", actualRemoveResult.getBody());
        assertEquals(HttpStatus.NO_CONTENT, actualRemoveResult.getStatusCode());
        assertTrue(actualRemoveResult.getHeaders().isEmpty());
        verify(planRepository).existsById(Mockito.<Long>any());
        verify(planRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#remove(Long)}
     */
    @Test
    void testRemove2() {
        doThrow(new PlanNotFoundException(1L)).when(planRepository).deleteById(Mockito.<Long>any());
        when(planRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        assertThrows(PlanNotFoundException.class, () -> planServiceImpl.remove(1L));
        verify(planRepository).existsById(Mockito.<Long>any());
        verify(planRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#remove(Long)}
     */
    @Test
    void testRemove3() {
        when(planRepository.existsById(Mockito.<Long>any())).thenReturn(false);
        assertThrows(PlanNotFoundException.class, () -> planServiceImpl.remove(1L));
        verify(planRepository).existsById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#findByCategory(Long)}
     */
    @Test
    void testFindByCategory() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.PlanServiceImpl.findByCategory(PlanServiceImpl.java:131)
        //   See https://diff.blue/R013 to resolve this issue.

        PlanRepository planRepository = mock(PlanRepository.class);
        ArrayList<Plan> planList = new ArrayList<>();
        when(planRepository.findByCategory(Mockito.<Category>any())).thenReturn(planList);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<List<PlanDto>> actualFindByCategoryResult = (new PlanServiceImpl(planRepository,
                categoryRepository, mock(FeatureRepository.class))).findByCategory(1L);
        assertEquals(planList, actualFindByCategoryResult.getBody());
        assertEquals(HttpStatus.OK, actualFindByCategoryResult.getStatusCode());
        assertTrue(actualFindByCategoryResult.getHeaders().isEmpty());
        verify(planRepository).findByCategory(Mockito.<Category>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#findByCategory(Long)}
     */
    @Test
    void testFindByCategory2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.PlanServiceImpl.findByCategory(PlanServiceImpl.java:131)
        //   See https://diff.blue/R013 to resolve this issue.

        PlanRepository planRepository = mock(PlanRepository.class);
        when(planRepository.findByCategory(Mockito.<Category>any())).thenThrow(new PlanNotFoundException(1L));
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> ofResult = Optional.of(mock(Category.class));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        assertThrows(PlanNotFoundException.class,
                () -> (new PlanServiceImpl(planRepository, categoryRepository, mock(FeatureRepository.class)))
                        .findByCategory(1L));
        verify(planRepository).findByCategory(Mockito.<Category>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link PlanServiceImpl#findByCategory(Long)}
     */
    @Test
    void testFindByCategory3() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   amiss.wedding.errors.CategoryNotFoundException: The Category with the id: 1 was not found!
        //       at amiss.wedding.services.categories.impl.PlanServiceImpl.findByCategory(PlanServiceImpl.java:131)
        //   See https://diff.blue/R013 to resolve this issue.

        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> emptyResult = Optional.empty();
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);
        assertThrows(CategoryNotFoundException.class,
                () -> (new PlanServiceImpl(mock(PlanRepository.class), categoryRepository, mock(FeatureRepository.class)))
                        .findByCategory(1L));
        verify(categoryRepository).findById(Mockito.<Long>any());
    }
}

