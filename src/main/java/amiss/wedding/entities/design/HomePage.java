package amiss.wedding.entities.design;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.Review;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;


@Entity
@Table(name = "home_pages")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HomePage extends BaseEntity {

    @OneToMany()
    private Set<Listing> listing;

    @OneToMany()
    private Set<Article> articles;

    @OneToMany()
    private Set<DesignPhoto> designPhotos;

    @OneToMany()
    private Set<Review> reviews;

}
