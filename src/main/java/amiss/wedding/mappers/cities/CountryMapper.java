package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.dto.CountryDto;

public interface CountryMapper {
    Country countryDtoToCountry(CountryDto countryDto);
    CountryDto countryToCountryDto(Country country);
    
}
