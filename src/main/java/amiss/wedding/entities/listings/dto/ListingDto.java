package amiss.wedding.entities.listings.dto;


import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.listings.*;

import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.plans.ListingPlanDto;
import lombok.*;

import java.util.HashSet;
import java.util.Set;


@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListingDto {

    private Long id;
    private String title;
    private String email;
    private String phone;
    private String address;
    private String descriptions;
    private Long coverImg_id;
    private String coverImg;
    private Status status;
    private Long country_id;
    private String country;
    private Long region_id;
    private String region;
    private Long city_id;
    private String city;
    private Long district_id;
    private String district;
    private Long position_id;
    private Long client_id;
    private ListingPlanDto listingPlanDto;
    private Long category_id;
    private String category;
    private Set<ReviewDto> reviewsList = new HashSet();
    private Set<Long> listingsPhotos_ID = new HashSet();
    private Set<String> listingsPhotos = new HashSet();
    private Set<AttributeValueDto> attributeValueList = new HashSet();
    private Set<SocialMediaDto> socialMediaList = new HashSet();
    private Set<OfferDto> offersList = new HashSet();
    private int priority;
    private String capacity;
    private String price;
    private int contOfReviews;
    private int contOfPhotos;
    private int contOfVideo;
    public ListingDto(Listing listing){
        this.id = listing.getId();
        this.title = listing.getTitle();
        if (listing.getEmail() != null) {
            this.email = listing.getEmail();
        }
        this.phone = listing.getPhone();
        if (listing.getAddress() != null) {
            this.address = listing.getAddress();
        }
        this.descriptions = listing.getDescriptions();
        if (listing.getCoverImg() != null){
            this.coverImg_id = listing.getCoverImg().getId();
            this.coverImg = listing.getCoverImg().getPath();
        }

        if (listing.getStatus() != null){
            this.status = listing.getStatus();
        } else {
            this.status = Status.PENDING;
        }
        this.country_id = listing.getCountry().getId();
        this.country = listing.getCountry().getName();
        this.region_id = listing.getRegion().getId();
        this.region = listing.getRegion().getName();
        this.city_id = listing.getCity().getId();
        this.city = listing.getCity().getName();
        if (listing.getDistrict() != null) {
            this.district_id = listing.getDistrict().getId();
            this.district = listing.getDistrict().getName();
        }
        if (listing.getPosition() != null) {
            this.position_id = listing.getPosition().getId();
        }
        if (listing.getClient() != null) {
            this.client_id = listing.getClient().getId();
        }
        if (listing.getListingPlan() != null) {
            this.listingPlanDto = new ListingPlanDto(listing.getListingPlan());

        }
        this.category_id = listing.getCategory().getId();
        this.category = listing.getCategory().getName();
        if (!listing.getReviews().isEmpty()) {
            for (Review re: listing.getReviews()) {
                this.reviewsList.add(new ReviewDto(re.getId(), this.id, re.getTitle()
                , re.getContent(), re.getDatePost(), re.getRating(), re.getUser().getId()));
            }
        }
        if (!listing.getListingPhotos().isEmpty()) {
            for (ListingPhoto photo: listing.getListingPhotos()) {
                this.listingsPhotos.add(photo.getPath());
                this.listingsPhotos_ID.add(photo.getId());
            }
        }

        if (!listing.getAttributeValues().isEmpty()) {
            for (AttributeValue attributeValue: listing.getAttributeValues()) {
                Attribute attribute = attributeValue.getAttribute();
                this.attributeValueList.add(new AttributeValueDto(attributeValue.getId(),
                        attributeValue.getValue(),
                        this.id,
                        attribute.getId(),
                        attribute.getName(),
                        attribute.getAttributeType(),
                        attributeValue.isActive()));
            }
        }

        if (!listing.getSocialMedia().isEmpty()) {
            for (SocialMedia socialMedia: listing.getSocialMedia()) {
                this.socialMediaList.add(new SocialMediaDto(socialMedia.getId(),
                        socialMedia.getPath(),
                        socialMedia.getSocialMediaTyp(),
                        this.id));
            }
        }

        if (!listing.getOffers().isEmpty()) {
            for (Offer offer: listing.getOffers()) {
                this.offersList.add(new OfferDto(offer.getId(),
                        this.id,
                        offer.getTitle(),
                        offer.getContent(),
                        offer.getExpiryDate()));
            }
        }
        this.priority = listing.getPriority();
    }

}
