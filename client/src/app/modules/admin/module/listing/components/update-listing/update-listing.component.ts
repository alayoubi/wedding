import { Attribute } from '@core/models/category/Attribute';
import { HttpErrorResponse } from '@angular/common/http';

import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { Category } from '@core/models/category/Category';
import { City } from '@core/models/city/City';
import { District } from '@core/models/city/District';
import { Region } from '@core/models/city/Region';
import { Listing } from '@core/models/listing/Listing';

import { AttributeService } from '@core/services/category/attribute.service';
// import { Attribute } from '@core/models/category/Attribute';
import { DistrictService } from '@core/services/city/district.service';

import { CategoryService } from 'src/app/services/category/category.service';

import { RegionService } from '@core/services/city/region.service';
import { CityService } from '@core/services/city/city.service';
import { ListingService } from '@core/services/listing/listing.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-update-listing',
  templateUrl: './update-listing.component.html',
  styleUrls: ['./update-listing.component.css'],
})
export class UpdateListingComponent implements OnInit {
  @Input() list: any = {};

  public selectedCat: any;
  public base64: any = '';

  constructor(
    private snackBar: MatSnackBar,
    private title: Title,
    private categoryService: CategoryService,
    private regionService: RegionService,
    private cityservice: CityService,
    private districtService: DistrictService,
    private attributeService: AttributeService,
    private listingService: ListingService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialogRef<UpdateListingComponent>
  ) {
    this.base64 = data.image;
    title.setTitle(`تعديل  ${this.data?.title}`);
  }

  addListingForm = new FormGroup({
    title: new FormControl<string | null>(
      this.data?.title || '',
      Validators.required
    ),
    email: new FormControl<string | null>(
      this.data?.email || '',
      Validators.required
    ),
    phone: new FormControl<string | null>(
      this.data?.phone || '',
      Validators.required
    ),
    address: new FormControl<string | null>(
      this.data?.address || '',
      Validators.required
    ),
    descriptions: new FormControl<string | null>(
      this.data?.descriptions || '',
      Validators.required
    ),
    status: new FormControl<string | null>(
      this.data?.status || '',
      Validators.required
    ),
    country_id: new FormControl<number | null>(
      this.data?.country_id || '',
      Validators.required
    ),
    region_id: new FormControl<number | null>(
      this.data?.region_id || '',
      Validators.required
    ),
    city_id: new FormControl<number>(this.data?.city_id, Validators.required),
    district_id: new FormControl<number | null>(
      this.data?.district_id || '',
      Validators.required
    ),
    client_id: new FormControl<number | null>(
      this.data?.client_id || '',
      Validators.required
    ),
    plan_id: new FormControl<number | null>(
      this.data?.plan_id || '',
      Validators.required
    ),
    category_id: new FormControl<number>(
      this.data?.category_id,
      Validators.required
    ),
    attributeValueList: new FormArray<FormGroup<{
      attribute_Id: FormControl<number | null>,
      value: FormControl<any | null>,
    }>>([]),
    mainAttributeValue: new FormArray<FormGroup<{
      id: FormControl<number | null>,
      value: FormControl<string | null>,
      name: FormControl<string | null>
    }>>([]),
    secondaryAttributeValue: new FormArray<FormGroup<{
      id: FormControl<number | null>,
      value: FormControl<string | null>,
      name: FormControl<string | null>
    }>>([]),
    generaleAttributeValue: new FormArray<FormGroup<{
      id: FormControl<number | null>,
      value: FormControl<boolean | null>,
      name: FormControl<string | null>
    }>>([]),
    questionAttributeValue: new FormArray<FormGroup<{
      id: FormControl<number | null>,
      value: FormControl<string | null>,
      name: FormControl<string | null>
    }>>([]),
    covidAttributeValue: new FormArray<FormGroup<{
      id: FormControl<number | null>,
      value: FormControl<boolean | null>,
      name: FormControl<string | null>
    }>>([]),
    otherAttributeValue: new FormArray<FormGroup<{
      id: FormControl<number | null>,
      value: FormControl<string | null>,
      name: FormControl<string | null>
    }>>([]),
    socialMediaList : new FormArray([
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("PHONE", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("EMAIL", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("WHATSAPP", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("FACEBOOK", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("INSTAGRAM", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("TWITTER", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("TIKTOK", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("SNAPCHAT", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("PRIVATE", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("YOUTUBE", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("PINTEREST", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("TELEGRAM", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("LINKEDIN", null),
      }),
      new FormGroup({
        path: new FormControl<string | null>(null, null),
        socialMediaTyp: new FormControl<string | null>("OTHER", null),
      }),
    ]),
  });

  public statuses: String[] = ["PENDING",
  "PROCESSING",
  "COMPLETED",
  "DECLINE" ]

  public categories: Category[];
  public regions: Region[];
  public cities: City[];
  public districts: District[];
  public mainAttributes: Attribute[];
  public secondaryAttributes: Attribute[];
  public generaleAttributes: Attribute[];
  public questionAttributes: Attribute[];
  public covidAttributes: Attribute[];
  public otherAttributes: Attribute[];

  public selectedCategory = this.addListingForm.controls.category_id.value;
  public selectedRegion = this.addListingForm.controls.region_id.value;
  public selectedCity = this.addListingForm.controls.city_id.value;

  ngOnInit(): void {


    this.getAllCategories();
    this.getAllActiveRegion();
    this.getAllCitiesByRegion(this.data?.region_id);
    this.getAllDistrictsByCity(this.data?.city_id);
    this.onRegionChange();
    this.onCityChange();
    this.onFocus();
    this.getSocialMedia();
    this.getMainAttributesByCategory(this.selectedCategory);
    this.getSecondaryAttributesByCategory(this.selectedCategory);
    this.getGeneralAttributesByCategory(this.selectedCategory);
    this.getQuestionsAttributesByCategory(this.selectedCategory);
    this.getCovidAttributesByCategory(this.selectedCategory);
    this.getOtherAttributesByCategory(this.selectedCategory);
    }

  // auto complete for categories
  myControlCat = new FormControl<string | Category>('');
  filteredOptionsCat: Observable<Category[]>;

  displayFnCat(category: Category): string {
    return category && category.name ? category.name : '';
  }

  private _filterCat(name: string): Category[] {
    const filterValue = name.toLowerCase();

    return this.categories.filter((option) =>
      option.name.toLowerCase().includes(filterValue)
    );
  }

  getSocialMedia() {
    this.data.socialMediaList.forEach((item) => {

      if (item.socialMediaTyp == 'PHONE') {
        this.addListingForm.controls.socialMediaList.controls.at(0)?.controls
        .path.setValue(item.path);
      } else if (item.socialMediaTyp == 'EMAIL') {
        this.addListingForm.controls.socialMediaList.controls.at(1)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'WHATSAPP') {
        this.addListingForm.controls.socialMediaList.controls.at(2)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'FACEBOOK') {
        this.addListingForm.controls.socialMediaList.controls.at(3)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'INSTAGRAM') {
        this.addListingForm.controls.socialMediaList.controls.at(4)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'TWITTER') {
        this.addListingForm.controls.socialMediaList.controls.at(5)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'TIKTOK') {
        this.addListingForm.controls.socialMediaList.controls.at(6)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'SNAPCHAT') {
        this.addListingForm.controls.socialMediaList.controls.at(7)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'PRIVATE') {
        this.addListingForm.controls.socialMediaList.controls.at(8)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'YOUTUBE') {
        this.addListingForm.controls.socialMediaList.controls.at(9)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'PINTEREST') {
        this.addListingForm.controls.socialMediaList.controls.at(10)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'TELEGRAM') {
        this.addListingForm.controls.socialMediaList.controls.at(11)?.controls
        .path.setValue(item.path);

      } else if (item.socialMediaTyp == 'LINKEDIN') {
        this.addListingForm.controls.socialMediaList.controls.at(12)?.controls
        .path.setValue(item.path);

      } else {

        this.addListingForm.controls.socialMediaList.controls.at(13)?.controls
        .path.setValue(item.path);

      }

    });
  }

  onFocus(ev?: any, id?: any) {}

  public getAllCitiesByRegion(regionId: any) {
    this.cityservice.getCitiesByRegion(regionId).subscribe((cityservice) => {
      this.cities = cityservice;
    });
  }

  public getAllDistrictsByCity(cityId: any) {
    this.districtService
      .getDistrictsByCity(cityId)
      .subscribe((districtService) => {
        this.districts = districtService;
      });
  }

  onRegionChange(): void {
    this.getAllCitiesByRegion(this.selectedRegion);
  }

  onCityChange(): void {
    this.getAllDistrictsByCity(this.selectedCity);
  }
  // file js
  selectedFile: any = null;

  onFileSelected(event: any): void {
    this.selectedFile = event.target.files[0] ?? null;
  }
  // end file

  update() {
    this.snackBar.open(' تم تعديل الإعلان بنجاح ✔', 'تم', {
      duration: 2000,

      panelClass: ['green-snackbar', 'login-snackbar', 'my-custom-snackbar'],
    });

    this.addListingForm.controls.mainAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValue = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValue
          );
        }
      }
    );

    this.addListingForm.controls.secondaryAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValue = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValue
          );
        }
      }
    );

    this.addListingForm.controls.generaleAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value == true) {
          const attributeValue = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValue
          );
        }
      }
    );

    this.addListingForm.controls.questionAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValue = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValue
          );
        }
      }
    );

    this.addListingForm.controls.covidAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value == true) {
          const attributeValue = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValue
          );
        }
      }
    );

    this.addListingForm.controls.otherAttributeValue.controls.forEach(
      (attribute) => {
        if (attribute.controls.value.value != '') {
          const attributeValue = new FormGroup({
            attribute_Id: new FormControl(attribute.controls.id.value),
            value: new FormControl(attribute.controls.value.value),
          });
          this.addListingForm.controls.attributeValueList.push(
            attributeValue
          );
        }
      }
    );

    // console.log(this.addListingForm.value);
    this.listingService.updateListing(this.data?.id, this.addListingForm.value).subscribe(
      (response: Listing) => {
        console.log(response);

      },
      (error: HttpErrorResponse) => {
      alert(error.message);
     },
     () =>{
          console.log("done");
     }

    );
  }

  reset(form?: any) {
    form.reset();
    this.base64 = '';
  }

  select(ev: any) {
    // this.addListingForm.get('category')?.setValue(ev);
  }
  getImagePath(evt: any) {
    const file = evt.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64 = reader.result;
      // this.addListingForm.get('image')?.setValue(this.base64);
    };
  }

  close() {
    this.dialog.close();
    this.title.setTitle('الإعلانات');
  }

  onCategoryChange(): void {
    this.getMainAttributesByCategory(this.selectedCategory);
    this.getSecondaryAttributesByCategory(this.selectedCategory);
    this.getGeneralAttributesByCategory(this.selectedCategory);
    this.getQuestionsAttributesByCategory(this.selectedCategory);
    this.getCovidAttributesByCategory(this.selectedCategory);
    this.getOtherAttributesByCategory(this.selectedCategory);
  }

  get attributes(): FormArray {
    return this.addListingForm.get('attributeValueList') as FormArray;
  }

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
    });
  }

  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
    });
  }

  public getMainAttributesByCategory(categoryId: any) {
    this.attributeService
      .getMainAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        attributes.forEach((attribute) => {
          var value = '';
          this.data?.attributeValueList.forEach((element) => {
            if (element.attribute_Id == attribute.id) {
              value = element.value;
            }
          });
          const mainAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(value),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.mainAttributeValue.push(
            mainAttributeFromGroup
          );
        });
      });
  }

  public getSecondaryAttributesByCategory(categoryId: any) {
    this.attributeService
      .getSecondaryAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        attributes.forEach((attribute) => {
          var value = '';
          this.data?.attributeValueList.forEach((element) => {
            if (element.attribute_Id == attribute.id) {
              value = element.value;
            }
          });
          const secondaryAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(value),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.secondaryAttributeValue.push(
            secondaryAttributeFromGroup
          );
        });
      });
  }

  public getGeneralAttributesByCategory(categoryId: any) {
    this.attributeService
      .getGeneralAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        attributes.forEach((attribute) => {
          var value = false;
          this.data?.attributeValueList.forEach((element) => {
            if (element.attribute_Id == attribute.id) {
              value = true;
            }
          });
          const generaleAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(value),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.generaleAttributeValue.push(
            generaleAttributeFromGroup
          );
        });
      });
  }

  public getQuestionsAttributesByCategory(categoryId: any) {
    this.attributeService
      .getQuestionsAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        attributes.forEach((attribute) => {
          var value = '';
          this.data?.attributeValueList.forEach((element) => {
            if (element.attribute_Id == attribute.id) {
              value = element.value;
            }
          });
          const questionAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(value),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.questionAttributeValue.push(
            questionAttributeFromGroup
          );
        });
      });
  }

  public getCovidAttributesByCategory(categoryId: any) {
    this.attributeService
      .getCovidAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        attributes.forEach((attribute) => {
          var value = false;
          this.data?.attributeValueList.forEach((element) => {
            if (element.attribute_Id == attribute.id) {
              value = true;
            }
          });
          const covidAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(value),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.covidAttributeValue.push(
            covidAttributeFromGroup
          );
        });
      });
  }

  public getOtherAttributesByCategory(categoryId: any) {
    this.attributeService
      .getOtherAttributesByCategory(categoryId)
      .subscribe((attributes) => {
        attributes.forEach((attribute) => {
          var value = '';
          this.data?.attributeValueList.forEach((element) => {
            if (element.attribute_Id == attribute.id) {
              value = element.value;
            }
          });
          const otherAttributeFromGroup = new FormGroup({
            id: new FormControl(attribute.id),
            value: new FormControl(value),
            name: new FormControl(attribute.name),
          });
          this.addListingForm.controls.otherAttributeValue.push(
            otherAttributeFromGroup
          );
        });
      });
  }
}
