package amiss.wedding.entities.articles;

import amiss.wedding.entities.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;


@Entity
@Table(name = "article_categories")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleCategory extends BaseEntity {

    @NotNull
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", length = 100)
    @Size(max = 100, message = "Name may not be more than 100 characters")
    private String name;

    @NotNull
    @NotBlank(message = "Description may not be blank")
    @Column(name = "description")
    private String description;

    @Column(name = "active", columnDefinition = "boolean  default false")
    private boolean active;

    @ManyToMany(mappedBy = "articleCategories")
    @JsonIgnore
    private Set<Article> articles;
}
