package amiss.wedding.entities.categories.plans;


public enum PlanType  {
    FREE, SILVER, GOLD, DIAMOND
}
