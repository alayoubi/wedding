package amiss.wedding.repositories.cities;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {
}
