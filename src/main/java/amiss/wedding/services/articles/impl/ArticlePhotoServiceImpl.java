package amiss.wedding.services.articles.impl;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticleCategory;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.articles.dto.ArticlePhotoDto;
import amiss.wedding.entities.articles.dto.MiniArticleDto;
import amiss.wedding.errors.ArticleNotFoundException;
import amiss.wedding.errors.ArticlePhotoNotFoundException;
import amiss.wedding.errors.MiniArticleNotFoundException;
import amiss.wedding.mappers.articles.ArticleMapper;
import amiss.wedding.mappers.articles.ArticlePhotoMapper;
import amiss.wedding.mappers.articles.impl.ArticleMapperImpl;
import amiss.wedding.mappers.articles.impl.ArticlePhotoMapperImpl;
import amiss.wedding.repositories.articles.ArticleCategoryRepository;
import amiss.wedding.repositories.articles.ArticlePhotoRepository;
import amiss.wedding.repositories.articles.ArticleRepository;
import amiss.wedding.repositories.articles.MiniArticleRepository;
import amiss.wedding.services.articles.ArticlePhotoService;
import amiss.wedding.storage.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Service
public class ArticlePhotoServiceImpl implements ArticlePhotoService {

    private final ArticlePhotoRepository articlePhotoRepository;
    private final ArticleRepository articleRepository;
    private final MiniArticleRepository miniArticleRepository;
    private final ArticlePhotoMapper mapper = new ArticlePhotoMapperImpl();
    private final ArticleMapper articleMapper = new ArticleMapperImpl();
    @Value(value = "${spring.profiles.active}")
    private String version;

    @Value(value = "${application.bucket.link}")
    private String link;

    @Autowired
    private StorageService storageService;
    public static final String FILE_NAME = "Photo";

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<ArticlePhotoDto>> findAll() {

        List<ArticlePhoto> articlePhotos = this.articlePhotoRepository.findAll();
        List<ArticlePhotoDto> articlePhotoDtoList =
                articlePhotos.stream().map(mapper::ArticlePhotoToArticlePhotoDto).collect(Collectors.toList());
        return new ResponseEntity<>(articlePhotoDtoList, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<ArticlePhotoDto> findById(Long id) {
        ArticlePhoto articlePhoto = this.articlePhotoRepository.findById(id).orElseThrow(() ->
                new ArticlePhotoNotFoundException(id));
        ArticlePhotoDto articlePhotoDto = mapper.ArticlePhotoToArticlePhotoDto(articlePhoto);
        return new ResponseEntity<>(articlePhotoDto, HttpStatus.OK);
    }

    /**
     * @param articleId
     * @return
     */
    @Override
    public ResponseEntity<List<ArticlePhotoDto>> findAllByArticle(Long articleId) {
        Article article = this.articleRepository.findById(articleId).orElseThrow(() ->
                new ArticleNotFoundException(articleId));
        List<ArticlePhoto> articlePhotos = this.articlePhotoRepository.findAllByArticle(article);
        List<ArticlePhotoDto> articlePhotoDtoList =
                articlePhotos.stream().map(mapper::ArticlePhotoToArticlePhotoDto).collect(Collectors.toList());
        return new ResponseEntity<>(articlePhotoDtoList, HttpStatus.OK);
    }

    /**
     * @param miniArticleId
     * @return
     */
    @Override
    public ResponseEntity<List<ArticlePhotoDto>> findAllByMiniArticle(Long miniArticleId) {
        MiniArticle miniArticle = this.miniArticleRepository.findById(miniArticleId).orElseThrow(() ->
                new MiniArticleNotFoundException(miniArticleId));
        List<ArticlePhoto> articlePhotos = this.articlePhotoRepository.findAllByMiniArticle(miniArticle);
        List<ArticlePhotoDto> articlePhotoDtoList =
                articlePhotos.stream().map(mapper::ArticlePhotoToArticlePhotoDto).collect(Collectors.toList());
        return new ResponseEntity<>(articlePhotoDtoList, HttpStatus.OK);
    }

    /**
     * @param articlePhotoDto
     * @return
     */
    @Override
    public ArticlePhoto creatArticlePhoto(ArticlePhotoDto articlePhotoDto) {
        ArticlePhoto articlePhoto = createPhotoAndSave(articlePhotoDto);
        return articlePhoto;
    }

    private ArticlePhoto createPhotoAndSave(ArticlePhotoDto articlePhotoDto) {
        ArticlePhoto articlePhoto = mapper.articlePhotoDtoToArticlePhoto(articlePhotoDto);
        if (articlePhotoDto.getArticle_Id() != null) {
            Article article = articleRepository.findById(articlePhotoDto.getArticle_Id()).orElseThrow(() ->
                    new ArticleNotFoundException(articlePhotoDto.getArticle_Id()));
            articlePhoto.setArticle(article);
        }

        if (articlePhotoDto.getMiniArticle_Id() != null) {
            MiniArticle miniArticle = miniArticleRepository.findById(
                    articlePhotoDto.getMiniArticle_Id()).orElseThrow(() ->
                    new MiniArticleNotFoundException(articlePhotoDto.getMiniArticle_Id()));
            articlePhoto.setMiniArticle(miniArticle);
        }
        return articlePhotoRepository.save(articlePhoto);
    }

    /**
     * @param articlePhotoDtoList
     * @return
     */
    @Override
    public Set<ArticlePhoto> creatListOfArticlePhoto(
            Set<ArticlePhotoDto> articlePhotoDtoList) {
        Set<ArticlePhoto> articlePhotoSet = new HashSet();
        for (ArticlePhotoDto articlePhotoDto: articlePhotoDtoList) {
            ArticlePhoto articlePhoto = createPhotoAndSave(articlePhotoDto);
            articlePhotoSet.add(articlePhoto);
        }
        return articlePhotoSet;
    }

    /**
     * @param articleId
     * @param photos
     * @return
     */
    @Override
    public ResponseEntity<ArticleDto> uploadArticlePhotos(Long articleId, List<MultipartFile> photos) {
        Article existingArticle = this.articleRepository.findById(articleId).orElseThrow(() ->
                new ArticleNotFoundException(articleId));

        Set<ArticlePhoto> articlePhotoSet = new HashSet<>();
        String key = version + "/articles/" + existingArticle.getId() + "/" + FILE_NAME + "_";
        String link =  this.link + version + "%2Farticles%2F" + existingArticle.getId() + "%2F" + FILE_NAME + "_";
        int index = 0;
        for (MultipartFile photo : photos) {
            String path = link;
            String fileName = key;
            path = storageService.uploadFile(photo, fileName, path);
            ArticlePhoto articlePhoto = new ArticlePhoto(fileName, path, existingArticle.getTitle(), null,
                    existingArticle, photo.getSize());
            articlePhotoRepository.save(articlePhoto);
            articlePhotoSet.add(articlePhoto);
            if (index == 0 && existingArticle.getCoverImg() == null) {
                existingArticle.setCoverImg(articlePhoto);
            }
            index++;
        }
        existingArticle.setArticlePhotos(articlePhotoSet);
        this.articleRepository.save(existingArticle);
        ArticleDto articleDto = articleMapper.articleToArticleDto(existingArticle);
        return new ResponseEntity<>(articleDto, HttpStatus.OK);
    }

    /**
     * @param miniArticleId
     * @param photos
     * @return
     */
    @Override
    public ResponseEntity<MiniArticleDto> uploadMiniArticlePhotos(Long miniArticleId, List<MultipartFile> photos) {
        MiniArticle existingMiniArticle = this.miniArticleRepository.findById(miniArticleId).orElseThrow(() ->
                new MiniArticleNotFoundException(miniArticleId));
        Set<ArticlePhoto> articlePhotoSet = new HashSet<>();
        String key = version + "/miniarticles/" + existingMiniArticle.getId() + "/" + FILE_NAME + "_";
        String link =  this.link + version + "%2Fminiarticles%2F" + existingMiniArticle.getId() + "%2F" + FILE_NAME + "_";
        for (MultipartFile photo : photos) {
            String path = link;
            String fileName = key;
            path = storageService.uploadFile(photo, fileName, path);
            ArticlePhoto articlePhoto = new ArticlePhoto(fileName, path, existingMiniArticle.getTitle(),
                    existingMiniArticle, null, photo.getSize());
            articlePhotoRepository.save(articlePhoto);
            articlePhotoSet.add(articlePhoto);
        }
        existingMiniArticle.setMiniArticlePhotos(articlePhotoSet);
        this.miniArticleRepository.save(existingMiniArticle);
        MiniArticleDto miniArticleDto = articleMapper.miniArticleToMiniArticleDto(existingMiniArticle);
        return new ResponseEntity<>(miniArticleDto, HttpStatus.OK);
    }

    /**
     * @param id
     * @param multipartFile
     * @return
     */
    @Override
    public ResponseEntity<ArticlePhotoDto> update(Long id, MultipartFile multipartFile) {
        return null;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> remove(Long id) {
        return null;
    }
}
