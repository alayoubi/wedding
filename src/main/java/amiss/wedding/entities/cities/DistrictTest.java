package amiss.wedding.entities.cities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DistrictTest {

    private Long district_id;

    private Long city_id;

    private Long region_id;

    private String name_ar;

    private String name_en;
}
