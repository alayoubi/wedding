package amiss.wedding.mappers.users;

import amiss.wedding.entities.users.client.Client;
import amiss.wedding.entities.users.client.dto.ClientDto;

public class ClientMapperImpl implements ClientMapper{

    @Override
    public Client clientDtoToClient(ClientDto clientDto) {
        Client client = new Client();
        client.setName(clientDto.getName());
        client.setMobile(clientDto.getMobile());
        client.setAddress(clientDto.getAddress());
        client.setPhoto(clientDto.getPhoto());
        return client;
    }

    @Override
    public ClientDto clientToClientDto(Client client) {
        return new ClientDto(client);
    }
}
