export interface Attribute{
  id: number;
  name: string;
  valueType: string;
  attributeType: string;
  unit: string;
  active: boolean;
}