package amiss.wedding.mappers.users;


import amiss.wedding.entities.users.client.Client;
import amiss.wedding.entities.users.client.dto.ClientDto;

public interface ClientMapper {


    Client clientDtoToClient(ClientDto clientDto);

    ClientDto clientToClientDto(Client client);
}
