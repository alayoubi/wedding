import { ClientService } from '@core/services/client/client.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { DefaultModule } from './default/default.module';
import { AddListingCompontsModule } from './module/listing/components/add.listing.componts/add.listing.componts.module';


@NgModule({
  declarations: [AdminComponent],
  imports: [CommonModule, AdminRoutingModule, DefaultModule, AddListingCompontsModule],
  providers: [ClientService],

})
export class AdminModule {}
