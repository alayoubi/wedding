package amiss.wedding.repositories.categories.attribute;

import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.attributes.AttributeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Long> {
    List<Attribute> findByCategory(Category category);
    List<Attribute> findByCategoryAndAttributeType(Category category, AttributeType attributeType);
}
