package amiss.wedding.controllers.admin.users;

import amiss.wedding.entities.users.dto.RoleDto;
import amiss.wedding.services.users.RoleService;
import amiss.wedding.services.users.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping(path = "/api/v1/admin/user")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;
    private final UserService userService;


    @RequestMapping(value = "/{id}/role", method = RequestMethod.PUT)
    @ApiOperation(value = "Set Role for User",
            authorizations = {@Authorization("basicAuth")})
    public ResponseEntity updateRole(Principal principal, @PathVariable long id, @RequestBody RoleDto role) {

//        userService.checkRole(principal, Role.ADMINISTRATOR);

        roleService.setRole(id, role.getRole());
        return new ResponseEntity(HttpStatus.OK);
    }
}
