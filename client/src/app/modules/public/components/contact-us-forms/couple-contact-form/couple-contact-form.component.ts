import { CoupleContactDoneComponent } from './../couple-contact-done/couple-contact-done.component';
import { RegionService } from '@core/services/city/region.service';
import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-couple-contact-form',
  templateUrl: './couple-contact-form.component.html',
  styleUrls: ['./couple-contact-form.component.css'],
})
export class CoupleContactFormComponent implements OnInit {
  public regions: any;
  constructor(
    private regionService: RegionService,
    private titleService: Title
  ) {
    titleService.setTitle('إستمارة التواصل');
  }

  contactForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    regions: new FormControl('', [Validators.required]),
    msg: new FormControl(''),
  });

  get emailFun() {
    return this.contactForm.get('email');
  }

  get regionFun() {
    return this.contactForm.get('regions');
  }

  ngOnInit(): void {
    this.getAllActiveRegion();
  }

  public submit() {
    console.log(this.contactForm.valid);
    console.log(this.contactForm.value);
    // this.contactForm.reset();
    // this.snackBar.openFromComponent(CoupleContactDoneComponent, {
    //   duration:3000
    // });
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }

  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
      console.log(this.regions);
    });
  }
}
