package amiss.wedding.mappers.users;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.dto.UserDto;

public interface UserMapper {

    AppUser userDtoToUser(UserDto userDto);

    UserDto userToUserDto(AppUser user);
}
