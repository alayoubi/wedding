package amiss.wedding.repositories.listings;


import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListingPhotoRepository extends JpaRepository<ListingPhoto, Long> {

    List<ListingPhoto> findAllByListing(Listing listing);
    List<ListingPhoto> findAllByStatus(Status status);
    List<ListingPhoto> findAllByListingAndStatus(Listing listing, Status status);
}
