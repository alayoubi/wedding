import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideographersLandingComponent } from './videographers-landing.component';

describe('VideographersLandingComponent', () => {
  let component: VideographersLandingComponent;
  let fixture: ComponentFixture<VideographersLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideographersLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VideographersLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
