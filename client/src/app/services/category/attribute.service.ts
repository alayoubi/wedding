import { API } from './../../config/api.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Attribute } from '@core/models/category/Attribute';
@Injectable({
  providedIn: 'root',
})
export class AttributeService {
  constructor(private http: HttpClient) {}

  getAttributes(): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(API.ATTRIBUTES.BASE);
  }

  getAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(API.ATTRIBUTES.CATEGORY.ID(countryId));
  }

  getMainAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(API.ATTRIBUTES.CATEGORY.MAIN(countryId));
  }

  getSecondaryAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(
      API.ATTRIBUTES.CATEGORY.SECONDARY(countryId)
    );
  }

  getGeneralAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(
      API.ATTRIBUTES.CATEGORY.GENERAL(countryId)
    );
  }

  getQuestionsAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(
      API.ATTRIBUTES.CATEGORY.QUESTIONS(countryId)
    );
  }

  getCovidAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(API.ATTRIBUTES.CATEGORY.COVID(countryId));
  }

  getOtherAttributesByCategory(countryId: number): Observable<Attribute[]> {
    return this.http.get<Attribute[]>(API.ATTRIBUTES.CATEGORY.OTHER(countryId));
  }

  getAttribute(attributeId: number): Observable<Attribute> {
    return this.http.get<Attribute>(API.ATTRIBUTES.ID(attributeId));
  }

  createAtt(categoryId: number, model:any): Observable<any> {
    return this.http.post(API.ATTRIBUTES.POST(categoryId),model)
  }
}
