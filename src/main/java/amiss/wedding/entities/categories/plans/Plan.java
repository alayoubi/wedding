package amiss.wedding.entities.categories.plans;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.listings.Listing;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "plans")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Plan extends BaseEntity {

    @NotNull
    @JoinColumn(name="duration")
    private Integer duration;

    @NotNull
    @JoinColumn(name="cost")
    private BigDecimal cost;

    @ManyToOne
    @JoinColumn(name="category_id", nullable=false)
    private Category category;

    @Column(name = "plan_type", length = 32, columnDefinition = "varchar(255) default 'FREE'")
    @Enumerated(EnumType.STRING)
    private PlanType planType;

    @OneToMany(mappedBy="plan")
    private Set<Listing> listings;

    @OneToMany(mappedBy="plan", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Feature> features;

    @Column(name = "count_of_photos", columnDefinition = "Integer default 0")
    private Integer countOfPhotos;

    @Column(columnDefinition = "boolean default false")
    private boolean video;
}
