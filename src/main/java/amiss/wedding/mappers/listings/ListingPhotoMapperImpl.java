package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;

public class ListingPhotoMapperImpl implements ListingPhotoMapper{

    @Override
    public ListingPhoto listingPhotoDtoToListingPhoto(ListingPhotoDto listingPhotoDto) {
        ListingPhoto listingPhoto = new ListingPhoto();
        listingPhoto.setFileName(listingPhotoDto.getFileName());
        listingPhoto.setPath(listingPhotoDto.getPath());
        listingPhoto.setStatus(listingPhotoDto.getStatus());
        listingPhoto.setSize(listingPhotoDto.getSize());
        return listingPhoto;
    }

    @Override
    public ListingPhotoDto listingPhotoToListingPhotoDto(ListingPhoto listingPhoto) {
        return new ListingPhotoDto(listingPhoto);
    }
}
