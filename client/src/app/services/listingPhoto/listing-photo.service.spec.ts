import { TestBed } from '@angular/core/testing';

import { ListingPhotoService } from './listing-photo.service';

describe('ListingPhotoService', () => {
  let service: ListingPhotoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListingPhotoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
