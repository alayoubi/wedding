import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dresses',
  templateUrl: './dresses.component.html',
  styleUrls: ['./dresses.component.css']
})
export class DressesComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('فساتين الزفاف')
   }

  ngOnInit(): void {
  }

}
