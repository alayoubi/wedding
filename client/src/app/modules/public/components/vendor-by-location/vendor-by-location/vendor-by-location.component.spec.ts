import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorByLocationComponent } from './vendor-by-location.component';

describe('VendorByLocationComponent', () => {
  let component: VendorByLocationComponent;
  let fixture: ComponentFixture<VendorByLocationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorByLocationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorByLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
