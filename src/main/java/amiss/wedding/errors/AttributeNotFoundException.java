package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class AttributeNotFoundException extends ApplicationRuntimeException {

    public AttributeNotFoundException(Long id){
        super("The Attribute with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
