package amiss.wedding.entities.users.dto;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.Role;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;
    private String firstname;
    private String lastname;
    @NotEmpty
    public String password;
    @NotNull
    private String email;
    private boolean active;
    private Instant verified_at;
    private String role;

    public UserDto(String password, String email) {
        this.password = password;
        this.email = email;
    }

    public UserDto(AppUser user){
        this.id = user.getId();
        this.firstname = user.getFirstname();
        this.lastname = user.getLastname();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.active = user.isActive();
        this.verified_at = user.getVerified_at();
        Set<Long> roles = new HashSet();
        this.role = role;
    }
}
