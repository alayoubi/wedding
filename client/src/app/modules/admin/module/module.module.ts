import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsComponent } from './posts/posts.component';
import { CategoriesComponent } from './categories/categories.component';
import { CustomersModule } from './customers/customers.module';
import { MatDividerModule } from '@angular/material/divider';
import { AddListingCompontsComponent } from './listing/components/add.listing.componts/add.listing.componts.component';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatSortModule} from '@angular/material/sort';
import { ArticlesComponent } from './articles/articles/articles.component';
import { AddWeddingHallComponent } from './listing/add-wedding-hall/add-wedding-hall.component';
import { ListingsComponent } from './listing/components/listings/listings.component';
import { UpdateListingComponent } from './listing/components/update-listing/update-listing.component';
import { CreateAttributeComponent } from './listing/components/create-attribute/create-attribute.component';
import { SpinnerOverlayComponent } from './listing/components/spinner-overlay/spinner-overlay.component';
import { UploadPhotoOverlayComponent } from './listing/components/upload-photo-overlay/upload-photo-overlay.component';

import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PublicModule } from '@core/modules/public/public.module';
import { TextEditorComponent } from './listing/components/text-editor/text-editor.component';
// import { NgxEditorModule } from 'ngx-editor';
import { DialogComponent } from './categories/dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { UpdateComponent } from './categories/update/update.component';
import { RouterLink } from '@angular/router';
import { AddArticleComponent } from './articles/add-article/add-article.component';
import { AddMiniArticleComponent } from './articles/add-mini-article/add-mini-article.component';
import { UploadPhotosMiniArticleComponent } from './articles/upload-photos-mini-article/upload-photos-mini-article.component';



@NgModule({
  declarations: [
    PostsComponent,
    CategoriesComponent,
    AddListingCompontsComponent,
    ArticlesComponent,
    AddWeddingHallComponent,
    ListingsComponent,
    UpdateListingComponent,
    CreateAttributeComponent,
    SpinnerOverlayComponent,
    UploadPhotoOverlayComponent,
    TextEditorComponent,
    DialogComponent,
    UpdateComponent,
    AddArticleComponent,
    AddMiniArticleComponent,
    UploadPhotosMiniArticleComponent,
  ],
  imports: [
    CommonModule,
    CustomersModule,
    MatDividerModule,
    MatListModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatTableModule,
    MatIconModule,
    MatCheckboxModule,
    MatButtonModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    MatNativeDateModule,
    MatRippleModule,
    MatSortModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    PublicModule,
    // NgxEditorModule,
    MatDialogModule,
    MatToolbarModule,
    RouterLink

  ],
})
export class ModuleModule {}
