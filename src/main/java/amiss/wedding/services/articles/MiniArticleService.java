package amiss.wedding.services.articles;

import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.MiniArticleDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;

public interface MiniArticleService {

    ResponseEntity<List<MiniArticleDto>> findAll();
    ResponseEntity<MiniArticleDto> findById(long id);
    ResponseEntity<List<MiniArticleDto>> findAllByArticle(Long id);
    ResponseEntity<MiniArticleDto> createStartMiniArticle(MiniArticleDto articleDto);
    ResponseEntity<MiniArticleDto> create(MiniArticleDto articleDto);

    Set<MiniArticle> createFullMiniArticle(Set<MiniArticleDto> miniArticleDtoSet, Long article_Id);
    ResponseEntity<MiniArticleDto> update(Long id, MiniArticleDto articleDto);
    ResponseEntity<String> remove(Long id);
}
