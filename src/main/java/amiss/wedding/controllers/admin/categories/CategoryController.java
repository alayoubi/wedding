package amiss.wedding.controllers.admin.categories;

import amiss.wedding.entities.categories.dto.CategoryDto;
import amiss.wedding.services.categories.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("")
    public ResponseEntity<List<CategoryDto>> getCategories(){
        return this.categoryService.findAll();
    }

    @GetMapping(path = "/{categoryId}")
    public ResponseEntity<CategoryDto> getCategory(@PathVariable("categoryId") Long categoryId){
        return this.categoryService.findById(categoryId);
    }

//    @GetMapping(path = "/{categoryName}")
//    public ResponseEntity<CategoryDto> getCategoryByName(@PathVariable("categoryName") String categoryName){
//        return this.categoryService.findByName(categoryName);
//    }

    @PostMapping()
    public ResponseEntity<CategoryDto> createCategory(@RequestBody CategoryDto categoryDto){
        return this.categoryService.create(categoryDto);
    }

    @PutMapping(path = "/{categoryId}")
    public ResponseEntity<CategoryDto> updateCategory(@PathVariable Long categoryId, @RequestBody CategoryDto categoryDto){
        return this.categoryService.update(categoryId, categoryDto);
    }

    @DeleteMapping(path = "/{categoryId}")
    public ResponseEntity<String> removeCategory(@PathVariable Long categoryId){
        return this.categoryService.remove(categoryId);
    }
}
