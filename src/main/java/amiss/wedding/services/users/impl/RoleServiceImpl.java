package amiss.wedding.services.users.impl;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.Role;
import amiss.wedding.errors.UserNotFoundException;
import amiss.wedding.repositories.users.UserRepository;
import amiss.wedding.services.users.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final UserRepository repository;


    /**
     * @param id
     * @param role
     */
    @Override
    public void setRole(long id, Role role) {

        AppUser user = this.repository.findById(id).orElseThrow(() ->
                new UserNotFoundException(id));
        user.setRole(role);
        this.repository.save(user);
    }
}
