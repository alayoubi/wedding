import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingVendorCategoriesComponent } from './landing-vendor-categories.component';

describe('LandingVendorCategoriesComponent', () => {
  let component: LandingVendorCategoriesComponent;
  let fixture: ComponentFixture<LandingVendorCategoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingVendorCategoriesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LandingVendorCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
