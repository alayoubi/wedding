import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-show-category-data-landing',
  templateUrl: './show-category-data-landing.component.html',
  styleUrls: ['./show-category-data-landing.component.css'],
})
export class ShowCategoryDataLandingComponent implements OnInit {
  public titleStr: any
  constructor(private title : Title,private route: ActivatedRoute, public router: Router) {
    this.route.params.subscribe((params) => {
      this.title.setTitle(`${params['category']}`);
      this.titleStr=title
    });
  }

  ngOnInit(): void { }

}
