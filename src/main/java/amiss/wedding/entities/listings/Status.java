package amiss.wedding.entities.listings;

public enum Status {

    PENDING("pending", 0),
    PROCESSING( "processing", 1),
    COMPLETED( "completed", 2),
    DECLINE("decline", 3),
    ARCHIVED("archived", 4),;

    private String name;
    private Integer level;
    Status(String name, Integer level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public Integer getLevel() {
        return level;
    }

    public static String[] getAllRoles() {
        String[] result = new String[Status.values().length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Status.values()[i].name();
        }
        return result;
    }


    public static Status findByName(String name) {
        for (Status status : values()) {
            if (status.name.equals(name)) {
                return status;
            }
        }
        // either throw the IAE or return null, your choice.
        throw new IllegalArgumentException(String.valueOf(name));
    }

    public static Status findByLevel(int level) {
        for (Status status : values()) {
            if (status.level == level) {
                return status;
            }
        }
        // either throw the IAE or return null, your choice.
        throw new IllegalArgumentException(String.valueOf(level));
    }
}
