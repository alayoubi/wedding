package amiss.wedding.entities.articles.dto;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticleCategory;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ArticleCategoryDto {

    private Long id;
    private String name;
    private String description;
    private boolean active;
    private Set<ArticleDto> articles = new HashSet();

    public ArticleCategoryDto(ArticleCategory articleCategory) {
        this.id = articleCategory.getId();
        this.name = articleCategory.getName();
        this.description = articleCategory.getDescription();
        this.active = articleCategory.isActive();
        if (!articleCategory.getArticles().isEmpty()) {
            for (Article article : articleCategory.getArticles()) {
                this.articles.add(new ArticleDto(article));
            }
        }
    }
}
