import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  postcustomers(data: any) {
    return this.http.post<any>('http://localhost:3000/customersList/', data);
  }
  getcustomers() {
    return this.http.get<any>('http://localhost:3000/customersList/');
  }
  putCustomers(data: any, id: number) {
    return this.http.put<any>(
      'http://localhost:3000/customersList/'+ id,
      data
    );
  }
  deleteCustomers(id: number) {
    return this.http.delete<any>('http://localhost:3000/customersList/' + id);
  }
}
