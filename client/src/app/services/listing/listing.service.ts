import { Listing } from './../../models/listing/Listing';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API } from '@core/config/api.config';
import { Category } from '@core/models/category/Category';

@Injectable({
  providedIn: 'root',
})
export class ListingService {
  constructor(private http: HttpClient) {}

  /**
   *
   * @returns
   */
  getListings(): Observable<Listing[]> {
    return this.http.get<Listing[]>(API.LISTINGS.BASE);
  }

  /**
   *
   * @param categoryId
   * @returns
   */
  getListing(listingId: number): Observable<Listing> {
    return this.http.get<Listing>(API.LISTINGS.ID(listingId));
  }

  /**
   *
   * @param categoryId
   * @param RegionID
   * @returns
   */
  getListingByCatAndReg(catID: number, regID: number): Observable<Listing> {
    return this.http.get<Listing>(API.LISTINGS.CATIDANDREGID(catID, regID));
  }

  /**
   *
   * @param categoryId
   *
   * @returns
   */
  getListingByCatId(categoryId: any): Observable<Category> {
    return this.http.get<Category>(API.LISTINGS.DATACATID(categoryId));
  }

  /**
   *
   * @param categoryId
   *
   * @returns
   */
  getListingByRegId(regionId: any): Observable<Category> {
    return this.http.get<Category>(API.LISTINGS.DATAREGID(regionId));
  }

  addListing(listing: any): Observable<Listing> {
    return this.http.post<Listing>(API.LISTINGS.BASE, listing, {reportProgress: true});
  }

  BASE_URL = 'https://fakestoreapi.com';

  getAllProducts(): Observable<any> {
    return this.http.get(`${this.BASE_URL}/products`);
  }

  /**
   *
   * @param updateListing
   *
   * @returns
   */
  updateListing(listingId: number, Model: any): Observable<Listing> {
    return this.http.put<Listing>(API.LISTINGS.UPDATELISTING(listingId), Model);
  }

  /**
   *
   * @param deleteListing
   *
   * @returns
   */
  deleteListing(listingId: any): Observable<Listing> {
    return this.http.delete<Listing>(API.LISTINGS.DELETELISTING(listingId));
  }
}
