package amiss.wedding.services.articles.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.articles.ArticleCategory;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.articles.dto.ArticlePhotoDto;
import amiss.wedding.errors.ArticleCategoryNotFoundException;
import amiss.wedding.repositories.articles.ArticleCategoryRepository;
import amiss.wedding.repositories.articles.ArticlePhotoRepository;
import amiss.wedding.repositories.articles.ArticleRepository;
import amiss.wedding.repositories.articles.MiniArticleRepository;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.articles.ArticlePhotoService;
import amiss.wedding.services.articles.MiniArticleService;
import amiss.wedding.storage.StorageService;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ArticleServiceImpl.class})
@ExtendWith(SpringExtension.class)
class ArticleServiceImplTest {
    @MockBean
    private ArticleCategoryRepository articleCategoryRepository;

    @MockBean
    private ArticlePhotoRepository articlePhotoRepository;

    @MockBean
    private ArticlePhotoService articlePhotoService;

    @MockBean
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleServiceImpl articleServiceImpl;

    @MockBean
    private CategoryRepository categoryRepository;

    @MockBean
    private ListingsRepository listingsRepository;

    @MockBean
    private MiniArticleRepository miniArticleRepository;

    @MockBean
    private MiniArticleService miniArticleService;

    @MockBean
    private StorageService storageService;

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    void testCreateFullArticle() {
        ResponseEntity<ArticleDto> actualCreateFullArticleResult = articleServiceImpl.createFullArticle(new ArticleDto());
        assertTrue(actualCreateFullArticleResult.hasBody());
        assertTrue(actualCreateFullArticleResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualCreateFullArticleResult.getStatusCode());
    }

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testCreateFullArticle2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "java.util.Collection.isEmpty()" because "that" is null
        //       at amiss.wedding.services.articles.impl.ArticleServiceImpl.createFullArticle(ArticleServiceImpl.java:139)
        //   See https://diff.blue/R013 to resolve this issue.

        ArticleDto.ArticleDtoBuilder builderResult = ArticleDto.builder();
        ArticleDto.ArticleDtoBuilder articleCategoriesResult = builderResult.articleCategories(new HashSet<>());
        ArticleDto.ArticleDtoBuilder articleCategories_IDsResult = articleCategoriesResult
                .articleCategories_IDs(new HashSet<>());
        ArticleDto.ArticleDtoBuilder articlePhotosResult = articleCategories_IDsResult.articlePhotos(new HashSet<>());
        ArticleDto.ArticleDtoBuilder articlePhotos_IDResult = articlePhotosResult.articlePhotos_ID(new HashSet<>());
        ArticleDto.ArticleDtoBuilder categoriesResult = articlePhotos_IDResult.categories(new HashSet<>());
        ArticleDto.ArticleDtoBuilder coverImg_idResult = categoriesResult.category_IDs(new HashSet<>())
                .content("Not all who wander are lost")
                .coverImg("Cover Img");
        ArticleDto.ArticleDtoBuilder idResult = coverImg_idResult
                .creat_at(Date.from(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant()))
                .id(1L);
        ArticleDto.ArticleDtoBuilder listingsResult = idResult.listings(new HashSet<>());
        ArticleDto.ArticleDtoBuilder listings_IDsResult = listingsResult.listings_IDs(new HashSet<>());
        ArticleDto.ArticleDtoBuilder miniArticlesResult = listings_IDsResult.miniArticles(new HashSet<>());
        ArticleDto articleDto = miniArticlesResult.miniArticles_ID(new HashSet<>()).readTime(1).title("Dr").build();
        articleServiceImpl.createFullArticle(articleDto);
    }

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testCreateFullArticle3() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.articles.dto.ArticleDto.getTitle()" because "articleDto" is null
        //       at amiss.wedding.mappers.articles.impl.ArticleMapperImpl.articleDtoToArticle(ArticleMapperImpl.java:24)
        //       at amiss.wedding.services.articles.impl.ArticleServiceImpl.createFullArticle(ArticleServiceImpl.java:102)
        //   See https://diff.blue/R013 to resolve this issue.

        articleServiceImpl.createFullArticle(null);
    }

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    void testCreateFullArticle4() {
        ArticleDto articleDto = mock(ArticleDto.class);
        when(articleDto.getContent()).thenReturn("Not all who wander are lost");
        when(articleDto.getTitle()).thenReturn("Dr");
        when(articleDto.getArticleCategories_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getArticlePhotoDto()).thenReturn(new HashSet<>());
        when(articleDto.getCategory_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getListings_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getMiniArticles()).thenReturn(new HashSet<>());
        ResponseEntity<ArticleDto> actualCreateFullArticleResult = articleServiceImpl.createFullArticle(articleDto);
        assertTrue(actualCreateFullArticleResult.hasBody());
        assertEquals(HttpStatus.OK, actualCreateFullArticleResult.getStatusCode());
        assertTrue(actualCreateFullArticleResult.getHeaders().isEmpty());
        verify(articleDto).getContent();
        verify(articleDto).getTitle();
        verify(articleDto).getArticleCategories_IDs();
        verify(articleDto).getArticlePhotoDto();
        verify(articleDto).getCategory_IDs();
        verify(articleDto).getListings_IDs();
        verify(articleDto).getMiniArticles();
    }

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    void testCreateFullArticle5() {
        ArticleCategory articleCategory = new ArticleCategory();
        articleCategory.setActive(true);
        articleCategory.setArticles(new HashSet<>());
        articleCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        articleCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        articleCategory.setDescription("The characteristics of someone or something");
        articleCategory.setId(1L);
        articleCategory.setName("Name");
        articleCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        articleCategory.setVersion(1);
        Optional<ArticleCategory> ofResult = Optional.of(articleCategory);
        when(articleCategoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);

        HashSet<Long> resultLongSet = new HashSet<>();
        resultLongSet.add(5L);
        ArticleDto articleDto = mock(ArticleDto.class);
        when(articleDto.getContent()).thenReturn("Not all who wander are lost");
        when(articleDto.getTitle()).thenReturn("Dr");
        when(articleDto.getArticleCategories_IDs()).thenReturn(resultLongSet);
        when(articleDto.getArticlePhotoDto()).thenReturn(new HashSet<>());
        when(articleDto.getCategory_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getListings_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getMiniArticles()).thenReturn(new HashSet<>());
        ResponseEntity<ArticleDto> actualCreateFullArticleResult = articleServiceImpl.createFullArticle(articleDto);
        assertTrue(actualCreateFullArticleResult.hasBody());
        assertEquals(HttpStatus.OK, actualCreateFullArticleResult.getStatusCode());
        assertTrue(actualCreateFullArticleResult.getHeaders().isEmpty());
        verify(articleCategoryRepository).findById(Mockito.<Long>any());
        verify(articleDto).getContent();
        verify(articleDto).getTitle();
        verify(articleDto, atLeast(1)).getArticleCategories_IDs();
        verify(articleDto).getArticlePhotoDto();
        verify(articleDto).getCategory_IDs();
        verify(articleDto).getListings_IDs();
        verify(articleDto).getMiniArticles();
    }

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    void testCreateFullArticle6() {
        Optional<ArticleCategory> emptyResult = Optional.empty();
        when(articleCategoryRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);

        HashSet<Long> resultLongSet = new HashSet<>();
        resultLongSet.add(5L);
        ArticleDto articleDto = mock(ArticleDto.class);
        when(articleDto.getContent()).thenReturn("Not all who wander are lost");
        when(articleDto.getTitle()).thenReturn("Dr");
        when(articleDto.getArticleCategories_IDs()).thenReturn(resultLongSet);
        when(articleDto.getCategory_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getListings_IDs()).thenReturn(new HashSet<>());
        assertThrows(ArticleCategoryNotFoundException.class, () -> articleServiceImpl.createFullArticle(articleDto));
        verify(articleCategoryRepository).findById(Mockito.<Long>any());
        verify(articleDto).getContent();
        verify(articleDto).getTitle();
        verify(articleDto, atLeast(1)).getArticleCategories_IDs();
        verify(articleDto).getCategory_IDs();
        verify(articleDto).getListings_IDs();
    }

    /**
     * Method under test: {@link ArticleServiceImpl#createFullArticle(ArticleDto)}
     */
    @Test
    void testCreateFullArticle7() {
        ArticleCategory articleCategory = new ArticleCategory();
        articleCategory.setActive(true);
        articleCategory.setArticles(new HashSet<>());
        articleCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        articleCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        articleCategory.setDescription("The characteristics of someone or something");
        articleCategory.setId(1L);
        articleCategory.setName("Name");
        articleCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        articleCategory.setVersion(1);
        Optional<ArticleCategory> ofResult = Optional.of(articleCategory);
        when(articleCategoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        when(articlePhotoService.creatListOfArticlePhoto(Mockito.<Set<ArticlePhotoDto>>any()))
                .thenReturn(new HashSet<>());

        HashSet<Long> resultLongSet = new HashSet<>();
        resultLongSet.add(5L);

        HashSet<ArticlePhotoDto> articlePhotoDtoSet = new HashSet<>();
        ArticlePhotoDto buildResult = ArticlePhotoDto.builder()
                .article_Id(1L)
                .fileName("foo.txt")
                .id(1L)
                .miniArticle_Id(1L)
                .path("Path")
                .size(10.0d)
                .title("Dr")
                .build();
        articlePhotoDtoSet.add(buildResult);
        ArticleDto articleDto = mock(ArticleDto.class);
        when(articleDto.getContent()).thenReturn("Not all who wander are lost");
        when(articleDto.getTitle()).thenReturn("Dr");
        when(articleDto.getArticleCategories_IDs()).thenReturn(resultLongSet);
        when(articleDto.getArticlePhotoDto()).thenReturn(articlePhotoDtoSet);
        when(articleDto.getCategory_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getListings_IDs()).thenReturn(new HashSet<>());
        when(articleDto.getMiniArticles()).thenReturn(new HashSet<>());
        ResponseEntity<ArticleDto> actualCreateFullArticleResult = articleServiceImpl.createFullArticle(articleDto);
        assertTrue(actualCreateFullArticleResult.hasBody());
        assertEquals(HttpStatus.OK, actualCreateFullArticleResult.getStatusCode());
        assertTrue(actualCreateFullArticleResult.getHeaders().isEmpty());
        verify(articleCategoryRepository).findById(Mockito.<Long>any());
        verify(articlePhotoService).creatListOfArticlePhoto(Mockito.<Set<ArticlePhotoDto>>any());
        verify(articleDto).getContent();
        verify(articleDto).getTitle();
        verify(articleDto, atLeast(1)).getArticleCategories_IDs();
        verify(articleDto, atLeast(1)).getArticlePhotoDto();
        verify(articleDto).getCategory_IDs();
        verify(articleDto).getListings_IDs();
        verify(articleDto).getMiniArticles();
    }
}

