import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccommodationsAmenitiesIncludedComponent } from './accommodations-amenities-included.component';

describe('AccommodationsAmenitiesIncludedComponent', () => {
  let component: AccommodationsAmenitiesIncludedComponent;
  let fixture: ComponentFixture<AccommodationsAmenitiesIncludedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccommodationsAmenitiesIncludedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccommodationsAmenitiesIncludedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
