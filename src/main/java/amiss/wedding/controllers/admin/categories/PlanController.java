package amiss.wedding.controllers.admin.categories;


import amiss.wedding.entities.categories.plans.PlanDto;
import amiss.wedding.services.categories.PlanService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/plans")
@AllArgsConstructor
public class PlanController {

    private final PlanService planService;

    @GetMapping("")
    public ResponseEntity<List<PlanDto>> getPlans(){
        return this.planService.findAll();
    }

    @GetMapping(path = "/{planId}")
    public ResponseEntity<PlanDto> getPlan(@PathVariable("planId") Long planId){
        return this.planService.findById(planId);
    }

    @PostMapping()
    public ResponseEntity<PlanDto> createPlan(@RequestBody PlanDto planDto){
        return this.planService.create(planDto);
    }

    @PutMapping(path = "/{planId}")
    public ResponseEntity<PlanDto> updatePlan(@PathVariable Long planId, @RequestBody PlanDto planDto){
        return this.planService.update(planId, planDto);
    }

    @DeleteMapping(path = "/{planId}")
    public ResponseEntity<String> removePlan(@PathVariable Long planId){
        return this.planService.remove(planId);
    }

    @GetMapping(path = "/category/{category_id}")
    public ResponseEntity<List<PlanDto>> getPlansByCategoryId(@PathVariable("category_id") Long category_id){
        return this.planService.findByCategory(category_id);
    }
}
