package amiss.wedding.entities.categories.dto;


import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.attributes.Attribute;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernate_lazy_initializer", "handler"})
public class CategoryDto {

    private Long id;
    private String name;
    private String description;
    private boolean active;
    private Long coverImg_id;
    private String cover_img;
    private Set<PlanDto> plans = new HashSet();
    private Set<Long> plans_ID = new HashSet();
    private Set<AttributeDto> attributes = new HashSet();
    private Set<Long> attributes_ID = new HashSet();
    private Long category_id;
    private String category;

    public CategoryDto(Long id, String name, String description, boolean active, String cover_img
    , Long category_id) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.active = active;
        this.cover_img = cover_img;
        this.category_id = category_id;
    }

    public CategoryDto(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.description = category.getDescription();
        this.active = category.isActive();
        this.coverImg_id = category.getCoverImg() == null? null : category.getCoverImg().getId();
        this.cover_img = category.getCoverImg() == null? null : category.getCoverImg().getPath();

        if (!category.getPlan().isEmpty()) {
            for (Plan plan: category.getPlan()) {
                this.plans_ID.add(plan.getId());
                this.plans.add(new PlanDto(plan));
            }
        }

        if (!category.getAttributes().isEmpty()) {
            for (Attribute attribute: category.getAttributes()) {
                this.attributes_ID.add(attribute.getId());
                this.attributes.add(new AttributeDto(attribute));
            }
        }
        this.category = category.getParentCategory()== null? null : category.getParentCategory().getName();
        this.category_id = category.getParentCategory()== null? null : category.getParentCategory().getId();

    }
}
