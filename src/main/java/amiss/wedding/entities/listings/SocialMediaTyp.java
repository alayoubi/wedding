package amiss.wedding.entities.listings;

public enum SocialMediaTyp {
    PHONE,
    EMAIL,
    WHATSAPP,
    FACEBOOK,
    INSTAGRAM,
    LINKEDIN,
    TIKTOK,
    SNAPCHAT,
    TWITTER,
    YOUTUBE,
    PINTEREST,
    PRIVATE,
    TELEGRAM,
    OTHER
}
