package amiss.wedding.services.categories;

import amiss.wedding.entities.categories.attributes.AttributeType;
import amiss.wedding.entities.categories.attributes.dto.AttributeDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AttributeService {

    ResponseEntity<List<AttributeDto>> findAll();
    ResponseEntity<AttributeDto> findById(Long id);
    ResponseEntity<AttributeDto> create(AttributeDto attributeDto, Long category_id);
    ResponseEntity<AttributeDto> update(Long id, AttributeDto attributeDto);
    ResponseEntity<String> remove(Long id);
    ResponseEntity<List<AttributeDto>> findByCategory(Long category_id);
    ResponseEntity<List<AttributeDto>> findByCategoryAndAttributeTyp(Long category_id, AttributeType attributeType);

}
