package amiss.wedding.registration;


import amiss.wedding.entities.users.Role;
import lombok.*;

public record RegistrationRequest(String firstName, String lastName, String email, String password, Role role) {

}
