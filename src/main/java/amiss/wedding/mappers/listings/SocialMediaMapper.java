package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.AttributeValue;
import amiss.wedding.entities.listings.SocialMedia;
import amiss.wedding.entities.listings.dto.AttributeValueDto;
import amiss.wedding.entities.listings.dto.SocialMediaDto;

public interface SocialMediaMapper {

    SocialMedia socialMediaDtoToSocialMedia(SocialMediaDto socialMediaDto);

    SocialMediaDto socialMediaToSocialMediaDto(SocialMedia socialMedia);
}
