package amiss.wedding.entities.cities.dto;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import lombok.*;

import java.util.Set;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RegionDto {

    private Long id;
    private String capital_city;
    private String code;
    private String name;
    private String name_en;
    private String country;
    private boolean active;

    public RegionDto(Region region){
        this.id = region.getId();
        this.code = region.getCode();
        this.name = region.getName();
        this.name_en = region.getName_en();
        this.active = region.isActive();
        if (region.getCountry() != null) {
            this.country = region.getCountry().getName();
        }
    }
}
