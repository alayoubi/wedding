import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-custom-listings-landing',
  templateUrl: './custom-listings-landing.component.html',
  styleUrls: ['./custom-listings-landing.component.css']
})
export class CustomListingsLandingComponent implements OnInit {

  constructor(private title:Title) { }

  ngOnInit(): void {
    this.title.setTitle('ما تريد وين ما بدك')
  }

}
