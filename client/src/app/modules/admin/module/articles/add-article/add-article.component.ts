import { MiniArticle } from './../../../../../models/article/Article';

import { Attribute, Component, ElementRef, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { Category } from '@core/models/category/Category';
import { CategoryService } from '@core/services/category/category.service';
import { AddMiniArticleComponent } from '../add-mini-article/add-mini-article.component';
import * as AWS from '../../../../../../assets/JS/sdk.amazonaws.com_js_aws-sdk-2.100.0.min.js';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css'],
})
export class AddArticleComponent implements OnInit {
  public photos: File[] = [];
  public base64: any;
  urls: any[] = [];
  covers: any[] = [];
  public urlsMini: any[] = [];
  public photosMini: File[] = [];
  
  addArticleForm = new FormGroup({
    title: new FormControl(''),
    readTime: new FormControl(''),
    content: new FormControl(''),
    coverImgDto: new FormControl(''),
    articlePhotoDto: new FormArray([]),
    categories: new FormArray([]),
    miniArticles: new FormArray([
      new FormGroup({
        title: new FormControl<string | null>(''),
        content: new FormControl<string | null>(''),
        listing_id: new FormControl(''),
        miniArticlePhotos: new FormArray([]),
      }),
    ]),
    listing_id_main: new FormControl(''),
  });

  categories: Category[];
  success = false;
  public selectedCategory: number;
  public selectedRegion: number;
  public selectedCity: number;

  constructor(
    private host: ElementRef<HTMLInputElement>,
    private categoryService: CategoryService,
    private title: Title,
    public dialog: MatDialog
  ) {
    title.setTitle('إضافة مقالة');
  }

  ngOnInit(): void {
    this.getCategories();
    // console.log(AWS)
  }

  getCategories() {
    this.categoryService.getCategories().subscribe((res) => {
      this.categories = res;
    });
  }

  onCoverSelected(event: any) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        this.covers.push(fileList[i]);

        var reader = new FileReader();
        reader.readAsDataURL(fileList[i]);
        reader.onload = (events: any) => {
          // this.urls.push(events.target.result);
          console.log(this.urls, 'urls');
          this.base64 = reader.result;
          console.log(this.base64, 'base');
        };
      }
    }
  }

  onArticlePhotosChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        this.photos.push(fileList[i]);
        var reader = new FileReader();
        reader.readAsDataURL(fileList[i]);
        reader.onload = (events: any) => {
          this.urls.push(events.target.result);
        };
      }
    }
  }

  onMiniArticlePhotosChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        this.photosMini.push(fileList[i]);
        var reader = new FileReader();
        reader.readAsDataURL(fileList[i]);
        reader.onload = (events: any) => {
          this.urlsMini.push(events.target.result);
        };
      }
    }
  }

  updateCheckboxes(event: any, tag: any) {
    const tagsFormArray = this.addArticleForm.get('categories') as FormArray;

    if (event.target.checked) {
      tagsFormArray.push(new FormControl(tag));
    } else {
      const index = tagsFormArray.controls.findIndex((x) => x.value === tag);
      tagsFormArray.removeAt(index);
    }
  }

  addArticle() {
    console.log(this.addArticleForm.value);
  }

  addArticleByName() {

  }

  reset() {
    this.addArticleForm.reset();
  }

  openMiniArticleForm() {
    let dialogConf = new MatDialogConfig();
    dialogConf.width = '100%';
    dialogConf.disableClose = true;
    dialogConf.autoFocus = false;
    dialogConf.height = '80%';
    dialogConf.enterAnimationDuration = '700ms';
    this.dialog.open(AddMiniArticleComponent, dialogConf);
  }

  createMiniArticle() {
    const MiniArticle = <FormArray>this.addArticleForm.controls['miniArticles'];
    MiniArticle.push(
      new FormGroup({
        title: new FormControl<string | null>(''),
        content: new FormControl<string | null>(''),
        listing_id: new FormControl(''),
        miniArticlePhotos: new FormArray([]),
      })
    );
  }

  removeMiniArticle(index) {
    const MiniArticle = <FormArray>this.addArticleForm.controls.miniArticles;
    MiniArticle.removeAt(index);
  }
}
