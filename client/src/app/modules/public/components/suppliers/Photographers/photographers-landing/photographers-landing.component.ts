import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photographers-landing',
  templateUrl: './photographers-landing.component.html',
  styleUrls: ['./photographers-landing.component.css']
})
export class PhotographersLandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
