package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class UserNotFoundException extends ApplicationRuntimeException {

    public UserNotFoundException(long id) {
        super("The user with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
