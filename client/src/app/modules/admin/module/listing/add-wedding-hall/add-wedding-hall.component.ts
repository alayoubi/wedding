import { HttpErrorResponse } from '@angular/common/http';
import { ListingService } from '@core/services/listing/listing.service';
import { AttributeService } from '@core/services/category/attribute.service';
import { Attribute } from '@core/models/category/Attribute';
import { DistrictService } from '@core/services/city/district.service';
import { District } from '@core/models/city/District';
import { City } from '@core/models/city/City';
import { Category } from 'src/app/models/category/Category';
import { CategoryService } from 'src/app/services/category/category.service';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Region } from '@core/models/city/Region';
import { RegionService } from '@core/services/city/region.service';
import { CityService } from '@core/services/city/city.service';
import { FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { Listing } from '@core/models/listing/Listing';

@Component({
  selector: 'app-add-wedding-hall',
  templateUrl: './add-wedding-hall.component.html',
  styleUrls: ['./add-wedding-hall.component.css']
})
export class AddWeddingHallComponent implements OnInit {

  private file: File | null = null;

  success = false;

  addListingForm = new FormGroup({
    title: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.required),
    address: new FormControl(null, Validators.required),
    descriptions: new FormControl(null, Validators.required),
    status: new FormControl(null, Validators.required),
    country_id: new FormControl(null, Validators.required),
    region_id: new FormControl(null, Validators.required),
    city_id: new FormControl(null, Validators.required),
    district_id: new FormControl(null, Validators.required),
    client_id: new FormControl(null, Validators.required),
    plan_id: new FormControl("1", Validators.required),
    category_id: new FormControl(null, Validators.required),
    coverImg: new FormControl(null, Validators.required),
    attributeValueList : new FormArray([
      new FormGroup({
        value: new FormControl("", null),
        attribute_Id: new FormControl("1", null),
      }),

    ]),
    socialMediaList : new FormArray([
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("PHONE", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("EMAIL", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("WHATSAPP", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("FACEBOOK", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("INSTAGRAM", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("TWITTER", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("TIKTOK", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("SNAPCHAT", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("PRIVATE", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("YOUTUBE", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("PINTEREST", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("TELEGRAM", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("LINKEDIN", null),
      }),
      new FormGroup({
        path: new FormControl(null, null),
        socialMediaTyp: new FormControl("OTHER", null),
      }),
    ]),
  });

  public statuses: String[] = ["PENDING",
    "PROCESSING",
    "COMPLETED",
    "DECLINE"  ]

  public regions: Region[];
  public cities: City[];
  public districts: District[];
  public attributes: Attribute[];

  public selectedCategory: number;
  public selectedRegion: number;
  public selectedCity: number;

  constructor(private host: ElementRef<HTMLInputElement>,
    private listingsService: ListingService,
    private categoryService: CategoryService,
    private regionService: RegionService,
    private cityservice: CityService,
    private districtService: DistrictService,
    private attributeService: AttributeService) {}

    ngOnInit(): void {
      this.getAllActiveRegion();
      for (let i = 2; i <= 52; i++) {
        const control = new FormGroup({
          value: new FormControl(null, null),
          attribute_Id: new FormControl("" + i + "", Validators.required),
        });
        (<FormArray>this.addListingForm.get('attributeValueList')).push(control);
      }

      console.log(this.addListingForm);
    }

    public onAddListing(): void {
      console.log(this.addListingForm.value);
       this.listingsService.addListing(this.addListingForm.value).subscribe(
        (response: Listing) => {
         console.log(response);

        },
        (error: HttpErrorResponse) => {
        alert(error.message);
       },
       () =>{
            console.log("done");
       }

      );
    }

    public reset(addForm: FormGroup): void {
      addForm.reset();
    }


  onRegionChange(): void {
    this.getAllCitiesByRegion(this.selectedRegion);
  }

  onCityChange(): void {
    this.getAllDistrictsByCity(this.selectedCity);
  }

  uploadFile(event) {
    console.log('file selected!');
    const file = event.target.files[0];
    this.addListingForm.patchValue({
      coverImg: file
    });
    this.addListingForm.get('coverImg')?.updateValueAndValidity();

    console.log("file: ", file);
    console.log(this.addListingForm.value);

  }


  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe(
      regionService => {
        this.regions = regionService;
      }
    )
  }

  public getAllCitiesByRegion(regionId: number) {
    this.cityservice.getCitiesByRegion(regionId).subscribe(
      cityservice => {
        this.cities = cityservice;
      }
    )
  }

  public getAllDistrictsByCity(cityId: number) {
    this.districtService.getDistrictsByCity(cityId).subscribe(
      districtService => {
        this.districts = districtService;
      }
    )
  }

  requiredFileType( type: string ) {
    return function (control: FormControl) {
      const file = control.value;
      if ( file ) {
        const extension = file.name.split('.')[1].toLowerCase();
        if ( type.toLowerCase() !== extension.toLowerCase() ) {
          return {
            requiredFileType: true
          };
        }

        return null;
      }

      return null;
    };
  }
}
