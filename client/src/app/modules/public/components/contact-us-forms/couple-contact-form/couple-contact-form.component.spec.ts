import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoupleContactFormComponent } from './couple-contact-form.component';

describe('CoupleContactFormComponent', () => {
  let component: CoupleContactFormComponent;
  let fixture: ComponentFixture<CoupleContactFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoupleContactFormComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoupleContactFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
