//package amiss.wedding.security.error;
//
//import amiss.wedding.errors.apierror.ApplicationRuntimeException;
//import org.springframework.http.HttpStatus;
//
//public class UserNotActiveException extends ApplicationRuntimeException {
//    public UserNotActiveException(String email) {
//        super("User: " + email + " not active", HttpStatus.UNAUTHORIZED);
//    }
//}
