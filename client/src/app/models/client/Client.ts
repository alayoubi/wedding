import { User } from './../user/User';
export interface Client {
  id: number;
  name: string;
  mobile: string;
  adress: string;
  photo: string;
  user_id: number;
}
