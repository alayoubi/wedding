package amiss.wedding.entities.listings.dto;

import amiss.wedding.entities.listings.Offer;
import lombok.*;

import java.util.Date;


@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OfferDto {

    private Long id;
    private Long listingId;
    private String title;
    private String content;
    private Date expiryDate;

    public OfferDto(Offer offer) {
        this.id = offer.getId();
        this.listingId = offer.getListing().getId();
        this.title = offer.getTitle();
        this.content = offer.getContent();
        this.expiryDate = offer.getExpiryDate();
    }
}
