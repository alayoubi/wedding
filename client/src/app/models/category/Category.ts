import { CategoryListComponent } from "@core/modules/public/module/category/components/category-list/category-list.component";

export interface Category {
  // 1
  id: number;
  name: string;
  description: string;
  active: boolean;
  cover_img: any;
  plans: string[];
  attributes: string[];
  // 2
  // id: number;
  // name: string;
  // category_id: Number;
  // valueType: string;
  // attributeType: string;
  // unit: string;
  // active: boolean;
  // attributes_ID: []
//   coverImg_id: number;
//   cover_img: string;
//   description: string;
//   id: number;
//   name: string;
//   active: boolean;
//   attributes: [
//     {
//       active: boolean;
//       attributeType: string;
//       category_id: number;
//       id: number;
//       name: string;
//       unit: string;
//       valueType: string;
//     }
//   ];
//   attributes_ID: [];

//   plans: [
//     {
//       category_id: number;
//       cost: number;
//       duration: number;
//       id: number;
//       planType: string;
//     }
//   ];
//   plans_ID: [];
}

