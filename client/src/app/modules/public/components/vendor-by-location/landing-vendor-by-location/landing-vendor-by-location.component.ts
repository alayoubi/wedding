import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-landing-vendor-by-location',
  templateUrl: './landing-vendor-by-location.component.html',
  styleUrls: ['./landing-vendor-by-location.component.css'],
})
export class LandingVendorByLocationComponent implements OnInit {
  constructor(private title: Title) {}

  ngOnInit(): void {
    this.title.setTitle('البائعين حسب الموقع');
  }
}
