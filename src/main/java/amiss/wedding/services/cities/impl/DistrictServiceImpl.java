package amiss.wedding.services.cities.impl;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.cities.dto.DistrictDto;
import amiss.wedding.errors.DistrictNotFoundException;
import amiss.wedding.errors.RegionNotFoundException;
import amiss.wedding.mappers.cities.DistrictMapper;
import amiss.wedding.mappers.cities.DistrictMapperImpl;
import amiss.wedding.repositories.cities.CityRepository;
import amiss.wedding.repositories.cities.DistrictRepository;
import amiss.wedding.repositories.cities.RegionRepository;
import amiss.wedding.services.cities.DistrictService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DistrictServiceImpl implements DistrictService {

    private final RegionRepository regionRepository;
    private final CityRepository cityRepository;
    private final DistrictRepository districtRepository;
    private static final DistrictMapper mapper = new DistrictMapperImpl();

    public DistrictServiceImpl(RegionRepository regionRepository,
                               CityRepository cityRepository,
                               DistrictRepository districtRepository) {
        this.regionRepository = regionRepository;
        this.cityRepository = cityRepository;
        this.districtRepository = districtRepository;
    }

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<DistrictDto>> findAll() {
        List<District> districts = this.districtRepository.findAll();
        List<DistrictDto> districtDtoList =
                districts.stream().map(mapper::districtToDistrictDto).collect(Collectors.toList());
        return new ResponseEntity<>(districtDtoList, HttpStatus.OK);
    }

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<DistrictDto>> findAllByRegion(Long region_id) {
        Optional<Region> region = this.regionRepository.findById(region_id);
        if (region.isEmpty()) {
            throw new RegionNotFoundException(region_id);
        }
        List<District> districts = this.districtRepository.findAllByRegion(region.get());
        List<DistrictDto> districtDtoList =
                districts.stream().map(mapper::districtToDistrictDto).collect(Collectors.toList());
        return new ResponseEntity<>(districtDtoList, HttpStatus.OK);
    }

    /**
     * @param city_id
     * @return
     */
    @Override
    public ResponseEntity<List<DistrictDto>> findAllByCity(Long city_id) {
        Optional<City> city = this.cityRepository.findById(city_id);
        if (city.isEmpty()) {
            throw new RegionNotFoundException(city_id);
        }
        List<District> districts = this.districtRepository.findAllByCity(city.get());
        List<DistrictDto> districtDtoList =
                districts.stream().map(mapper::districtToDistrictDto).collect(Collectors.toList());
        return new ResponseEntity<>(districtDtoList, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<DistrictDto> findById(Long id) {
        Optional<District> district = this.districtRepository.findById(id);
        if (district.isEmpty()){
            throw new DistrictNotFoundException(id);
        }
        DistrictDto districtDto = mapper.districtToDistrictDto(district.get());
        return new ResponseEntity<>(districtDto, HttpStatus.OK);
    }

    /**
     * @param districtDto
     * @return
     */
    @Override
    public ResponseEntity<DistrictDto> create(DistrictDto districtDto) {
        District district = mapper.districtDtoToDistrict(districtDto);
        Region region = this.regionRepository.findByName(districtDto.getRegion());
        City city = this.cityRepository.findByNameIs(districtDto.getCity());
        district.setCity(city);
        district.setRegion(region);
        final District createdDistrict = this.districtRepository.save(district);
        return new ResponseEntity<>(districtDto, HttpStatus.CREATED);
    }

    /**
     * @param id
     * @param districtDto
     * @return
     */
    @Override
    public ResponseEntity<DistrictDto> update(Long id, DistrictDto districtDto) {
        // TODO
        return null;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> remove(Long id) {
        // TODO
        return null;
    }
}
