package amiss.wedding.entities.listings;


import amiss.wedding.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "positions")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Position extends BaseEntity {

    private Long latitude;
    private Long longitude;

    @OneToOne
    private Listing listing;

}
