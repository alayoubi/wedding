import { RegionService } from '@core/services/city/region.service';
import { Region } from '@core/models/city/Region';
import { Category } from 'src/app/models/category/Category';
import { CategoryService } from 'src/app/services/category/category.service';
import {  OnInit } from '@angular/core';
import { Component } from '@angular/core';



@Component({
  selector: 'app-dropdown-menu',
  templateUrl: './dropdown-menu.component.html',
  styleUrls: ['./dropdown-menu.component.css'],
})
export class DropdownMenuComponent implements OnInit {
  advance: boolean = true;
  visible: boolean = false;

  onclick() {
    this.advance = !this.advance;
    this.visible = !this.visible;
  }

  click() {
    alert('Submit');
    console.log('submit');
  }

  constructor(
    private categoryService: CategoryService,
    private regionService: RegionService
  ) {}
  categories: Category[];
  regions: Region[];
  public idOptionCat: any;
  public idOptionReg: any;
  public engNameCat: any;
  public engNameReg: any;

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllRegions();
  }

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
      console.log(this.categories);
    });
  }

  getAllRegions(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
      console.log(this.regions);
    });
  }

  public getIdForOption(ev: any) {
    let option = ev.source.selected.viewValue;
    // console.log(option)

    this.categories.forEach((el) => {
      if (el.name === option) {
        this.idOptionCat = el.id;
        this.engNameCat = el.name;
        console.log(this.engNameCat);
        console.log(this.idOptionCat);
      }
    });

    this.regions.forEach((el) => {
      if (el.name === option) {
        this.idOptionReg = el.id;
        this.engNameReg = el.name;
        console.log(this.engNameReg);
        console.log(this.idOptionReg);
      }
    });
  }
}
