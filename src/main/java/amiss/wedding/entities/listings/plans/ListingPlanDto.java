package amiss.wedding.entities.listings.plans;


import amiss.wedding.entities.categories.plans.PlanDto;
import amiss.wedding.entities.listings.dto.ListingDto;
import lombok.*;

import java.time.LocalDateTime;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListingPlanDto {

    private Long id;
    private Long listingId;
    private PlanDto planDto;
    private LocalDateTime startedAt;
    private LocalDateTime expiresAt;
    private ListingPlanStatus listingPlanStatus;


    public ListingPlanDto(ListingPlan listingPlan) {
        this.id = listingPlan.getId();
        if (listingPlan.getListing() != null) {
            this.listingId = listingPlan.getListing().getId();
        }
        this.planDto = new PlanDto(listingPlan.getPlan());
        this.startedAt = listingPlan.getStartedAt() != null ? listingPlan.getStartedAt() : null;
        this.expiresAt = listingPlan.getExpiresAt() != null ? listingPlan.getExpiresAt() : null;
        this.listingPlanStatus = listingPlan.getListingPlanStatus() != null ? listingPlan.getListingPlanStatus() : null;
    }
}
