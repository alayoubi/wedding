import { CategoryService } from '@core/services/category/category.service';
import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { RegionService } from '@core/services/city/region.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-join-us-form',
  templateUrl: './join-us-form.component.html',
  styleUrls: ['./join-us-form.component.css'],
})
export class JoinUsFormComponent implements OnInit {
  public regions: any;
  public categories: any;
  constructor(
    private regionService: RegionService,
    private categoryService: CategoryService,
    title: Title
  ) {
    title.setTitle('إنضم إلينا');
  }

  joinUsForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    nameLastFirst: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required]),
  });

  ngOnInit(): void {
    this.getAllActiveRegion();
    this.getAllCategories();
  }

  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
      console.log(this.regions);
    });
  }

  public getAllCategories() {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
      console.log(this.categories);
    });
  }

  public submit() {
    console.log(this.joinUsForm.valid);
    console.log(this.joinUsForm.value);
    // this.contactForm.reset();
    // this.snackBar.openFromComponent(CoupleContactDoneComponent, {
    //   duration:3000
    // });
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }
}
