package amiss.wedding.controllers.user.registation;


import amiss.wedding.entities.users.client.dto.ClientDto;
import amiss.wedding.services.users.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/clients")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("")
    public ResponseEntity<List<ClientDto>> getusers(){
        return this.clientService.findAll();
    }

    @GetMapping(path = "/{clientId}")
    public ResponseEntity<ClientDto> getUser(@PathVariable Long clientId){
        return this.clientService.findById(clientId);
    }

    @PostMapping()
    public ResponseEntity<ClientDto> createUser(@RequestBody ClientDto clientDto){
        return this.clientService.create(clientDto);
    }

    @PutMapping(path = "/{clientId}")
    public ResponseEntity<ClientDto> updateUser(@PathVariable Long clientId, @RequestBody ClientDto clientDto){
        return this.clientService.update(clientId, clientDto);
    }

    @DeleteMapping(path = "/{clientId}")
    public ResponseEntity<String> removeUser(@PathVariable Long clientId){
        return this.clientService.remove(clientId);
    }
}
