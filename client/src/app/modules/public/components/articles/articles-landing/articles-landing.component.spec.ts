import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesLandingComponent } from './articles-landing.component';

describe('ArticlesLandingComponent', () => {
  let component: ArticlesLandingComponent;
  let fixture: ComponentFixture<ArticlesLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArticlesLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
