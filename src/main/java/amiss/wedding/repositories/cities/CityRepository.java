package amiss.wedding.repositories.cities;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    City findByNameIsAndRegion(Region region, String name);
    List<City> findAllByRegion(Region region);
    City findByNameIs(String name);
}
