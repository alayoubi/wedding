import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CakesLandingComponent } from './cakes-landing.component';

describe('CakesLandingComponent', () => {
  let component: CakesLandingComponent;
  let fixture: ComponentFixture<CakesLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CakesLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CakesLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
