package amiss.wedding.entities.cities.dto;

import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import lombok.*;

import java.util.Set;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CountryDto {

    private Long id;
    private String name;
    private String name_en;
    private Set<String> regions;
    private String capitalCity;
    private String code;

    public CountryDto(Country country){
        this.id = country.getId();
        this.name = country.getName();
        this.name_en = country.getName_en();
        this.code = country.getCode();
    }
}
