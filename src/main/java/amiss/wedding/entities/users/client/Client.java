package amiss.wedding.entities.users.client;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.users.AppUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "clients")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Client extends BaseEntity {

    @NotNull
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", length = 100)
    @Size(max = 100, message = "Name may not be more than 100 characters")
    private String name;

    @Column(name = "mobile", length = 100)
    @Size(max = 100, message = "Mobile may not be more than 100 characters")
    private String mobile;

    @JsonIgnore
    @Column(name = "address")
    @ApiModelProperty(notes = "The person's encrypted password")
    private String address;

    @Column(name = "photo")
    private String photo;

    @Column(name = "role", length = 100)
    @Size(max = 100, message = "Role may not be more than 100 characters")
    private String role;

    @OneToOne()
    private AppUser user;

    @OneToMany(mappedBy="client")
    private Set<Listing> listing; // = new HashSet<Listing>();

}
