import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographersLandingComponent } from './photographers-landing.component';

describe('PhotographersLandingComponent', () => {
  let component: PhotographersLandingComponent;
  let fixture: ComponentFixture<PhotographersLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotographersLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PhotographersLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
