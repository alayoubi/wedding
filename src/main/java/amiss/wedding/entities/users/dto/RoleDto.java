package amiss.wedding.entities.users.dto;

import amiss.wedding.entities.users.Role;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class RoleDto {

    @NotNull
    @NotEmpty
    private Role role;

}
