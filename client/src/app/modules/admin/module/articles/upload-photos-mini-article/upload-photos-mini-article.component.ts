import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ListingPhotoService } from '@core/services/listingPhoto/listing-photo.service';
import {AWS} from '../../../../../../assets/JS/sdk.amazonaws.com_js_aws-sdk-2.100.0.min.js';
// import { DATA } from '@core/config/S3-AWS.config.js';

@Component({
  selector: 'app-upload-photos-mini-article',
  templateUrl: './upload-photos-mini-article.component.html',
  styleUrls: ['./upload-photos-mini-article.component.css'],
})
export class UploadPhotosMiniArticleComponent implements OnInit {
  constructor(
    public dialog: MatDialogRef<UploadPhotosMiniArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private listingPhotoService: ListingPhotoService,
  ) {

  }

  public id: number = this.data?.id;
  public photos: File[] = [];
  urls: any[] = [];


  ngOnInit(): void {

  }



  onMiniArticlePhotosChange(event: any): void {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      for (let i = 0; i < fileList.length; i++) {
        this.photos.push(fileList[i]);
        var reader = new FileReader();
        reader.readAsDataURL(fileList[i]);
        reader.onload = (events: any) => {
          this.urls.push(events.target.result);
        };
      }
    }
  }

  onSubmit() {
    const formData = new FormData();

    console.log(this.id);
    for (let i = 0; i < this.photos?.length; i++) {
      formData.append('photos', this.photos[i]);
    }

    this.listingPhotoService.uploadsListingPhoto(this.id, formData).subscribe(
      (response) => {
        // File upload success

        console.log('photo uploded');

        // alert for 2 secound

        this.close();
      },
      (error) => {
        // File upload error
      }
    );

    console.log(formData);
  }

  close() {
    this.dialog.close();
  }

}
