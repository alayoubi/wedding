package amiss.wedding.entities.cities.dto;

import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import lombok.*;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DistrictDto {
    private Long id;
    private String name;
    private String name_en;
    private String region;
    private String city;

    public DistrictDto(District district){
        this.id = district.getId();
        this.name = district.getName();
        this.name_en = district.getName_en();
        this.region = district.getRegion().getName();
        this.city = district.getCity().getName();
    }
}
