package amiss.wedding.services.listings.impl;

import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.plans.ListingPlan;
import amiss.wedding.entities.listings.plans.ListingPlanDto;
import amiss.wedding.entities.listings.plans.ListingPlanStatus;
import amiss.wedding.errors.ListingNotFoundException;
import amiss.wedding.errors.ListingPlanNotFoundException;
import amiss.wedding.errors.PlanNotFoundException;
import amiss.wedding.mappers.listings.ListingPlanMapper;
import amiss.wedding.mappers.listings.ListingPlanMapperImpl;
import amiss.wedding.repositories.categories.PlanRepository;
import amiss.wedding.repositories.listings.ListingPlanRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.listings.ListingPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class ListingPlanServiceImpl implements ListingPlanService {

    private final ListingsRepository listingsRepository;
    private final ListingPlanRepository listingPlanRepository;
    private final PlanRepository planRepository;

    private final ListingPlanMapper listingPlanMapper = new ListingPlanMapperImpl();


    /**
     * @param listingPlanId
     * @return
     */
    @Override
    public ResponseEntity<ListingPlanDto> findById(Long listingPlanId) {
        ListingPlan listingPlan = listingPlanRepository.findById(listingPlanId).orElseThrow(() ->
                new ListingPlanNotFoundException(listingPlanId));
        return new ResponseEntity<>(listingPlanMapper.listingPlanToListingPlanDto(listingPlan), HttpStatus.OK);
    }

    /**
     * @param listingId
     * @return
     */
    @Override
    public ResponseEntity<ListingPlanDto> findListingPlanForListingById(Long listingId) {
        Listing listing = listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));

        ListingPlan listingPlan = listingPlanRepository.findByListing(listing);
        if (listingPlan == null) {
            throw new ListingPlanNotFoundException(listing);
        }
        return new ResponseEntity<>(listingPlanMapper.listingPlanToListingPlanDto(listingPlan), HttpStatus.OK);
    }

    /**
     * @param planId
     * @param listingId
     * @return
     */
    @Override
    public ResponseEntity<ListingPlanDto> create(Long planId, Long listingId) {
        Listing listing = listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        Plan plan = planRepository.findById(planId).orElseThrow(() ->
                new PlanNotFoundException(planId));

        ListingPlan listingPlan = new ListingPlan();
        listingPlan.setPlan(plan);
        listingPlan.setStartedAt(LocalDateTime.now());
        listingPlan.setExpiresAt(listingPlan.getStartedAt().plusMonths(plan.getDuration()));
        listingPlan.setListingPlanStatus(ListingPlanStatus.AVAILABLE);
        listingPlan.setListing(listing);
        listingPlan = listingPlanRepository.save(listingPlan);
        listing.setListingPlan(listingPlan);
        listingsRepository.save(listing);
        return new ResponseEntity<>(listingPlanMapper.listingPlanToListingPlanDto(listingPlan), HttpStatus.OK);
    }

    /**
     * @param listingPlanDto
     * @return
     */
    @Override
    public ResponseEntity<ListingPlanDto> update(ListingPlanDto listingPlanDto) {
        Plan plan = planRepository.findById(listingPlanDto.getPlanDto().getId()).orElseThrow(() ->
                new PlanNotFoundException(listingPlanDto.getPlanDto().getId()));

        ListingPlan listingPlan = listingPlanRepository.findById(listingPlanDto.getId()).orElseThrow(() ->
                new ListingPlanNotFoundException(listingPlanDto.getId()));
        if (!plan.equals(listingPlan.getPlan())) {
            listingPlan.setPlan(plan);
        }
        listingPlan.setStartedAt(LocalDateTime.now());
        listingPlan.setExpiresAt(listingPlan.getStartedAt().plusMonths(plan.getDuration()));
        listingPlan.setListingPlanStatus(ListingPlanStatus.AVAILABLE);
        listingPlan = listingPlanRepository.save(listingPlan);
        return new ResponseEntity<>(listingPlanMapper.listingPlanToListingPlanDto(listingPlan), HttpStatus.OK);
    }

    /**
     * @param listingPlanDto
     * @return
     */
    @Override
    public ListingPlanDto expired(Long listingPlanDto) {
        ListingPlan listingPlan = listingPlanRepository.findById(listingPlanDto).orElseThrow(() ->
                new ListingPlanNotFoundException(listingPlanDto));

        listingPlan.setListingPlanStatus(ListingPlanStatus.EXPIRED);
        listingPlanRepository.save(listingPlan);
        return listingPlanMapper.listingPlanToListingPlanDto(listingPlan);
    }
}
