
--
-- Datenbank: `wedding`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `regions`
--

CREATE TABLE `regions` (
  `id` bigint(20) NOT NULL,
  `capital_city_id` bigint(20) NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(64) NOT NULL,
  `name_en` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `regions`
--

INSERT INTO `regions` (`id`, `capital_city_id`, `code`, `name`, `name_en`) VALUES
(1, 3, 'RD', 'منطقة الرياض', 'Riyadh'),
(2, 6, 'MQ', 'منطقة مكة المكرمة', 'Makkah'),
(3, 14, 'MN', 'منطقة المدينة المنورة', 'Madinah'),
(4, 11, 'QA', 'منطقة القصيم', 'Qassim'),
(5, 13, 'SQ', 'المنطقة الشرقية', 'Eastern Province'),
(6, 15, 'AS', 'منطقة عسير', 'Asir'),
(7, 1, 'TB', 'منطقة تبوك', 'Tabuk'),
(8, 10, 'HA', 'منطقة حائل', 'Hail'),
(9, 2213, 'SH', 'منطقة الحدود الشمالية', 'Northern Borders'),
(10, 17, 'GA', 'منطقة جازان', 'Jazan'),
(11, 3417, 'NG', 'منطقة نجران', 'Najran'),
(12, 1542, 'BA', 'منطقة الباحة', 'Bahah'),
(13, 2237, 'GO', 'منطقة الجوف', 'Jawf');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `regions`
--
ALTER TABLE `regions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
