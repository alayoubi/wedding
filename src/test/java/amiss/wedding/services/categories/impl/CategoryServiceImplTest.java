package amiss.wedding.services.categories.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.dto.CategoryDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanType;
import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Position;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.Role;
import amiss.wedding.entities.users.client.Client;
import amiss.wedding.errors.CategoryNotFoundException;
import amiss.wedding.repositories.categories.CategoryRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CategoryServiceImplTest {
    @MockBean
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryServiceImpl categoryServiceImpl;

    /**
     * Method under test: {@link CategoryServiceImpl#findAll()}
     */
    @Test
    void testFindAll() {
        ArrayList<Category> categoryList = new ArrayList<>();
        when(categoryRepository.findAll()).thenReturn(categoryList);
        ResponseEntity<List<CategoryDto>> actualFindAllResult = categoryServiceImpl.findAll();
        assertEquals(categoryList, actualFindAllResult.getBody());
        assertEquals(HttpStatus.OK, actualFindAllResult.getStatusCode());
        assertTrue(actualFindAllResult.getHeaders().isEmpty());
        verify(categoryRepository).findAll();
    }

    /**
     * Method under test: {@link CategoryServiceImpl#findAll()}
     */
    @Test
    void testFindAll2() {
        when(categoryRepository.findAll()).thenThrow(new CategoryNotFoundException(1L));
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceImpl.findAll());
        verify(categoryRepository).findAll();
    }

    /**
     * Method under test: {@link CategoryServiceImpl#findById(Long)}
     */
    @Test
    void testFindById() {
        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(new ListingPhoto());
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(new Category());
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        City city = new City();
        city.setCountry(new Country());
        city.setDistricts(new HashSet<>());
        city.setId(1L);
        city.setListing(new HashSet<>());
        city.setName("Name");
        city.setName_en("Name en");
        city.setRegion(new Region());

        Client client = new Client();
        client.setAddress("42 Main St");
        client.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setId(1L);
        client.setListing(new HashSet<>());
        client.setMobile("Mobile");
        client.setName("Name");
        client.setPhoto("alice.liddell@example.org");
        client.setRole("Role");
        client.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setUser(new AppUser());
        client.setVersion(1);

        Country country = new Country();
        country.setCapital_city(new City());
        country.setCode("Code");
        country.setId(1L);
        country.setName("Name");
        country.setName_en("Name en");
        country.setRegions(new HashSet<>());

        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        District district = new District();
        district.setCity(new City());
        district.setId(1L);
        district.setListing(new HashSet<>());
        district.setName("Name");
        district.setName_en("Name en");
        district.setRegion(new Region());

        Plan plan = new Plan();
        plan.setCategory(new Category());
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Position position = new Position();
        position.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setId(1L);
        position.setLatitude(1L);
        position.setListing(new Listing());
        position.setLongitude(1L);
        position.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setVersion(1);

        Region region = new Region();
        region.setActive(true);
        region.setCapital_city(new City());
        region.setCities(new HashSet<>());
        region.setCode("Code");
        region.setCountry(new Country());
        region.setId(1L);
        region.setListing(new HashSet<>());
        region.setName("Name");
        region.setName_en("Name en");

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(category);
        listing.setCity(city);
        listing.setClient(client);
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(country);
        listing.setCoverImg(coverImg);
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(district);
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(plan);
        listing.setPosition(position);
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(region);
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(listing);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing2);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(coverImg4);
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(parentCategory);
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(coverImg3);
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(parentCategory2);
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(coverImg2);
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(parentCategory3);
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);
        Optional<Category> ofResult = Optional.of(category2);
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<CategoryDto> actualFindByIdResult = categoryServiceImpl.findById(1L);
        assertTrue(actualFindByIdResult.hasBody());
        assertTrue(actualFindByIdResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualFindByIdResult.getStatusCode());
        CategoryDto body = actualFindByIdResult.getBody();
        assertEquals("Name", body.getName());
        assertEquals(1L, body.getId().longValue());
        assertEquals("The characteristics of someone or something", body.getDescription());
        assertEquals("Path", body.getCover_img());
        assertEquals(1L, body.getCoverImg_id().longValue());
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals("Name", body.getCategory());
        assertTrue(body.getAttributes_ID().isEmpty());
        assertTrue(body.getAttributes().isEmpty());
        assertTrue(body.isActive());
        assertTrue(body.getPlans().isEmpty());
        assertTrue(body.getPlans_ID().isEmpty());
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#findByName(String)}
     */
    @Test
    void testFindByName() {
        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Country country = new Country();
        country.setCapital_city(new City());
        country.setCode("Code");
        country.setId(1L);
        country.setName("Name");
        country.setName_en("Name en");
        country.setRegions(new HashSet<>());

        Region region = new Region();
        region.setActive(true);
        region.setCapital_city(new City());
        region.setCities(new HashSet<>());
        region.setCode("Code");
        region.setCountry(new Country());
        region.setId(1L);
        region.setListing(new HashSet<>());
        region.setName("Name");
        region.setName_en("Name en");

        City city = new City();
        city.setCountry(country);
        city.setDistricts(new HashSet<>());
        city.setId(1L);
        city.setListing(new HashSet<>());
        city.setName("Name");
        city.setName_en("Name en");
        city.setRegion(region);

        AppUser user = new AppUser();
        user.setActive(true);
        user.setClient(new Client());
        user.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setEmail("jane.doe@example.org");
        user.setFirstname("Jane");
        user.setId(1L);
        user.setLastname("Doe");
        user.setLocked(true);
        user.setPassword("iloveyou");
        user.setReviews(new HashSet<>());
        user.setRole(Role.USER);
        user.setTokens(new ArrayList<>());
        user.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setVerified_at(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setVersion(1);

        Client client = new Client();
        client.setAddress("42 Main St");
        client.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setId(1L);
        client.setListing(new HashSet<>());
        client.setMobile("Mobile");
        client.setName("Name");
        client.setPhoto("alice.liddell@example.org");
        client.setRole("Role");
        client.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setUser(user);
        client.setVersion(1);

        City capital_city = new City();
        capital_city.setCountry(new Country());
        capital_city.setDistricts(new HashSet<>());
        capital_city.setId(1L);
        capital_city.setListing(new HashSet<>());
        capital_city.setName("Name");
        capital_city.setName_en("Name en");
        capital_city.setRegion(new Region());

        Country country2 = new Country();
        country2.setCapital_city(capital_city);
        country2.setCode("Code");
        country2.setId(1L);
        country2.setName("Name");
        country2.setName_en("Name en");
        country2.setRegions(new HashSet<>());

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(listing);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        City city2 = new City();
        city2.setCountry(new Country());
        city2.setDistricts(new HashSet<>());
        city2.setId(1L);
        city2.setListing(new HashSet<>());
        city2.setName("Name");
        city2.setName_en("Name en");
        city2.setRegion(new Region());

        Region region2 = new Region();
        region2.setActive(true);
        region2.setCapital_city(new City());
        region2.setCities(new HashSet<>());
        region2.setCode("Code");
        region2.setCountry(new Country());
        region2.setId(1L);
        region2.setListing(new HashSet<>());
        region2.setName("Name");
        region2.setName_en("Name en");

        District district = new District();
        district.setCity(city2);
        district.setId(1L);
        district.setListing(new HashSet<>());
        district.setName("Name");
        district.setName_en("Name en");
        district.setRegion(region2);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(new ListingPhoto());
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(new Category());
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category2);
        plan.setCost(BigDecimal.valueOf(1L));
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        Position position = new Position();
        position.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setId(1L);
        position.setLatitude(1L);
        position.setListing(listing2);
        position.setLongitude(1L);
        position.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setVersion(1);

        City capital_city2 = new City();
        capital_city2.setCountry(new Country());
        capital_city2.setDistricts(new HashSet<>());
        capital_city2.setId(1L);
        capital_city2.setListing(new HashSet<>());
        capital_city2.setName("Name");
        capital_city2.setName_en("Name en");
        capital_city2.setRegion(new Region());

        Country country3 = new Country();
        country3.setCapital_city(new City());
        country3.setCode("Code");
        country3.setId(1L);
        country3.setName("Name");
        country3.setName_en("Name en");
        country3.setRegions(new HashSet<>());

        Region region3 = new Region();
        region3.setActive(true);
        region3.setCapital_city(capital_city2);
        region3.setCities(new HashSet<>());
        region3.setCode("Code");
        region3.setCountry(country3);
        region3.setId(1L);
        region3.setListing(new HashSet<>());
        region3.setName("Name");
        region3.setName_en("Name en");

        Listing listing3 = new Listing();
        listing3.setAddress("42 Main St");
        listing3.setArticles(new HashSet<>());
        listing3.setCapacity("Capacity");
        listing3.setCategory(category);
        listing3.setCity(city);
        listing3.setClient(client);
        listing3.setContOfPhotos(1);
        listing3.setContOfReviews(1);
        listing3.setContOfVideo(1);
        listing3.setCountry(country2);
        listing3.setCoverImg(coverImg2);
        listing3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing3.setDescriptions("Descriptions");
        listing3.setDistrict(district);
        listing3.setEmail("jane.doe@example.org");
        listing3.setId(1L);
        listing3.setListingVideos(new HashSet<>());
        listing3.setMiniArticles(new HashSet<>());
        listing3.setOffers(new HashSet<>());
        listing3.setPhone("6625550144");
        listing3.setPlan(plan);
        listing3.setPosition(position);
        listing3.setPrice("Price");
        listing3.setPriority(1);
        listing3.setRegion(region3);
        listing3.setReviews(new HashSet<>());
        listing3.setStatus(Status.PENDING);
        listing3.setTitle("Dr");
        listing3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing3.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing3);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        Category category3 = new Category();
        category3.setActive(true);
        category3.setArticles(new HashSet<>());
        category3.setAttributes(new HashSet<>());
        category3.setCoverImg(new ListingPhoto());
        category3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category3.setDescription("The characteristics of someone or something");
        category3.setId(1L);
        category3.setListings(new HashSet<>());
        category3.setName("Name");
        category3.setParentCategory(new Category());
        category3.setPlan(new HashSet<>());
        category3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category3.setVersion(1);

        City city3 = new City();
        city3.setCountry(new Country());
        city3.setDistricts(new HashSet<>());
        city3.setId(1L);
        city3.setListing(new HashSet<>());
        city3.setName("Name");
        city3.setName_en("Name en");
        city3.setRegion(new Region());

        Client client2 = new Client();
        client2.setAddress("42 Main St");
        client2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client2.setId(1L);
        client2.setListing(new HashSet<>());
        client2.setMobile("Mobile");
        client2.setName("Name");
        client2.setPhoto("alice.liddell@example.org");
        client2.setRole("Role");
        client2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client2.setUser(new AppUser());
        client2.setVersion(1);

        Country country4 = new Country();
        country4.setCapital_city(new City());
        country4.setCode("Code");
        country4.setId(1L);
        country4.setName("Name");
        country4.setName_en("Name en");
        country4.setRegions(new HashSet<>());

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        District district2 = new District();
        district2.setCity(new City());
        district2.setId(1L);
        district2.setListing(new HashSet<>());
        district2.setName("Name");
        district2.setName_en("Name en");
        district2.setRegion(new Region());

        Plan plan2 = new Plan();
        plan2.setCategory(new Category());
        plan2.setCountOfPhotos(3);
        plan2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDuration(1);
        plan2.setFeatures(new HashSet<>());
        plan2.setId(1L);
        plan2.setListings(new HashSet<>());
        plan2.setPlanType(PlanType.FREE);
        plan2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setVersion(1);
        plan2.setVideo(true);

        Position position2 = new Position();
        position2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position2.setId(1L);
        position2.setLatitude(1L);
        position2.setListing(new Listing());
        position2.setLongitude(1L);
        position2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position2.setVersion(1);

        Region region4 = new Region();
        region4.setActive(true);
        region4.setCapital_city(new City());
        region4.setCities(new HashSet<>());
        region4.setCode("Code");
        region4.setCountry(new Country());
        region4.setId(1L);
        region4.setListing(new HashSet<>());
        region4.setName("Name");
        region4.setName_en("Name en");

        Listing listing4 = new Listing();
        listing4.setAddress("42 Main St");
        listing4.setArticles(new HashSet<>());
        listing4.setCapacity("Capacity");
        listing4.setCategory(category3);
        listing4.setCity(city3);
        listing4.setClient(client2);
        listing4.setContOfPhotos(1);
        listing4.setContOfReviews(1);
        listing4.setContOfVideo(1);
        listing4.setCountry(country4);
        listing4.setCoverImg(coverImg4);
        listing4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing4.setDescriptions("Descriptions");
        listing4.setDistrict(district2);
        listing4.setEmail("jane.doe@example.org");
        listing4.setId(1L);
        listing4.setListingVideos(new HashSet<>());
        listing4.setMiniArticles(new HashSet<>());
        listing4.setOffers(new HashSet<>());
        listing4.setPhone("6625550144");
        listing4.setPlan(plan2);
        listing4.setPosition(position2);
        listing4.setPrice("Price");
        listing4.setPriority(1);
        listing4.setRegion(region4);
        listing4.setReviews(new HashSet<>());
        listing4.setStatus(Status.PENDING);
        listing4.setTitle("Dr");
        listing4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing4.setVersion(1);

        ListingPhoto coverImg5 = new ListingPhoto();
        coverImg5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg5.setFileName("foo.txt");
        coverImg5.setId(1L);
        coverImg5.setListing(listing4);
        coverImg5.setPath("Path");
        coverImg5.setSize(10.0d);
        coverImg5.setStatus(Status.PENDING);
        coverImg5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg5.setVersion(1);

        Listing listing5 = new Listing();
        listing5.setAddress("42 Main St");
        listing5.setArticles(new HashSet<>());
        listing5.setCapacity("Capacity");
        listing5.setCategory(new Category());
        listing5.setCity(new City());
        listing5.setClient(new Client());
        listing5.setContOfPhotos(1);
        listing5.setContOfReviews(1);
        listing5.setContOfVideo(1);
        listing5.setCountry(new Country());
        listing5.setCoverImg(new ListingPhoto());
        listing5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing5.setDescriptions("Descriptions");
        listing5.setDistrict(new District());
        listing5.setEmail("jane.doe@example.org");
        listing5.setId(1L);
        listing5.setListingVideos(new HashSet<>());
        listing5.setMiniArticles(new HashSet<>());
        listing5.setOffers(new HashSet<>());
        listing5.setPhone("6625550144");
        listing5.setPlan(new Plan());
        listing5.setPosition(new Position());
        listing5.setPrice("Price");
        listing5.setPriority(1);
        listing5.setRegion(new Region());
        listing5.setReviews(new HashSet<>());
        listing5.setStatus(Status.PENDING);
        listing5.setTitle("Dr");
        listing5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing5.setVersion(1);

        ListingPhoto coverImg6 = new ListingPhoto();
        coverImg6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg6.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg6.setFileName("foo.txt");
        coverImg6.setId(1L);
        coverImg6.setListing(listing5);
        coverImg6.setPath("Path");
        coverImg6.setSize(10.0d);
        coverImg6.setStatus(Status.PENDING);
        coverImg6.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg6.setVersion(1);

        ListingPhoto coverImg7 = new ListingPhoto();
        coverImg7.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg7.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg7.setFileName("foo.txt");
        coverImg7.setId(1L);
        coverImg7.setListing(new Listing());
        coverImg7.setPath("Path");
        coverImg7.setSize(10.0d);
        coverImg7.setStatus(Status.PENDING);
        coverImg7.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg7.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(new ListingPhoto());
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(new Category());
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(coverImg7);
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(parentCategory2);
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category parentCategory4 = new Category();
        parentCategory4.setActive(true);
        parentCategory4.setArticles(new HashSet<>());
        parentCategory4.setAttributes(new HashSet<>());
        parentCategory4.setCoverImg(coverImg6);
        parentCategory4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setDescription("The characteristics of someone or something");
        parentCategory4.setId(1L);
        parentCategory4.setListings(new HashSet<>());
        parentCategory4.setName("Name");
        parentCategory4.setParentCategory(parentCategory3);
        parentCategory4.setPlan(new HashSet<>());
        parentCategory4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setVersion(1);

        Category parentCategory5 = new Category();
        parentCategory5.setActive(true);
        parentCategory5.setArticles(new HashSet<>());
        parentCategory5.setAttributes(new HashSet<>());
        parentCategory5.setCoverImg(coverImg5);
        parentCategory5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory5.setDescription("The characteristics of someone or something");
        parentCategory5.setId(1L);
        parentCategory5.setListings(new HashSet<>());
        parentCategory5.setName("Name");
        parentCategory5.setParentCategory(parentCategory4);
        parentCategory5.setPlan(new HashSet<>());
        parentCategory5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory5.setVersion(1);

        Category category4 = new Category();
        category4.setActive(true);
        category4.setArticles(new HashSet<>());
        category4.setAttributes(new HashSet<>());
        category4.setCoverImg(coverImg3);
        category4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category4.setDescription("The characteristics of someone or something");
        category4.setId(1L);
        category4.setListings(new HashSet<>());
        category4.setName("Name");
        category4.setParentCategory(parentCategory5);
        category4.setPlan(new HashSet<>());
        category4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category4.setVersion(1);
        when(categoryRepository.findByName(Mockito.<String>any())).thenReturn(category4);
        ResponseEntity<CategoryDto> actualFindByNameResult = categoryServiceImpl.findByName("Name");
        assertTrue(actualFindByNameResult.hasBody());
        assertTrue(actualFindByNameResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualFindByNameResult.getStatusCode());
        CategoryDto body = actualFindByNameResult.getBody();
        assertEquals("Name", body.getName());
        assertEquals(1L, body.getId().longValue());
        assertEquals("The characteristics of someone or something", body.getDescription());
        assertEquals("Path", body.getCover_img());
        assertEquals(1L, body.getCoverImg_id().longValue());
        assertEquals(1L, body.getCategory_id().longValue());
        assertEquals("Name", body.getCategory());
        assertTrue(body.getAttributes_ID().isEmpty());
        assertTrue(body.getAttributes().isEmpty());
        assertTrue(body.isActive());
        assertTrue(body.getPlans().isEmpty());
        assertTrue(body.getPlans_ID().isEmpty());
        verify(categoryRepository).findByName(Mockito.<String>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#create(CategoryDto)}
     */
    @Test
    void testCreate() {
        ListingPhoto coverImg = new ListingPhoto();
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setListing(new Listing());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setStatus(Status.PENDING);
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Category parentCategory = new Category();
        parentCategory.setActive(true);
        parentCategory.setArticles(new HashSet<>());
        parentCategory.setAttributes(new HashSet<>());
        parentCategory.setCoverImg(new ListingPhoto());
        parentCategory.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setDescription("The characteristics of someone or something");
        parentCategory.setId(1L);
        parentCategory.setListings(new HashSet<>());
        parentCategory.setName("Name");
        parentCategory.setParentCategory(new Category());
        parentCategory.setPlan(new HashSet<>());
        parentCategory.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory.setVersion(1);

        Category category = new Category();
        category.setActive(true);
        category.setArticles(new HashSet<>());
        category.setAttributes(new HashSet<>());
        category.setCoverImg(coverImg);
        category.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setDescription("The characteristics of someone or something");
        category.setId(1L);
        category.setListings(new HashSet<>());
        category.setName("Name");
        category.setParentCategory(parentCategory);
        category.setPlan(new HashSet<>());
        category.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category.setVersion(1);

        Country country = new Country();
        country.setCapital_city(new City());
        country.setCode("Code");
        country.setId(1L);
        country.setName("Name");
        country.setName_en("Name en");
        country.setRegions(new HashSet<>());

        Region region = new Region();
        region.setActive(true);
        region.setCapital_city(new City());
        region.setCities(new HashSet<>());
        region.setCode("Code");
        region.setCountry(new Country());
        region.setId(1L);
        region.setListing(new HashSet<>());
        region.setName("Name");
        region.setName_en("Name en");

        City city = new City();
        city.setCountry(country);
        city.setDistricts(new HashSet<>());
        city.setId(1L);
        city.setListing(new HashSet<>());
        city.setName("Name");
        city.setName_en("Name en");
        city.setRegion(region);

        AppUser user = new AppUser();
        user.setActive(true);
        user.setClient(new Client());
        user.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setEmail("jane.doe@example.org");
        user.setFirstname("Jane");
        user.setId(1L);
        user.setLastname("Doe");
        user.setLocked(true);
        user.setPassword("iloveyou");
        user.setReviews(new HashSet<>());
        user.setRole(Role.USER);
        user.setTokens(new ArrayList<>());
        user.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setVerified_at(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        user.setVersion(1);

        Client client = new Client();
        client.setAddress("42 Main St");
        client.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setId(1L);
        client.setListing(new HashSet<>());
        client.setMobile("Mobile");
        client.setName("Name");
        client.setPhoto("alice.liddell@example.org");
        client.setRole("Role");
        client.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client.setUser(user);
        client.setVersion(1);

        City capital_city = new City();
        capital_city.setCountry(new Country());
        capital_city.setDistricts(new HashSet<>());
        capital_city.setId(1L);
        capital_city.setListing(new HashSet<>());
        capital_city.setName("Name");
        capital_city.setName_en("Name en");
        capital_city.setRegion(new Region());

        Country country2 = new Country();
        country2.setCapital_city(capital_city);
        country2.setCode("Code");
        country2.setId(1L);
        country2.setName("Name");
        country2.setName_en("Name en");
        country2.setRegions(new HashSet<>());

        Listing listing = new Listing();
        listing.setAddress("42 Main St");
        listing.setArticles(new HashSet<>());
        listing.setCapacity("Capacity");
        listing.setCategory(new Category());
        listing.setCity(new City());
        listing.setClient(new Client());
        listing.setContOfPhotos(1);
        listing.setContOfReviews(1);
        listing.setContOfVideo(1);
        listing.setCountry(new Country());
        listing.setCoverImg(new ListingPhoto());
        listing.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setDescriptions("Descriptions");
        listing.setDistrict(new District());
        listing.setEmail("jane.doe@example.org");
        listing.setId(1L);
        listing.setListingVideos(new HashSet<>());
        listing.setMiniArticles(new HashSet<>());
        listing.setOffers(new HashSet<>());
        listing.setPhone("6625550144");
        listing.setPlan(new Plan());
        listing.setPosition(new Position());
        listing.setPrice("Price");
        listing.setPriority(1);
        listing.setRegion(new Region());
        listing.setReviews(new HashSet<>());
        listing.setStatus(Status.PENDING);
        listing.setTitle("Dr");
        listing.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing.setVersion(1);

        ListingPhoto coverImg2 = new ListingPhoto();
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setListing(listing);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setStatus(Status.PENDING);
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        City city2 = new City();
        city2.setCountry(new Country());
        city2.setDistricts(new HashSet<>());
        city2.setId(1L);
        city2.setListing(new HashSet<>());
        city2.setName("Name");
        city2.setName_en("Name en");
        city2.setRegion(new Region());

        Region region2 = new Region();
        region2.setActive(true);
        region2.setCapital_city(new City());
        region2.setCities(new HashSet<>());
        region2.setCode("Code");
        region2.setCountry(new Country());
        region2.setId(1L);
        region2.setListing(new HashSet<>());
        region2.setName("Name");
        region2.setName_en("Name en");

        District district = new District();
        district.setCity(city2);
        district.setId(1L);
        district.setListing(new HashSet<>());
        district.setName("Name");
        district.setName_en("Name en");
        district.setRegion(region2);

        Category category2 = new Category();
        category2.setActive(true);
        category2.setArticles(new HashSet<>());
        category2.setAttributes(new HashSet<>());
        category2.setCoverImg(new ListingPhoto());
        category2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setDescription("The characteristics of someone or something");
        category2.setId(1L);
        category2.setListings(new HashSet<>());
        category2.setName("Name");
        category2.setParentCategory(new Category());
        category2.setPlan(new HashSet<>());
        category2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category2.setVersion(1);

        Plan plan = new Plan();
        plan.setCategory(category2);
        plan.setCost(BigDecimal.valueOf(1L));
        plan.setCountOfPhotos(3);
        plan.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setDuration(1);
        plan.setFeatures(new HashSet<>());
        plan.setId(1L);
        plan.setListings(new HashSet<>());
        plan.setPlanType(PlanType.FREE);
        plan.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan.setVersion(1);
        plan.setVideo(true);

        Listing listing2 = new Listing();
        listing2.setAddress("42 Main St");
        listing2.setArticles(new HashSet<>());
        listing2.setCapacity("Capacity");
        listing2.setCategory(new Category());
        listing2.setCity(new City());
        listing2.setClient(new Client());
        listing2.setContOfPhotos(1);
        listing2.setContOfReviews(1);
        listing2.setContOfVideo(1);
        listing2.setCountry(new Country());
        listing2.setCoverImg(new ListingPhoto());
        listing2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setDescriptions("Descriptions");
        listing2.setDistrict(new District());
        listing2.setEmail("jane.doe@example.org");
        listing2.setId(1L);
        listing2.setListingVideos(new HashSet<>());
        listing2.setMiniArticles(new HashSet<>());
        listing2.setOffers(new HashSet<>());
        listing2.setPhone("6625550144");
        listing2.setPlan(new Plan());
        listing2.setPosition(new Position());
        listing2.setPrice("Price");
        listing2.setPriority(1);
        listing2.setRegion(new Region());
        listing2.setReviews(new HashSet<>());
        listing2.setStatus(Status.PENDING);
        listing2.setTitle("Dr");
        listing2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing2.setVersion(1);

        Position position = new Position();
        position.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setId(1L);
        position.setLatitude(1L);
        position.setListing(listing2);
        position.setLongitude(1L);
        position.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position.setVersion(1);

        City capital_city2 = new City();
        capital_city2.setCountry(new Country());
        capital_city2.setDistricts(new HashSet<>());
        capital_city2.setId(1L);
        capital_city2.setListing(new HashSet<>());
        capital_city2.setName("Name");
        capital_city2.setName_en("Name en");
        capital_city2.setRegion(new Region());

        Country country3 = new Country();
        country3.setCapital_city(new City());
        country3.setCode("Code");
        country3.setId(1L);
        country3.setName("Name");
        country3.setName_en("Name en");
        country3.setRegions(new HashSet<>());

        Region region3 = new Region();
        region3.setActive(true);
        region3.setCapital_city(capital_city2);
        region3.setCities(new HashSet<>());
        region3.setCode("Code");
        region3.setCountry(country3);
        region3.setId(1L);
        region3.setListing(new HashSet<>());
        region3.setName("Name");
        region3.setName_en("Name en");

        Listing listing3 = new Listing();
        listing3.setAddress("42 Main St");
        listing3.setArticles(new HashSet<>());
        listing3.setCapacity("Capacity");
        listing3.setCategory(category);
        listing3.setCity(city);
        listing3.setClient(client);
        listing3.setContOfPhotos(1);
        listing3.setContOfReviews(1);
        listing3.setContOfVideo(1);
        listing3.setCountry(country2);
        listing3.setCoverImg(coverImg2);
        listing3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing3.setDescriptions("Descriptions");
        listing3.setDistrict(district);
        listing3.setEmail("jane.doe@example.org");
        listing3.setId(1L);
        listing3.setListingVideos(new HashSet<>());
        listing3.setMiniArticles(new HashSet<>());
        listing3.setOffers(new HashSet<>());
        listing3.setPhone("6625550144");
        listing3.setPlan(plan);
        listing3.setPosition(position);
        listing3.setPrice("Price");
        listing3.setPriority(1);
        listing3.setRegion(region3);
        listing3.setReviews(new HashSet<>());
        listing3.setStatus(Status.PENDING);
        listing3.setTitle("Dr");
        listing3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing3.setVersion(1);

        ListingPhoto coverImg3 = new ListingPhoto();
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setListing(listing3);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setStatus(Status.PENDING);
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        Category category3 = new Category();
        category3.setActive(true);
        category3.setArticles(new HashSet<>());
        category3.setAttributes(new HashSet<>());
        category3.setCoverImg(new ListingPhoto());
        category3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category3.setDescription("The characteristics of someone or something");
        category3.setId(1L);
        category3.setListings(new HashSet<>());
        category3.setName("Name");
        category3.setParentCategory(new Category());
        category3.setPlan(new HashSet<>());
        category3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category3.setVersion(1);

        City city3 = new City();
        city3.setCountry(new Country());
        city3.setDistricts(new HashSet<>());
        city3.setId(1L);
        city3.setListing(new HashSet<>());
        city3.setName("Name");
        city3.setName_en("Name en");
        city3.setRegion(new Region());

        Client client2 = new Client();
        client2.setAddress("42 Main St");
        client2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client2.setId(1L);
        client2.setListing(new HashSet<>());
        client2.setMobile("Mobile");
        client2.setName("Name");
        client2.setPhoto("alice.liddell@example.org");
        client2.setRole("Role");
        client2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        client2.setUser(new AppUser());
        client2.setVersion(1);

        Country country4 = new Country();
        country4.setCapital_city(new City());
        country4.setCode("Code");
        country4.setId(1L);
        country4.setName("Name");
        country4.setName_en("Name en");
        country4.setRegions(new HashSet<>());

        ListingPhoto coverImg4 = new ListingPhoto();
        coverImg4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setFileName("foo.txt");
        coverImg4.setId(1L);
        coverImg4.setListing(new Listing());
        coverImg4.setPath("Path");
        coverImg4.setSize(10.0d);
        coverImg4.setStatus(Status.PENDING);
        coverImg4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg4.setVersion(1);

        District district2 = new District();
        district2.setCity(new City());
        district2.setId(1L);
        district2.setListing(new HashSet<>());
        district2.setName("Name");
        district2.setName_en("Name en");
        district2.setRegion(new Region());

        Plan plan2 = new Plan();
        plan2.setCategory(new Category());
        plan2.setCountOfPhotos(3);
        plan2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setDuration(1);
        plan2.setFeatures(new HashSet<>());
        plan2.setId(1L);
        plan2.setListings(new HashSet<>());
        plan2.setPlanType(PlanType.FREE);
        plan2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        plan2.setVersion(1);
        plan2.setVideo(true);

        Position position2 = new Position();
        position2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position2.setId(1L);
        position2.setLatitude(1L);
        position2.setListing(new Listing());
        position2.setLongitude(1L);
        position2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        position2.setVersion(1);

        Region region4 = new Region();
        region4.setActive(true);
        region4.setCapital_city(new City());
        region4.setCities(new HashSet<>());
        region4.setCode("Code");
        region4.setCountry(new Country());
        region4.setId(1L);
        region4.setListing(new HashSet<>());
        region4.setName("Name");
        region4.setName_en("Name en");

        Listing listing4 = new Listing();
        listing4.setAddress("42 Main St");
        listing4.setArticles(new HashSet<>());
        listing4.setCapacity("Capacity");
        listing4.setCategory(category3);
        listing4.setCity(city3);
        listing4.setClient(client2);
        listing4.setContOfPhotos(1);
        listing4.setContOfReviews(1);
        listing4.setContOfVideo(1);
        listing4.setCountry(country4);
        listing4.setCoverImg(coverImg4);
        listing4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing4.setDescriptions("Descriptions");
        listing4.setDistrict(district2);
        listing4.setEmail("jane.doe@example.org");
        listing4.setId(1L);
        listing4.setListingVideos(new HashSet<>());
        listing4.setMiniArticles(new HashSet<>());
        listing4.setOffers(new HashSet<>());
        listing4.setPhone("6625550144");
        listing4.setPlan(plan2);
        listing4.setPosition(position2);
        listing4.setPrice("Price");
        listing4.setPriority(1);
        listing4.setRegion(region4);
        listing4.setReviews(new HashSet<>());
        listing4.setStatus(Status.PENDING);
        listing4.setTitle("Dr");
        listing4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing4.setVersion(1);

        ListingPhoto coverImg5 = new ListingPhoto();
        coverImg5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg5.setFileName("foo.txt");
        coverImg5.setId(1L);
        coverImg5.setListing(listing4);
        coverImg5.setPath("Path");
        coverImg5.setSize(10.0d);
        coverImg5.setStatus(Status.PENDING);
        coverImg5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg5.setVersion(1);

        Listing listing5 = new Listing();
        listing5.setAddress("42 Main St");
        listing5.setArticles(new HashSet<>());
        listing5.setCapacity("Capacity");
        listing5.setCategory(new Category());
        listing5.setCity(new City());
        listing5.setClient(new Client());
        listing5.setContOfPhotos(1);
        listing5.setContOfReviews(1);
        listing5.setContOfVideo(1);
        listing5.setCountry(new Country());
        listing5.setCoverImg(new ListingPhoto());
        listing5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing5.setDescriptions("Descriptions");
        listing5.setDistrict(new District());
        listing5.setEmail("jane.doe@example.org");
        listing5.setId(1L);
        listing5.setListingVideos(new HashSet<>());
        listing5.setMiniArticles(new HashSet<>());
        listing5.setOffers(new HashSet<>());
        listing5.setPhone("6625550144");
        listing5.setPlan(new Plan());
        listing5.setPosition(new Position());
        listing5.setPrice("Price");
        listing5.setPriority(1);
        listing5.setRegion(new Region());
        listing5.setReviews(new HashSet<>());
        listing5.setStatus(Status.PENDING);
        listing5.setTitle("Dr");
        listing5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        listing5.setVersion(1);

        ListingPhoto coverImg6 = new ListingPhoto();
        coverImg6.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg6.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg6.setFileName("foo.txt");
        coverImg6.setId(1L);
        coverImg6.setListing(listing5);
        coverImg6.setPath("Path");
        coverImg6.setSize(10.0d);
        coverImg6.setStatus(Status.PENDING);
        coverImg6.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg6.setVersion(1);

        ListingPhoto coverImg7 = new ListingPhoto();
        coverImg7.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg7.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg7.setFileName("foo.txt");
        coverImg7.setId(1L);
        coverImg7.setListing(new Listing());
        coverImg7.setPath("Path");
        coverImg7.setSize(10.0d);
        coverImg7.setStatus(Status.PENDING);
        coverImg7.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg7.setVersion(1);

        Category parentCategory2 = new Category();
        parentCategory2.setActive(true);
        parentCategory2.setArticles(new HashSet<>());
        parentCategory2.setAttributes(new HashSet<>());
        parentCategory2.setCoverImg(new ListingPhoto());
        parentCategory2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setDescription("The characteristics of someone or something");
        parentCategory2.setId(1L);
        parentCategory2.setListings(new HashSet<>());
        parentCategory2.setName("Name");
        parentCategory2.setParentCategory(new Category());
        parentCategory2.setPlan(new HashSet<>());
        parentCategory2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory2.setVersion(1);

        Category parentCategory3 = new Category();
        parentCategory3.setActive(true);
        parentCategory3.setArticles(new HashSet<>());
        parentCategory3.setAttributes(new HashSet<>());
        parentCategory3.setCoverImg(coverImg7);
        parentCategory3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setDescription("The characteristics of someone or something");
        parentCategory3.setId(1L);
        parentCategory3.setListings(new HashSet<>());
        parentCategory3.setName("Name");
        parentCategory3.setParentCategory(parentCategory2);
        parentCategory3.setPlan(new HashSet<>());
        parentCategory3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory3.setVersion(1);

        Category parentCategory4 = new Category();
        parentCategory4.setActive(true);
        parentCategory4.setArticles(new HashSet<>());
        parentCategory4.setAttributes(new HashSet<>());
        parentCategory4.setCoverImg(coverImg6);
        parentCategory4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setDescription("The characteristics of someone or something");
        parentCategory4.setId(1L);
        parentCategory4.setListings(new HashSet<>());
        parentCategory4.setName("Name");
        parentCategory4.setParentCategory(parentCategory3);
        parentCategory4.setPlan(new HashSet<>());
        parentCategory4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory4.setVersion(1);

        Category parentCategory5 = new Category();
        parentCategory5.setActive(true);
        parentCategory5.setArticles(new HashSet<>());
        parentCategory5.setAttributes(new HashSet<>());
        parentCategory5.setCoverImg(coverImg5);
        parentCategory5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory5.setDescription("The characteristics of someone or something");
        parentCategory5.setId(1L);
        parentCategory5.setListings(new HashSet<>());
        parentCategory5.setName("Name");
        parentCategory5.setParentCategory(parentCategory4);
        parentCategory5.setPlan(new HashSet<>());
        parentCategory5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        parentCategory5.setVersion(1);

        Category category4 = new Category();
        category4.setActive(true);
        category4.setArticles(new HashSet<>());
        category4.setAttributes(new HashSet<>());
        category4.setCoverImg(coverImg3);
        category4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category4.setDescription("The characteristics of someone or something");
        category4.setId(1L);
        category4.setListings(new HashSet<>());
        category4.setName("Name");
        category4.setParentCategory(parentCategory5);
        category4.setPlan(new HashSet<>());
        category4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        category4.setVersion(1);
        when(categoryRepository.save(Mockito.<Category>any())).thenReturn(category4);
        ResponseEntity<CategoryDto> actualCreateResult = categoryServiceImpl.create(new CategoryDto());
        assertTrue(actualCreateResult.hasBody());
        assertTrue(actualCreateResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.CREATED, actualCreateResult.getStatusCode());
        verify(categoryRepository).save(Mockito.<Category>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#update(Long, CategoryDto)}
     */
    @Test
    void testUpdate() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.Category.getId()" because "category" is null
        //       at amiss.wedding.entities.categories.dto.CategoryDto.<init>(CategoryDto.java:51)
        //       at amiss.wedding.mappers.categories.CategoryMapperImpl.categoryToCategoryDto(CategoryMapperImpl.java:19)
        //       at amiss.wedding.services.categories.impl.CategoryServiceImpl.update(CategoryServiceImpl.java:72)
        //   See https://diff.blue/R013 to resolve this issue.

        Category category = mock(Category.class);
        doNothing().when(category).setDescription(Mockito.<String>any());
        doNothing().when(category).setName(Mockito.<String>any());
        Optional<Category> ofResult = Optional.of(category);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        when(categoryRepository.save(Mockito.<Category>any())).thenThrow(new CategoryNotFoundException(1L));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        CategoryServiceImpl categoryServiceImpl = new CategoryServiceImpl(categoryRepository);
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceImpl.update(1L, new CategoryDto()));
        verify(categoryRepository).save(Mockito.<Category>any());
        verify(categoryRepository).findById(Mockito.<Long>any());
        verify(category).setDescription(Mockito.<String>any());
        verify(category).setName(Mockito.<String>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#update(Long, CategoryDto)}
     */
    @Test
    void testUpdate2() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.Category.getId()" because "category" is null
        //       at amiss.wedding.entities.categories.dto.CategoryDto.<init>(CategoryDto.java:51)
        //       at amiss.wedding.mappers.categories.CategoryMapperImpl.categoryToCategoryDto(CategoryMapperImpl.java:19)
        //       at amiss.wedding.services.categories.impl.CategoryServiceImpl.update(CategoryServiceImpl.java:72)
        //   See https://diff.blue/R013 to resolve this issue.

        Category category = mock(Category.class);
        doThrow(new CategoryNotFoundException(1L)).when(category).setName(Mockito.<String>any());
        Optional<Category> ofResult = Optional.of(category);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        CategoryServiceImpl categoryServiceImpl = new CategoryServiceImpl(categoryRepository);
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceImpl.update(1L, new CategoryDto()));
        verify(categoryRepository).findById(Mockito.<Long>any());
        verify(category).setName(Mockito.<String>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#update(Long, CategoryDto)}
     */
    @Test
    void testUpdate3() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.Category.getId()" because "category" is null
        //       at amiss.wedding.entities.categories.dto.CategoryDto.<init>(CategoryDto.java:51)
        //       at amiss.wedding.mappers.categories.CategoryMapperImpl.categoryToCategoryDto(CategoryMapperImpl.java:19)
        //       at amiss.wedding.services.categories.impl.CategoryServiceImpl.update(CategoryServiceImpl.java:72)
        //   See https://diff.blue/R013 to resolve this issue.

        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        Optional<Category> emptyResult = Optional.empty();
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(emptyResult);
        CategoryServiceImpl categoryServiceImpl = new CategoryServiceImpl(categoryRepository);
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceImpl.update(1L, new CategoryDto()));
        verify(categoryRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#update(Long, CategoryDto)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testUpdate4() {
        //   Diffblue Cover was unable to write a Spring test,
        //   so wrote a non-Spring test instead.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.Category.getId()" because "category" is null
        //       at amiss.wedding.entities.categories.dto.CategoryDto.<init>(CategoryDto.java:51)
        //       at amiss.wedding.mappers.categories.CategoryMapperImpl.categoryToCategoryDto(CategoryMapperImpl.java:19)
        //       at amiss.wedding.services.categories.impl.CategoryServiceImpl.update(CategoryServiceImpl.java:72)
        //   See https://diff.blue/R013 to resolve this issue.

        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "amiss.wedding.entities.categories.dto.CategoryDto.getName()" because "newCategory" is null
        //       at amiss.wedding.services.categories.impl.CategoryServiceImpl.update(CategoryServiceImpl.java:69)
        //   See https://diff.blue/R013 to resolve this issue.

        Category category = mock(Category.class);
        doNothing().when(category).setDescription(Mockito.<String>any());
        doNothing().when(category).setName(Mockito.<String>any());
        Optional<Category> ofResult = Optional.of(category);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        when(categoryRepository.save(Mockito.<Category>any())).thenThrow(new CategoryNotFoundException(1L));
        when(categoryRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        (new CategoryServiceImpl(categoryRepository)).update(1L, null);
    }

    /**
     * Method under test: {@link CategoryServiceImpl#remove(Long)}
     */
    @Test
    void testRemove() {
        doNothing().when(categoryRepository).deleteById(Mockito.<Long>any());
        when(categoryRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        ResponseEntity<String> actualRemoveResult = categoryServiceImpl.remove(1L);
        assertEquals("Category has been removed successfully!", actualRemoveResult.getBody());
        assertEquals(HttpStatus.NO_CONTENT, actualRemoveResult.getStatusCode());
        assertTrue(actualRemoveResult.getHeaders().isEmpty());
        verify(categoryRepository).existsById(Mockito.<Long>any());
        verify(categoryRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#remove(Long)}
     */
    @Test
    void testRemove2() {
        doThrow(new CategoryNotFoundException(1L)).when(categoryRepository).deleteById(Mockito.<Long>any());
        when(categoryRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceImpl.remove(1L));
        verify(categoryRepository).existsById(Mockito.<Long>any());
        verify(categoryRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link CategoryServiceImpl#remove(Long)}
     */
    @Test
    void testRemove3() {
        when(categoryRepository.existsById(Mockito.<Long>any())).thenReturn(false);
        assertThrows(CategoryNotFoundException.class, () -> categoryServiceImpl.remove(1L));
        verify(categoryRepository).existsById(Mockito.<Long>any());
    }
}

