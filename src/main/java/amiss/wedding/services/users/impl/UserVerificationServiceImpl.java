package amiss.wedding.services.users.impl;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.VerificationToken;
import amiss.wedding.repositories.users.VerificationTokenRepository;
import amiss.wedding.services.users.UserVerificationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserVerificationServiceImpl implements UserVerificationService {

    private final VerificationTokenRepository verificationTokenRepository;

    @Override
    public VerificationToken createAndSaveToken(AppUser user) {

        VerificationToken verificationToken = new VerificationToken(
                UUID.randomUUID().toString(),
                LocalDateTime.now().plusHours(2),
                user);
        verificationToken.setToken(UUID.randomUUID().toString());
        return verificationTokenRepository.save(verificationToken);
    }

    public AppUser getUser(String tokenDTO) {
        VerificationToken token = verificationTokenRepository.findByToken(tokenDTO).get();
        return token != null ? token.getAppUser() : null;
    }

    /**
     * @param token
     * @return
     */
    @Override
    public Optional<VerificationToken> getToken(String token) {
        return verificationTokenRepository.findByToken(token);    }

    @Override
    public void invalidateToken(String tokenDTO) {
        VerificationToken token = verificationTokenRepository.findByToken(tokenDTO).get();
        verificationTokenRepository.delete(token);
    }
}
