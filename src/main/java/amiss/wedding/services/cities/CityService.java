package amiss.wedding.services.cities;

import amiss.wedding.entities.cities.dto.CityDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CityService {

    ResponseEntity<List<CityDto>> findAll();

    ResponseEntity<List<CityDto>> findAllByRegion(Long region_id);

    ResponseEntity<CityDto> findById(Long id);
    ResponseEntity<CityDto> create(CityDto cityDto);
    ResponseEntity<CityDto> update(Long id, CityDto cityDto);
    ResponseEntity<String> remove(Long id);
}
