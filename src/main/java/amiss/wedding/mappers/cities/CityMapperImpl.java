package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.dto.CityDto;

public class CityMapperImpl implements CityMapper {
    @Override
    public City cityDtoToCity(CityDto cityDto) {
        City city = new City();
        city.setName(cityDto.getName());
        city.setName_en(cityDto.getName_en());
        return city;
    }

    @Override
    public CityDto cityToCityDto(City city) {
        return new CityDto(city);
    }

    /**
     * @param city
     * @return
     */
    @Override
    public CityDto cityToCityDtoBaseData(City city) {
        return new CityDto(city.getId(), city.getName(), city.getName_en(), null, null);
    }
}
