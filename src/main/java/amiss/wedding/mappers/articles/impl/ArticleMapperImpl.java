package amiss.wedding.mappers.articles.impl;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.ArticleType;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.articles.dto.ArticlePhotoDto;
import amiss.wedding.entities.articles.dto.MiniArticleDto;
import amiss.wedding.mappers.articles.ArticleMapper;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ArticleMapperImpl implements ArticleMapper {
    /**
     * @param articleDto
     * @return
     */
    @Override
    public Article articleDtoToArticle(ArticleDto articleDto) {

        Article article = new Article();
        article.setTitle(articleDto.getTitle());
        article.setContent(article.getContent() != null ? article.getContent() : null);
        article.setReadTime( Integer.valueOf(article.getReadTime()) != null ? article.getReadTime() : 5); // TODO
        article.setArticleType(ArticleType.INFORMATION != null ? ArticleType.INFORMATION : null);
        return article;
    }

    /**
     * @param article
     * @return
     */
    @Override
    public ArticleDto articleToArticleDto(Article article) {
        return new ArticleDto(article);
    }

    /**
     * @param article
     * @return
     */
    @Override
    public ArticleDto articleToArticleDtoBaseData(Article article) {

        Set<Long> articlePhotos_ID = new HashSet();
        Set<String> articlePhotos = new HashSet();
        if (!article.getArticlePhotos().isEmpty()) {
            for (ArticlePhoto articlePhoto: article.getArticlePhotos()) {
                articlePhotos.add(articlePhoto.getPath());
                articlePhotos_ID.add(articlePhoto.getId());
            }
        }

        ArticleDto articleDto = new ArticleDto(article.getId(),
            article.getTitle(),
            article.getContent() != null ? article.getContent() : null,
            article.getReadTime(),
            article.getCoverImg() != null ? article.getCoverImg().getPath() : null,
                article.getCoverImg() != null ? new ArticlePhotoDto(article.getCoverImg()) : null,
            Date.from(article.getUpdatedAt()),
            articlePhotos_ID,
            articlePhotos,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null);


        return articleDto;
    }

    /**
     * @param miniArticleDto
     * @return
     */
    @Override
    public MiniArticle miniArticleDtoToMiniArticle(MiniArticleDto miniArticleDto) {
        MiniArticle miniArticle = new MiniArticle();
        if (miniArticleDto.getId() != null) {
            miniArticle.setId(miniArticleDto.getId());
        }
        miniArticle.setTitle(miniArticleDto.getTitle());
        miniArticle.setContent(miniArticle.getContent());
        return miniArticle;
    }

    /**
     * @param miniArticle
     * @return
     */
    @Override
    public MiniArticleDto miniArticleToMiniArticleDto(MiniArticle miniArticle) {
        return new MiniArticleDto(miniArticle);
    }

    /**
     * @param miniArticle
     * @return
     */
    @Override
    public MiniArticleDto miniArticleToMiniArticleDtoBaseData(MiniArticle miniArticle) {
        return null;
    }
}
