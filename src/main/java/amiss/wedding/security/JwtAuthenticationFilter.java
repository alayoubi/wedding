//package amiss.wedding.security;
//
//
//import lombok.SneakyThrows;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//
//public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
//
////    private String jwtAudience;
////    private String jwtIssuer;
////    private String jwtSecret;
////    private String jwtType;
////
////    private RestAuthenticationEntryPoint authenticationEntryPoint;
////
////
////    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
////                                   String jwtAudience, String jwtIssuer,
////                                   String jwtSecret, String jwtType, RestAuthenticationEntryPoint authenticationEntryPoint) {
////        this.jwtAudience = jwtAudience;
////        this.jwtIssuer = jwtIssuer;
////        this.jwtSecret = jwtSecret;
////        this.jwtType = jwtType;
////        this.authenticationEntryPoint = authenticationEntryPoint;
////        this.setAuthenticationManager(authenticationManager);
////        setFilterProcessesUrl("/api/login");
////    }
////
////
////    @SneakyThrows
////    @Override
////    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
////        Authentication authentication = null;
////        try {
////            authentication = super.attemptAuthentication(request, response);
////        } catch (AuthenticationException authenticationException) {
////            this.authenticationEntryPoint.commence(request, response, authenticationException);
////        }
////        return authentication;
////    }
////    protected void successfulAuthentication(
////            HttpServletRequest request, HttpServletResponse response,
////            FilterChain filterChain, Authentication authentication) throws IOException {
////
////        User user = (User) authentication.getPrincipal();
////
////        SecretKey secretKey = Keys.hmacShaKeyFor(jwtSecret.getBytes());
////
////        Map<String, Object> claims = new HashMap<>();
////
////        claims.put("roles", user.getAuthorities().stream()
////                .map(GrantedAuthority::getAuthority)
////                .collect(Collectors.toList()));
////
////        String token = Jwts.builder()
////                .signWith(secretKey, SignatureAlgorithm.HS512)
////                .setHeaderParam("typ", jwtType)
////                .setIssuer(jwtIssuer)
////                .setAudience(jwtAudience)
////                .setSubject(user.getUsername())
////                .addClaims(claims)
////                .setExpiration(new Date(System.currentTimeMillis() + 86400000))
////                .compact();
////
////        String jsonToken= "\"token\": \"" + token+"\"";
////
////        response.getWriter().write("{" + jsonToken + "}");
////        response.setContentType("application/json");
////    }
//}
