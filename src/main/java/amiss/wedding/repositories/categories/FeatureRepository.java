package amiss.wedding.repositories.categories;

import amiss.wedding.entities.categories.plans.Feature;
import amiss.wedding.entities.categories.plans.Plan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FeatureRepository extends JpaRepository<Feature, Long> {
    List<Feature> findAllByPlan(Plan plan);
}
