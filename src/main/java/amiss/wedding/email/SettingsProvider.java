package amiss.wedding.email;

import amiss.wedding.errors.SettingsNotReadableException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@Service
public class SettingsProvider {

    public static final String EMAIL_SETTINGS_PATH = "email.json";

    private SettingsStorage settingsStorage;

    public SettingsProvider(@Value("classpath:defaults/settings/email.json") Resource emailSettingsTemplate,
                            SettingsStorage settingsStorage) throws IOException {
        this.settingsStorage = settingsStorage;
        writeDefault(emailSettingsTemplate, EMAIL_SETTINGS_PATH);
    }

    private void writeDefault(Resource settingsTemplate, String settingsPath) throws IOException {
        if (!new File("settings", settingsPath).exists()) {
            try (InputStream resource = settingsTemplate.getInputStream()) {
                String json = new BufferedReader(new InputStreamReader(resource,
                        StandardCharsets.UTF_8)).lines().collect(Collectors.joining());
                this.settingsStorage.write(settingsPath, json);
            }

        }
    }

    private EmailSettings getEmail() throws SettingsNotReadableException {
        return (EmailSettings) settingsStorage.readSettings(EMAIL_SETTINGS_PATH, EmailSettings.class);
    }

    public MainSettings getAllSettings() {
        MainSettings settings = new MainSettings();
        try {
            settings.setEmailSettings(getEmail());
        } catch (SettingsNotReadableException e) {
            e.printStackTrace();
        }
        return settings;
    }

}
