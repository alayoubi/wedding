package amiss.wedding.services.articles.impl;

import amiss.wedding.entities.articles.*;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.listings.Listing;
import amiss.wedding.errors.*;
import amiss.wedding.mappers.articles.ArticleMapper;
import amiss.wedding.mappers.articles.impl.ArticleMapperImpl;
import amiss.wedding.repositories.articles.ArticleCategoryRepository;
import amiss.wedding.repositories.articles.ArticlePhotoRepository;
import amiss.wedding.repositories.articles.ArticleRepository;
import amiss.wedding.repositories.articles.MiniArticleRepository;
import amiss.wedding.repositories.categories.CategoryRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.articles.ArticlePhotoService;
import amiss.wedding.services.articles.ArticleService;
import amiss.wedding.services.articles.MiniArticleService;
import amiss.wedding.storage.StorageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;
    private final MiniArticleRepository miniArticleRepository;
    private final ArticlePhotoRepository articlePhotoRepository;
    private final CategoryRepository categoryRepository;
    private final ListingsRepository listingsRepository;
    private final ArticleCategoryRepository articleCategoryRepository;
    private final ArticleMapper mapper = new ArticleMapperImpl();
    private StorageService storageService;
    private final ArticlePhotoService articlePhotoService;
    private final MiniArticleService miniArticleService;
    public static final String FILE_NAME = "Cover";

    /**
     * @return all articles
     */
    @Override
    public ResponseEntity<List<ArticleDto>> findAll() {
        List<Article> articles = this.articleRepository.findAll();
        List<ArticleDto> articleDtoList =
                articles.stream().map(mapper::articleToArticleDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(articleDtoList, HttpStatus.OK);
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<ArticleDto> findById(long id) {
        Article article = this.articleRepository.findById(id).orElseThrow(() ->
                new ArticleNotFoundException(id));;
        ArticleDto articleDto = mapper.articleToArticleDto(article);
        return new ResponseEntity<>(articleDto, HttpStatus.OK);
    }

    /**
     * @return
     */
    @Override
    public ResponseEntity<List<ArticleDto>> findAllByCategory(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() ->
                new CategoryNotFoundException(id));
        List<Article> articles = articleRepository.findByCategoriesName(category.getName());
        List<ArticleDto> articleDtoList =
                articles.stream().map(mapper::articleToArticleDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(articleDtoList, HttpStatus.OK);
    }

    /**
     * @param articleDto
     * @return
     */
    @Override
    public ResponseEntity<ArticleDto> create(ArticleDto articleDto) {
        Article article = mapper.articleDtoToArticle(articleDto);
        Article createdArticle = articleRepository.save(article);
        return new ResponseEntity<>(mapper.articleToArticleDto(createdArticle), HttpStatus.OK);
    }

    /**
     * @param articleDto
     * @return
     */
    @Override
    public ResponseEntity<ArticleDto> createFullArticle(ArticleDto articleDto) {
        Article article = mapper.articleDtoToArticle(articleDto);

        // Add Categories
        Set<Category> categories = new HashSet();
        if (!articleDto.getCategory_IDs().isEmpty()) {
            for (Long id: articleDto.getCategory_IDs()) {
                Category category = categoryRepository.findById(id).orElseThrow(() ->
                        new CategoryNotFoundException(id));
                categories.add(category);
            }
            article.setCategories(categories);
        }

        // Add CoverPhoto
        article.setCoverImg(articlePhotoService.creatArticlePhoto(articleDto.getCoverImgDto()));

        // Add Listings
        Set<Listing> listings = new HashSet();
        if (!articleDto.getListings_IDs().isEmpty()) {
            for (Long id: articleDto.getListings_IDs()) {
                Listing listing = listingsRepository.findById(id).orElseThrow(() ->
                        new ListingNotFoundException(id));
                listings.add(listing);
            }
            article.setListings(listings);
        }

        // Add ArticleCategory
        Set<ArticleCategory> articleCategories = new HashSet();
        if (!articleDto.getArticleCategories_IDs().isEmpty()) {
            for (Long id: articleDto.getArticleCategories_IDs()) {
                ArticleCategory articleCategory = articleCategoryRepository.findById(id).orElseThrow(() ->
                        new ArticleCategoryNotFoundException(id));
                articleCategories.add(articleCategory);
            }
            article.setArticleCategories(articleCategories);
        }

        // Add ArticlePhotos
        Set<ArticlePhoto> articlePhoto = new HashSet<>();
        if (!articleDto.getArticlePhotoDto().isEmpty()) {
            articlePhoto = articlePhotoService.creatListOfArticlePhoto(articleDto.getArticlePhotoDto());
        }
        article.setArticlePhotos(articlePhoto);

        // Add MiniArticle
        Set<MiniArticle> miniArticles = new HashSet<>();
        if (!articleDto.getMiniArticles().isEmpty()) {
            miniArticles = miniArticleService.createFullMiniArticle(articleDto.getMiniArticles(), article.getId());
        }
        article.setMiniArticles(miniArticles);

        return new ResponseEntity<>(mapper.articleToArticleDto(articleRepository.save(article)),
                HttpStatus.OK);
    }

    /**
     * @param articleDto
     * @return
     */
    @Override
    public ResponseEntity<ArticleDto> createArticleWithPhoto(ArticleDto articleDto) {
        return null;
    }

    /**
     * @param id
     * @param articleDto
     * @return
     */
    @Override
    public ResponseEntity<ArticleDto> update(Long id, ArticleDto articleDto) {
        return null;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.articleRepository.existsById(id);
        if (!exists) {
            throw new ArticleNotFoundException(id);
        }
        this.articleRepository.deleteById(id);
        return new ResponseEntity<>("Article has been removed successfully!", HttpStatus.NO_CONTENT);
    }

}
