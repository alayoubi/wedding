package amiss.wedding.email;

import lombok.Data;

@Data
public class MainSettings {
    private EmailSettings emailSettings;
    private BasicSettings basicSettings;
}
