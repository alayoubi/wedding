package amiss.wedding.email;

import amiss.wedding.errors.SettingsNotReadableException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


@Service
public class SettingsStorage {


    public Object readSettings(String settingsPath, Class clazz) throws SettingsNotReadableException {
        File settings = getSettingsFile(settingsPath);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(Files.readString(settings.toPath()), clazz);
        } catch (Exception e) {
            throw new SettingsNotReadableException();
        }
    }

    public void write(String settingsPath, String data) {
        File settings = getSettingsFile(settingsPath);
        try {
            Files.writeString(settings.toPath(), data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



        private File getSettingsFile(String settingsPath) {
        File folder = new File("settings");
        File settings = new File("settings", settingsPath);
        folder.mkdirs();
        return settings;
    }
}
