export interface District {
  id: number;
  name: string;
  name_en: string;
  region: string;
  city: string;
}