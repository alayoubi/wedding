package amiss.wedding.services.users;

import amiss.wedding.entities.categories.dto.CategoryDto;
import amiss.wedding.entities.users.client.dto.ClientDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ClientService {
    ResponseEntity<List<ClientDto>> findAll();
    ResponseEntity<ClientDto> findById(Long id);
    ResponseEntity<ClientDto> findByUser(Long id);
    ResponseEntity<ClientDto> create(ClientDto clientDto);
    ResponseEntity<ClientDto> update(Long id, ClientDto clientDto);
    ResponseEntity<String> remove(Long id);
}
