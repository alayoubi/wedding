package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class RoleNotFoundException extends ApplicationRuntimeException {

    public RoleNotFoundException(long id) {
        super("The Role with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
