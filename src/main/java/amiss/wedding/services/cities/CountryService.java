package amiss.wedding.services.cities;

import amiss.wedding.entities.cities.dto.CityDto;
import amiss.wedding.entities.cities.dto.CountryDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CountryService {

    ResponseEntity<List<CountryDto>> findAll();
    ResponseEntity<CountryDto> findById(Long id);
    ResponseEntity<CountryDto> create(CountryDto countryDto);
    ResponseEntity<CountryDto> update(Long id, CountryDto countryDto);
    ResponseEntity<String> remove(Long id);
}
