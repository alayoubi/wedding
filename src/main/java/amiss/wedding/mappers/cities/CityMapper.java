package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.dto.CityDto;

public interface CityMapper {
    City cityDtoToCity(CityDto cityDto);
    CityDto cityToCityDto(City city);

    CityDto cityToCityDtoBaseData(City city);

}
