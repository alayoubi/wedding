package amiss.wedding.entities.categories.attributes;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.listings.AttributeValue;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "attributes")
@Setter
@Getter
@NoArgsConstructor
public class Attribute extends BaseEntity {

    @NotNull
    @NotBlank(message = "Name may not be blank")
    @Column(name = "name", length = 100)
    @Size(max = 100, message = "Name may not be more than 100 characters")
    private String name;

    @Column(name = "value_type", length = 32, columnDefinition = "varchar(32) default 'TEXT'")
    @Enumerated(EnumType.STRING)
    private ValueType valueType;

    @Column(name = "attribute_type", length = 32, columnDefinition = "varchar(32) default 'OTHER'")
    @Enumerated(EnumType.STRING)
    private AttributeType attributeType;

    @Column(name = "unit", length = 100)
    @Size(max = 100, message = "Unit may not be more than 100 characters")
    private String unit;

    @Column(name = "active", columnDefinition = "boolean  default false")
    private boolean active;

    @ManyToOne
    @JoinColumn(name="category_id", nullable=false)
    private Category category;

    @OneToMany(mappedBy="attribute")
    private Set<AttributeValue> attributeValues;

    public Attribute(String name, ValueType valueType, AttributeType attributeType, String unit, boolean active, Category category){
        this.name = name;
        this.valueType = valueType;
        this.attributeType = attributeType;
        this.unit = unit;
        this.active = active;
        this.category = category;
    }
}
