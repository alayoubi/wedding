import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSingleOneComponent } from './list-single-one.component';

describe('ListSingleOneComponent', () => {
  let component: ListSingleOneComponent;
  let fixture: ComponentFixture<ListSingleOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSingleOneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListSingleOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
