import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-contact-us-landing',
  templateUrl: './contact-us-landing.component.html',
  styleUrls: ['./contact-us-landing.component.css']
})
export class ContactUsLandingComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('تواصل معنا')
   }

  ngOnInit(): void {
  }

}
