import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { UploadPhotosMiniArticleComponent } from '../upload-photos-mini-article/upload-photos-mini-article.component';
import { ArticleService } from '@core/services/article/article.service';
import { error } from 'console';

@Component({
  selector: 'app-add-mini-article',
  templateUrl: './add-mini-article.component.html',
  styleUrls: ['./add-mini-article.component.css'],
})
export class AddMiniArticleComponent implements OnInit {
  public selectedCat: any;
  public photos: File[] = [];
  public base64: any;
  files: any = [];
  public fileName: string;
  date: any = '';
  dayName: string;
  monthName: string;
  day: number;
  year: number;
  month: number;
  imgPath: string;
  public tags: any[] = [];
  selectedTags: any[] = [];
  selectedOptions: any = [];

  addMiniArticleForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
    listing_id: new FormControl(''),
  });

  urls: any[] = [];
  covers: any[] = [];
  selectedAuthorId: any;
  selectesAuthorName: any;
  authors: any;
  base64File: any;

  constructor(
    private title: Title,
    private snackBar: MatSnackBar,
    public dialog: MatDialogRef<AddMiniArticleComponent>,
    private dialog1: MatDialog,
    private articleService: ArticleService
  ) {
    title.setTitle('إضافة مقالة فرعية');
  }

  addMiniArticle() {
    this.articleService.addMiniArticle(this.addMiniArticleForm.value).subscribe(
      (res) => {
        this.close();
        if (res) {
          this.openDialogUploadPhoto(res);
        }
      },
      (error) => {
        console.log(error.error);
      }
    );
  }

  reset() {
    this.addMiniArticleForm.reset();
    this.base64 = '';
    this.date = '';
    this.fileName = '';
  }

  openDialogUploadPhoto(row?: any) {
    let dialogConf = new MatDialogConfig();
    dialogConf.width = '100%';
    dialogConf.disableClose = true;
    dialogConf.autoFocus = false;
    dialogConf.height = '80%';
    dialogConf.enterAnimationDuration = '700ms';
    dialogConf.data = row;
    this.dialog1.open(UploadPhotosMiniArticleComponent, dialogConf);
  }

  close() {
    this.dialog.close();
  }

  ngOnInit(): void {
    //  this.openDialogUploadPhoto()
  }
}
