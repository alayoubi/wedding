import { Title } from '@angular/platform-browser';
import { ArticleService } from './../../../../../services/article/article.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
})
export class ArticleComponent implements OnInit {
  public article: any;
  public elId: number = 0
  public isThereMiniArt: boolean = true
  public miniArt :Array<any>

  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
    public router: Router,
    private sanitizer: DomSanitizer,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.getArticle();
  }

  getArticle() {
    this.route.params.subscribe((params) => {
      this.articleService
        .getArticleById(params['article-id'])
        .subscribe((article: any) => {
          const sanitizedHtml = this.sanitizer.bypassSecurityTrustHtml(article);
          this.article=sanitizedHtml
          this.titleService.setTitle(
            this.article.changingThisBreaksApplicationSecurity.title
          );
          this.miniArt =
            this.article.changingThisBreaksApplicationSecurity.miniArticles;

          if (this.miniArt.length === 0) {
            this.isThereMiniArt = false;
            console.log('Done')
          }



        });
    });
  }
}
