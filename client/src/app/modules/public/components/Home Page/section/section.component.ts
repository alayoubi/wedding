import { RegionService } from './../../../../../services/city/region.service';
import { Category } from 'src/app/models/category/Category';
import { Region } from '@core/models/city/Region';
import { CategoryService } from 'src/app/services/category/category.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSelect } from '@angular/material/select';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css'],
})
export class SectionComponent implements OnInit {
  public categories: Category[];
  public regions: Region[];
  public idOptionCat: any;
  public idOptionReg: any;
  public engNameCat: any;
  public engNameReg:any

  constructor(
    private categoryService: CategoryService,
    private regionService: RegionService
  ) {}

  ngOnInit(): void {
    this.getAllCategories();
    this.getAllActiveRegion();
  }

  public getAllCategories(): void {
    this.categoryService.getCategories().subscribe((categoryService) => {
      this.categories = categoryService;
    });
  }

  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
    });
  }

  public getIdForOption(ev: any) {
    let option = ev.source.selected.viewValue;
    // console.log(option)

    this.categories.forEach((el) => {
      if (el.name === option) {
        this.idOptionCat = el.id;
        this.engNameCat = el.name
        console.log(this.engNameCat)
        console.log(this.idOptionCat);
      }
    });

    this.regions.forEach((el) => {
      if (el.name === option) {
        this.idOptionReg = el.id;
        this.engNameReg = el.name
        console.log(this.engNameReg)
        console.log(this.idOptionReg);
      }
    });
  }
}
