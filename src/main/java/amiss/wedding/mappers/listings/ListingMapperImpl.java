package amiss.wedding.mappers.listings;


import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.plans.ListingPlanDto;

public class ListingMapperImpl implements ListingMapper {


    @Override
    public Listing listingDtoToListing(ListingDto listingDto) {
        Listing listing = new Listing();
        listing.setTitle(listingDto.getTitle());
        listing.setEmail(listingDto.getEmail());
        listing.setPhone(listingDto.getPhone());
        listing.setAddress(listingDto.getAddress());
        listing.setDescriptions(listingDto.getDescriptions());
        listing.setStatus(listingDto.getStatus());
        listing.setPriority(listingDto.getPriority());
        listing.setCapacity(listingDto.getCapacity());
        listing.setPrice(listingDto.getPrice());
        return listing;
    }

    @Override
    public ListingDto listingToListingDto(Listing listing) {
        return new ListingDto(listing);
    }

    /**
     * @param listing
     * @return
     */
    @Override
    public ListingDto listingToListingDtoBaseData(Listing listing) {

        ListingDto listingDto = new ListingDto(listing.getId(),
                listing.getTitle(),
                listing.getEmail(),
                listing.getPhone(),
                listing.getAddress(),
                listing.getDescriptions(),
                listing.getCoverImg() != null ? listing.getCoverImg().getId() : null,
                listing.getCoverImg() != null ? listing.getCoverImg().getPath() : null,
                listing.getStatus(),
                listing.getCountry().getId(),
                listing.getCountry().getName(),
                listing.getRegion().getId(),
                listing.getRegion().getName(),
                listing.getCity().getId(),
                listing.getCity().getName(),
                listing.getDistrict() != null ? listing.getDistrict().getId() : null,
                listing.getDistrict() != null ? listing.getDistrict().getName() : null,
                null,
                null,
                listing.getListingPlan() != null ? new ListingPlanDto(listing.getListingPlan()) : null,
                listing.getCategory().getId(),
                listing.getCategory().getName(),
                null,
                null,
                null,
                null,
                null,
                null,
                listing.getPriority(),
                listing.getCapacity() != null ? listing.getCapacity() : null,
                listing.getPrice() != null ? listing.getPrice() : null,
                listing.getContOfReviews(),
                listing.getContOfPhotos(),
                listing.getContOfVideo());
        return listingDto;
    }
}
