import { ArticleService } from './../../../../../services/article/article.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-articles-content',
  templateUrl: './articles-content.component.html',
  styleUrls: ['./articles-content.component.css']
})
export class ArticlesContentComponent implements OnInit {

  public articles: any;

  constructor(private articleService:ArticleService) { }

  ngOnInit(): void {
    this.getAllArticles()
  }

  getAllArticles() {
    this.articleService.getArticles().subscribe((article) => {
      
      this.articles = article;
      console.log(this.articles)
    })
  }

}
