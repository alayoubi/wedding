import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddListingCompontsComponent } from './add.listing.componts.component';

describe('AddListingCompontsComponent', () => {
  let component: AddListingCompontsComponent;
  let fixture: ComponentFixture<AddListingCompontsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddListingCompontsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddListingCompontsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
