import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-show-region-data-landing',
  templateUrl: './show-region-data-landing.component.html',
  styleUrls: ['./show-region-data-landing.component.css'],
})
export class ShowRegionDataLandingComponent implements OnInit {
  public titleStr: any;
  constructor(
    private title: Title,
    private route: ActivatedRoute,
    public router: Router
  ) {
    this.route.params.subscribe((params) => {
      this.title.setTitle(`${params['region']}`);
      this.titleStr = title;
    });
  }

  ngOnInit(): void {}
}
