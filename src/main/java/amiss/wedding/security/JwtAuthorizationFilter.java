//package amiss.wedding.security;
//
//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jws;
//import io.jsonwebtoken.Jwts;
//import org.springframework.http.HttpHeaders;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class JwtAuthorizationFilter { //extends BasicAuthenticationFilter {
//
////    private String jwtSecret;
////
////    private RestAuthenticationEntryPoint authenticationEntryPoint;
////
////    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
////                                  String jwtSecret, RestAuthenticationEntryPoint authenticationEntryPoint) {
////        super(authenticationManager);
////        this.jwtSecret = jwtSecret;
////        this.authenticationEntryPoint = authenticationEntryPoint;
////    }
////
////
////    private UsernamePasswordAuthenticationToken parseToken(HttpServletRequest request) {
////        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
////        if (token != null && token.startsWith("Bearer ")) {
////            String claims = token.replace("Bearer ", "");
////
////            Jws<Claims> claimsJws = Jwts.parser()
////                    .setSigningKey(jwtSecret.getBytes())
////                    .parseClaimsJws(claims);
////
////            String username = claimsJws.getBody().getSubject();
////
////            if ("".equals(username) || username == null) {
////                return null;
////            }
////
////            List<SimpleGrantedAuthority> authorities = ((ArrayList<String>) claimsJws.getBody().get("roles")).stream()
////                    .map((role) -> new SimpleGrantedAuthority(role))
////                    .collect(Collectors.toList());
////
////            return new UsernamePasswordAuthenticationToken(username, null, authorities);
////
////        }
////
////        return null;
////    }
////
////    @Override
////    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
////                                    FilterChain filterChain) throws ServletException, IOException {
////
////        try {
////            UsernamePasswordAuthenticationToken authentication = parseToken(request);
////            if (authentication != null) {
////                SecurityContextHolder.getContext().setAuthentication(authentication);
////            } else {
////                SecurityContextHolder.clearContext();
////            }
////        } catch (Exception exception) {
////            authenticationEntryPoint.commence(request, response, exception);
////            return;
////        }
////
////        filterChain.doFilter(request, response);
////    }
//
//}
