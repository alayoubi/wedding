import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesGalleryComponent } from './features-gallery.component';

describe('FeaturesGalleryComponent', () => {
  let component: FeaturesGalleryComponent;
  let fixture: ComponentFixture<FeaturesGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeaturesGalleryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FeaturesGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
