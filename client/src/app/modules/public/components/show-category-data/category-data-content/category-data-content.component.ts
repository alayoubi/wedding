import { ListingService } from '@core/services/listing/listing.service';
import { Title } from '@angular/platform-browser';
import { CategoryService } from './../../../../../services/category/category.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { title } from 'process';

@Component({
  selector: 'app-category-data-content',
  templateUrl: './category-data-content.component.html',
  styleUrls: ['./category-data-content.component.css'],
})
export class CategoryDataContentComponent implements OnInit {
  public listings:any
  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.getCategoryById()
  }

  public getCategoryById(): void {
    this.route.params.subscribe((params) => {
      this.listingService
        .getListingByCatId(params['category_id'])
        .subscribe((listingService) => {
          this.listings = listingService;
          this.titleService.setTitle(params['category']);
          console.log(this.listings);
        });
    });
  }
}
