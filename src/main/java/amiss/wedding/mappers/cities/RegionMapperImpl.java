package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.cities.dto.RegionDto;

public class RegionMapperImpl implements RegionMapper{
    /**
     * @param regionDto
     * @return
     */
    @Override
    public Region regionDtoToRegion(RegionDto regionDto) {
        Region region = new Region();
        region.setCode(regionDto.getCode());
        region.setName(regionDto.getName());
        region.setName_en(regionDto.getName_en());
        return region;
    }

    /**
     * @param region
     * @return
     */
    @Override
    public RegionDto regionToRegionDto(Region region) {
       RegionDto regionDto = new RegionDto(region);
       return regionDto;
    }
}
