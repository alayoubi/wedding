import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactBlockSectionComponent } from './contact-block-section.component';

describe('ContactBlockSectionComponent', () => {
  let component: ContactBlockSectionComponent;
  let fixture: ComponentFixture<ContactBlockSectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactBlockSectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContactBlockSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
