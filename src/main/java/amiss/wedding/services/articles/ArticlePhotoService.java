package amiss.wedding.services.articles;

import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.entities.articles.dto.ArticlePhotoDto;
import amiss.wedding.entities.articles.dto.MiniArticleDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Set;

public interface ArticlePhotoService {
    ResponseEntity<List<ArticlePhotoDto>> findAll();
    ResponseEntity<ArticlePhotoDto> findById(Long id);
    ResponseEntity<List<ArticlePhotoDto>> findAllByArticle(Long articleId);
    ResponseEntity<List<ArticlePhotoDto>> findAllByMiniArticle(Long miniArticleId);
    ArticlePhoto creatArticlePhoto(ArticlePhotoDto articlePhotoDto);
    Set<ArticlePhoto> creatListOfArticlePhoto(Set<ArticlePhotoDto> articlePhotoDtoList);
    ResponseEntity<ArticleDto> uploadArticlePhotos(Long articleId, List<MultipartFile> photos);
    ResponseEntity<MiniArticleDto> uploadMiniArticlePhotos(Long miniArticleId, List<MultipartFile> photos);
    ResponseEntity<ArticlePhotoDto> update(Long id, MultipartFile multipartFile);
    ResponseEntity<String> remove(Long id);
}
