package amiss.wedding.services.listings.impl;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.Status;
import amiss.wedding.entities.listings.dto.ListingDto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;
import amiss.wedding.errors.ListingNotFoundException;
import amiss.wedding.errors.ListingPhotoNotFoundException;
import amiss.wedding.mappers.listings.ListingMapper;
import amiss.wedding.mappers.listings.ListingMapperImpl;
import amiss.wedding.mappers.listings.ListingPhotoMapper;
import amiss.wedding.mappers.listings.ListingPhotoMapperImpl;
import amiss.wedding.repositories.listings.ListingPhotoRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.listings.ListingPhotoService;
import amiss.wedding.storage.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;



@RequiredArgsConstructor
@Service
public class ListingPhotoServiceImpl implements ListingPhotoService {

    private final ListingsRepository listingsRepository;
    private final ListingPhotoRepository listingPhotoRepository;
    private final ListingPhotoMapper mapper = new ListingPhotoMapperImpl();
    private final ListingMapper listingMapper = new ListingMapperImpl();

    @Autowired
    private StorageService storageService;

    public static final String FILE_NAME = "Photo";
    @Value(value = "${spring.profiles.active}")
    private String version;

    @Value(value = "${application.bucket.link}")
    private String link;

    @Override
    public ResponseEntity<List<ListingPhotoDto>> findAll() {
        List<ListingPhoto> listingPhotos = this.listingPhotoRepository.findAll();
        List<ListingPhotoDto> listingPhotoDtoList =
                listingPhotos.stream().map(mapper::listingPhotoToListingPhotoDto).collect(Collectors.toList());

        return new ResponseEntity<>(listingPhotoDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ListingPhotoDto> findById(Long id) {
        Optional<ListingPhoto> listingPhoto = this.listingPhotoRepository.findById(id);
        if (listingPhoto.isEmpty()) {
            throw new ListingPhotoNotFoundException(id);
        }
        ListingPhotoDto listingPhotoDto = mapper.listingPhotoToListingPhotoDto(listingPhoto.get());

        return new ResponseEntity<>(listingPhotoDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ListingPhotoDto>> findAllByListingId(Long listingId) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        List<ListingPhoto> listingPhotoList = this.listingPhotoRepository.findAllByListing(listing);
        List<ListingPhotoDto> listingPhotoDtoList =
                listingPhotoList.stream().map(mapper::listingPhotoToListingPhotoDto).collect(Collectors.toList());

        return new ResponseEntity<>(listingPhotoDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ListingPhotoDto> create(MultipartFile multipartFile, Long listingId) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        String key = "listings/" + listingId + "/" + FILE_NAME + "_";
        String link = "https://lilty.sgp1.digitaloceanspaces.com/";
        link +=  "listings%2F" + listingId + "%2F" + FILE_NAME + "_";
        link = storageService.uploadFile(multipartFile, key, link);
        ListingPhoto listingPhoto = new ListingPhoto(key, link, listing,
                Status.PENDING, multipartFile.getSize());
        listingPhotoRepository.save(listingPhoto);
        ListingPhotoDto listingPhotoDto = mapper.listingPhotoToListingPhotoDto(listingPhoto);
        return new ResponseEntity<>(listingPhotoDto, HttpStatus.OK);
    }

    /**
     * @param listingId
     * @param photos
     * @return
     */
    @Override
    public ResponseEntity<ListingDto> uploadsPhotos(Long listingId, List<MultipartFile> photos) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        Set<ListingPhoto> listingPhotoDtoList = new HashSet<>();
        String key = version + "/listings/" + listing.getId() + "/" + FILE_NAME + "_";
        String link =  this.link + version + "%2Flistings%2F" + listing.getId() + "%2F" + FILE_NAME + "_";
        int index = 0;
        for (MultipartFile photo : photos) {
            String path = link;
            path = storageService.uploadFile(photo, key, path);
            ListingPhoto listingPhoto = new ListingPhoto(key, path, listing,
                    Status.COMPLETED, photo.getSize());
            listingPhotoRepository.save(listingPhoto);
            listingPhotoDtoList.add(listingPhoto);
            if (index == 0 && listing.getCoverImg() == null) {
                listing.setCoverImg(listingPhoto);
            }
            index++;
        }
        listing.setContOfPhotos(listingPhotoDtoList.size());
        listingsRepository.save(listing);
        ListingDto listingDto = listingMapper.listingToListingDto(listing);
        return new ResponseEntity<>(listingDto, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<ListingPhotoDto> update(Long id, MultipartFile multipartFile) {
        Optional<ListingPhoto> listingPhoto = this.listingPhotoRepository.findById(id);
        if (listingPhoto.isEmpty()) {
            throw new ListingPhotoNotFoundException(id);
        }
        ListingPhoto existingPhoto = listingPhoto.get();
        Listing listing = existingPhoto.getListing();
        storageService.deleteFile(existingPhoto.getPath());
        String key = "listings/" + listing.getId() + "/" + FILE_NAME + "_";
        String link = "https://lilty.sgp1.digitaloceanspaces.com/";
        link +=  "listings%2F" + listing.getId() + "%2F" + FILE_NAME + "_";
        link = storageService.uploadFile(multipartFile, key, link);
        existingPhoto.setPath(link);
        existingPhoto.setSize(multipartFile.getSize());
        listingPhotoRepository.save(existingPhoto);
        ListingPhotoDto listingPhotoDto = mapper.listingPhotoToListingPhotoDto(existingPhoto);
        return new ResponseEntity<>(listingPhotoDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.listingPhotoRepository.existsById(id);
        if (!exists) {
            throw new ListingPhotoNotFoundException(id);
        }
        Optional<ListingPhoto> listingPhoto = this.listingPhotoRepository.findById(id);
        if (listingPhoto.isEmpty()) {
            throw new ListingPhotoNotFoundException(id);
        }
        ListingPhoto existingPhoto = listingPhoto.get();
        storageService.deleteFile(existingPhoto.getPath());
        this.listingPhotoRepository.deleteById(id);
        return new ResponseEntity<>("ListingPhoto has been removed successfully!", HttpStatus.NO_CONTENT);
    }

    /**
     * @param listingPhotoDto
     * @param listing
     * @return
     */
    @Override
    public ListingPhoto addListingPhoto(ListingPhotoDto listingPhotoDto, Listing listing) {
        ListingPhoto listingPhoto = mapper.listingPhotoDtoToListingPhoto(listingPhotoDto);
        listingPhoto.setListing(listing);
        listingPhoto = listingPhotoRepository.save(listingPhoto);
        return listingPhoto;
    }

    /**
     * @param listingPhotoDtoSet
     * @param listing
     */
    @Override
    public Set<ListingPhoto> addListOfListingPhoto(Set<ListingPhotoDto> listingPhotoDtoSet, Listing listing) {
        Set<ListingPhoto> listingPhotoList = listing.getListingPhotos();
        for (ListingPhotoDto listingPhotoDto : listingPhotoDtoSet) {
            listingPhotoList.add(addListingPhoto(listingPhotoDto, listing));
        }
        return listingPhotoList;
    }

    @Override
    public ResponseEntity<ListingDto> addCoverImg(Long listingId, ListingPhotoDto listingPhotoDto) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        ListingPhoto coverImg = addListingPhoto(listingPhotoDto, listing);
        if (listing.getCoverImg() != null) {
            ListingPhoto listingPhoto = listingPhotoRepository.findById(listing.getCoverImg().getId()).orElseThrow(
                    ()-> new ListingPhotoNotFoundException(listingId));
            listingPhoto.setListing(null);
            listingPhotoRepository.delete(listingPhoto);
        }
        listing.setCoverImg(coverImg);
        final Listing updatedListing = this.listingsRepository.save(listing);
        ListingDto listingDto = listingMapper.listingToListingDto(updatedListing);
        return ResponseEntity.ok(listingDto);
    }

    /**
     * @param listingPhotoDtolist
     * @param listingId
     * @return
     */
    @Override
    public ResponseEntity<ListingDto> addListingPhotos(List<ListingPhotoDto> listingPhotoDtolist, Long listingId) {
        Listing listing = this.listingsRepository.findById(listingId).orElseThrow(() ->
                new ListingNotFoundException(listingId));
        Set<ListingPhoto> listingPhotoList = addListOfListingPhoto(new HashSet<ListingPhotoDto>(listingPhotoDtolist),
                listing);
        listing.setListingPhotos(listingPhotoList);
        listing.setContOfPhotos(listingPhotoList.size());
        listing = listingsRepository.save(listing);
        return ResponseEntity.ok(listingMapper.listingToListingDto(listing));
    }
}
