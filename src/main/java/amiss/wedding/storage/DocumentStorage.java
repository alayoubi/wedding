//package amiss.wedding.storage;
//
//import org.springframework.stereotype.Service;
//import org.springframework.util.StringUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//
//@Service
//public class DocumentStorage extends StorageService{
//
//    public StorageFileDescription store(MultipartFile file, String pathPrefix) throws IOException {
//        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//        String key = String.format("%s/%s", pathPrefix, fileName);
//        long size = file.getSize();
//        store(file.getInputStream(), key);
//        return new StorageFileDescription(fileName, key, size);
//    }
//}
