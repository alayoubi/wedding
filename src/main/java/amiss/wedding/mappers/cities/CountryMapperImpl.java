package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.dto.CountryDto;

public class CountryMapperImpl implements CountryMapper{
    /**
     * @param countryDto
     * @return
     */
    @Override
    public Country countryDtoToCountry(CountryDto countryDto) {
        Country country = new Country();
        country.setName(countryDto.getName());
        country.setName_en(country.getName_en());
        country.setCode(countryDto.getCode());
        return country;
    }

    /**
     * @param country
     * @return
     */
    @Override
    public CountryDto countryToCountryDto(Country country) {
        return new CountryDto(country);
    }
}
