package amiss.wedding.controllers.admin.articles;

import amiss.wedding.entities.articles.dto.ArticleDto;
import amiss.wedding.services.articles.ArticleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/admin/articles")
@AllArgsConstructor
public class AdminArticleController {

    private final ArticleService articleService;

    @GetMapping("")
    public ResponseEntity<List<ArticleDto>> getArticles() {
        return this.articleService.findAll();
    }

    @GetMapping(path = "/{article_id}")
    public ResponseEntity<ArticleDto> getListing(@PathVariable("article_id") Long article_id){
        return this.articleService.findById(article_id);
    }

    @GetMapping(path = "/category/{category_id}")
    public ResponseEntity<List<ArticleDto>> getArticlesByCategory(@PathVariable("category_id") Long category_id){
        return this.articleService.findAllByCategory(category_id);
    }


    @PostMapping()
    public ResponseEntity<ArticleDto> createArticle(@RequestBody ArticleDto articleDto){
        return this.articleService.create(articleDto);
    }

    @PutMapping()
    public ResponseEntity<ArticleDto> createFullArticle(@RequestBody ArticleDto articleDto){
        return this.articleService.createFullArticle(articleDto);
    }

    @PutMapping(path = "/{article_id}")
    public ResponseEntity<ArticleDto> updateArticle(@PathVariable("article_id") Long article_id,
                                                            @RequestBody ArticleDto articleDto){
        return this.articleService.update(article_id, articleDto);
    }

    @DeleteMapping(path = "/{article_id}")
    public ResponseEntity<String> removeArticle(@PathVariable("article_id") Long article_id){
        return this.articleService.remove(article_id);
    }
}
