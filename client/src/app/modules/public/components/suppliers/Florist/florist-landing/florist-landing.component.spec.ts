import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloristLandingComponent } from './florist-landing.component';

describe('FloristLandingComponent', () => {
  let component: FloristLandingComponent;
  let fixture: ComponentFixture<FloristLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloristLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FloristLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
