package amiss.wedding.entities.articles.dto;

import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.listings.Listing;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class MiniArticleDto {

    private Long id;
    private String title;
    private String content;
    private Set<Long> articlePhotos_ID = new HashSet();
    private Set<String> articlePhotos = new HashSet();
    private Set<ArticlePhotoDto> articlePhotoDto = new HashSet();
    private Long article_Id;
    private Set<Long> listings_ID = new HashSet();
    private Set<String> listings = new HashSet();

    public MiniArticleDto(MiniArticle miniArticle) {
        this.id = miniArticle.getId();
        this.title = miniArticle.getTitle();
        this.content = miniArticle.getContent();
        if (miniArticle.getMiniArticlePhotos() != null && !miniArticle.getMiniArticlePhotos().isEmpty()) { // Todo add articlePhotoDto
            for (ArticlePhoto articlePhoto : miniArticle.getMiniArticlePhotos()) {
                this.articlePhotos_ID.add(articlePhoto.getId());
                this.articlePhotos.add(articlePhoto.getPath());
                articlePhotoDto.add(new ArticlePhotoDto(
                        articlePhoto.getId(),
                        articlePhoto.getFileName(),
                        articlePhoto.getTitle(),
                        articlePhoto.getPath(),
                        (miniArticle.getId() != null) ? miniArticle.getId(): null,
                        miniArticle.getId(),
                        articlePhoto.getSize()
                ));
            }
        }

        this.article_Id = (miniArticle.getId() != null) ? miniArticle.getId(): null;
        if (miniArticle.getListings() != null &&  !miniArticle.getListings().isEmpty()) {
            for (Listing listing : miniArticle.getListings()) {
                this.listings_ID.add(listing.getId());
                this.listings.add(listing.getTitle());
            }
        }
    }
}
