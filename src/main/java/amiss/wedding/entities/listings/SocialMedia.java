package amiss.wedding.entities.listings;

import amiss.wedding.entities.BaseEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "social_media")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SocialMedia extends BaseEntity {

    @NotNull
    @NotBlank(message = "Path may not be blank")
    @Column(name = "path")
    private String path;

    @ManyToOne
    @JoinColumn(name="listing_id", nullable=false)
    private Listing listing;

    @NotNull
    @Column(name = "typ", length = 32, columnDefinition = "varchar(32) default 'OTHER'")
    @Enumerated(EnumType.STRING)
    private SocialMediaTyp socialMediaTyp;
}
