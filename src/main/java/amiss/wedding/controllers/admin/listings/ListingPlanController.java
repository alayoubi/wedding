package amiss.wedding.controllers.admin.listings;


import amiss.wedding.entities.listings.dto.AttributeValueDto;
import amiss.wedding.entities.listings.plans.ListingPlanDto;
import amiss.wedding.services.listings.ListingPlanService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/v1/admin/listings/listing_plan")
@AllArgsConstructor
public class ListingPlanController {

    private final ListingPlanService listingPlanService;


    @GetMapping(path = "/{listingPlanId}")
    public ResponseEntity<ListingPlanDto> getListingPlan(@PathVariable("listingPlanId") Long listingPlanId){
        return this.listingPlanService.findById(listingPlanId);
    }

    @GetMapping(path = "/listing/{listing_id}")
    public ResponseEntity<ListingPlanDto> getListingPlanForListingById(@PathVariable("listing_id") Long listing_id){
        return this.listingPlanService.findListingPlanForListingById(listing_id);
    }

    @PostMapping("/{listing_id}/{plan_id}")
    public ResponseEntity<ListingPlanDto> createListingPlan(@PathVariable("plan_id") Long plan_id,
                                                            @PathVariable("listing_id") Long listing_id){
        return this.listingPlanService.create(plan_id, listing_id);
    }

    @PutMapping(path = "")
    public ResponseEntity<ListingPlanDto> updateListingPlan(
            @RequestBody ListingPlanDto listingPlanDto){
        return this.listingPlanService.update(listingPlanDto);
    }
}
