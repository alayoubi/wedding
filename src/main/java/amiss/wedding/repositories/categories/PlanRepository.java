package amiss.wedding.repositories.categories;

import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlanRepository extends JpaRepository<Plan, Long> {

    List<Plan> findByCategory(Category category);
    Plan findByCategoryAndAndPlanType(Category category, PlanType planType);
}
