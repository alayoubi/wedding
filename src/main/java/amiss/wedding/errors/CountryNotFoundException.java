package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class CountryNotFoundException extends ApplicationRuntimeException {

    public CountryNotFoundException(Long id){
        super("The Country with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
