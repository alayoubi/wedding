import { UpdateListingComponent } from './../module/listing/components/update-listing/update-listing.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from './default.component';
// import { DashboardComponent } from '../module/dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
// import { PostsComponent } from '../module/posts/posts.component';
import { SharedModule } from '../shared/shared.module';
// import { CustomersModule } from '../module/customers/customers.module';
// import { CategoriesComponent } from '../module/categories/categories.component';
// import { CustomersComponent } from '../module/customers/customers.component';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { DashboradService } from '../module/dashboard.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { ApiService } from '../module/customers/services/api.service';
import { ModuleModule } from '../module/module.module';
import { DashboardComponent } from '../module/dashboard/dashboard.component';
import { AddListingCompontsModule } from '../module/listing/components/add.listing.componts/add.listing.componts.module';
import { PublicModule } from '@core/modules/public/public.module';



@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent

  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    ModuleModule,
    AddListingCompontsModule,
    PublicModule
  ],
  providers: [DashboradService,ApiService],

})
export class DefaultModule {}
