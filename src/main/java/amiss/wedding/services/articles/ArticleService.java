package amiss.wedding.services.articles;

import amiss.wedding.entities.articles.dto.ArticleDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ArticleService {

    ResponseEntity<List<ArticleDto>> findAll();
    ResponseEntity<ArticleDto> findById(long id);
    ResponseEntity<List<ArticleDto>> findAllByCategory(Long id);
    ResponseEntity<ArticleDto> create(ArticleDto articleDto);

    ResponseEntity<ArticleDto> createFullArticle(ArticleDto articleDto);

    ResponseEntity<ArticleDto> createArticleWithPhoto(ArticleDto articleDto);

    ResponseEntity<ArticleDto> update(Long id, ArticleDto articleDto);
    ResponseEntity<String> remove(Long id);
}
