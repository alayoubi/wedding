package amiss.wedding.entities.cities;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.listings.Listing;
import com.google.cloud.firestore.GeoPoint;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import software.amazon.ion.Decimal;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Set;

@Entity
@Table(name = "cities")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class City {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the contact")
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @Column(name = "name", length = 64)
    private String name;

    @NotNull
    @Column(name = "name_en", length = 64)
    private String name_en;

    @ManyToOne
    @JoinColumn(name="country_id", nullable=false)
    private Country country;

    @ManyToOne
    @JoinColumn(name="region_id", nullable=false)
    private Region region;

    @ManyToOne
    @JoinColumn(name="sub_region_id")
    private SubRegion subRegion;

    @OneToMany(mappedBy="city")
    private Set<Listing> listing;

    @OneToMany(mappedBy="city")
    private Set<District> districts;
}
