import { Component, OnInit } from '@angular/core';
import { Region } from '@core/models/city/Region';
import { RegionService } from '@core/services/city/region.service';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.css'],
})
export class LocationsComponent implements OnInit {
  public regions: Region[];

  constructor(private regionService: RegionService) {}

  ngOnInit(): void {
    this.getAllActiveRegion()
  }

  public getAllActiveRegion(): void {
    this.regionService.getRegions().subscribe((regionService) => {
      this.regions = regionService;
      
    });
  }
}
