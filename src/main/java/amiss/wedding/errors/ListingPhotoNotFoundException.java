package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ListingPhotoNotFoundException extends ApplicationRuntimeException {

    public ListingPhotoNotFoundException(Long id){
        super("The ListingPhoto with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
