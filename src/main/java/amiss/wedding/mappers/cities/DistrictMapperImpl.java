package amiss.wedding.mappers.cities;

import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.dto.DistrictDto;

public class DistrictMapperImpl implements DistrictMapper{
    /**
     * @param districtDto
     * @return
     */
    @Override
    public District districtDtoToDistrict(DistrictDto districtDto) {
        District district = new District();
        district.setName(districtDto.getName());
        district.setName_en(districtDto.getName_en());
        return district;
    }

    /**
     * @param district
     * @return
     */
    @Override
    public DistrictDto districtToDistrictDto(District district) {
        DistrictDto districtDto = new DistrictDto(district);
        return districtDto;
    }

    /**
     * @param district
     * @return
     */
    @Override
    public DistrictDto districtToDistrictDtoBaseData(District district) {
        return null;
    }
}
