import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ListingService } from '@core/services/listing/listing.service';

@Component({
  selector: 'app-region-data-content',
  templateUrl: './region-data-content.component.html',
  styleUrls: ['./region-data-content.component.css'],
})
export class RegionDataContentComponent implements OnInit {
  listings: any
  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.getRegionById()
  }

  public getRegionById(): void {
    this.route.params.subscribe((params) => {
      this.listingService
        .getListingByRegId(params['region_id'])
        .subscribe((listingService) => {
          this.listings = listingService;
          this.titleService.setTitle(params['region']);
          console.log(this.listings);
        });
    });
  }
}
