package amiss.wedding.entities.categories.plans;

import amiss.wedding.entities.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "features")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Feature extends BaseEntity {

    @NotNull
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @ManyToOne
    @JoinColumn(name="plan_id", nullable = false)
    private Plan plan;
}
