import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowCategoryDataLandingComponent } from './show-category-data-landing.component';

describe('ShowCategoryDataLandingComponent', () => {
  let component: ShowCategoryDataLandingComponent;
  let fixture: ComponentFixture<ShowCategoryDataLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowCategoryDataLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowCategoryDataLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
