package amiss.wedding.controllers.admin.cities;

import amiss.wedding.entities.cities.dto.CityDto;
import amiss.wedding.services.cities.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/cities")
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }


    @GetMapping("")
    public ResponseEntity<List<CityDto>> getCities(){
        return this.cityService.findAll();
    }

    @GetMapping("/region/{regionId}")
    public ResponseEntity<List<CityDto>> getCitiesByRegion(@PathVariable Long regionId){
        return this.cityService.findAllByRegion(regionId);
    }

    @GetMapping(path = "/{cityId}")
    public ResponseEntity<CityDto> getCity(@PathVariable Long cityId){
        return this.cityService.findById(cityId);
    }

    @PostMapping()
    public ResponseEntity<CityDto> createCity(@RequestBody CityDto cityDto){
        return this.cityService.create(cityDto);
    }

    @PutMapping(path = "/{cityId}")
    public ResponseEntity<CityDto> updateCity(@PathVariable Long cityId, @RequestBody CityDto cityDto){
        return this.cityService.update(cityId, cityDto);
    }

    @DeleteMapping(path = "/{cityId}")
    public ResponseEntity<String> removeCity(@PathVariable Long cityId){
        return this.cityService.remove(cityId);
    }
}
