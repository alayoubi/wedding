package amiss.wedding.email;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class EmailSettings {

    @NotBlank
    private String host;

    @NotNull
    private Integer port;

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    @NotBlank
    private String sender;

    @NotNull
    private Boolean debug;

    @NotNull
    private Smtp smtp;

}
