package amiss.wedding.controllers.admin.cities;

import amiss.wedding.entities.cities.dto.CityDto;
import amiss.wedding.entities.cities.dto.RegionDto;
import amiss.wedding.services.cities.CityService;
import amiss.wedding.services.cities.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/regions")
public class RegionController {

    private final RegionService regionService;

    @Autowired
    public RegionController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping("")
    public ResponseEntity<List<RegionDto>> getRegions(){
        return this.regionService.findAll();
    }

    @GetMapping("/active")
    public ResponseEntity<List<RegionDto>> getRegionsByActive(){
        return this.regionService.findAllActive();
    }

    @GetMapping("/country/{country_id}")
    public ResponseEntity<List<RegionDto>> getRegionsByCountry(@PathVariable Long country_id){
        return this.regionService.findAllByCountry(country_id);
    }

    @GetMapping("/active/{country_id}")
    public ResponseEntity<List<RegionDto>> getRegionsByCountryAndActive(@PathVariable Long country_id){
        return this.regionService.findAllByCountryAndActive(country_id);
    }

    @GetMapping("/{regionId}")
    public ResponseEntity<RegionDto> getRegion(@PathVariable Long regionId){
        return this.regionService.findById(regionId);
    }


    @PostMapping()
    public ResponseEntity<RegionDto> createRegion(@RequestBody RegionDto regionDto){
        return this.regionService.create(regionDto);
    }

    @PutMapping(path = "/{regionId}")
    public ResponseEntity<RegionDto> updateRegion(@PathVariable Long regionId, @RequestBody RegionDto regionDto){
        return this.regionService.update(regionId, regionDto);
    }

    @DeleteMapping(path = "/{regionId}")
    public ResponseEntity<String> removeRegion(@PathVariable Long regionId){
        return this.regionService.remove(regionId);
    }
}
