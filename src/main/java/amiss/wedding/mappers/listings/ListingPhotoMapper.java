package amiss.wedding.mappers.listings;

import amiss.wedding.entities.listings.photos.ListingPhoto;
import amiss.wedding.entities.listings.photos.ListingPhotoDto;

public interface ListingPhotoMapper {

    ListingPhoto listingPhotoDtoToListingPhoto(ListingPhotoDto listingPhotoDto);
    ListingPhotoDto listingPhotoToListingPhotoDto(ListingPhoto listingPhoto);
}
