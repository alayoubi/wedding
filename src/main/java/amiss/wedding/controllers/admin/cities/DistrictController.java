package amiss.wedding.controllers.admin.cities;

import amiss.wedding.entities.cities.dto.CityDto;
import amiss.wedding.entities.cities.dto.DistrictDto;
import amiss.wedding.services.cities.CityService;
import amiss.wedding.services.cities.DistrictService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/districts")
public class DistrictController {

    private final DistrictService districtService;

    @Autowired
    public DistrictController(DistrictService districtService) {
        this.districtService = districtService;
    }


    @GetMapping("")
    @ApiOperation(value = "استدعاء كل الاحياء", notes = "استدعاء كل الاحياء")
    public ResponseEntity<List<DistrictDto>> getDistricts(){
        return this.districtService.findAll();
    }

    @GetMapping("/region/{regionId}")
    @ApiOperation(value = "استدعاء الاحياء التابعة لاحد المحافظات", notes = "استدعاء الاحياء التابعة ل احد المحافظات بناء على ال ID")
    public ResponseEntity<List<DistrictDto>> getDistrictsByRegion(@PathVariable Long regionId){
        return this.districtService.findAllByRegion(regionId);
    }

    @GetMapping("/city/{cityId}")
    @ApiOperation(value = "استدعاء الاحياء التابعة لاحد المدن", notes = "استدعاء الاحياء التابعة ل احد المدن بناء على ال ID")
    public ResponseEntity<List<DistrictDto>> getDistrictsByCity(@PathVariable Long cityId){
        return this.districtService.findAllByCity(cityId);
    }

    @GetMapping(path = "/{districtId}")
    @ApiOperation(value = "استدعاء احد الاحياء", notes = "استدعاء احد الاحياء بناء على ال ID")
    public ResponseEntity<DistrictDto> getDistrict(@PathVariable Long districtId){
        return this.districtService.findById(districtId);
    }

    @PostMapping()
    @ApiOperation(value = "انشاء حي جديد", notes = "ليس فعال")
    public ResponseEntity<DistrictDto> createDistrict(@RequestBody DistrictDto districtDto){
        return this.districtService.create(districtDto);
    }

    @PutMapping(path = "/{districtId}")
    @ApiOperation(value = "تحديث حي", notes = "ليس فعال")
    public ResponseEntity<DistrictDto> updateDistrict(@PathVariable Long districtId, @RequestBody DistrictDto districtDto){
        return this.districtService.update(districtId, districtDto);
    }

    @DeleteMapping(path = "/{districtId}")
    @ApiOperation(value = "حذف حي", notes = "ليس فعال")
    public ResponseEntity<String> removeDistrict(@PathVariable Long districtId){
        return this.districtService.remove(districtId);
    }
}
