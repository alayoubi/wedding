package amiss.wedding.repositories.cities;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.District;
import amiss.wedding.entities.cities.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {
    List<District> findAllByRegion(Region region);
    List<District> findAllByCity(City city);
    District findByNameAndCity(String name, City city);
}
