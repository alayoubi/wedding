package amiss.wedding.entities.categories.attributes;

public enum AttributeType {
    MAIN, SECONDARY, GENERAL, QUESTIONS, COVID, OTHER
}
