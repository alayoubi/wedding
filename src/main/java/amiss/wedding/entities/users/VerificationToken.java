package amiss.wedding.entities.users;

import amiss.wedding.entities.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class VerificationToken extends BaseEntity {
    private String token;

    @Column(nullable = false)
    private LocalDateTime expiresAt;

    @OneToOne(targetEntity = AppUser.class)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private AppUser appUser;

    public VerificationToken(String token,
                             LocalDateTime expiresAt,
                             AppUser appUser) {
        this.token = token;
        this.expiresAt = expiresAt;
        this.appUser = appUser;;
    }

}
