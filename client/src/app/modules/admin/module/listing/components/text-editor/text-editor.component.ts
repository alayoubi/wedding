import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
// import { Validators, Editor, Toolbar } from 'ngx-editor';

@Component({
  selector: 'app-text-editor',
  templateUrl: './text-editor.component.html',
  styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent implements OnInit, OnDestroy{

  // editordoc = "هنا يمكن ادخال وصف الصالة";

  // editor: Editor;
  // toolbar: Toolbar = [
  //   ['bold', 'italic'],
  //   ['underline', 'strike'],
  //   ['code', 'blockquote'],
  //   ['ordered_list', 'bullet_list'],
  //   [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
  //   ['link', 'image'],
  //   ['text_color', 'background_color'],
  //   ['align_left', 'align_center', 'align_right', 'align_justify'],
  // ];


  // editorContent: string;
  // htmlContent: string;



  ngOnInit(): void {
    // this.editor = new Editor();
  }

  ngOnDestroy(): void {
    // this.editor.destroy();
  }

  // convertToHtml() {
  //   // Logic to convert the editor content to HTML
  //   // For example, using the DOMParser to parse the content as HTML
  //   const parser = new DOMParser();
  //   const parsedDoc = parser.parseFromString(this.editorContent, 'text/html');
  //   this.htmlContent = parsedDoc.body.innerHTML;

  // }
}
