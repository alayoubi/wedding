package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class AttributeValueNotFoundException extends ApplicationRuntimeException {

    public AttributeValueNotFoundException(Long id){
        super("The AttributeValue with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
