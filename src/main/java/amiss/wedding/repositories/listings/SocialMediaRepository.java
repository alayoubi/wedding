package amiss.wedding.repositories.listings;

import amiss.wedding.entities.listings.Listing;
import amiss.wedding.entities.listings.SocialMedia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SocialMediaRepository extends JpaRepository<SocialMedia, Long> {

    List<SocialMedia> findAllByListing(Listing listing);

    void deleteAllByListing(Listing listing);
}
