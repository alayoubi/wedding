package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ArticleNotFoundException extends ApplicationRuntimeException {

    public ArticleNotFoundException(long id) {
        super("The Article with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
