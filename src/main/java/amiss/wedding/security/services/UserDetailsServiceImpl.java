//package amiss.wedding.security.services;
//
//import amiss.wedding.entities.users.AppUser;
//import amiss.wedding.repositories.users.UserRepository;
//import amiss.wedding.security.utils.AppUserDetail;
//import lombok.RequiredArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.User.UserBuilder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//
//@Service
//@RequiredArgsConstructor
//public class UserDetailsServiceImpl implements UserDetailsService {
//
//    private UserRepository repository;
//
//    @Autowired
//    public UserDetailsServiceImpl(UserRepository repository) {
//        this.repository = repository;
//    }
//
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        AppUser user = repository.findUserByEmail(username).orElseThrow(() -> new UsernameNotFoundException(username));
//        return new AppUserDetail(user);
//    }
//
//    public AppUser save(AppUser entity) {
//        return repository.save(entity);
//    }
//}
