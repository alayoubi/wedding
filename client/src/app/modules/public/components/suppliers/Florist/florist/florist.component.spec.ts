import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FloristComponent } from './florist.component';

describe('FloristComponent', () => {
  let component: FloristComponent;
  let fixture: ComponentFixture<FloristComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FloristComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FloristComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
