package amiss.wedding.controllers.admin.categories;

import static org.mockito.Mockito.when;

import amiss.wedding.entities.categories.plans.PlanDto;
import amiss.wedding.services.categories.PlanService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {PlanController.class})
@ExtendWith(SpringExtension.class)
class PlanControllerTest {
    @Autowired
    private PlanController planController;

    @MockBean
    private PlanService planService;

    /**
     * Method under test: {@link PlanController#createPlan(PlanDto)}
     */
    @Test
    void testCreatePlan() throws Exception {
        when(planService.findAll()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.get("/api/v1/plans")
                .contentType(MediaType.APPLICATION_JSON);

        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content(objectMapper.writeValueAsString(new PlanDto()));
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController).build().perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link PlanController#getPlan(Long)}
     */
    @Test
    void testGetPlan() throws Exception {
        when(planService.findById(Mockito.<Long>any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/plans/{planId}", 1L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link PlanController#getPlan(Long)}
     */
    @Test
    void testGetPlan2() throws Exception {
        when(planService.findAll()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        when(planService.findById(Mockito.<Long>any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/plans/{planId}", "",
                "Uri Variables");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link PlanController#getPlans()}
     */
    @Test
    void testGetPlans() throws Exception {
        when(planService.findAll()).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/plans");
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link PlanController#getPlansByCategoryId(Long)}
     */
    @Test
    void testGetPlansByCategoryId() throws Exception {
        when(planService.findByCategory(Mockito.<Long>any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/plans/category/{category_id}",
                1L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link PlanController#removePlan(Long)}
     */
    @Test
    void testRemovePlan() throws Exception {
        when(planService.remove(Mockito.<Long>any())).thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/plans/{planId}", 1L);
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }

    /**
     * Method under test: {@link PlanController#updatePlan(Long, PlanDto)}
     */
    @Test
    void testUpdatePlan() throws Exception {
        when(planService.update(Mockito.<Long>any(), Mockito.<PlanDto>any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.CONTINUE));
        MockHttpServletRequestBuilder contentTypeResult = MockMvcRequestBuilders.put("/api/v1/plans/{planId}", 1L)
                .contentType(MediaType.APPLICATION_JSON);

        ObjectMapper objectMapper = new ObjectMapper();
        MockHttpServletRequestBuilder requestBuilder = contentTypeResult
                .content(objectMapper.writeValueAsString(new PlanDto()));
        ResultActions actualPerformResult = MockMvcBuilders.standaloneSetup(planController)
                .build()
                .perform(requestBuilder);
        actualPerformResult.andExpect(MockMvcResultMatchers.status().is(100));
    }
}

