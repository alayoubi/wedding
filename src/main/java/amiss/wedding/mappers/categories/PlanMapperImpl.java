package amiss.wedding.mappers.categories;

import amiss.wedding.entities.categories.plans.Feature;
import amiss.wedding.entities.categories.plans.FeatureDto;
import amiss.wedding.entities.categories.plans.Plan;
import amiss.wedding.entities.categories.plans.PlanDto;

public class PlanMapperImpl implements PlanMapper{

    @Override
    public Plan planDtoToPlan(PlanDto planDto) {
        Plan plan = new Plan();
        plan.setDuration(planDto.getDuration());
        plan.setCost(planDto.getCost());
        plan.setPlanType(planDto.getPlanType());
        plan.setCountOfPhotos(plan.getCountOfPhotos());
        plan.setVideo(planDto.isVideo());
        return plan;
    }

    @Override
    public PlanDto planToPlanDto(Plan plan) {

        return new PlanDto(plan);
    }

    /**
     * @param feature
     * @return
     */
    @Override
    public FeatureDto featureToFeatureDto(Feature feature) {
        return new FeatureDto(feature);
    }

    /**
     * @param featureDto
     * @return
     */
    @Override
    public Feature FeatureDtoToFeature(FeatureDto featureDto) {
        Feature feature = new Feature();
        feature.setTitle(featureDto.getTitle());
        return feature;
    }


}
