import { API } from './../../config/api.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from '@core/models/city/Country';

@Injectable({
  providedIn: 'root'
})

export class CountryService {

  constructor(private http: HttpClient) { }

  getCountries() : Observable<Country[]> {
       return this.http.get<Country[]>(API.COUNTRIES.BASE)
  }

  getCountry(countryId: number) : Observable<Country> {
    return this.http.get<Country>(API.COUNTRIES.ID(countryId));
  }
}
