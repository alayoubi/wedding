import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewsHeadComponent } from './reviews-head.component';

describe('ReviewsHeadComponent', () => {
  let component: ReviewsHeadComponent;
  let fixture: ComponentFixture<ReviewsHeadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewsHeadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReviewsHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
