package amiss.wedding.controllers.user.registation;

import amiss.wedding.entities.users.dto.UserDto;
import amiss.wedding.services.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    public ResponseEntity<List<UserDto>> getUsers(){
        return this.userService.findAll();
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long userId){
        return this.userService.findById(userId);
    }

    @PostMapping()
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto){
        return this.userService.create(userDto);
    }

    @PutMapping(path = "/{userId}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Long userId, @RequestBody UserDto userDto){
        return this.userService.update(userId, userDto);
    }

    @DeleteMapping(path = "/{userId}")
    public ResponseEntity<String> removeUser(@PathVariable Long userId){
        return this.userService.remove(userId);
    }

}
