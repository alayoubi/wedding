package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ArticleCategoryNotFoundException extends ApplicationRuntimeException {

    public ArticleCategoryNotFoundException(long id) {
        super("The Article Category with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
