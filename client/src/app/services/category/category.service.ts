import { API } from './../../config/api.config';
import { Observable, of, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from 'src/app/models/category/Category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  private categories: Category[];
  constructor(private http: HttpClient) {}

  /**
   *
   * @returns
   */
  // getCategories(): Observable<Category[]> {
  //   return this.http.get<Category[]>(API.CATEGORIES.BASE);
  // }

  getCategories(): Observable<Category[]> {
    if (this.categories) {
      // Return cached categories if they exist
      return of(this.categories);
    } else {
      // Load categories from API and cache the result
      return this.http
        .get<Category[]>(API.CATEGORIES.BASE)
        .pipe(tap((categories) => (this.categories = categories)));
    }
  }

  /**
   *
   * @param categoryId
   * @returns
   */
  getCategory(categoryId: number): Observable<any> {
    return this.http.get<Category>(API.CATEGORIES.ID(categoryId));
  }

  /**
   *
   * @param category
   * @returns
   */
  addCategory(category: Category): Observable<Category> {
    return this.http.post<Category>(
      '${this.apiServerUrl}/categories',
      category
    );
  }

  /**
   *
   * @param categoryId
   * @param category
   * @returns
   */

  updateCategory(categoryId: number, Model: any ,): Observable<Category> {
    return this.http.put<Category>(API.CATEGORIES.BASE, Model,);
  }
  u

  /**
   *
   * @param categoryId
   * @returns
   */
  // deleteCategory(categoryId: number): Observable<string> {
  //   return this.http.delete<string>(
  //     '${this.apiServerUrl}/categories/(categoryId)'
  //   );

  // }

  deleteCategory(id: number): Observable<any> {
    return this.http.delete<any>(`${API.CATEGORIES.BASE}` + id);
  }
  grtCategory(id){
    return this.http.get("http://212.87.214.159:8080/api/v1/categories/"+id);
  }

  putCategory(data: any, id: number): Observable<Category[]> {
    return this.http.put<Category[]>(`${API.CATEGORIES.BASE}` + id, data);
  }
  postCategory(category: Category[]): Observable<Category[]> {
    return this.http.post<Category[]>(`${API.CATEGORIES.BASE}`, category);
  }
}
