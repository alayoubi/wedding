import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideographersContentComponent } from './videographers-content.component';

describe('VideographersContentComponent', () => {
  let component: VideographersContentComponent;
  let fixture: ComponentFixture<VideographersContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideographersContentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VideographersContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
