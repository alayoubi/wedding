package amiss.wedding.email;

public interface EmailSender {
    void send(String to, String email);
}
