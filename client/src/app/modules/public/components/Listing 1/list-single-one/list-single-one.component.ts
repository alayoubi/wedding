import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Listing } from '@core/models/listing/Listing';
import { ListingService } from '@core/services/listing/listing.service';

@Component({
  selector: 'app-list-single-one',
  templateUrl: './list-single-one.component.html',
  styleUrls: ['./list-single-one.component.css'],
})
export class ListSingleOneComponent implements OnInit {
  public listings: Array<Listing>;
  public listing: Listing;
  public listingAttributes: Array<any>;
  public listingAttributesMain: Array<any> = [];
  public listingPhotos: string[];
  public title: any;
  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router,
    private titleService: Title
  ) {}

  public getListingById(): void {
    this.route.params.subscribe((params) => {
      this.listingService
        .getListing(params['listingId'])
        .subscribe((listingService) => {
          this.listing = listingService;
          this.titleService.setTitle(`${this.title}`);
          console.log(this.listing);
        });
    });
  }

  ngOnInit(): void {
    this.getListingById();
  }
}
