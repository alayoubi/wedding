package amiss.wedding.entities.listings.dto;


import amiss.wedding.entities.listings.SocialMedia;
import amiss.wedding.entities.listings.SocialMediaTyp;
import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SocialMediaDto {

    private Long id;
    private String path;
    private SocialMediaTyp socialMediaTyp;
    private Long listingId;

    public SocialMediaDto(SocialMedia socialMedia){
        this.id = socialMedia.getId();
        this.path = socialMedia.getPath();
        this.socialMediaTyp = socialMedia.getSocialMediaTyp();
        this.listingId = socialMedia.getListing().getId();
    }
}
