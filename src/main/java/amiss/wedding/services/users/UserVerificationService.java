package amiss.wedding.services.users;

import amiss.wedding.entities.users.AppUser;
import amiss.wedding.entities.users.VerificationToken;

import java.util.Optional;

public interface UserVerificationService {

    VerificationToken createAndSaveToken(AppUser user);

    AppUser getUser(String tokenDTO);

    public Optional<VerificationToken> getToken(String token);

    public void invalidateToken(String tokenDTO);

}
