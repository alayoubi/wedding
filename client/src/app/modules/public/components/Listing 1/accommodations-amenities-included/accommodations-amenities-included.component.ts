import { Attribute } from '@core/models/category/Attribute';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Listing } from '@core/models/listing/Listing';
import { ListingService } from '@core/services/listing/listing.service';
@Component({
  selector: 'app-accommodations-amenities-included',
  templateUrl: './accommodations-amenities-included.component.html',
  styleUrls: ['./accommodations-amenities-included.component.css'],
})
export class AccommodationsAmenitiesIncludedComponent implements OnInit {
  public listings: Array<Listing>;
  @Input() listing:any
  public listingAttributes: Array<any>;
  public listingAttributesMain: Array<any> = [];
  public listingPhotos: string[];

  public MainAtt: Array<Attribute>;
  public SecAtt: Array<Attribute>;
  public GenAtt: Array<Attribute>;
  public QuesAtt: Array<Attribute>;
  public CovAtt: Array<Attribute>;

  constructor(
    private listingService: ListingService,
    private route: ActivatedRoute,
    public router: Router
  ) {}

  ngOnInit(): void {

          this.listingAttributes = this.listing.attributeValueList;
          this.listingAttributes.forEach((el) => {
            if (el.typ === 'MAIN') {
              this.MainAtt = el;
            } else if (el.typ === 'GENERAL') {
              this.GenAtt = el;
            } else if (el.typ === 'SECONDARY') {
              this.SecAtt = el;
            } else if (el.typ === 'COVID') {
              this.CovAtt = el;
            } else if (el.typ === 'QUESTIONS') {
              this.QuesAtt = el;
            }
          });
  }


  }

