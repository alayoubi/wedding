import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowRegionDataComponent } from './show-region-data.component';

describe('ShowRegionDataComponent', () => {
  let component: ShowRegionDataComponent;
  let fixture: ComponentFixture<ShowRegionDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowRegionDataComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowRegionDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
