import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinUsLandingComponent } from './join-us-landing.component';

describe('JoinUsLandingComponent', () => {
  let component: JoinUsLandingComponent;
  let fixture: ComponentFixture<JoinUsLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JoinUsLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JoinUsLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
