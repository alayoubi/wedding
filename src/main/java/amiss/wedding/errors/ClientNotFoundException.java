package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class ClientNotFoundException extends ApplicationRuntimeException {

    public ClientNotFoundException(long id){
        super("The Client with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
