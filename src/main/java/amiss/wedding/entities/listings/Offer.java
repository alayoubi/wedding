package amiss.wedding.entities.listings;

import amiss.wedding.entities.BaseEntity;
import amiss.wedding.entities.users.client.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "offers")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Offer extends BaseEntity {


    @NotNull
    @NotBlank(message = "Title may not be blank")
    @Column(name = "title", length = 100)
    @Size(max = 100, message = "Title may not be more than 100 characters")
    private String title;

    @Column(name = "contents", nullable=false)
    private String content;

    @Column(name = "expiry_dates", nullable=false)
    private Date expiryDate;

    @ManyToOne
    @JoinColumn(name="listing_id", nullable=false)
    private Listing listing;

    @ManyToOne()
    @JoinColumn(name="client_id")
    private Client client;

}
