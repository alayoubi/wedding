package amiss.wedding.services.cities.impl;

import amiss.wedding.entities.cities.City;
import amiss.wedding.entities.cities.Country;
import amiss.wedding.entities.cities.Region;
import amiss.wedding.entities.cities.dto.CityDto;
import amiss.wedding.errors.CityNotFoundException;
import amiss.wedding.errors.RegionNotFoundException;
import amiss.wedding.mappers.cities.CityMapper;
import amiss.wedding.mappers.cities.CityMapperImpl;
import amiss.wedding.repositories.cities.CityRepository;
import amiss.wedding.repositories.cities.CountryRepository;
import amiss.wedding.repositories.cities.RegionRepository;
import amiss.wedding.services.cities.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CityServiceImpl implements CityService {

    private final CountryRepository countryRepository;
    private final RegionRepository regionRepository;
    private final CityRepository cityRepository;

    private static final CityMapper mapper = new CityMapperImpl();

    @Autowired
    public CityServiceImpl(CountryRepository countryRepository,
                           RegionRepository regionRepository,
                           CityRepository cityRepository) {
        this.countryRepository = countryRepository;
        this.regionRepository = regionRepository;
        this.cityRepository = cityRepository;
    }

    @Override
    public ResponseEntity<List<CityDto>> findAll() {
        List<City> cities = this.cityRepository.findAll();
        List<CityDto> cityDtoList =
                cities.stream().map(mapper::cityToCityDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(cityDtoList, HttpStatus.OK);
    }

    /**
     * @param region_id
     * @return
     */
    @Override
    public ResponseEntity<List<CityDto>> findAllByRegion(Long region_id) {
        Optional<Region> region = this.regionRepository.findById(region_id);
        if (region.isEmpty()) {
            throw new RegionNotFoundException(region_id);
        }
        List<City> cities = this.cityRepository.findAllByRegion(region.get());
        List<CityDto> cityDtoList =
                cities.stream().map(mapper::cityToCityDtoBaseData).collect(Collectors.toList());
        return new ResponseEntity<>(cityDtoList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CityDto> findById(Long id) {
        Optional<City> city = this.cityRepository.findById(id);
        if (city.isEmpty()){
            throw new CityNotFoundException(id);
        }
        CityDto cityDto = mapper.cityToCityDto(city.get());
        return new ResponseEntity<>(cityDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<CityDto> create(CityDto cityDto) {
        City city = mapper.cityDtoToCity(cityDto);
        Country country = this.countryRepository.findByName(cityDto.getCountry());
        Region region = this.regionRepository.findByName(cityDto.getRegion());
        city.setCountry(country);
        city.setRegion(region);
        final City createdCity = this.cityRepository.save(city);
        return new ResponseEntity<>(cityDto, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<CityDto> update(Long id, CityDto newCityDto) {
        Optional<City> city = this.cityRepository.findById(id);
        if (city.isEmpty()){
            throw new CityNotFoundException(id);
        }
        City existingCity = city.get();
        existingCity.setName(newCityDto.getName());
        Country country = this.countryRepository.findByName(newCityDto.getCountry());
        Region region = this.regionRepository.findByName(newCityDto.getRegion());
        existingCity.setCountry(country);
        existingCity.setRegion(region);
        City updatedCity = this.cityRepository.save(existingCity);
        CityDto cityDto = mapper.cityToCityDto(updatedCity);
        return new ResponseEntity<>(cityDto, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> remove(Long id) {
        boolean exists = this.cityRepository.existsById(id);
        if (!exists){
            throw new CityNotFoundException(id);
        }
        this.cityRepository.deleteById(id);
        return new ResponseEntity<>("City has been removed successfully!", HttpStatus.NO_CONTENT);
    }
}
