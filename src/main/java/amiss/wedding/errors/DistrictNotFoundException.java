package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class DistrictNotFoundException extends ApplicationRuntimeException {

    public DistrictNotFoundException(Long id){
        super("The District with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }
}
