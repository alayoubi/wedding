package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class MiniArticleNotFoundException extends ApplicationRuntimeException {

    public MiniArticleNotFoundException(long id) {
        super("The Mini Article with the id: " + id + " was not found", HttpStatus.NOT_FOUND);
    }

}
