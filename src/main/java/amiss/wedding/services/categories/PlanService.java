package amiss.wedding.services.categories;

import amiss.wedding.entities.categories.plans.FeatureDto;
import amiss.wedding.entities.categories.plans.PlanDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PlanService {

    ResponseEntity<List<PlanDto>> findAll();
    ResponseEntity<PlanDto> findById(Long id);
    ResponseEntity<PlanDto> create(PlanDto planDto);

    ResponseEntity<PlanDto> createFeature(Long plan_id, FeatureDto featureDto);

    ResponseEntity<PlanDto> update(Long id, PlanDto planDto);

    ResponseEntity<String> remove(Long id);
    ResponseEntity<List<PlanDto>> findByCategory(Long category_id);
}
