import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddListingCompontsComponent } from './add.listing.componts.component';
import { SpinnerOverlayComponent } from '../spinner-overlay/spinner-overlay.component';

@NgModule({
  declarations: [
  ],
  imports: [CommonModule],
})
export class AddListingCompontsModule {}
