package amiss.wedding.services.articles.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.articles.ArticlePhoto;
import amiss.wedding.entities.articles.ArticleType;
import amiss.wedding.entities.articles.MiniArticle;
import amiss.wedding.entities.articles.dto.MiniArticleDto;
import amiss.wedding.errors.ArticleNotFoundException;
import amiss.wedding.errors.MiniArticleNotFoundException;
import amiss.wedding.repositories.articles.ArticleRepository;
import amiss.wedding.repositories.articles.MiniArticleRepository;
import amiss.wedding.repositories.listings.ListingsRepository;
import amiss.wedding.services.articles.ArticlePhotoService;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {MiniArticleServiceImpl.class})
@ExtendWith(SpringExtension.class)
class MiniArticleServiceImplTest {
    @MockBean
    private ArticlePhotoService articlePhotoService;

    @MockBean
    private ArticleRepository articleRepository;

    @MockBean
    private ListingsRepository listingsRepository;

    @MockBean
    private MiniArticleRepository miniArticleRepository;

    @Autowired
    private MiniArticleServiceImpl miniArticleServiceImpl;

    /**
     * Method under test: {@link MiniArticleServiceImpl#findAll()}
     */
    @Test
    void testFindAll() {
        ArrayList<MiniArticle> miniArticleList = new ArrayList<>();
        when(miniArticleRepository.findAll()).thenReturn(miniArticleList);
        ResponseEntity<List<MiniArticleDto>> actualFindAllResult = miniArticleServiceImpl.findAll();
        assertEquals(miniArticleList, actualFindAllResult.getBody());
        assertEquals(HttpStatus.OK, actualFindAllResult.getStatusCode());
        assertTrue(actualFindAllResult.getHeaders().isEmpty());
        verify(miniArticleRepository).findAll();
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#findAll()}
     */
    @Test
    void testFindAll2() {
        when(miniArticleRepository.findAll()).thenThrow(new ArticleNotFoundException(1L));
        assertThrows(ArticleNotFoundException.class, () -> miniArticleServiceImpl.findAll());
        verify(miniArticleRepository).findAll();
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#findById(long)}
     */
    @Test
    void testFindById() {
        Article article = new Article();
        article.setArticleCategories(new HashSet<>());
        article.setArticlePhotos(new HashSet<>());
        article.setArticleType(ArticleType.INFORMATION);
        article.setCategories(new HashSet<>());
        article.setContent("Not all who wander are lost");
        article.setCoverImg(new ArticlePhoto());
        article.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setId(1L);
        article.setListings(new HashSet<>());
        article.setMiniArticles(new HashSet<>());
        article.setReadTime(1);
        article.setTitle("Dr");
        article.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setVersion(1);

        MiniArticle miniArticle = new MiniArticle();
        miniArticle.setArticle(new Article());
        miniArticle.setContent("Not all who wander are lost");
        miniArticle.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setId(1L);
        miniArticle.setListings(new HashSet<>());
        miniArticle.setMiniArticlePhotos(new HashSet<>());
        miniArticle.setTitle("Dr");
        miniArticle.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setVersion(1);

        ArticlePhoto coverImg = new ArticlePhoto();
        coverImg.setArticle(article);
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setMiniArticle(miniArticle);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setTitle("Dr");
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Article article2 = new Article();
        article2.setArticleCategories(new HashSet<>());
        article2.setArticlePhotos(new HashSet<>());
        article2.setArticleType(ArticleType.INFORMATION);
        article2.setCategories(new HashSet<>());
        article2.setContent("Not all who wander are lost");
        article2.setCoverImg(coverImg);
        article2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setId(1L);
        article2.setListings(new HashSet<>());
        article2.setMiniArticles(new HashSet<>());
        article2.setReadTime(1);
        article2.setTitle("Dr");
        article2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setVersion(1);

        MiniArticle miniArticle2 = new MiniArticle();
        miniArticle2.setArticle(article2);
        miniArticle2.setContent("Not all who wander are lost");
        miniArticle2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setId(1L);
        miniArticle2.setListings(new HashSet<>());
        miniArticle2.setMiniArticlePhotos(new HashSet<>());
        miniArticle2.setTitle("Dr");
        miniArticle2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setVersion(1);
        Optional<MiniArticle> ofResult = Optional.of(miniArticle2);
        when(miniArticleRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ResponseEntity<MiniArticleDto> actualFindByIdResult = miniArticleServiceImpl.findById(1L);
        assertTrue(actualFindByIdResult.hasBody());
        assertTrue(actualFindByIdResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualFindByIdResult.getStatusCode());
        MiniArticleDto body = actualFindByIdResult.getBody();
        assertEquals(1L, body.getId().longValue());
        assertEquals("Not all who wander are lost", body.getContent());
        assertEquals(1L, body.getArticle_Id().longValue());
        assertTrue(body.getArticlePhotos_ID().isEmpty());
        assertTrue(body.getArticlePhotos().isEmpty());
        assertTrue(body.getArticlePhotoDto().isEmpty());
        assertEquals("Dr", body.getTitle());
        assertTrue(body.getListings().isEmpty());
        assertTrue(body.getListings_ID().isEmpty());
        verify(miniArticleRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#findAllByArticle(Long)}
     */
    @Test
    void testFindAllByArticle() {
        ArticlePhoto coverImg = new ArticlePhoto();
        coverImg.setArticle(new Article());
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setMiniArticle(new MiniArticle());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setTitle("Dr");
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Article article = new Article();
        article.setArticleCategories(new HashSet<>());
        article.setArticlePhotos(new HashSet<>());
        article.setArticleType(ArticleType.INFORMATION);
        article.setCategories(new HashSet<>());
        article.setContent("Not all who wander are lost");
        article.setCoverImg(coverImg);
        article.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setId(1L);
        article.setListings(new HashSet<>());
        article.setMiniArticles(new HashSet<>());
        article.setReadTime(1);
        article.setTitle("Dr");
        article.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setVersion(1);

        Article article2 = new Article();
        article2.setArticleCategories(new HashSet<>());
        article2.setArticlePhotos(new HashSet<>());
        article2.setArticleType(ArticleType.INFORMATION);
        article2.setCategories(new HashSet<>());
        article2.setContent("Not all who wander are lost");
        article2.setCoverImg(new ArticlePhoto());
        article2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setId(1L);
        article2.setListings(new HashSet<>());
        article2.setMiniArticles(new HashSet<>());
        article2.setReadTime(1);
        article2.setTitle("Dr");
        article2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setVersion(1);

        MiniArticle miniArticle = new MiniArticle();
        miniArticle.setArticle(article2);
        miniArticle.setContent("Not all who wander are lost");
        miniArticle.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setId(1L);
        miniArticle.setListings(new HashSet<>());
        miniArticle.setMiniArticlePhotos(new HashSet<>());
        miniArticle.setTitle("Dr");
        miniArticle.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setVersion(1);

        ArticlePhoto coverImg2 = new ArticlePhoto();
        coverImg2.setArticle(article);
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setMiniArticle(miniArticle);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setTitle("Dr");
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Article article3 = new Article();
        article3.setArticleCategories(new HashSet<>());
        article3.setArticlePhotos(new HashSet<>());
        article3.setArticleType(ArticleType.INFORMATION);
        article3.setCategories(new HashSet<>());
        article3.setContent("Not all who wander are lost");
        article3.setCoverImg(coverImg2);
        article3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setId(1L);
        article3.setListings(new HashSet<>());
        article3.setMiniArticles(new HashSet<>());
        article3.setReadTime(1);
        article3.setTitle("Dr");
        article3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setVersion(1);
        Optional<Article> ofResult = Optional.of(article3);
        when(articleRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        ArrayList<MiniArticle> miniArticleList = new ArrayList<>();
        when(miniArticleRepository.findAllByArticle(Mockito.<Article>any())).thenReturn(miniArticleList);
        ResponseEntity<List<MiniArticleDto>> actualFindAllByArticleResult = miniArticleServiceImpl.findAllByArticle(1L);
        assertEquals(miniArticleList, actualFindAllByArticleResult.getBody());
        assertEquals(HttpStatus.OK, actualFindAllByArticleResult.getStatusCode());
        assertTrue(actualFindAllByArticleResult.getHeaders().isEmpty());
        verify(articleRepository).findById(Mockito.<Long>any());
        verify(miniArticleRepository).findAllByArticle(Mockito.<Article>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#findAllByArticle(Long)}
     */
    @Test
    void testFindAllByArticle2() {
        ArticlePhoto coverImg = new ArticlePhoto();
        coverImg.setArticle(new Article());
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setMiniArticle(new MiniArticle());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setTitle("Dr");
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Article article = new Article();
        article.setArticleCategories(new HashSet<>());
        article.setArticlePhotos(new HashSet<>());
        article.setArticleType(ArticleType.INFORMATION);
        article.setCategories(new HashSet<>());
        article.setContent("Not all who wander are lost");
        article.setCoverImg(coverImg);
        article.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setId(1L);
        article.setListings(new HashSet<>());
        article.setMiniArticles(new HashSet<>());
        article.setReadTime(1);
        article.setTitle("Dr");
        article.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setVersion(1);

        Article article2 = new Article();
        article2.setArticleCategories(new HashSet<>());
        article2.setArticlePhotos(new HashSet<>());
        article2.setArticleType(ArticleType.INFORMATION);
        article2.setCategories(new HashSet<>());
        article2.setContent("Not all who wander are lost");
        article2.setCoverImg(new ArticlePhoto());
        article2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setId(1L);
        article2.setListings(new HashSet<>());
        article2.setMiniArticles(new HashSet<>());
        article2.setReadTime(1);
        article2.setTitle("Dr");
        article2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setVersion(1);

        MiniArticle miniArticle = new MiniArticle();
        miniArticle.setArticle(article2);
        miniArticle.setContent("Not all who wander are lost");
        miniArticle.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setId(1L);
        miniArticle.setListings(new HashSet<>());
        miniArticle.setMiniArticlePhotos(new HashSet<>());
        miniArticle.setTitle("Dr");
        miniArticle.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setVersion(1);

        ArticlePhoto coverImg2 = new ArticlePhoto();
        coverImg2.setArticle(article);
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setMiniArticle(miniArticle);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setTitle("Dr");
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Article article3 = new Article();
        article3.setArticleCategories(new HashSet<>());
        article3.setArticlePhotos(new HashSet<>());
        article3.setArticleType(ArticleType.INFORMATION);
        article3.setCategories(new HashSet<>());
        article3.setContent("Not all who wander are lost");
        article3.setCoverImg(coverImg2);
        article3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setId(1L);
        article3.setListings(new HashSet<>());
        article3.setMiniArticles(new HashSet<>());
        article3.setReadTime(1);
        article3.setTitle("Dr");
        article3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setVersion(1);
        Optional<Article> ofResult = Optional.of(article3);
        when(articleRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        when(miniArticleRepository.findAllByArticle(Mockito.<Article>any())).thenThrow(new ArticleNotFoundException(1L));
        assertThrows(ArticleNotFoundException.class, () -> miniArticleServiceImpl.findAllByArticle(1L));
        verify(articleRepository).findById(Mockito.<Long>any());
        verify(miniArticleRepository).findAllByArticle(Mockito.<Article>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#create(MiniArticleDto)}
     */
    @Test
    void testCreate() {
        ArticlePhoto coverImg = new ArticlePhoto();
        coverImg.setArticle(new Article());
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setMiniArticle(new MiniArticle());
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setTitle("Dr");
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Article article = new Article();
        article.setArticleCategories(new HashSet<>());
        article.setArticlePhotos(new HashSet<>());
        article.setArticleType(ArticleType.INFORMATION);
        article.setCategories(new HashSet<>());
        article.setContent("Not all who wander are lost");
        article.setCoverImg(coverImg);
        article.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setId(1L);
        article.setListings(new HashSet<>());
        article.setMiniArticles(new HashSet<>());
        article.setReadTime(1);
        article.setTitle("Dr");
        article.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setVersion(1);

        Article article2 = new Article();
        article2.setArticleCategories(new HashSet<>());
        article2.setArticlePhotos(new HashSet<>());
        article2.setArticleType(ArticleType.INFORMATION);
        article2.setCategories(new HashSet<>());
        article2.setContent("Not all who wander are lost");
        article2.setCoverImg(new ArticlePhoto());
        article2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setId(1L);
        article2.setListings(new HashSet<>());
        article2.setMiniArticles(new HashSet<>());
        article2.setReadTime(1);
        article2.setTitle("Dr");
        article2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setVersion(1);

        MiniArticle miniArticle = new MiniArticle();
        miniArticle.setArticle(article2);
        miniArticle.setContent("Not all who wander are lost");
        miniArticle.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setId(1L);
        miniArticle.setListings(new HashSet<>());
        miniArticle.setMiniArticlePhotos(new HashSet<>());
        miniArticle.setTitle("Dr");
        miniArticle.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setVersion(1);

        ArticlePhoto coverImg2 = new ArticlePhoto();
        coverImg2.setArticle(article);
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setMiniArticle(miniArticle);
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setTitle("Dr");
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Article article3 = new Article();
        article3.setArticleCategories(new HashSet<>());
        article3.setArticlePhotos(new HashSet<>());
        article3.setArticleType(ArticleType.INFORMATION);
        article3.setCategories(new HashSet<>());
        article3.setContent("Not all who wander are lost");
        article3.setCoverImg(coverImg2);
        article3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setId(1L);
        article3.setListings(new HashSet<>());
        article3.setMiniArticles(new HashSet<>());
        article3.setReadTime(1);
        article3.setTitle("Dr");
        article3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setVersion(1);

        MiniArticle miniArticle2 = new MiniArticle();
        miniArticle2.setArticle(article3);
        miniArticle2.setContent("Not all who wander are lost");
        miniArticle2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setId(1L);
        miniArticle2.setListings(new HashSet<>());
        miniArticle2.setMiniArticlePhotos(new HashSet<>());
        miniArticle2.setTitle("Dr");
        miniArticle2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setVersion(1);
        when(miniArticleRepository.save(Mockito.<MiniArticle>any())).thenReturn(miniArticle2);
        MiniArticleDto articleDto = new MiniArticleDto();
        ResponseEntity<MiniArticleDto> actualCreateResult = miniArticleServiceImpl.create(articleDto);
        MiniArticleDto body = actualCreateResult.getBody();
        assertEquals(articleDto, body);
        assertTrue(actualCreateResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualCreateResult.getStatusCode());
        assertEquals(1L, body.getId().longValue());
        assertEquals("Not all who wander are lost", body.getContent());
        assertEquals(1L, body.getArticle_Id().longValue());
        assertTrue(body.getArticlePhotos_ID().isEmpty());
        assertTrue(body.getArticlePhotos().isEmpty());
        assertTrue(body.getArticlePhotoDto().isEmpty());
        assertEquals("Dr", body.getTitle());
        assertTrue(body.getListings().isEmpty());
        assertTrue(body.getListings_ID().isEmpty());
        verify(miniArticleRepository).save(Mockito.<MiniArticle>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#createFullMiniArticle(Set, Long)}
     */
    @Test
    void testCreateFullMiniArticle() {
        assertTrue(miniArticleServiceImpl.createFullMiniArticle(new HashSet<>(), 1L).isEmpty());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#createFullMiniArticle(Set, Long)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testCreateFullMiniArticle2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "java.util.Collection.isEmpty()" because "that" is null
        //       at amiss.wedding.services.articles.impl.MiniArticleServiceImpl.createFullMiniArticle(MiniArticleServiceImpl.java:114)
        //   See https://diff.blue/R013 to resolve this issue.

        HashSet<MiniArticleDto> miniArticleDtoSet = new HashSet<>();
        MiniArticleDto.MiniArticleDtoBuilder builderResult = MiniArticleDto.builder();
        MiniArticleDto.MiniArticleDtoBuilder articlePhotosResult = builderResult.articlePhotos(new HashSet<>());
        MiniArticleDto.MiniArticleDtoBuilder idResult = articlePhotosResult.articlePhotos_ID(new HashSet<>())
                .article_Id(1L)
                .content("Not all who wander are lost")
                .id(1L);
        MiniArticleDto.MiniArticleDtoBuilder listingsResult = idResult.listings(new HashSet<>());
        MiniArticleDto buildResult = listingsResult.listings_ID(new HashSet<>()).title("Dr").build();
        miniArticleDtoSet.add(buildResult);
        miniArticleServiceImpl.createFullMiniArticle(miniArticleDtoSet, 1L);
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#update(Long, MiniArticleDto)}
     */
    @Test
    void testUpdate() {
        Article article = new Article();
        article.setArticleCategories(new HashSet<>());
        article.setArticlePhotos(new HashSet<>());
        article.setArticleType(ArticleType.INFORMATION);
        article.setCategories(new HashSet<>());
        article.setContent("Not all who wander are lost");
        article.setCoverImg(new ArticlePhoto());
        article.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setId(1L);
        article.setListings(new HashSet<>());
        article.setMiniArticles(new HashSet<>());
        article.setReadTime(1);
        article.setTitle("Dr");
        article.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setVersion(1);

        MiniArticle miniArticle = new MiniArticle();
        miniArticle.setArticle(new Article());
        miniArticle.setContent("Not all who wander are lost");
        miniArticle.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setId(1L);
        miniArticle.setListings(new HashSet<>());
        miniArticle.setMiniArticlePhotos(new HashSet<>());
        miniArticle.setTitle("Dr");
        miniArticle.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setVersion(1);

        ArticlePhoto coverImg = new ArticlePhoto();
        coverImg.setArticle(article);
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setMiniArticle(miniArticle);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setTitle("Dr");
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Article article2 = new Article();
        article2.setArticleCategories(new HashSet<>());
        article2.setArticlePhotos(new HashSet<>());
        article2.setArticleType(ArticleType.INFORMATION);
        article2.setCategories(new HashSet<>());
        article2.setContent("Not all who wander are lost");
        article2.setCoverImg(coverImg);
        article2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setId(1L);
        article2.setListings(new HashSet<>());
        article2.setMiniArticles(new HashSet<>());
        article2.setReadTime(1);
        article2.setTitle("Dr");
        article2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setVersion(1);

        MiniArticle miniArticle2 = new MiniArticle();
        miniArticle2.setArticle(article2);
        miniArticle2.setContent("Not all who wander are lost");
        miniArticle2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setId(1L);
        miniArticle2.setListings(new HashSet<>());
        miniArticle2.setMiniArticlePhotos(new HashSet<>());
        miniArticle2.setTitle("Dr");
        miniArticle2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setVersion(1);
        Optional<MiniArticle> ofResult = Optional.of(miniArticle2);

        ArticlePhoto coverImg2 = new ArticlePhoto();
        coverImg2.setArticle(new Article());
        coverImg2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setFileName("foo.txt");
        coverImg2.setId(1L);
        coverImg2.setMiniArticle(new MiniArticle());
        coverImg2.setPath("Path");
        coverImg2.setSize(10.0d);
        coverImg2.setTitle("Dr");
        coverImg2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg2.setVersion(1);

        Article article3 = new Article();
        article3.setArticleCategories(new HashSet<>());
        article3.setArticlePhotos(new HashSet<>());
        article3.setArticleType(ArticleType.INFORMATION);
        article3.setCategories(new HashSet<>());
        article3.setContent("Not all who wander are lost");
        article3.setCoverImg(coverImg2);
        article3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setId(1L);
        article3.setListings(new HashSet<>());
        article3.setMiniArticles(new HashSet<>());
        article3.setReadTime(1);
        article3.setTitle("Dr");
        article3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article3.setVersion(1);

        Article article4 = new Article();
        article4.setArticleCategories(new HashSet<>());
        article4.setArticlePhotos(new HashSet<>());
        article4.setArticleType(ArticleType.INFORMATION);
        article4.setCategories(new HashSet<>());
        article4.setContent("Not all who wander are lost");
        article4.setCoverImg(new ArticlePhoto());
        article4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article4.setId(1L);
        article4.setListings(new HashSet<>());
        article4.setMiniArticles(new HashSet<>());
        article4.setReadTime(1);
        article4.setTitle("Dr");
        article4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article4.setVersion(1);

        MiniArticle miniArticle3 = new MiniArticle();
        miniArticle3.setArticle(article4);
        miniArticle3.setContent("Not all who wander are lost");
        miniArticle3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle3.setId(1L);
        miniArticle3.setListings(new HashSet<>());
        miniArticle3.setMiniArticlePhotos(new HashSet<>());
        miniArticle3.setTitle("Dr");
        miniArticle3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle3.setVersion(1);

        ArticlePhoto coverImg3 = new ArticlePhoto();
        coverImg3.setArticle(article3);
        coverImg3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setFileName("foo.txt");
        coverImg3.setId(1L);
        coverImg3.setMiniArticle(miniArticle3);
        coverImg3.setPath("Path");
        coverImg3.setSize(10.0d);
        coverImg3.setTitle("Dr");
        coverImg3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg3.setVersion(1);

        Article article5 = new Article();
        article5.setArticleCategories(new HashSet<>());
        article5.setArticlePhotos(new HashSet<>());
        article5.setArticleType(ArticleType.INFORMATION);
        article5.setCategories(new HashSet<>());
        article5.setContent("Not all who wander are lost");
        article5.setCoverImg(coverImg3);
        article5.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article5.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article5.setId(1L);
        article5.setListings(new HashSet<>());
        article5.setMiniArticles(new HashSet<>());
        article5.setReadTime(1);
        article5.setTitle("Dr");
        article5.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article5.setVersion(1);

        MiniArticle miniArticle4 = new MiniArticle();
        miniArticle4.setArticle(article5);
        miniArticle4.setContent("Not all who wander are lost");
        miniArticle4.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle4.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle4.setId(1L);
        miniArticle4.setListings(new HashSet<>());
        miniArticle4.setMiniArticlePhotos(new HashSet<>());
        miniArticle4.setTitle("Dr");
        miniArticle4.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle4.setVersion(1);
        when(miniArticleRepository.save(Mockito.<MiniArticle>any())).thenReturn(miniArticle4);
        when(miniArticleRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        MiniArticleDto miniArticleDto = new MiniArticleDto();
        ResponseEntity<MiniArticleDto> actualUpdateResult = miniArticleServiceImpl.update(1L, miniArticleDto);
        MiniArticleDto body = actualUpdateResult.getBody();
        assertEquals(miniArticleDto, body);
        assertTrue(actualUpdateResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.OK, actualUpdateResult.getStatusCode());
        assertEquals(1L, body.getId().longValue());
        assertEquals("Not all who wander are lost", body.getContent());
        assertEquals(1L, body.getArticle_Id().longValue());
        assertTrue(body.getArticlePhotos_ID().isEmpty());
        assertTrue(body.getArticlePhotos().isEmpty());
        assertTrue(body.getArticlePhotoDto().isEmpty());
        assertEquals("Dr", body.getTitle());
        assertTrue(body.getListings().isEmpty());
        assertTrue(body.getListings_ID().isEmpty());
        verify(miniArticleRepository).save(Mockito.<MiniArticle>any());
        verify(miniArticleRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#update(Long, MiniArticleDto)}
     */
    @Test
    void testUpdate2() {
        Article article = new Article();
        article.setArticleCategories(new HashSet<>());
        article.setArticlePhotos(new HashSet<>());
        article.setArticleType(ArticleType.INFORMATION);
        article.setCategories(new HashSet<>());
        article.setContent("Not all who wander are lost");
        article.setCoverImg(new ArticlePhoto());
        article.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setId(1L);
        article.setListings(new HashSet<>());
        article.setMiniArticles(new HashSet<>());
        article.setReadTime(1);
        article.setTitle("Dr");
        article.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article.setVersion(1);

        MiniArticle miniArticle = new MiniArticle();
        miniArticle.setArticle(new Article());
        miniArticle.setContent("Not all who wander are lost");
        miniArticle.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setId(1L);
        miniArticle.setListings(new HashSet<>());
        miniArticle.setMiniArticlePhotos(new HashSet<>());
        miniArticle.setTitle("Dr");
        miniArticle.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle.setVersion(1);

        ArticlePhoto coverImg = new ArticlePhoto();
        coverImg.setArticle(article);
        coverImg.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setFileName("foo.txt");
        coverImg.setId(1L);
        coverImg.setMiniArticle(miniArticle);
        coverImg.setPath("Path");
        coverImg.setSize(10.0d);
        coverImg.setTitle("Dr");
        coverImg.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        coverImg.setVersion(1);

        Article article2 = new Article();
        article2.setArticleCategories(new HashSet<>());
        article2.setArticlePhotos(new HashSet<>());
        article2.setArticleType(ArticleType.INFORMATION);
        article2.setCategories(new HashSet<>());
        article2.setContent("Not all who wander are lost");
        article2.setCoverImg(coverImg);
        article2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setId(1L);
        article2.setListings(new HashSet<>());
        article2.setMiniArticles(new HashSet<>());
        article2.setReadTime(1);
        article2.setTitle("Dr");
        article2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        article2.setVersion(1);

        MiniArticle miniArticle2 = new MiniArticle();
        miniArticle2.setArticle(article2);
        miniArticle2.setContent("Not all who wander are lost");
        miniArticle2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setDeletedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setId(1L);
        miniArticle2.setListings(new HashSet<>());
        miniArticle2.setMiniArticlePhotos(new HashSet<>());
        miniArticle2.setTitle("Dr");
        miniArticle2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        miniArticle2.setVersion(1);
        Optional<MiniArticle> ofResult = Optional.of(miniArticle2);
        when(miniArticleRepository.save(Mockito.<MiniArticle>any())).thenThrow(new ArticleNotFoundException(1L));
        when(miniArticleRepository.findById(Mockito.<Long>any())).thenReturn(ofResult);
        assertThrows(ArticleNotFoundException.class, () -> miniArticleServiceImpl.update(1L, new MiniArticleDto()));
        verify(miniArticleRepository).save(Mockito.<MiniArticle>any());
        verify(miniArticleRepository).findById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#remove(Long)}
     */
    @Test
    void testRemove() {
        doNothing().when(miniArticleRepository).deleteById(Mockito.<Long>any());
        when(miniArticleRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        ResponseEntity<String> actualRemoveResult = miniArticleServiceImpl.remove(1L);
        assertEquals("Mini article has been removed successfully!", actualRemoveResult.getBody());
        assertEquals(HttpStatus.NO_CONTENT, actualRemoveResult.getStatusCode());
        assertTrue(actualRemoveResult.getHeaders().isEmpty());
        verify(miniArticleRepository).existsById(Mockito.<Long>any());
        verify(miniArticleRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#remove(Long)}
     */
    @Test
    void testRemove2() {
        doThrow(new ArticleNotFoundException(1L)).when(miniArticleRepository).deleteById(Mockito.<Long>any());
        when(miniArticleRepository.existsById(Mockito.<Long>any())).thenReturn(true);
        assertThrows(ArticleNotFoundException.class, () -> miniArticleServiceImpl.remove(1L));
        verify(miniArticleRepository).existsById(Mockito.<Long>any());
        verify(miniArticleRepository).deleteById(Mockito.<Long>any());
    }

    /**
     * Method under test: {@link MiniArticleServiceImpl#remove(Long)}
     */
    @Test
    void testRemove3() {
        when(miniArticleRepository.existsById(Mockito.<Long>any())).thenReturn(false);
        assertThrows(MiniArticleNotFoundException.class, () -> miniArticleServiceImpl.remove(1L));
        verify(miniArticleRepository).existsById(Mockito.<Long>any());
    }
}

