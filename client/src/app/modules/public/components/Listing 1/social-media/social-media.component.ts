import { ListingService } from './../../../../../services/listing/listing.service';
import { Component, Input, OnInit } from '@angular/core';
import { Listing } from '@core/models/listing/Listing';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { NgImageSliderModule } from 'ng-image-slider';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.css'],
})
export class SocialMediaComponent implements OnInit {
  @Input() listing:Listing
  title: string;
  listings: Listing[];
  listingAttributes: any;
  listingSocialMedia: any[];
  facebookLink: string
  youtubeLink: string
  linkedInLink: string
  snapchatLink: string
  twitterLink: string
  tiktokLink: string
  whatsappLink: string
  pinterestLink: string
  instagramLink: string
  telegramLink: string
  otherLink:string

  constructor(
    private listingService: ListingService,
    public router: Router,
    public route: ActivatedRoute,
    private titleService: Title
  ) {}

  ngOnInit(): void {
      console.log(this.listingSocialMedia, 'media');

    // this.getAllListings();
    if (this.listing) {
      this.listingAttributes = this.listing.attributeValueList;
      this.listingSocialMedia = this.listing.socialMediaList;
      console.log(this.listingSocialMedia, 'media');

      this.listingSocialMedia.map((item) => {
        if (item.socialMediaTyp == 'FACEBOOK') {
          this.facebookLink = item.path;
        } else if (item.socialMediaTyp == 'YOUTUBE') {
          this.youtubeLink = item.path;
        } else if (item.socialMediaTyp == 'LINKEDIN') {
          this.linkedInLink = item.path;
        } else if (item.socialMediaTyp == 'SNAPCHAT') {
          this.snapchatLink = item.path;
        } else if (item.socialMediaTyp == 'TWITTER') {
          this.twitterLink = item.path;
        } else if (item.socialMediaTyp == 'TIKTOK') {
          this.tiktokLink = item.path;
        } else if (item.socialMediaTyp == 'WHATSAPP') {
          this.whatsappLink = item.path;
        } else if (item.socialMediaTyp == 'PINTEREST') {
          this.pinterestLink = item.path;
        } else if (item.socialMediaTyp == 'INSTAGRAM') {
          this.instagramLink = item.path;
          console.log(this.instagramLink, 'insta');
        } else if (item.socialMediaTyp == 'TELEGRAM') {
          this.telegramLink = item.path;
        }
        else {
          this.otherLink = item.path
        }
      });

      this.title = this.listing.title;
      this.titleService.setTitle(`${this.title}`);
    }
  }

  // public getAllListings(): void {
  //   this.listingService.getListings().subscribe((listingService) => {
  //     this.listings = listingService;
  //     console.log(this.listings);

  //     const id: any = this.route.snapshot.paramMap.get('id');
  //     console.log(`listings length ${this.listings.length}`);
  //     if (id > this.listings.length) {
  //       this.router.navigate(['/notFound']);
  //     }
  //   });
  // }




  }

//   imageObject: Array<object> = [
//     {
//       image: '../../../../../../assets/images/',
//       thumbImage: 'assets/img/slider/1_min.jpeg',
//       alt: 'alt of image',
//       title: 'title of image',
//     },
//     {
//       image: '../../../../../../assets/images/',
//       thumbImage: 'assets/img/slider/1_min.jpeg',
//       alt: 'alt of image',
//       title: 'title of image',
//     },
//   ];
// }
/*
TELEGRAM', listingId: 3}
1
:
{id: 14, path: '1FACEBOOK.lilte.com', socialMediaTyp: 'FACEBOOK', listingId: 3}
2
:
{id: 22, path: '1PRIVATE.lilte.com', socialMediaTyp: 'PRIVATE', listingId: 3}
3
:
{id: 24, path: '1YOUTUBE.lilte.com', socialMediaTyp: 'YOUTUBE', listingId: 3}
4
:
{id: 16, path: '1LINKEDIN.lilte.com', socialMediaTyp: 'LINKEDIN', listingId: 3}
5
:
{id: 20, path: '1SNAPCHAT.lilte.com', socialMediaTyp: 'SNAPCHAT', listingId: 3}
6
:
{id: 18, path: '1TWITTER.lilte.com', socialMediaTyp: 'TWITTER', listingId: 3}
7
:
{id: 21, path: '1TIKTOK.lilte.com', socialMediaTyp: 'TIKTOK', listingId: 3}
8
:
{id: 23, path: '1OTHER.lilte.com', socialMediaTyp: 'OTHER', listingId: 3}
9
:
{id: 15, path: '1WHATSAPP.lilte.com', socialMediaTyp: 'WHATSAPP', listingId: 3}
10
:
{id: 19, path: '1PINTEREST.lilte.com', socialMediaTyp: 'PINTEREST', listingId: 3}
11
:
{id: 13, path: '1INSTAGRAM.lilte', socialMediaTyp: 'INSTAGRAM',
*/
