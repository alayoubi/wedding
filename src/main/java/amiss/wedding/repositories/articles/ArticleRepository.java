package amiss.wedding.repositories.articles;

import amiss.wedding.entities.articles.Article;
import amiss.wedding.entities.categories.Category;
import amiss.wedding.entities.listings.Listing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    List<Article> findByCategoriesName(String categoryName);
}
