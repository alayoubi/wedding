package amiss.wedding.email;

public enum Template {
    VERIFICATION("user-verification.html");
    private String path;

    Template(String path) {
        this.path = path;
    }
}
