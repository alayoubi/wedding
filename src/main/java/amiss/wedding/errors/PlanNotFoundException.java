package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class PlanNotFoundException extends ApplicationRuntimeException {

    public PlanNotFoundException(long id) {
        super("The Plan with the id: " + id + " was not found!", HttpStatus.NOT_FOUND);
    }

}
