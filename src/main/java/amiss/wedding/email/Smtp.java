package amiss.wedding.email;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class Smtp {

    @NotNull
    private Boolean auth;

    @NotNull
    private Boolean starttls;

    @NotNull
    private Integer connectionTimeout;

    @NotNull
    private Integer timeout;

    @NotNull
    private Integer writetimeout;

}
