package amiss.wedding.errors;

import amiss.wedding.errors.apierror.ApplicationRuntimeException;
import org.springframework.http.HttpStatus;

public class TokenNotValidException extends ApplicationRuntimeException {

    public TokenNotValidException(String token) {
        super(String.format("Token not valid: %s", token), HttpStatus.BAD_REQUEST);
    }

}
