package amiss.wedding.entities.users.client.dto;


import amiss.wedding.entities.users.client.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.security.core.parameters.P;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDto {

    private Long id;
    private String name;
    private String mobile;
    private String address;
    private String photo;
    private Long userId;

    public ClientDto(Client client){
        this.id = client.getId();
        this.name = client.getName();
        this.mobile = client.getMobile();
        this.address = client.getAddress();
        this.photo = client.getPhoto();
        this.userId = client.getUser().getId();
    }
}
