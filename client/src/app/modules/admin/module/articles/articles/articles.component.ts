import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder,FormArray, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Listing } from '@core/models/listing/Listing';
import { CategoryService } from '@core/services/category/category.service';
import { ListingService } from '@core/services/listing/listing.service';
import { UpdateListingComponent } from '../../listing/components/update-listing/update-listing.component';
import { ArticleService } from '@core/services/article/article.service';
import { Article } from '@core/models/article/Article';
import { AddArticleComponent } from '../add-article/add-article.component';


@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css'],
})
export class ArticlesComponent implements OnInit {


  public articles: Article[] = [];

  public listing: Listing;

  // public categories: Category[];

  public nameP: any;

  public r;

  categories: any;

  indexes = 1;

  @Input() list: any = {};

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private categoryService: CategoryService,
    private listingService: ListingService,
    private articleService: ArticleService,
    private title: Title,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
    title.setTitle('المقالات');
  }


  public selectedStatus: any;

  displayedColumns: string[] = [
    'id',
    'image',
    'title',
    'category',
    'action',
  ];
  dataSource!: MatTableDataSource<any>;

  ngAfterViewInit() {}

  ngOnInit(): void {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    this.getArticles();
  }

  getArticles() {
    this.articleService.getArticles().subscribe((res) => {
      this.articles = res;
      this.dataSource = new MatTableDataSource(this.articles);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterVal = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterVal.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addArticle() {
    this.router.navigate(['/admin/articles/add-article']);
  }
  delete(articleId:any) {
    this.articleService.deleteArticle(articleId).subscribe((res) => {
      console.log(res,'res')
    })
  }
}


