import { Component, NgModule, OnInit } from '@angular/core';
import {
  FormControl,
  Validators,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from '@core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public isValid: boolean;
  constructor(
    private titleService: Title,
    private authService: AuthService,
    private r: Router,
    private snackBar: MatSnackBar
  ) {
    this.titleService.setTitle('تسجيل/تسجيل دخول');
  }
  @NgModule({
    imports: [FormControl, Validators, ReactiveFormsModule, FormGroup],
  })
  registerForm = new FormGroup({
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      // Validators.minLength(8),
    ]),
  });

  loginForm = new FormGroup({
    emailLogin: new FormControl('', [Validators.required, Validators.email]),
    passwordLogin: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });

  register() {
    this.authService.register(this.registerForm.value).subscribe(
      (res:any) => {
        // this.registerForm.reset();

        this.snackBar.open('✔ تم التسجيل بنجاح', 'تم', {
          duration: 2000,
          panelClass: [
            'green-snackbar',
            'login-snackbar',
            'my-custom-snackbar',
          ],
        });
        localStorage.setItem('access_token', res.access_token);
        localStorage.setItem('refresh_token',res.refresh_token)
        setTimeout(() => {
          this.r.navigate(['/']);
        }, 3000);
      },
      (error) => {
        this.snackBar.open(`${error.error.message}`, 'حاول مجدددا!', {
          duration: 3000,
          panelClass: ['red-snackbar', 'login-snackbar', 'my-custom-snackbar'],
        });
      }
    );

    console.log(this.registerForm.value, 'form');
  }

  submitLogin() {
    console.log(this.loginForm.valid);
    console.log(this.loginForm.value);
    this.loginForm.reset();
    // this.isValid = this.registerForm.valid;
  }

  get registerUser() {
    return this.registerForm.get('user');
  }
  get registerEmail() {
    return this.registerForm.get('email');
  }
  get registerPass() {
    return this.registerForm.get('password');
  }
  get registerDate() {
    return this.registerForm.get('date');
  }
  get LoginEmail() {
    return this.loginForm.get('emailLogin');
  }
  get LoginPass() {
    return this.loginForm.get('passwordLogin');
  }

  ngOnInit(): void {}
}
